// flow-typed signature: d264d6bff68e407c84e910729988b442
// flow-typed version: <<STUB>>/dva-loading_v^0.2.1/flow_v0.46.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'dva-loading'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'dva-loading' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'dva-loading/lib/index' {
  declare module.exports: any;
}

declare module 'dva-loading/src/index' {
  declare module.exports: any;
}

// Filename aliases
declare module 'dva-loading/lib/index.js' {
  declare module.exports: $Exports<'dva-loading/lib/index'>;
}
declare module 'dva-loading/src/index.js' {
  declare module.exports: $Exports<'dva-loading/src/index'>;
}
