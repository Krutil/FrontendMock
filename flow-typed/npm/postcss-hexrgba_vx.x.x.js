// flow-typed signature: 714894495e5464b13513a02a4b5da7d9
// flow-typed version: <<STUB>>/postcss-hexrgba_v^0.2.1/flow_v0.46.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'postcss-hexrgba'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'postcss-hexrgba' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */


// Filename aliases
declare module 'postcss-hexrgba/index' {
  declare module.exports: $Exports<'postcss-hexrgba'>;
}
declare module 'postcss-hexrgba/index.js' {
  declare module.exports: $Exports<'postcss-hexrgba'>;
}
