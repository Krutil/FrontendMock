const Watch = require('watchjs');
const qs = require('qs');
const Mock = require('mockjs');
import mockStorage from '../src/utils/mockStorage';

let dataKey;

/* Nevytvarime opet mock, pokud jiz jeden ulozeny existuje. */

if (localStorage.getItem('portalActivity') == null) {
  dataKey = mockStorage('Activity', Mock.mock({
    data: [],
  }));
} else {
  dataKey = mockStorage('Activity');
}

const activityData = global[dataKey];

module.exports = {

  'GET /api/activity': function (req, res) {
        // console.log('req', req);

    const data = activityData.data;
        // console.log('GET /api/activity ->>', data);

    res.json({ success: true, data });
  },

  'POST /api/activity': function (req, res) {
    const newData = req.body;
    const localData = { ...activityData };
        // console.log('POST /api/activity newData', newData);
        // console.log('POST /api/activity localData', localData);

    const dataObject = {};
    dataObject.id = localData.data.length + 1;
    dataObject.activity_type = newData.activity_type;
    dataObject.visibility = newData.visibility ? newData.visibility : 'public';
    dataObject.assigned_to = newData.assigned_to ? newData.assigned_to : null;
    dataObject.item_title = newData.item_title;
    dataObject.item_type = newData.item_type ? newData.item_type : null;
    dataObject.item_type_key = newData.item_type_key ? newData.item_type_key : null;
    dataObject.item_type_key_node = newData.item_type_key_node ? newData.item_type_key_node : null;
    dataObject.user = newData.user ? newData.user : null;
    dataObject.created_at = Mock.mock('@now');

    localData.data.unshift(dataObject);

        // console.log('POST /api/activity activityData', localData);
    global[dataKey] = { ...localData };

    res.json({ success: true, data: localData.data });
  },
};
