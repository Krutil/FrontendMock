const Cookie = require('js-cookie');
const Mock = require('mockjs');
import mockStorage from '../src/utils/mockStorage';

const dataKey = mockStorage('AdminUsersList', [
  {
    id: 1,
    username: 'risk-manager',
    password: '123',
    firstname: 'Jan',
    lastname: 'Novák',
    department: 'Alfa',
    phone: '717 123 456',
    email: 'novak.jan@abc.cz',
    date_check_notification: '2017-03-20 07:09:15',
    date_check_approval: '2017-03-20 07:09:15',
    date_check_messages: '2017-03-20 07:09:15',
    date_check_trash: '2017-03-20 07:09:15',
    avatar: 'https://randomuser.me/api/portraits/men/27.jpg',
    role: 'administrator',
    date_last_login: Mock.mock('@now'),
  },
  {
    id: 2,
    username: 'risk-owner',
    password: '123',
    firstname: 'David',
    lastname: 'Slunečný',
    department: 'Alfa',
    phone: '717 654 321',
    email: 'slunecny.petr@abc.cz',
    date_check_notification: '2017-03-20 07:09:15',
    date_check_approval: '2017-03-20 07:09:15',
    date_check_messages: '2017-03-20 07:09:15',
    date_check_trash: '2017-03-20 07:09:15',
    avatar: 'https://randomuser.me/api/portraits/men/3.jpg',
    role: 'riskOwner',
    date_last_login: Mock.mock('@now'),
  },
  {
    id: 3,
    username: 'auditorka',
    password: '123',
    firstname: 'Martina',
    lastname: 'Krásná',
    department: 'Alfa',
    phone: '717 654 320',
    email: 'krasna.martina@abc.cz',
    date_check_notification: '2017-03-20 07:09:15',
    date_check_approval: '2017-03-20 07:09:15',
    date_check_messages: '2017-03-20 07:09:15',
    date_check_trash: '2017-03-20 07:09:15',
    avatar: 'https://randomuser.me/api/portraits/women/10.jpg',
    role: 'auditor',
    date_last_login: Mock.mock('@now'),
  },
  {
    id: 4,
    username: 'risk-owner-2',
    password: '123',
    firstname: 'Robert',
    lastname: 'Polenka',
    department: 'Alfa',
    phone: '716 001 321',
    email: 'polenka.robert@abc.cz',
    date_check_notification: '2017-03-20 07:09:15',
    date_check_approval: '2017-03-20 07:09:15',
    date_check_messages: '2017-03-20 07:09:15',
    date_check_trash: '2017-03-20 07:09:15',
    avatar: 'https://randomuser.me/api/portraits/men/8.jpg',
    role: 'riskOwner',
    date_last_login: Mock.mock('@now'),
  },
  {
    id: 5,
    username: 'risk-user',
    password: '123',
    firstname: 'Alexander',
    lastname: 'Procházka',
    department: 'Alfa',
    phone: '616 441 551',
    email: 'prochazka.alexander@abc.cz',
    date_check_notification: '2017-03-20 07:09:15',
    date_check_approval: '2017-03-20 07:09:15',
    date_check_messages: '2017-03-20 07:09:15',
    date_check_trash: '2017-03-20 07:09:15',
    avatar: 'https://randomuser.me/api/portraits/men/11.jpg',
    role: 'Identifikace rizik',
    date_last_login: Mock.mock('@now'),
  },
]);

let userData = global[dataKey];

module.exports = {
  'POST /api/login': function (req, res) {
    const userItem = req.body;
    const response = {
      success: false,
      message: '',
      results: null,
    };
    const d = global[dataKey].filter(function (item) {
            // console.log('AdminUsersList item', item);
      return item.username === userItem.username;
    });

    if (d.length) {
      if (d[0].password === userItem.password) {
        const now = new Date();
        now.setDate(now.getDate() + 1);
        Cookie.set('user_session', now.getTime(), { path: '/' });
        Cookie.set('user_name', userItem.username, { path: '/' });
        response.message = 'Login successful.';
        response.success = true;
        response.results = d[0];
        console.log("response result", response.results);
        Cookie.set('user_id', response.results.id);
      } else {
        response.message = 'Incorrect password.';
      }
    } else {
      response.message = 'User does not exist.';
    }
    res.json(response);
  },

  'GET /api/userInfo': function (req, res) {
    const username = Cookie.get('user_name') || '';
    const user = userData.filter(function (item) {
      return item.username === username;
    });

    const response = {
      success: Cookie.get('user_session') && Cookie.get('user_session') > new Date().getTime(),
      username: Cookie.get('user_name') || '',
      user: user[0],
      message: '',
    };
    res.json(response);
  },

  'POST /api/logout': function (req, res) {
    Cookie.remove('user_session', { path: '/' });
    Cookie.remove('user_name', { path: '/' });
    res.json({
      success: true,
      message: 'You were logout.',
    });
  },

  'PUT /api/notificationChecked': function (req, res) {
    const user = req.body;

    userData = userData.map(function (item) {
            // console.log('item', item);

      if (item.id === user.id) {
        item.date_check_notification = Mock.mock('@now');
        user.date_check_notification = item.date_check_notification;
      }
      return item;
    });

    res.json({ success: true, user });
  },

  'PUT /api/messagesChecked': (req, res) => {
    const user = req.body;

    userData = userData.map((item) => {
            // console.log('item', item);

      if (item.id === user.id) {
        item.date_check_messages = Mock.mock('@now');
        user.date_check_messages = item.date_check_messages;
      }
      return item;
    });

    res.json({ success: true, user });
  },

  'PUT /api/approvalChecked': function (req, res) {
    const user = req.body;

    userData = userData.map(function (item) {
            // console.log('item', item);

      if (item.id === user.id) {
        item.date_check_approval = Mock.mock('@now');
        user.date_check_approval = item.date_check_approval;
      }
      return item;
    });

    res.json({ success: true, user });
  },

  'PUT /api/trashChecked': function (req, res) {
    const user = req.body;

    userData = userData.map(function (item) {
            // console.log('item', item);

      if (item.id === user.id) {
        item.date_check_trash = Mock.mock('@now');
        user.date_check_trash = item.date_check_trash;
      }
      return item;
    });

    res.json({ success: true, user });
  },
};
