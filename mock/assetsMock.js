const Watch = require('watchjs');
const qs = require('qs');
const Mock = require('mockjs');
import mockStorage from '../src/utils/mockStorage';

let dataKey;

function getUser() {
  if (localStorage.getItem('portalUsersList')) {
    const items = JSON.parse((localStorage.getItem('portalUsersList'))).data;
    return items[Math.floor(Math.random() * items.length)];
  }
  return null;
}

/* Nevytvarime opet mock, pokud jiz jeden ulozeny existuje. */

if (localStorage.getItem('portalAssetsList') == null) {
  dataKey = mockStorage('AssetsList', Mock.mock({
    'data|30': [
      {
        'id|+1': 1,
        'key|+1': [
          'abc001',
          'abc002',
          'abc003',
          'abc004',
          'abc005',
          'abc006',
          'abc011',
          'abc111',
          'abc211',
          'abc311',
          'abc411',
          'abc021',
          'abc121',
          'abc221',
          'abc321',
          'abc031',
          'abc131',
          'abc231',
          'abc331',
          'abc431',
          'abc531',
          'abc631',
          'abc014',
          'abc114',
          'abc214',
          'abc314',
          'abc414',
          'abc114a',
          'abc114b',
          'abc024',
        ],
        'key_parents|+1': [
          null,
          null,
          null,
          null,
          null,
          null,
          'abc001',
          'abc001.abc011',
          'abc001.abc011',
          'abc001.abc011',
          'abc001.abc011',
          'abc001',
          'abc001.abc021',
          'abc001.abc021',
          'abc001.abc021',
          'abc001',
          'abc001.abc031',
          'abc001.abc031',
          'abc001.abc031',
          'abc001.abc031',
          'abc001.abc031',
          'abc001.abc031',
          'abc004',
          'abc004.abc014',
          'abc004.abc014',
          'abc004.abc014',
          'abc004.abc014',
          'abc004.abc014.abc114',
          'abc004.abc014.abc114',
          'abc004',
        ],
        'type|+1': [
          'folder',
          'folder',
          'folder',
          'folder',
          'folder',
          'folder',
          'folder',
          'item',
          'item',
          'item',
          'item',
          'folder',
          'item',
          'item',
          'item',
          'folder',
          'item',
          'item',
          'item',
          'item',
          'item',
          'item',
          'folder',
          'folder',
          'item',
          'item',
          'item',
          'item',
          'item',
          'folder',
        ],
        item_type: 'assets',
        status: 'accepted',
        private: 0,
        'title|+1': [
          'Procesy',
          'Projekty',
          'Finanční zdroje',
          'Nehmotný majetek',
          'Hmotný majetek',
          'Organizační struktura',
          'Řídící procesy',
          'Řízení rozvoje společnosti',
          'Správa systému řízení',
          'Řízení rizik',
          'Interní kontrola a audit',
          'Hlavní procesy',
          'Řízení marketingových činností a prodeje',
          'Core business 1: Řízení zákaznických projektů',
          'Core business 2: Vývoj nových produktů',
          'Podpůrné procesy',
          'Provoz a údržba budov',
          'Řízení IT',
          'Personální management',
          'Zajišťování zdrojů (nákup a vstupní logistika)',
          'Spisová a archivační služba',
          'Správa příjmů a vymáhání',
          'Software',
          'Kancelářský SW',
          'CAD/CAM/CAE',
          'ERP',
          'Infrastruktura a OS',
          'MS Office',
          'Adobe Acrobat',
          'Patenty',
        ],
        number: '@string("number", 8)',
        assigned_user: null,
        schema: [],
        date_plan_from: '@date',
        date_plan_to: '@date',
        date_real_from: '@date',
        date_real_to: '@date',
        description: `<p>${Mock.mock('@sentence')}</p>`,
        user: getUser(),
        created_at: '@now',
        updated_at: null,
      },
    ],
  }));
} else {
  dataKey = mockStorage('AssetsList');
}

const assetsListData = global[dataKey];

module.exports = {

  'GET /api/risk/assets': function (req, res) {
    res.json({ success: true, list: assetsListData.data });
  },

  'POST /api/risk/assets': function (req, res) {
    const body = req.body;

    const dataObject = body.item;
    dataObject.id = assetsListData.data.length + 1;
    dataObject.key = Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/);
    dataObject.item_type = 'assets';
    dataObject.schema = body.item.schema || [];
    dataObject.created_at = Mock.mock('@now');
    dataObject.updated_at = null;
    dataObject.user = {
      id: body.appUser.id,
      firstname: body.appUser.firstname,
      lastname: body.appUser.lastname,
      avatar: body.appUser.avatar,
    };

    assetsListData.data.push(dataObject);
    global[dataKey] = assetsListData;
    res.json({ success: true, list: assetsListData.data, results: dataObject });
  },

  'DELETE /api/risk/assets': function (req, res) {
    const deleteItem = req.body;

    assetsListData.data = assetsListData.data.filter(function (item) {
      return item.id !== deleteItem.id;
    });

    global[dataKey] = assetsListData;
    res.json({ success: true, list: assetsListData.data });
  },

  'PUT /api/risk/assets/restore': function (req, res) {
    const restoredItem = req.body;
        // console.log('restoredItem', restoredItem);

    const data = assetsListData.data.map(function (item) {
      if (item.id === restoredItem.id) {
        item.updated_at = Mock.mock('@now');
        item.status = 'accepted';
      }
      return item;
    });

    assetsListData.data = data;
    global[dataKey] = assetsListData;
    localStorage.setItem(dataKey, JSON.stringify(assetsListData));
    res.json({ success: true, list: data });
  },

  /* select(params) */
  'PUT /api/risk/assets/item/id': function (req, res) {
    const body = req.body;
    let itemSelected = null;

    assetsListData.data.forEach(function (item) {
      if (item.key === body.key) {
        itemSelected = item;
      }
    });

    res.json({ success: true, currentItem: itemSelected });
  },

  'PUT /api/risk/assets/tree': function (req, res) {
    const tree = req.body;
    localStorage.setItem(dataKey, JSON.stringify({ data: tree }));
        // console.log('global global[dataKey]', global[dataKey]);
    res.json({ success: true, list: tree });
  },

  'PUT /api/risk/assets/schema': function (req, res) {
    const body = req.body;
    const itemUpdated = body;
    let currentItem = null;

    const data = assetsListData.data.map(function (item) {
      if (item.id === itemUpdated.id) {
        itemUpdated.updated_at = Mock.mock('@now');
        item.schema = item.schema || [];
        itemUpdated.schema.key = Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/);
        item.schema.push(itemUpdated.schema);
        currentItem = { ...item };
      }
      return item;
    });

    assetsListData.data = data;
    global[dataKey] = assetsListData;
    localStorage.setItem(dataKey, JSON.stringify(assetsListData));
    res.json({ success: true, list: data, currentItem });
  },

  'DELETE /api/risk/assets/schema': function (req, res) {
    const body = req.body;
    const itemUpdated = body;
    let currentItem = null;

        // console.log('DELETE itemUpdated', itemUpdated);

    const data = assetsListData.data.map(function (item) {
      if (item.id === itemUpdated.id) {
        itemUpdated.updated_at = Mock.mock('@now');
        const schemaArray = [];
        item.schema.forEach(function (schema, index) {
          if (schema.key !== itemUpdated.schema_key) {
            schemaArray.push(schema);
          }
        });

        item.schema = schemaArray;
        currentItem = { ...item };
      }
      return item;
    });

    assetsListData.data = data;
    global[dataKey] = assetsListData;
    res.json({ success: true, list: data, currentItem });
  },

  'PUT /api/risk/assets': function (req, res) {
    const body = req.body;
    let itemUpdated = body;
    let itemHistory = null;

    if (body.item) {
      itemUpdated = body.item;
    }
    delete itemUpdated.children;
        // console.log('********************PUT /api/risk/assets', itemUpdated);

    const data = assetsListData.data.map(function (item) {
      if (item.id !== itemUpdated.id && item.key_parents) {
        const index = item.key_parents.indexOf(itemUpdated.key);

        if (index !== -1) {
                    /* +1 for keys separator "." */
          const key_parents = item.key_parents.substring(index + itemUpdated.key.length + 1);

          if (key_parents) {
            item.key_parents = itemUpdated.key_parents ? `${itemUpdated.key_parents}.${itemUpdated.key}.${key_parents}` : `${itemUpdated.key}.${key_parents}`;
          } else {
            item.key_parents = itemUpdated.key_parents ? `${itemUpdated.key_parents}.${itemUpdated.key}` : itemUpdated.key;
          }
        }
      }

      if (item.id === itemUpdated.id) {
        itemHistory = { ...item };
        itemUpdated.updated_at = Mock.mock('@now');
        item = itemUpdated;
      }
      return item;
    });

    assetsListData.data = data;
    res.json({
      success: true,
      list: data,
      item: itemUpdated,
      itemHistory,
    });
  },

  'PUT /api/risk/assets/[0-9]': function (req, res) {
    const editItem = req.body;

    editItem.createTime = Mock.mock('@now');
    editItem.avatar = Mock.Random.image('100x100', Mock.Random.color(), '#fff', 'png', editItem.title.substr(0, 1));

    assetsListData.data = assetsListData.data.map(function (item) {
      if (item.id === editItem.id) {
        return editItem;
      }
      return item;
    });

    global[dataKey] = assetsListData;
    res.json({ success: true, list: assetsListData.data });
  },

  'POST /api/risk/assets/folder/create': function (req, res) {
    const body = req.body;

        // console.log('POST /api/risk/assets/folder/create', body);
        // console.log('POST assetsListData', assetsListData);

    const dataObject = {};
    dataObject.id = assetsListData.data.length + 1;
    dataObject.key = Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/);
    dataObject.key_parents = body.key_parents;
    dataObject.type = 'folder';
    dataObject.title = body.title;
    dataObject.private = body.private;
    dataObject.created_at = Mock.mock('@now');
    dataObject.updated_at = null;

    assetsListData.data.push(dataObject);
    global[dataKey] = assetsListData;
    res.json({ success: true, list: assetsListData.data });
  },

};
