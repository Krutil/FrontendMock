const Watch = require('watchjs');
const qs = require('qs');
const Mock = require('mockjs');
import mockStorage from '../src/utils/mockStorage';

let dataKey;

/* Nevytvarime opet mock, pokud jiz jeden ulozeny existuje. */
if (localStorage.getItem('portalComments') == null) {
  dataKey = mockStorage('Comments', Mock.mock({
    data: [],
  }));
} else {
  dataKey = mockStorage('Comments');
}

const commentsData = global[dataKey];

module.exports = {

  'GET /api/comments': function (req, res) {
    const data = commentsData.data;
    res.json({ success: true, data });
  },


  'POST /api/comments/store': function (req, res) {
    res.json({ success: true });
  },

  'POST /api/comments': function (req, res) {
    const newData = req.body;
    const dataObject = {};

    dataObject.id = Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/);
    dataObject.category = newData.category;
    dataObject.type_key = newData.keys.listKey;
    dataObject.type_key_node = newData.keys.listRowKey;
    dataObject.text = newData.text;
    dataObject.parentComment = null;
    dataObject.user = {
      id: newData.appUser.id,
      firstname: newData.appUser.firstname,
      lastname: newData.appUser.lastname,
      avatar: newData.appUser.avatar,
    };
    dataObject.created_at = Mock.mock('@now');

    if (newData.parentComment) {
      dataObject.parentComment = newData.parentComment;
    }

    commentsData.data.unshift(dataObject);
    global[dataKey] = commentsData;

    res.json({
      success: true,
      data: commentsData.data,
      results: dataObject,
      currentItem: newData.currentItem,
    });
  },

  'PUT /api/comments': function (req, res) {
    const itemUpdated = req.body;

    const data = commentsData.data.map(function (item) {
      if (item.key === itemUpdated.key) {
        item = itemUpdated;
      }
      return item;
    });

    commentsData.data = data;
    res.json({ success: true, data });
  },

  'DELETE /api/comments': function (req, res) {
    const dataObject = req.body;

    commentsData.data = commentsData.data.filter(function (item) {
      if (item.id === dataObject.id) {
        return false;
      }
      return true;
    });

    global[dataKey] = commentsData;

    res.json({ success: true, data: commentsData.data });
  },
};
