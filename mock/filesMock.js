const Watch = require('watchjs');
const qs = require('qs');
const Mock = require('mockjs');
import mockStorage from '../src/utils/mockStorage';

let dataKey;

/* Nevytvarime opet mock, pokud jiz jeden ulozeny existuje. */

if (localStorage.getItem('portalFiles') == null) {
  dataKey = mockStorage('Files', Mock.mock({
    data: [
      {
        id: 1,
        category: 'requirements',
        type_key: 'yS8uV4',
        type_key_node: 'djygggwv',
        title: 'Nazev souboru',
        user: 'John Doe',
        created_at: '@now',
        updated_at: null,
      },
    ],
  }));
} else {
  dataKey = mockStorage('Files');
}

const filesData = global[dataKey];

module.exports = {

  'GET /api/files': function (req, res) {
        // console.log('req', req);

    const data = filesData.data;
        // console.log('GET /api/files ->>', data);

    res.json({ success: true, data });
  },


  'POST /api/files/store': function (req, res) {
    res.json({ success: true });
  },

  'POST /api/files': function (req, res) {
    const newData = req.body;


    const dataObject = {};
    dataObject.id = filesData.data.length + 1;
    dataObject.category = newData.category;
    dataObject.type_key = newData.keys.listKey;
    dataObject.type_key_node = newData.keys.listRowKey;
    dataObject.title = newData.file.file.name.toLowerCase();
    dataObject.user = {
      id: newData.appUser.id,
      firstname: newData.appUser.firstname,
      lastname: newData.appUser.lastname,
      avatar: newData.appUser.avatar,
    };
    dataObject.created_at = Mock.mock('@now');
    dataObject.updated_at = null;

    filesData.data.push(dataObject);

    global[dataKey] = filesData;

    res.json({ success: true, data: filesData.data });
  },

  'PUT /api/files': function (req, res) {
    const itemUpdated = req.body;

    const data = filesData.data.map(function (item) {
      if (item.key === itemUpdated.key) {
        item = itemUpdated;
      }
      return item;
    });

        // console.log('dataUpdated', data);

    filesData.data = data;
        // global[dataKey] = {data: data};
        // localStorage.setItem(dataKey, JSON.stringify({data: data}));

        // console.log('global global[dataKey]', global[dataKey]);
    res.json({ success: true, data });
  },

  'DELETE /api/files': function (req, res) {
    const dataObject = req.body;
        // console.log('DELETE /api/files', dataObject);

    filesData.data = filesData.data.filter(function (item) {
            // console.log('item.id', item.id);
            // console.log('dataObject.id', dataObject.id);
      if (item.id === dataObject.id) {
                // console.log('DELETE item', item);
        return false;
      }
      return true;
    });

        // console.log('filesData.data', filesData.data);

    global[dataKey] = filesData;

    res.json({ success: true, data: filesData.data });
  },

};
