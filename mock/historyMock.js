const Watch = require('watchjs');
const qs = require('qs');
const Mock = require('mockjs');
import mockStorage from '../src/utils/mockStorage';

let dataKey;

/* Nevytvarime opet mock, pokud jiz jeden ulozeny existuje. */
if (localStorage.getItem('portalHistory') == null) {
  dataKey = mockStorage('History', []);
} else {
  dataKey = mockStorage('History');
}

const historyData = global[dataKey];

module.exports = {

  'GET /api/history': function (req, res) {
    res.json({ success: true, data: historyData });
  },

  'POST /api/history': function (req, res) {
    const data = req.body;
    data.id = historyData.length + 1;
    data.created_at = Mock.mock('@now');
    console.log('aa');
    historyData.unshift(data);

    localStorage.setItem(dataKey, JSON.stringify(historyData));
    res.json({ success: true, data: historyData });
  },
};
