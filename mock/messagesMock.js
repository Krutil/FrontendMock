const Watch = require('watchjs');
const qs = require('qs');
const Mock = require('mockjs');
import mockStorage from '../src/utils/mockStorage';
const Cookie = require('js-cookie');

let dataKey;

/* Nevytvarime opet mock, pokud jiz jeden ulozeny existuje. */

if (localStorage.getItem('portalMessages') == null) {
  dataKey = mockStorage('Messages', Mock.mock({
    data: [{
      conversationId: 'a7sfa',
      participants: [1, 2],
      messages: [{
        messageId: 'a7sfa-1',
        messageBody: 'Ahoj Petře',
        senderId: 1,
        createdAt: '2017-03-20 07:09:15',
        viewedAt: '2017-03-20 07:19:15',
      },
      {
        messageId: 'a7sfa-2',
        messageBody: 'Ahoj Honzo',
        senderId: 2,
        createdAt: '2017-03-20 07:09:15',
        viewedAt: undefined,
      }],
    },
    ],
  }),
);
} else {
  dataKey = mockStorage('Messages');
}

const messagesData = global[dataKey];

module.exports = {

  'GET /api/messages': (req, res) => {
    res.json({ success: true, data: messagesData });
  },

  'POST /api/messages': (req, res) => {
    const newMessage = req.body;
    const participants = req.body.participants;
    delete newMessage.participants;
    const conversationId = req.body.conversationId;
    if (conversationId === undefined) {
      const oConversation = {
        conversationId: Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/),
        participants,
        messages: [],
      };

      newMessage.messageId = `${oConversation.conversationId}-${Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/)}`;
      newMessage.createdAt = Mock.mock('@now');

      oConversation.messages.push(newMessage);

      messagesData.data.push(oConversation);
    } else {
      messagesData.data.map((oConversation) => {
        if (oConversation.conversationId === conversationId) {
          newMessage.messageId = `${conversationId}-${Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/)}`;
          newMessage.createdAt = Mock.mock('@now');
          oConversation.messages.push(newMessage);
          return oConversation;
        }
        return oConversation;
      },
    );
    }

    res.json({ success: true, data: messagesData });
  },

  'DELETE /api/messages': (req, res) => {
    const { messageId, conversationId } = req.body;

    messagesData.data.map((oConversation) => {
      if (oConversation.conversationId === conversationId) {
        oConversation.messages = oConversation.messages.filter(oMessage =>
          oMessage.messageId != messageId
        );
        return oConversation;
      }
      return oConversation;
    },
  );
    res.json({ success: true, data: messagesData });
  },
  'PUT /api/messageViewed': (req, res) => {
    const { conversationId } = req.body;
    const UserId = Cookie.get('user_id');
    messagesData.data.map((oConversation) => {
      if (oConversation.conversationId === conversationId) {
        oConversation.messages = oConversation.messages.map((oMessage) => {
          if (!oMessage.viewedAt && oMessage.senderId !== UserId) {
            oMessage.viewedAt = Mock.mock('@now');
            return oMessage;
          }
          return oMessage;
        });
        return oConversation;
      }
      return oConversation;
    },
    );
    res.json({ success: true, data: messagesData });
  },
};
