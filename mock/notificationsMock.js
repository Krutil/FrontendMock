import Watch from 'watchjs';
import Mock from 'mockjs';
import Cookie from 'js-cookie';
import mockStorage from '../src/utils/mockStorage';

let dataKey;


if (localStorage.getItem('portalNotifications') == null) {
  dataKey = mockStorage('Notifications', Mock.mock({
    data: [
    ],
  }));
} else {
  dataKey = mockStorage('Notifications');
}

const notificationsData = global[dataKey];


module.exports = {

  'GET /api/notifications': (req, res) => {
    const userName = Cookie.get('user_name');
    const data = notificationsData.data;
    console.log("data", data);

    // let dataFiltered =data.filter(oNotification => oNotification.user.username === userName
  //   &&
  //   oNotification.category_notification === 'spectators')
  //     .concat(
  //       data.filter(oNotification => oNotification.activity_type === 'mention')
  //         .filter(oNotification => oNotification.assigned_to === `@${userName}`)
  //     )
  //     .concat(
  //       data.filter(oNotification => oNotification.activity_type === 'mention')
  //         .filter(oNotification => oNotification.assigned_to === `@${userName}`)
  //     )
  //     .concat(
  //       data.filter(oNotification => oNotification.category_notification === 'approval'
  //       ||
  //       oNotification.category_notification === 'activity'
  //       &&
  //       oNotification.activity_type !== 'mention')
  //     )
  // });
    res.json({
      success: true,
      data })
  },

  'POST /api/notifications': (req, res) => {
    const newData = req.body;
    const localData = { ...notificationsData };
    const dataObject = {};
    dataObject.id = localData.data.length + 1;
    dataObject.activity_type = newData.activity_type;
    dataObject.assigned_to = newData.assigned_to ? newData.assigned_to : null;
    dataObject.item_title = newData.item_title;
    dataObject.item_type = newData.item_type ? newData.item_type : null;
    dataObject.item_type_key = newData.item_type_key ? newData.item_type_key : null;
    dataObject.user = newData.user ? newData.user : null;
    dataObject.category_notification = newData.category_notification;
    dataObject.created_at = Mock.mock('@now');
    dataObject.assigned_to = newData.assigned_to;
    dataObject.spectator = newData.spectator;
    // console.log("POST /api/notifications", dataObject);

    localData.data.unshift(dataObject);

    // console.log('POST /api/activity activityData', localData);
    global[dataKey] = { ...localData };

    res.json({ success: true, data: localData.data });

    const data = notificationsData.data;
    res.json({ success: true, data });
  },

};
