const Watch = require('watchjs');
const qs = require('qs');
const Mock = require('mockjs');
import mockStorage from '../src/utils/mockStorage';

let dataKey;

/* Nevytvarime opet mock, pokud jiz jeden ulozeny existuje. */

if (localStorage.getItem('portalRelationships') == null) {
  dataKey = mockStorage('Relationships', Mock.mock({
    data: [],
  }));
} else {
  dataKey = mockStorage('Relationships');
}

const relationshipsData = global[dataKey];

module.exports = {

  'GET /api/relationships': function (req, res) {
    const data = relationshipsData.data;
    res.json({ success: true, data });
  },

  'POST /api/relationships/store': function (req, res) {
    res.json({ success: true });
  },

  'POST /api/relationships': function (req, res) {
    const oData = req.body;
    const localData = { ...relationshipsData };
    console.log('POST /api/relationships oData', oData);

    const oRelationshipNew = {};
    oRelationshipNew.id = localData.data.length + 1;
    oRelationshipNew.from_key = oData.from_key;
    oRelationshipNew.from_key_node = oData.from_key_node || null;
    oRelationshipNew.from_item = oData.itemFrom;
    oRelationshipNew.to_key = oData.to_key;
    oRelationshipNew.to_key_node = oData.to_key_node || null;
    oRelationshipNew.to_item = oData.itemTo;
    oRelationshipNew.user = oData.appUser;
    oRelationshipNew.created_at = Mock.mock('@now');

    localData.data.push(oRelationshipNew);
    global[dataKey] = { ...localData };

    res.json({ success: true, data: relationshipsData.data });
  },

  'DELETE /api/relationships': function (req, res) {
    const dataObject = req.body;
        // console.log('DELETE /api/relationships', dataObject);

    relationshipsData.data = relationshipsData.data.filter(function (item) {
            // console.log('item.id', item.id);
            // console.log('dataObject.id', dataObject.id);
      if (item.id === dataObject.id) {
                // console.log('DELETE item', item);
        return false;
      }
      return true;
    });

    global[dataKey] = relationshipsData;

    res.json({ success: true, data: relationshipsData.data });
  },

};
