const Watch = require('watchjs');
const qs = require('qs');
const Mock = require('mockjs');
import mockStorage from '../src/utils/mockStorage';

let dataKey;

/* Nevytvarime opet mock, pokud jiz jeden ulozeny existuje. */

if (localStorage.getItem('portalRequirementsList') == null) {
  const mock = Mock.mock({
    'data|24': [
      {
        'id|+1': 100,
        'key|+1': [
          'abc001',
          'abc002',
          'aaabbb',
          'abc011',
          'abc111',
          'abc111a',
          'abc111b',
          'abc111c',
          'abc111d',
          'abc021',
          'abc031',
          'abc041',
          'abc051',
          'abc061',
          'abc071',
          'abc012',
          'abc022',
          'abc122',
          'abc222',
          '1aabbb',
          '2aabbb',
          '3aabbb',
          '4aabbb',
          '1aabbba',
          '1aabbbb',
        ],
        'key_parents|+1': [
          null,
          null,
          null,
          'abc001',
          'abc001.abc011',
          'abc001.abc011.abc111',
          'abc001.abc011.abc111',
          'abc001.abc011.abc111',
          'abc001.abc011.abc111',
          'abc001',
          'abc001',
          'abc001',
          'abc001',
          'abc001',
          'abc001',
          'abc002',
          'abc002',
          'abc002.abc022',
          'abc002.abc022',
          'aaabbb',
          'aaabbb',
          'aaabbb',
          'aaabbb',
          'aaabbb.1aabbb',
          'aaabbb.1aabbb',
        ],
        'type|+1': [
          'folder',
          'folder',
          'folder',
          'folder',
          'folder',
          'item',
          'item',
          'item',
          'item',
          'folder',
          'folder',
          'folder',
          'folder',
          'folder',
          'folder',
          'folder',
          'folder',
          'item',
          'item',
          'folder',
          'folder',
          'folder',
          'folder',
          'item',
          'item',
        ],
        item_type: 'requirements',
        'title|+1': [
          'Právní požadavky',
          'Jiné požadavky',
          'Odvozené požadavky',
          'Informace, informační systémy',
          'Zpracování a ochrana dat a informací',
          'Obecné nařízení o ochraně osobních údajů (GDPR)',
          'Zákon o ochraně osobních údajů',
          'Zákon o ochraně utajovaných informací a o bezpečnostní způsobilosti',
          'Zákon o elektronickém podpisu',
          'Daně a poplatky',
          'Duševní vlastnictví',
          'Ekonomika a podnikání',
          'Odpadové hospodářství',
          'Pracovní právo a personalistika',
          'Životní prostředí',
          'Smlouvy',
          'Adoptované normy',
          'ČSN EN ISO 9001',
          'ČSN EN ISO 14001',
          'Řízení bezpečnosti',
          'Správa daní a poplatků',
          'Environmentální management',
          'Personální management',
          'Bezpečnost informací',
          'Objektová bezpečnost',
        ],
        'code|+1': [
          null,
          null,
          null,
          null,
          null,
          '2016/679/EU',
          '101/2000 Sb.',
          '412/2005 Sb.',
          '227/2000 Sb.',
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
        ],
        date_from() {
          if (this.type === 'item') {
            return Mock.mock('@date');
          }
          return null;
        },
        date_to: null,
        data() {
          if (this.type === 'item') {
            const data = Mock.mock({
              'data|1-100': [
                {
                  'key|+1': '@string("lower", 8)',
                  'data|1': function () {
                    const data = Mock.mock({
                      'data|1': [
                          `<p>${Mock.mock('@sentence')}</p>`,
                          `<p>${Mock.mock('@sentence')}</p>`,
                          `<h2>${Mock.mock('@sentence')}</h2>`,
                          `<p>${Mock.mock('@sentence')}</p>`,
                          `<h3>${Mock.mock('@sentence')}</h3>`,
                          `<p>${Mock.mock('@sentence')}</p>`,
                          `<p>${Mock.mock('@sentence')}</p>`,
                          `<p>${Mock.mock('@sentence')}</p>`,
                          `<p>${Mock.mock('@sentence')}</p>`,
                          `<table><td>${Mock.mock('@sentence')}</td></table>`,
                          `<p>${Mock.mock('@sentence')}</p>`,
                          `<p>${Mock.mock('@sentence')}</p>`,
                          `<p>${Mock.mock('@sentence')}</p>`,
                          `<p>${Mock.mock('@sentence')}</p>`,
                          `<p>${Mock.mock('@sentence')}</p>`,
                          `<ul><li>${Mock.mock('@sentence')}</li><li>${Mock.mock('@sentence')}</li></ul>`,
                          `<p>${Mock.mock('@sentence')}</p>`,

                        ],
                    });

                    return data.data;
                  },
                },
              ],
            });

            return data.data;
          }
          return null;
        },
        usersAssigned: null,
        created_at: '@datetime',
        updated_at: '@now',
      },
    ],
  });

  mock.data.unshift({
    id: 1,
    key: 'abc111e',
    key_parents: 'abc001.abc011.abc111',
    type: 'item',
    item_type: 'requirements',
    title: 'Zákon o kybernetické bezpečnosti',
    code: '181/2014 Sb.',
    date_from() {
      if (this.type === 'item') {
        return Mock.mock('@date');
      }
      return null;
    },
    usersAssigned: [
      {
        id: 1,
        username: 'risk-manager',
        password: '123',
        firstname: 'Jan',
        lastname: 'Novák',
        department: 'Alfa',
        phone: '717 123 456',
        email: 'novak.jan@abc.cz',
        date_check_notification: '2017-03-20 07:09:15',
        date_check_approval: '2017-03-20 07:09:15',
        date_check_messages: '2017-03-20 07:09:15',
        date_check_trash: '2017-03-20 07:09:15',
        avatar: 'https://randomuser.me/api/portraits/men/27.jpg',
        role: 'administrator',
        date_last_login: Mock.mock('@now'),
        permissions: {
          read: true,
          write: true,
          edit: true,
        },
        expiration: '2018-03-20',
      },
    ],
    created_at: Mock.mock('@now'),
    updated_at: Mock.mock('@now'),
    data: [
      {
        key: Mock.mock('abc111e.node1'),
        data: '<div><p class="L1 CAST"><a id="f5278855"><i id="cast1"></i></a>ČÁST PRVNÍ</p><h3 class="L2 NADPIS"><a id="f5278856"></a>KYBERNETICKÁ BEZPEČNOST</h3><p class="L2 HLAVA"><a id="f5278857"><i id="cast1-hlava1"></i></a>HLAVA I</p><h3 class="L3 NADPIS"><a id="f5278858"></a>ZÁKLADNÍ USTANOVENÍ</h3><p class="L3 PARA"><a id="f5278859"><i id="p1"></i></a>§ 1</p><h3 class="L4 NADPIS"><a id="f5278860"></a>Předmět úpravy</h3><p class="L4"><a id="f5278861"><i id="p1-1"></i></a><var>(1)</var> Tento zákon upravuje práva a povinnosti osob a působnost a pravomoci orgánů veřejné moci v oblasti kybernetické bezpečnosti.</p><p class="L4"><a id="f5278862"><i id="p1-2"></i></a><var>(2)</var> Tento zákon se nevztahuje na informační nebo komunikační systémy, které nakládají s utajovanými informacemi.</p><h3 class="L3 NADPIS"><a id="f5278863"></a>Vymezení pojmů</h3><p class="L4 PARA"><a id="f5278864"><i id="p2"></i></a>§ 2</p><p class="L5"><a id="f5278865"><i id="p2-1"></i></a>V tomto zákoně se rozumí</p><p class="L6"><a id="f5278866"><i id="p2-1-a"></i></a><var>a)</var> kybernetickým prostorem digitální prostředí umožňující vznik, zpracování a výměnu informací, tvořené informačními systémy, a službami a sítěmi elektronických komunikací<a class="linknote" href="#f5279193"><sup>1</sup>)</a>,</p><p class="L6"><a id="f5278867"><i id="p2-1-b"></i></a><var>b)</var> kritickou informační infrastrukturou prvek nebo systém prvků kritické infrastruktury v odvětví komunikační a informační systémy<a class="linknote" href="#f5279194"><sup>2</sup>)</a> v oblasti kybernetické bezpečnosti,</p><p class="L6"><a id="f5278868"><i id="p2-1-c"></i></a><var>c)</var> bezpečností informací zajištění důvěrnosti, integrity a dostupnosti informací,</p><p class="L6"><a id="f5278869"><i id="p2-1-d"></i></a><var>d)</var> významným informačním systémem informační systém spravovaný orgánem veřejné moci, který není kritickou informační infrastrukturou a u kterého narušení bezpečnosti informací může omezit nebo výrazně ohrozit výkon působnosti orgánu veřejné moci,</p><p class="L6"><a id="f5278870"><i id="p2-1-e"></i></a><var>e)</var> správcem informačního systému orgán nebo osoba, které určují účel zpracování informací a podmínky provozování informačního systému,</p><p class="L6"><a id="f5278871"><i id="p2-1-f"></i></a><var>f)</var> správcem komunikačního systému orgán nebo osoba, které určují účel komunikačního systému a podmínky jeho provozování, a</p><p class="L6"><a id="f5278872"><i id="p2-1-g"></i></a><var>g)</var> významnou sítí síť elektronických komunikací<a class="linknote" href="#f5279193"><sup>1</sup>)</a> zajišťující přímé zahraniční propojení do veřejných komunikačních sítí nebo zajišťující přímé připojení ke kritické informační infrastruktuře.</p><p class="L4 PARA"><a id="f5278873"><i id="p3"></i></a>§ 3</p><p class="L5"><a id="f5278874"><i id="p3-1"></i></a>Orgány a osobami, kterým se ukládají povinnosti v oblasti kybernetické bezpečnosti, jsou</p><p class="L6"><a id="f5278875"><i id="p3-1-a"></i></a><var>a)</var> poskytovatel služby elektronických komunikací a subjekt zajišťující síť elektronických komunikací<a class="linknote" href="#f5279193"><sup>1</sup>)</a>, pokud není orgánem nebo osobou podle písmene b),</p><p class="L6"><a id="f5278876"><i id="p3-1-b"></i></a><var>b)</var> orgán nebo osoba zajišťující významnou síť, pokud nejsou správcem komunikačního systému podle písmene d),</p><p class="L6"><a id="f5278877"><i id="p3-1-c"></i></a><var>c)</var> správce informačního systému kritické informační infrastruktury,</p><p class="L6"><a id="f5278878"><i id="p3-1-d"></i></a><var>d)</var> správce komunikačního systému kritické informační infrastruktury a</p><p class="L6"><a id="f5278879"><i id="p3-1-e"></i></a><var>e)</var> správce významného informačního systému.</p><p class="L2 HLAVA"><a id="f5278880"><i id="cast1-hlava2"></i></a>HLAVA II</p><h3 class="L3 NADPIS"><a id="f5278881"></a>SYSTÉM ZAJIŠTĚNÍ KYBERNETICKÉ BEZPEČNOSTI</h3><h3 class="L3 NADPIS"><a id="f5278882"></a>Bezpečnostní opatření</h3><p class="L4 PARA"><a id="f5278883"><i id="p4"></i></a>§ 4</p><p class="L5"><a id="f5278884"><i id="p4-1"></i></a><var>(1)</var> Bezpečnostním opatřením se rozumí souhrn úkonů, jejichž cílem je zajištění bezpečnosti informací v informačních systémech a dostupnosti a spolehlivosti služeb a sítí elektronických komunikací<a class="linknote" href="#f5279193"><sup>1</sup>)</a> v kybernetickém prostoru.</p><p class="L5"><a id="f5278885"><i id="p4-2"></i></a><var>(2)</var> Orgány a osoby uvedené v § 3 písm. c) až e) jsou povinny v rozsahu nezbytném pro zajištění kybernetické bezpečnosti zavést a provádět bezpečnostní opatření pro informační systém kritické informační infrastruktury, komunikační systém kritické informační infrastruktury nebo významný informační systém a vést o nich bezpečnostní dokumentaci.</p><p class="L5"><a id="f5278886"><i id="p4-3"></i></a><var>(3)</var> Orgány a osoby uvedené v § 3 písm. c) až e) jsou povinny zohlednit požadavky vyplývající z bezpečnostních opatření při výběru dodavatelů pro informační systém kritické informační infrastruktury, komunikační systém kritické informační infrastruktury nebo významný informační systém. Zohlednění požadavků vyplývajících z bezpečnostních opatření podle věty první v míře nezbytné pro splnění povinností podle tohoto zákona nelze považovat za nezákonné omezení hospodářské soutěže nebo neodůvodněnou překážku hospodářské soutěži.</p></div>',
      },
      {
        key: Mock.mock('abc111e.node2'),
        data: '<div><p class="L4 PARA"><a id="f5278887"><i id="p5"></i></a>§ 5</p><p class="L5"><a id="f5278888"><i id="p5-1"></i></a><var>(1)</var> Bezpečnostními opatřeními jsou</p><p class="L6"><a id="f5278889"><i id="p5-1-a"></i></a><var>a)</var> organizační opatření a</p><p class="L6"><a id="f5278890"><i id="p5-1-b"></i></a><var>b)</var> technická opatření.</p><p class="L5"><a id="f5278891"><i id="p5-2"></i></a><var>(2)</var> Organizačními opatřeními jsou</p><p class="L6"><a id="f5278892"><i id="p5-2-a"></i></a><var>a)</var> systém řízení bezpečnosti informací,</p><p class="L6"><a id="f5278893"><i id="p5-2-b"></i></a><var>b)</var> řízení rizik,</p><p class="L6"><a id="f5278894"><i id="p5-2-c"></i></a><var>c)</var> bezpečnostní politika,</p><p class="L6"><a id="f5278895"><i id="p5-2-d"></i></a><var>d)</var> organizační bezpečnost,</p><p class="L6"><a id="f5278896"><i id="p5-2-e"></i></a><var>e)</var> stanovení bezpečnostních požadavků pro dodavatele,</p><p class="L6"><a id="f5278897"><i id="p5-2-f"></i></a><var>f)</var> řízení aktiv,</p><p class="L6"><a id="f5278898"><i id="p5-2-g"></i></a><var>g)</var> bezpečnost lidských zdrojů,</p><p class="L6"><a id="f5278899"><i id="p5-2-h"></i></a><var>h)</var> řízení provozu a komunikací kritické informační infrastruktury nebo významného informačního systému,</p><p class="L6"><a id="f5278900"><i id="p5-2-i"></i></a><var>i)</var> řízení přístupu osob ke kritické informační infrastruktuře nebo k významnému informačnímu systému,</p><p class="L6"><a id="f5278901"><i id="p5-2-j"></i></a><var>j)</var> akvizice, vývoj a údržba kritické informační infrastruktury a významných informačních systémů,</p><p class="L6"><a id="f5278902"><i id="p5-2-k"></i></a><var>k)</var> zvládání kybernetických bezpečnostních událostí a kybernetických bezpečnostních incidentů,</p><p class="L6"><a id="f5278903"><i id="p5-2-l"></i></a><var>l)</var> řízení kontinuity činností a</p><p class="L6"><a id="f5278904"><i id="p5-2-m"></i></a><var>m)</var> kontrola a audit kritické informační infrastruktury a významných informačních systémů.</p><p class="L5"><a id="f5278905"><i id="p5-3"></i></a></div>',
      },
      {
        key: Mock.mock('abc111e.node3'),
        data: '<div><var>(3)</var> Technickými opatřeními jsou<p></p><p class="L6"><a id="f5278906"><i id="p5-3-a"></i></a><var>a)</var> fyzická bezpečnost,</p><p class="L6"><a id="f5278907"><i id="p5-3-b"></i></a><var>b)</var> nástroj pro ochranu integrity komunikačních sítí,</p><p class="L6"><a id="f5278908"><i id="p5-3-c"></i></a><var>c)</var> nástroj pro ověřování identity uživatelů,</p><p class="L6"><a id="f5278909"><i id="p5-3-d"></i></a><var>d)</var> nástroj pro řízení přístupových oprávnění,</p><p class="L6"><a id="f5278910"><i id="p5-3-e"></i></a><var>e)</var> nástroj pro ochranu před škodlivým kódem,</p><p class="L6"><a id="f5278911"><i id="p5-3-f"></i></a><var>f)</var> nástroj pro zaznamenávání činnosti kritické informační infrastruktury a významných informačních systémů, jejich uživatelů a administrátorů,</p><p class="L6"><a id="f5278912"><i id="p5-3-g"></i></a><var>g)</var> nástroj pro detekci kybernetických bezpečnostních událostí,</p><p class="L6"><a id="f5278913"><i id="p5-3-h"></i></a><var>h)</var> nástroj pro sběr a vyhodnocení kybernetických bezpečnostních událostí,</p><p class="L6"><a id="f5278914"><i id="p5-3-i"></i></a><var>i)</var> aplikační bezpečnost,</p><p class="L6"><a id="f5278915"><i id="p5-3-j"></i></a><var>j)</var> kryptografické prostředky,</p><p class="L6"><a id="f5278916"><i id="p5-3-k"></i></a><var>k)</var> nástroj pro zajišťování úrovně dostupnosti informací a</p><p class="L6"><a id="f5278917"><i id="p5-3-l"></i></a><var>l)</var> bezpečnost průmyslových a řídících systémů.</p><p class="L4 PARA"><a id="f5278918"><i id="p6"></i></a>§ 6</p><p class="L5"><a id="f5278919"><i id="p6-1"></i></a>Prováděcí právní předpis stanoví</p><p class="L6"><a id="f5278920"><i id="p6-1-a"></i></a><var>a)</var> obsah bezpečnostních opatření,</p><p class="L6"><a id="f5278921"><i id="p6-1-b"></i></a><var>b)</var> obsah a strukturu bezpečnostní dokumentace,</p><p class="L6"><a id="f5278922"><i id="p6-1-c"></i></a><var>c)</var> rozsah bezpečnostních opatření pro orgány a osoby uvedené v § 3 písm. c) až e) a</p><p class="L6"><a id="f5278923"><i id="p6-1-d"></i></a><var>d)</var> významné informační systémy a jejich určující kritéria.</p><h3 class="L3 NADPIS"><a id="f5278924"></a>Kybernetická bezpečnostní událost a kybernetický bezpečnostní incident</h3><p class="L4 PARA"><a id="f5278925"><i id="p7"></i></a>§ 7</p><p class="L5"><a id="f5278926"><i id="p7-1"></i></a><var>(1)</var> Kybernetickou bezpečnostní událostí je událost, která může způsobit narušení bezpečnosti informací v informačních systémech nebo narušení bezpečnosti služeb anebo bezpečnosti a integrity sítí elektronických komunikací<a class="linknote" href="#f5279193"><sup>1</sup>)</a>.</p><p class="L5"><a id="f5278927"><i id="p7-2"></i></a><var>(2)</var> Kybernetickým bezpečnostním incidentem je narušení bezpečnosti informací v informačních systémech nebo narušení bezpečnosti služeb anebo bezpečnosti a integrity sítí elektronických komunikací<a class="linknote" href="#f5279193"><sup>1</sup>)</a> v důsledku kybernetické bezpečnostní události.</p><p class="L5"><a id="f5278928"><i id="p7-3"></i></a><var>(3)</var> Orgány a osoby uvedené v § 3 písm. b) až e) jsou povinny detekovat kybernetické bezpečnostní události v jejich významné síti, informačním systému kritické informační infrastruktury, komunikačním systému kritické informační infrastruktury nebo významném informačním systému.</p><p class="L4 PARA"><a id="f5278929"><i id="p8"></i></a>§ 8</p><h3 class="L5 NADPIS"><a id="f5278930"></a>Hlášení kybernetického bezpečnostního incidentu</h3><p class="L5"><a id="f5278931"><i id="p8-1"></i></a><var>(1)</var> Orgány a osoby uvedené v § 3 písm. b) až e) jsou povinny hlásit kybernetické bezpečnostní incidenty v jejich významné síti, informačním systému kritické informační infrastruktury, komunikačním systému kritické informační infrastruktury nebo významném informačním systému, a to bezodkladně po jejich detekci; tím není dotčena informační povinnost podle jiného právního předpisu<a class="linknote" href="#f5279195"><sup>3</sup>)</a>.</p><p class="L5"><a id="f5278932"><i id="p8-2"></i></a><var>(2)</var> Orgány a osoby uvedené v § 3 písm. b) hlásí kybernetické bezpečnostní incidenty provozovateli národního CERT.</p><p class="L5"><a id="f5278933"><i id="p8-3"></i></a><var>(3)</var> Orgány a osoby uvedené v § 3 písm. c) až e) hlásí kybernetické bezpečnostní incidenty Národnímu bezpečnostnímu úřadu (dále jen „Úřad“).</p><p class="L5"><a id="f5278934"><i id="p8-4"></i></a><var>(4)</var> Prováděcí právní předpis stanoví</p><p class="L6"><a id="f5278935"><i id="p8-4-a"></i></a><var>a)</var> typy a kategorie kybernetických bezpečnostních incidentů a</p><p class="L6"><a id="f5278936"><i id="p8-4-b"></i></a><var>b)</var> náležitosti a způsob hlášení kybernetického bezpečnostního incidentu.</p><h3 class="L3 NADPIS"><a id="f5278937"></a>Evidence</h3><p class="L4 PARA"><a id="f5278938"><i id="p9"></i></a>§ 9</p><p class="L5"><a id="f5278939"><i id="p9-1"></i></a><var>(1)</var> Úřad vede evidenci kybernetických bezpečnostních incidentů (dále jen „evidence incidentů“), která obsahuje</p><p class="L6"><a id="f5278940"><i id="p9-1-a"></i></a><var>a)</var> hlášení kybernetického bezpečnostního incidentu,</p><p class="L6"><a id="f5278941"><i id="p9-1-b"></i></a><var>b)</var> identifikační údaje systému, ve kterém se kybernetický bezpečnostní incident vyskytl,</p><p class="L6"><a id="f5278942"><i id="p9-1-c"></i></a><var>c)</var> údaje o zdroji kybernetického bezpečnostního incidentu a</p><p class="L6"><a id="f5278943"><i id="p9-1-d"></i></a><var>d)</var> postup při řešení kybernetického bezpečnostního incidentu a jeho výsledek.</p><p class="L5"><a id="f5278944"><i id="p9-2"></i></a><var>(2)</var> Součástí evidence incidentů jsou údaje podle § 20 písm. f) až h).</p><p class="L5"><a id="f5278945"><i id="p9-3"></i></a><var>(3)</var> Úřad poskytuje údaje z evidence incidentů orgánům veřejné moci pro výkon jejich působnosti.</p><p class="L5"><a id="f5278946"><i id="p9-4"></i></a><var>(4)</var> Úřad může poskytovat údaje z evidence incidentů provozovateli národního CERT, orgánům vykonávajícím působnost v oblasti kybernetické bezpečnosti v zahraničí a jiným osobám působícím v oblasti kybernetické bezpečnosti v rozsahu nezbytném pro zajištění ochrany kybernetického prostoru.</p><p class="L4 PARA"><a id="f5278947"><i id="p10"></i></a>§ 10</p><p class="L5"><a id="f5278948"><i id="p10-1"></i></a><var>(1)</var> Zaměstnanci České republiky zařazení k výkonu práce v Úřadu, kteří se podílejí na řešení kybernetického bezpečnostního incidentu, jsou vázáni povinností mlčenlivosti o údajích z evidence incidentů. Povinnost mlčenlivosti trvá i po skončení pracovněprávního vztahu k Úřadu.</p><p class="L5"><a id="f5278949"><i id="p10-2"></i></a><var>(2)</var> Ředitel Úřadu může osoby podle odstavce 1 zprostit povinnosti mlčenlivosti o údajích z evidence incidentů, s uvedením rozsahu údajů a rozsahu zproštění.</p><p class="L4 PARA"><a id="f5278950"><i id="p11"></i></a>§ 11</p><h3 class="L5 NADPIS"><a id="f5278951"></a>Opatření</h3><p class="L5"><a id="f5278952"><i id="p11-1"></i></a><var>(1)</var> Opatřeními se rozumí úkony, jichž je třeba k ochraně informačních systémů nebo služeb a sítí elektronických komunikací<a class="linknote" href="#f5279193"><sup>1</sup>)</a> před hrozbou v oblasti kybernetické bezpečnosti nebo před kybernetickým bezpečnostním incidentem anebo k řešení již nastalého kybernetického bezpečnostního incidentu.</p><p class="L5"><a id="f5278953"><i id="p11-2"></i></a><var>(2)</var> Opatřeními jsou</p><p class="L6"><a id="f5278954"><i id="p11-2-a"></i></a><var>a)</var> varování,</p><p class="L6"><a id="f5278955"><i id="p11-2-b"></i></a><var>b)</var> reaktivní opatření a</p></div>',
      },
    ],
  });

  dataKey = mockStorage('RequirementsList', mock);
} else {
  dataKey = mockStorage('RequirementsList');
}

const requirementsListData = global[dataKey];

module.exports = {

  'GET /api/risk/requirements': function (req, res) {
    const data = requirementsListData.data;
    res.json({ success: true, data });
  },

  'POST /api/risk/requirements': function (req, res) {
    const body = req.body;
    console.log('body', body);

    const oData = body.itemNew;
    oData.id = requirementsListData.data.length + 1;
    oData.key = Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/);
    oData.created_at = Mock.mock('@now');
    oData.updated_at = null;
    oData.item_type = 'requirements';
    oData.user = body.appUser;

    if (oData.data) {
      const dataObjectData = [];
      oData.data.forEach(function (item) {
        dataObjectData.push({
          key: Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/),
          data: item,
        });
      });
      oData.data = dataObjectData;
    }

    requirementsListData.data.push(oData);
    global[dataKey] = requirementsListData;
    res.json({ success: true, list: requirementsListData.data, results: oData });
  },

  'DELETE /api/risk/requirements': function (req, res) {
    let deleteItem = req.body;

    requirementsListData.data = requirementsListData.data.filter(function (item) {
      if (item.id === deleteItem.id) {
        deleteItem = item;
      }

      return item.id !== deleteItem.id;
    });

    const relationships = global.portalRelationships;
    relationships.data = relationships.data.filter(function (item) {
      return item.key !== deleteItem.key;
    });

    localStorage.setItem('portalRelationships', JSON.stringify(relationships));

    global[dataKey] = requirementsListData;
    res.json({ success: true, list: requirementsListData.data });
  },

  'PUT /api/risk/requirements/restore': function (req, res) {
    const restoredItem = req.body;
    console.log('restoredItem', restoredItem);

    const data = requirementsListData.data.map(function (item) {
      if (item.id === restoredItem.id) {
        item.updated_at = Mock.mock('@now');
        delete item.status;
      }
      return item;
    });

    requirementsListData.data = data;
    global[dataKey] = requirementsListData;
    localStorage.setItem(dataKey, JSON.stringify(requirementsListData));
    res.json({ success: true, list: data });
  },

  'PUT /api/risk/requirements/item/id': function (req, res) {
    const body = req.body;
    let itemSelected;
        // console.log('body', body);

    requirementsListData.data.forEach(function (item) {
      if (item.key === body.key) {
        itemSelected = item;
      }
    });

    res.json({ success: true, data: itemSelected });
  },

  'PUT /api/risk/requirements/tree': function (req, res) {
    const tree = req.body;
    localStorage.setItem(dataKey, JSON.stringify({ data: tree }));
        // console.log('global global[dataKey]', global[dataKey]);
    res.json({ success: true, data: tree });
  },


  'PUT /api/risk/requirements': function (req, res) {
    const body = req.body;
    let itemUpdated = body;
    let itemHistory = {};

    if (body.item) {
      itemUpdated = body.item;
    }
    delete itemUpdated.children;
        // console.log('********************PUT /api/risk/requirements', itemUpdated);

    const data = requirementsListData.data.map(function (item) {
      if (item.id !== itemUpdated.id && item.key_parents) {
        const index = item.key_parents.indexOf(itemUpdated.key);

        if (index !== -1) {
                    /* +1 for keys separator "." */
          const key_parents = item.key_parents.substring(index + itemUpdated.key.length + 1);

          if (key_parents) {
            item.key_parents = itemUpdated.key_parents ? `${itemUpdated.key_parents}.${itemUpdated.key}.${key_parents}` : `${itemUpdated.key}.${key_parents}`;
          } else {
            item.key_parents = itemUpdated.key_parents ? `${itemUpdated.key_parents}.${itemUpdated.key}` : itemUpdated.key;
          }
        }
      }

      if (item.id === itemUpdated.id) {
        itemHistory = { ...item };
        itemUpdated.updated_at = Mock.mock('@now');

        if (!item.data && itemUpdated.data) {
          const dataObjectData = [];
          itemUpdated.data.forEach(function (item) {
            dataObjectData.push({
              key: Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/),
              data: item,
            });
          });
          itemUpdated.data = dataObjectData;
        }
        item = itemUpdated;
      }
      return item;
    });

    requirementsListData.data = data;
    res.json({
      success: true,
      list: data,
      item: itemUpdated,
      itemHistory,
    });
  },

  'PUT /api/risk/requirements/[0-9]': function (req, res) {
    const editItem = req.body;

    editItem.createTime = Mock.mock('@now');
    editItem.avatar = Mock.Random.image('100x100', Mock.Random.color(), '#fff', 'png', editItem.title.substr(0, 1));

    requirementsListData.data = requirementsListData.data.map(function (item) {
      if (item.id === editItem.id) {
        return editItem;
      }
      return item;
    });

    global[dataKey] = requirementsListData;
    res.json({ success: true, data: requirementsListData.data });
  },


  'POST /api/risk/requirements/folder/create': function (req, res) {
    const body = req.body;

    const dataObject = {};
    dataObject.id = requirementsListData.data.length + 1;
    dataObject.key = Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/);
    dataObject.key_parents = body.key_parents;
    dataObject.type = 'folder';
    dataObject.title = body.title;
    dataObject.created_at = Mock.mock('@now');
    dataObject.updated_at = null;

    requirementsListData.data.push(dataObject);
    global[dataKey] = requirementsListData;
    res.json({ success: true, list: requirementsListData.data });
  },

};
