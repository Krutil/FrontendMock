const Watch = require('watchjs');
const qs = require('qs');
const Mock = require('mockjs');
import mockStorage from '../src/utils/mockStorage';

let dataKey;

function getUser() {
  if (localStorage.getItem('portalUsersList')) {
    const items = JSON.parse((localStorage.getItem('portalUsersList'))).data;
    return items[Math.floor(Math.random() * items.length)];
  }
  return null;
}

/* Nevytvarime opet mock, pokud jiz jeden ulozeny existuje. */

if (localStorage.getItem('portalRisksList') == null) {
  /*dataKey = mockStorage('RisksList', Mock.mock({
    'data|5': [
      {
        'id|+1': 1,
        'key|+1': /[a-z][A-Z][0-9][a-z][A-Z][0-9]/,
        key_parents: null,
        type: 'item',
        item_type: 'risks',
        status: 'accepted',
        title: '@Title(1,3)',
        number: '@string("number", 8)',
        assigned_user: null,
        schema: [],
        date_valid_from: '@date',
        date_valid_to: '@date',
        source: null,
        cause: `<p>${Mock.mock('@paragraph')}</p>`,
        impact: `<p>${Mock.mock('@paragraph')}</p>`,
        user: getUser(),
        created_at: '@now',
        updated_at: null,
      },
    ],
  }));*/

  dataKey = mockStorage('RisksList', {data: [
    {
      id: 1,
      key: Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/),
      key_parents: null,
      type: 'folder',
      item_type: 'risks',
      status: 'accepted',
      private: 0,
      title: 'HR',
      number: Mock.mock('@string("number", 8)'),
      assigned_user: null,
      schema: [],
      date_valid_from: Mock.mock('@date'),
      date_valid_to: Mock.mock('@date'),
      source: null,
      cause: `<p>${Mock.mock('@paragraph')}</p>`,
      impact: `<p>${Mock.mock('@paragraph')}</p>`,
      user: getUser(),
      created_at: Mock.mock('@now'),
      updated_at: null,
    },
    {
      id: 2,
      key: 'inf',
      key_parents: null,
      type: 'folder',
      item_type: 'risks',
      status: 'accepted',
      private: 0,
      title: 'Informační rizika',
      number: Mock.mock('@string("number", 8)'),
      assigned_user: null,
      schema: [],
      date_valid_from: Mock.mock('@date'),
      date_valid_to: Mock.mock('@date'),
      source: null,
      cause: `<p>${Mock.mock('@paragraph')}</p>`,
      impact: `<p>${Mock.mock('@paragraph')}</p>`,
      user: getUser(),
      created_at: Mock.mock('@now'),
      updated_at: null,
    },
    {
      id: 21,
      key: 'inf1',
      key_parents: 'inf',
      type: 'item',
      item_type: 'risks',
      status: 'accepted',
      private: 0,
      title: 'SW a metodická podpora pro řízení aktiv a rizik',
      number: Mock.mock('@string("number", 8)'),
      assigned_user: null,
      schema: [],
      date_valid_from: Mock.mock('@date'),
      date_valid_to: Mock.mock('@date'),
      source: null,
      cause: `<p>${Mock.mock('@paragraph')}</p>`,
      impact: `<p>${Mock.mock('@paragraph')}</p>`,
      user: getUser(),
      created_at: Mock.mock('@now'),
      updated_at: null,
    },
    {
      id: 22,
      key: 'inf22',
      key_parents: 'inf',
      type: 'item',
      item_type: 'risks',
      status: 'accepted',
      private: 0,
      title: 'Kybernetická rizika',
      number: Mock.mock('@string("number", 8)'),
      assigned_user: null,
      schema: [],
      date_valid_from: Mock.mock('@date'),
      date_valid_to: Mock.mock('@date'),
      source: null,
      cause: `<p>${Mock.mock('@paragraph')}</p>`,
      impact: `<p>${Mock.mock('@paragraph')}</p>`,
      user: getUser(),
      created_at: Mock.mock('@now'),
      updated_at: null,
    },
    {
      id: 221,
      key: 'inf221',
      key_parents: 'inf.inf22',
      type: 'item',
      item_type: 'risks',
      status: 'accepted',
      private: 0,
      title: 'Průnik do systému nebo omezení dostupnosti služeb',
      number: Mock.mock('@string("number", 8)'),
      assigned_user: null,
      schema: [],
      date_valid_from: Mock.mock('@date'),
      date_valid_to: Mock.mock('@date'),
      source: null,
      cause: `<p>${Mock.mock('@paragraph')}</p>`,
      impact: `<p>${Mock.mock('@paragraph')}</p>`,
      user: getUser(),
      created_at: Mock.mock('@now'),
      updated_at: null,
    },
    {
      id: 222,
      key: 'inf222',
      key_parents: 'inf.inf22',
      type: 'item',
      item_type: 'risks',
      status: 'accepted',
      private: 0,
      title: 'Škodlivý kód',
      number: Mock.mock('@string("number", 8)'),
      assigned_user: null,
      schema: [],
      date_valid_from: Mock.mock('@date'),
      date_valid_to: Mock.mock('@date'),
      source: null,
      cause: `<p>${Mock.mock('@paragraph')}</p>`,
      impact: `<p>${Mock.mock('@paragraph')}</p>`,
      user: getUser(),
      created_at: Mock.mock('@now'),
      updated_at: null,
    },
    {
      id: 223,
      key: 'inf223',
      key_parents: 'inf.inf22',
      type: 'item',
      item_type: 'risks',
      status: 'accepted',
      private: 0,
      title: 'Překonání technických opatření',
      number: Mock.mock('@string("number", 8)'),
      assigned_user: null,
      schema: [],
      date_valid_from: Mock.mock('@date'),
      date_valid_to: Mock.mock('@date'),
      source: null,
      cause: `<p>${Mock.mock('@paragraph')}</p>`,
      impact: `<p>${Mock.mock('@paragraph')}</p>`,
      user: getUser(),
      created_at: Mock.mock('@now'),
      updated_at: null,
    },
    {
      id: 224,
      key: 'inf224',
      key_parents: 'inf.inf22',
      type: 'item',
      item_type: 'risks',
      status: 'accepted',
      private: 0,
      title: 'Porušení organizačních opatření',
      number: Mock.mock('@string("number", 8)'),
      assigned_user: null,
      schema: [],
      date_valid_from: Mock.mock('@date'),
      date_valid_to: Mock.mock('@date'),
      source: null,
      cause: `<p>${Mock.mock('@paragraph')}</p>`,
      impact: `<p>${Mock.mock('@paragraph')}</p>`,
      user: getUser(),
      created_at: Mock.mock('@now'),
      updated_at: null,
    },
    {
      id: 225,
      key: 'inf225',
      key_parents: 'inf.inf22',
      type: 'item',
      item_type: 'risks',
      status: 'accepted',
      private: 0,
      title: 'Trvale působící hrozby',
      number: Mock.mock('@string("number", 8)'),
      assigned_user: null,
      schema: [],
      date_valid_from: Mock.mock('@date'),
      date_valid_to: Mock.mock('@date'),
      source: null,
      cause: `<p>${Mock.mock('@paragraph')}</p>`,
      impact: `<p>${Mock.mock('@paragraph')}</p>`,
      user: getUser(),
      created_at: Mock.mock('@now'),
      updated_at: null,
    },
    {
      id: 23,
      key: 'inf3',
      key_parents: 'inf',
      type: 'item',
      item_type: 'risks',
      status: 'accepted',
      private: 0,
      title: 'Ochrana osobních údajů (GDPR)',
      number: Mock.mock('@string("number", 8)'),
      assigned_user: null,
      schema: [],
      date_valid_from: Mock.mock('@date'),
      date_valid_to: Mock.mock('@date'),
      source: null,
      cause: `<p>${Mock.mock('@paragraph')}</p>`,
      impact: `<p>${Mock.mock('@paragraph')}</p>`,
      user: getUser(),
      created_at: Mock.mock('@now'),
      updated_at: null,
    },
    {
      id: 3,
      key: Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/),
      key_parents: null,
      type: 'folder',
      item_type: 'risks',
      status: 'accepted',
      private: 0,
      title: 'Finanční rizika',
      number: Mock.mock('@string("number", 8)'),
      assigned_user: null,
      schema: [],
      date_valid_from: Mock.mock('@date'),
      date_valid_to: Mock.mock('@date'),
      source: null,
      cause: `<p>${Mock.mock('@paragraph')}</p>`,
      impact: `<p>${Mock.mock('@paragraph')}</p>`,
      user: getUser(),
      created_at: Mock.mock('@now'),
      updated_at: null,
    },
    {
      id: 4,
      key: Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/),
      key_parents: null,
      type: 'folder',
      item_type: 'risks',
      status: 'accepted',
      private: 0,
      title: 'Objektová rizika',
      number: Mock.mock('@string("number", 8)'),
      assigned_user: null,
      schema: [],
      date_valid_from: Mock.mock('@date'),
      date_valid_to: Mock.mock('@date'),
      source: null,
      cause: `<p>${Mock.mock('@paragraph')}</p>`,
      impact: `<p>${Mock.mock('@paragraph')}</p>`,
      user: getUser(),
      created_at: Mock.mock('@now'),
      updated_at: null,
    },
    {
      id: 5,
      key: Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/),
      key_parents: null,
      type: 'folder',
      item_type: 'risks',
      status: 'accepted',
      private: 0,
      title: 'Environmentální rizika',
      number: Mock.mock('@string("number", 8)'),
      assigned_user: null,
      schema: [],
      date_valid_from: Mock.mock('@date'),
      date_valid_to: Mock.mock('@date'),
      source: null,
      cause: `<p>${Mock.mock('@paragraph')}</p>`,
      impact: `<p>${Mock.mock('@paragraph')}</p>`,
      user: getUser(),
      created_at: Mock.mock('@now'),
      updated_at: null,
    },
    {
      id: 6,
      key: Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/),
      key_parents: null,
      type: 'folder',
      item_type: 'risks',
      status: 'accepted',
      private: 0,
      title: 'Provozní rizika',
      number: Mock.mock('@string("number", 8)'),
      assigned_user: null,
      schema: [],
      date_valid_from: Mock.mock('@date'),
      date_valid_to: Mock.mock('@date'),
      source: null,
      cause: `<p>${Mock.mock('@paragraph')}</p>`,
      impact: `<p>${Mock.mock('@paragraph')}</p>`,
      user: getUser(),
      created_at: Mock.mock('@now'),
      updated_at: null,
    },
    {
      id: 7,
      key: Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/),
      key_parents: null,
      type: 'folder',
      item_type: 'risks',
      status: 'accepted',
      private: 0,
      title: 'Strategická rizika',
      number: Mock.mock('@string("number", 8)'),
      assigned_user: null,
      schema: [],
      date_valid_from: Mock.mock('@date'),
      date_valid_to: Mock.mock('@date'),
      source: null,
      cause: `<p>${Mock.mock('@paragraph')}</p>`,
      impact: `<p>${Mock.mock('@paragraph')}</p>`,
      user: getUser(),
      created_at: Mock.mock('@now'),
      updated_at: null,
    },
  ]});
} else {
  dataKey = mockStorage('RisksList');
}

const risksListData = global[dataKey];

module.exports = {

  'GET /api/risk/risks': function (req, res) {
    res.json({ success: true, list: risksListData.data });
  },

  'POST /api/risk/risks': function (req, res) {
    const body = req.body;

    const dataObject = body.item;
    dataObject.id = risksListData.data.length + 1;
    dataObject.key = Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/);
    dataObject.item_type = 'risks';
    dataObject.schema = body.item.schema || [];
    dataObject.created_at = Mock.mock('@now');
    dataObject.updated_at = null;
    dataObject.user = {
      id: body.appUser.id,
      firstname: body.appUser.firstname,
      lastname: body.appUser.lastname,
      avatar: body.appUser.avatar,
    };

    risksListData.data.push(dataObject);
    global[dataKey] = risksListData;
    res.json({ success: true, list: risksListData.data, results: dataObject });
  },

  'DELETE /api/risk/risks': function (req, res) {
    const deleteItem = req.body;

    risksListData.data = risksListData.data.filter(function (item) {
      return item.id !== deleteItem.id;
    });

    global[dataKey] = risksListData;
    res.json({ success: true, list: risksListData.data });
  },

  'PUT /api/risk/risks/restore': function (req, res) {
    const restoredItem = req.body;
        // console.log('restoredItem', restoredItem);

    const data = risksListData.data.map(function (item) {
      if (item.id === restoredItem.id) {
        item.updated_at = Mock.mock('@now');
        item.status = 'accepted';
      }
      return item;
    });

    risksListData.data = data;
    global[dataKey] = risksListData;
    localStorage.setItem(dataKey, JSON.stringify(risksListData));
    res.json({ success: true, list: data });
  },

  'PUT /api/risk/risks/item/id': function (req, res) {
    const body = req.body;
    let itemSelected;
        // console.log('body', body);

    risksListData.data.forEach(function (item) {
      if (item.key == body.key) {
        itemSelected = item;
      }
    });

    res.json({ success: true, currentItem: itemSelected });
  },

  'PUT /api/risk/risks/tree': function (req, res) {
    const tree = req.body;
    localStorage.setItem(dataKey, JSON.stringify({ data: tree }));
        // console.log('global global[dataKey]', global[dataKey]);
    res.json({ success: true, list: tree });
  },

  'PUT /api/risk/risks/schema': function (req, res) {
    const body = req.body;
    const itemUpdated = body;
    let currentItem = null;

    const data = risksListData.data.map(function (item) {
      if (item.id === itemUpdated.id) {
        itemUpdated.updated_at = Mock.mock('@now');
        item.schema = item.schema || [];
        itemUpdated.schema.key = Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/);
        item.schema.push(itemUpdated.schema);
        currentItem = { ...item };
      }
      return item;
    });

    risksListData.data = data;
    global[dataKey] = risksListData;
    localStorage.setItem(dataKey, JSON.stringify(risksListData));
    res.json({ success: true, list: data, currentItem });
  },

  'DELETE /api/risk/risks/schema': function (req, res) {
    const body = req.body;
    const itemUpdated = body;
    let currentItem = null;

        // console.log('DELETE itemUpdated', itemUpdated);

    const data = risksListData.data.map(function (item) {
      if (item.id === itemUpdated.id) {
        itemUpdated.updated_at = Mock.mock('@now');
        const schemaArray = [];
        item.schema.forEach(function (schema, index) {
          if (schema.key !== itemUpdated.schema_key) {
            schemaArray.push(schema);
          }
        });

        item.schema = schemaArray;
        currentItem = { ...item };
      }
      return item;
    });

    risksListData.data = data;
    global[dataKey] = risksListData;
    res.json({ success: true, list: data, currentItem });
  },

  'PUT /api/risk/risks': function (req, res) {
    const body = req.body;
    let itemUpdated = body;
    let itemHistory = null;

    if (body.item) {
      itemUpdated = body.item;
    }
    delete itemUpdated.children;
        // console.log('********************PUT /api/risk/risks', itemUpdated);

    const data = risksListData.data.map(function (item) {
      if (item.id !== itemUpdated.id && item.key_parents) {
        const index = item.key_parents.indexOf(itemUpdated.key);

        if (index !== -1) {
                    /* +1 for keys separator "." */
          const key_parents = item.key_parents.substring(index + itemUpdated.key.length + 1);

          if (key_parents) {
            item.key_parents = itemUpdated.key_parents ? `${itemUpdated.key_parents}.${itemUpdated.key}.${key_parents}` : `${itemUpdated.key}.${key_parents}`;
          } else {
            item.key_parents = itemUpdated.key_parents ? `${itemUpdated.key_parents}.${itemUpdated.key}` : itemUpdated.key;
          }
        }
      }

      if (item.id === itemUpdated.id) {
        itemHistory = { ...item };
        itemUpdated.updated_at = Mock.mock('@now');
        item = itemUpdated;
      }
      return item;
    });

    risksListData.data = data;
    res.json({
      success: true,
      list: data,
      item: itemUpdated,
      itemHistory,
    });
  },

  'PUT /api/risk/risks/[0-9]': function (req, res) {
    const editItem = req.body;

    editItem.createTime = Mock.mock('@now');
    editItem.avatar = Mock.Random.image('100x100', Mock.Random.color(), '#fff', 'png', editItem.title.substr(0, 1));

    risksListData.data = risksListData.data.map(function (item) {
      if (item.id === editItem.id) {
        return editItem;
      }
      return item;
    });

    global[dataKey] = risksListData;
    res.json({ success: true, list: risksListData.data });
  },

  'POST /api/risk/risks/folder/create': function (req, res) {
    const body = req.body;

        // console.log('POST /api/risk/risks/folder/create', body);
        // console.log('POST risksListData', risksListData);

    const dataObject = {};
    dataObject.id = risksListData.data.length + 1;
    dataObject.key = Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/);
    dataObject.key_parents = body.key_parents;
    dataObject.type = 'folder';
    dataObject.title = body.title;
    dataObject.private = body.private;
    dataObject.created_at = Mock.mock('@now');
    dataObject.updated_at = null;

    risksListData.data.push(dataObject);
    global[dataKey] = risksListData;
    res.json({ success: true, list: risksListData.data });
  },

};
