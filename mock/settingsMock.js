const Watch = require('watchjs');
const qs = require('qs');
const Mock = require('mockjs');
import mockStorage from '../src/utils/mockStorage';

let dataKey;

/* Nevytvarime opet mock, pokud jiz jeden ulozeny existuje. */
if (localStorage.getItem('portalSettings') == null) {
  dataKey = mockStorage('Settings', null);
} else {
  dataKey = mockStorage('Settings');
}

const settingsData = global[dataKey];

module.exports = {

  'GET /api/settings': function (req, res) {
    res.json({ success: true, data: settingsData });
  },

  'PUT /api/settings/risk': function (req, res) {
    const data = req.body;
    let settingsDataUpdate = settingsData;
    settingsDataUpdate = settingsDataUpdate || {};
    settingsDataUpdate.risk = settingsDataUpdate.risk || {};
    settingsDataUpdate.risk[data.type] = { ...data };
    localStorage.setItem(dataKey, JSON.stringify(settingsDataUpdate));

    res.json({
      success: true,
      data: settingsDataUpdate,
    });
  },
};
