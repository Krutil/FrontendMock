const Watch = require('watchjs');
const qs = require('qs');
const Mock = require('mockjs');
import mockStorage from '../src/utils/mockStorage';

let dataKey;

/* Nevytvarime opet mock, pokud jiz jeden ulozeny existuje. */

if (localStorage.getItem('portalSpectators') == null) {
  dataKey = mockStorage('Spectators', Mock.mock({
    data: [
    ],
  }));
} else {
  dataKey = mockStorage('Spectators');
}

const spectatorsData = global[dataKey];

module.exports = {

  'GET /api/spectators': function (req, res) {
        // console.log('req', req);

    const data = spectatorsData.data;
        // console.log('GET /api/spectators ->>', data);

    res.json({ success: true, data });
  },


  'POST /api/spectators/store': function (req, res) {
    res.json({ success: true });
  },

  'POST /api/spectators': function (req, res) {
    const newData = req.body;

    const dataObject = {};
    dataObject.id = spectatorsData.data.length + 1;
    dataObject.category = newData.category;
    dataObject.title = newData.title;
    dataObject.type_key = newData.type_key;
    dataObject.following = true;
    dataObject.disabled = false;
    dataObject.user = {
      id: newData.appUser.id,
      firstname: newData.appUser.firstname,
      lastname: newData.appUser.lastname,
      avatar: newData.appUser.avatar,
    };
    dataObject.created_at = Mock.mock('@now');
    dataObject.updated_at = dataObject.created_at;

    spectatorsData.data.push(dataObject);

    global[dataKey] = spectatorsData;

    res.json({ success: true, data: spectatorsData.data });
  },

  'PUT /api/spectators/updateDate': function (req, res) {
    const dataObject = req.body;
    console.log('UT /api/spectators/updateDat');

    const data = spectatorsData.data.map(function (item) {
      if (item.id === dataObject.id) {
        item.updated_at = Mock.mock('@now');
      }
      return item;
    });

        // console.log('dataUpdated', data);

    spectatorsData.data = data;
        // global[dataKey] = {data: data};
        // localStorage.setItem(dataKey, JSON.stringify({data: data}));

        // console.log('global global[dataKey]', global[dataKey]);
    res.json({ success: true, data });
  },

  'PUT /api/spectators': function (req, res) {
    const dataObject = req.body;

    const data = spectatorsData.data.map(function (item) {
      if (item.id === dataObject.id) {
        item.following = !item.following;
        item.updated_at = Mock.mock('@now');
      }
      return item;
    });

        // console.log('dataUpdated', data);

    spectatorsData.data = data;
        // global[dataKey] = {data: data};
        // localStorage.setItem(dataKey, JSON.stringify({data: data}));

        // console.log('global global[dataKey]', global[dataKey]);
    res.json({ success: true, data });
  },

  'DELETE /api/spectators': function (req, res) {
    const dataObject = req.body;
        // console.log('DELETE /api/spectators', dataObject);

    spectatorsData.data = spectatorsData.data.filter(function (item) {
            // console.log('item.id', item.id);
            // console.log('dataObject.id', dataObject.id);
      if (item.id === dataObject.id) {
                // console.log('DELETE item', item);
        return false;
      }
      return true;
    });

        // console.log('spectatorsData.data', spectatorsData.data);

    global[dataKey] = spectatorsData;

    res.json({ success: true, data: spectatorsData.data });
  },

};
