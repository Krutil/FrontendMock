const qs = require('qs');
const Mock = require('mockjs');
import mockStorage from '../src/utils/mockStorage';

const mock = Mock.mock({
  'data|15': [
    {
      'id|+1': 10,
      firstname: '@first',
      lastname: '@last',
      username() {
        return `${this.lastname.toLowerCase()}.${this.firstname.toLowerCase()}`;
      },
      password: '123456',
      phone: /^7[1-2][1-9] \d{3} \d{3}$/,
      'department|1': [
        'Alfa',
        'Beta',
        'Gamma',
      ],
      sex: /men|women/,
      email() {
        return `${this.lastname.toLowerCase()}.${this.firstname.toLowerCase()}@abc.cz`;
      },
      date_last_login: '@datetime',
      created_at: '@datetime',
      updated_at: '@datetime',
      avatar() {
        return `https://randomuser.me/api/portraits/${this.sex}/${this.id}.jpg`;
      },
      'role|1': [
        'Administrátor',
        'Vlastník rizik',
        'Identifikace rizik',
        'Uživatel',
        'Auditor',
      ],
    },
  ],
  page: {
    total: 20,
    current: 1,
  },
});

mock.data.unshift({
  id: 5,
  username: 'risk-user',
  password: '123',
  firstname: 'Alexander',
  lastname: 'Procházka',
  department: 'Alfa',
  phone: '616 441 551',
  email: 'prochazka.alexander@abc.cz',
  date_check_notification: '2017-03-20 07:09:15',
  date_check_approval: '2017-03-20 07:09:15',
  date_check_messages: '2017-03-20 07:09:15',
  date_check_trash: '2017-03-20 07:09:15',
  avatar: 'https://randomuser.me/api/portraits/men/11.jpg',
  role: 'Identifikace rizik',
  date_last_login: Mock.mock('@now'),
});

mock.data.unshift({
  id: 4,
  username: 'risk-owner-2',
  password: '123',
  firstname: 'Robert',
  lastname: 'Polenka',
  department: 'Alfa',
  phone: '716 001 321',
  email: 'polenka.robert@abc.cz',
  date_check_notification: '2017-03-20 07:09:15',
  date_check_approval: '2017-03-20 07:09:15',
  date_check_messages: '2017-03-20 07:09:15',
  date_check_trash: '2017-03-20 07:09:15',
  avatar: 'https://randomuser.me/api/portraits/men/8.jpg',
  role: 'Vlastník rizik',
  date_last_login: Mock.mock('@now'),
});

mock.data.unshift({
  id: 2,
  username: 'risk-owner',
  password: '123',
  firstname: 'David',
  lastname: 'Slunečný',
  department: 'Alfa',
  phone: '717 654 321',
  email: 'slunecny.petr@abc.cz',
  date_check_notification: '2017-03-20 07:09:15',
  date_check_approval: '2017-03-20 07:09:15',
  date_check_messages: '2017-03-20 07:09:15',
  date_check_trash: '2017-03-20 07:09:15',
  avatar: 'https://randomuser.me/api/portraits/men/3.jpg',
  role: 'Vlastník rizik',
  date_last_login: Mock.mock('@now'),
});

mock.data.unshift({
  id: 3,
  username: 'auditorka',
  password: '123',
  firstname: 'Martina',
  lastname: 'Krásná',
  department: 'Alfa',
  phone: '717 654 320',
  email: 'krasna.martina@abc.cz',
  date_check_notification: '2017-03-20 07:09:15',
  date_check_approval: '2017-03-20 07:09:15',
  date_check_messages: '2017-03-20 07:09:15',
  date_check_trash: '2017-03-20 07:09:15',
  avatar: 'https://randomuser.me/api/portraits/women/10.jpg',
  role: 'Auditor',
  date_last_login: Mock.mock('@now'),
});

mock.data.unshift({
  id: 1,
  username: 'risk-manager',
  password: '123',
  firstname: 'Jan',
  lastname: 'Novák',
  department: 'Alfa',
  phone: '717 123 456',
  email: 'novak.jan@abc.cz',
  date_check_notification: '2017-03-20 07:09:15',
  date_check_approval: '2017-03-20 07:09:15',
  date_check_messages: '2017-03-20 07:09:15',
  date_check_trash: '2017-03-20 07:09:15',
  avatar: 'https://randomuser.me/api/portraits/men/27.jpg',
  role: 'Administrátor, Risk Manager',
  date_last_login: Mock.mock('@now'),
});

const dataKey = mockStorage('UsersList', mock);
const usersListData = global[dataKey];

module.exports = {

  'GET /api/users': function (req, res) {
    const page = qs.parse(req.query);
    const pageSize = page.pageSize || usersListData.data.length;
    const currentPage = page.page || 1;

    let data;
    let newPage;

    const newData = usersListData.data.concat();

    if (page.field) {
      const d = newData.filter(function (item) {
        return item[page.field].indexOf(decodeURI(page.keyword)) > -1;
      });

      data = d.slice((currentPage - 1) * pageSize, currentPage * pageSize);

      newPage = {
        current: currentPage * 1,
        total: d.length,
      };
    } else {
      data = usersListData.data.slice((currentPage - 1) * pageSize, currentPage * pageSize);
      usersListData.page.current = currentPage * 1;
      newPage = usersListData.page;
    }
    res.json({ success: true, data, page: { ...newPage, pageSize: Number(pageSize) } });
  },

  'POST /api/users': function (req, res) {
    const newData = req.body;
    newData.createTime = Mock.mock('@now');
    newData.sex = 'men';
    newData.date_last_login = null;
    newData.created_at = Mock.mock('@datetime');
    newData.updated_at = Mock.mock('@datetime');

    newData.avatar = Mock.Random.image('100x100', Mock.Random.color(), '#ffffff', 'png', newData.lastname.substr(0, 1));

    newData.id = usersListData.data.length + 1;
    usersListData.data.unshift(newData);

    usersListData.page.total = usersListData.data.length;
    usersListData.page.current = 1;

    global[dataKey] = usersListData;

    res.json({ success: true, data: usersListData.data, page: usersListData.page });
  },

  'DELETE /api/users/[0-9]': function (req, res) {
        // console.log('req', req);
        // console.log('res', res);
    const deleteItem = req.body;

    usersListData.data = usersListData.data.filter(function (item) {
      if (item.id === deleteItem.id) {
        return false;
      }
      return true;
    });

    usersListData.page.total = usersListData.data.length;

    global[dataKey] = usersListData;

    res.json({ success: true, data: usersListData.data, page: usersListData.page });
  },

  'PUT /api/users': function (req, res) {
    const editItem = req.body;
    editItem.createTime = Mock.mock('@now');
    editItem.sex = 'men';
    editItem.date_last_login = null;
    editItem.created_at = Mock.mock('@datetime');
    editItem.updated_at = Mock.mock('@datetime');

    editItem.avatar = Mock.Random.image('100x100', Mock.Random.color(), '#ffffff', 'png', editItem.lastname.substr(0, 1));


    usersListData.data = usersListData.data.map(function (item) {
      if (item.id === editItem.id) {
        return editItem;
      }
      return item;
    });

    global[dataKey] = usersListData;
    res.json({ success: true, data: usersListData.data, page: usersListData.page });
  },

};
