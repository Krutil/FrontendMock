const rucksack = require('rucksack-css');
const autoprefixer = require('autoprefixer');
const postcssClearfix = require('postcss-clearfix');
const postcssFontPath = require('postcss-fontpath');
const postcssHexrgba = require('postcss-hexrgba');
const postcssEasings = require('postcss-easings');
const postcssInputStyle = require('postcss-input-style');
const postcssQuantityQueries = require('postcss-quantity-queries');
const postcssResponsiveType = require('postcss-responsive-type');
const postcssAlias = require('postcss-alias');

module.exports = {
  plugins: [
    postcssAlias,
    postcssResponsiveType,
    postcssQuantityQueries,
    postcssInputStyle,
    postcssClearfix,
    postcssFontPath,
    postcssHexrgba,
    postcssEasings,
    rucksack(),
    autoprefixer({
      browsers: ['last 2 versions', 'Firefox ESR', '> 1%', 'ie >= 8', 'iOS >= 8', 'Android >= 4'],
    }),
  ],
};
