require('babel-register');
require('babel-polyfill');
require('css-modules-require-hook')({
  generateScopedName: '[local]___[hash:base64:5]',
});
require('./server');
