const q = 'tasks';
const open = require('amqplib').connect('amqp://guest:guest@192.168.225.15:5672');

export default () => {
  open.then(function (conn) {
    return conn.createChannel();
  }).then(function (ch) {
    ch.assertQueue(q).then(function (ok) {
      return ch.consume(q, function (msg) {
        if (msg !== null) {
          console.log("msg", msg);
          ch.ack(msg);
        }
      });
    });
  }).catch(console.warn);
};
