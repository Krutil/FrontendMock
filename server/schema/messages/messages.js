
import {
  GraphQLList,
  GraphQLInt,
  GraphQLString,
  GraphQLNonNull,
} from 'graphql';


import Mock from 'mockjs';

import { conversationType, messageInputType } from './messagesTypes';

if (typeof localStorage === 'undefined' || localStorage === null) {
  const LocalStorage = require('node-localstorage').LocalStorage;
  var localStorage = new LocalStorage('./localstorage');
  localStorage.setItem('messages', JSON.stringify([{
    conversationId: 'a7sfa',
    participants: [1, 2],
    messages: [{
      messageId: 'a7sfa-1',
      messageBody: 'Ahoj Petře',
      senderId: 1,
      createdAt: '2017-03-20 07:09:15',
      viewedAt: '2017-03-20 07:19:15',
    },
    {
      messageId: 'a7sfa-2',
      messageBody: 'Ahoj Honzo',
      senderId: 2,
      createdAt: '2017-03-20 07:09:15',
      viewedAt: undefined,
    }],
  }]));
}


export const MessagesQueries = {
  messages: {
    type: new GraphQLList(conversationType),
    resolve: () => {
      const messages = localStorage.getItem('messages');
      return JSON.parse(messages);
    },
  },
};

export const MessagesMutations = {
  postMessage: {
    type: new GraphQLList(conversationType),
    args: {
      newMessage: {
        type: new GraphQLNonNull(messageInputType),
      },
      conversationId: {
        type: GraphQLString,
      },
      participants: {
        type: new GraphQLNonNull(new GraphQLList(GraphQLInt)),
      },
    },
    resolve: (root, { newMessage, conversationId, participants }) => {
      const messages = JSON.parse(localStorage.getItem('messages'));

      if (conversationId === undefined) {
        const oConversation = {
          conversationId: Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/),
          participants,
          messages: [],
        };
        newMessage.messageId = `${oConversation.conversationId}-${Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/)}`;
        newMessage.createdAt = Mock.mock('@now');

        oConversation.messages.push(newMessage);
        messages.push(oConversation);
      } else {
        messages.map((oConversation) => {
          if (oConversation.conversationId === conversationId) {
            newMessage.messageId = `${conversationId}-${Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/)}`;
            newMessage.createdAt = Mock.mock('@now');
            oConversation.messages.push(newMessage);
            return oConversation;
          }
          return oConversation;
        },
      );
      }

      localStorage.setItem('messages', JSON.stringify(messages));
      console.log("messages", JSON.stringify(messages));
      return messages;
    },
  },
  deleteMessage: {
    type: new GraphQLList(conversationType),
    args: {
      messageId: {
        type: new GraphQLNonNull(GraphQLString),
      },
      conversationId: {
        type: new GraphQLNonNull(GraphQLString),
      },
    },
    resolve: (root, { messageId, conversationId }) => {
      const messages = JSON.parse(localStorage.getItem('messages'));

      messages.map((oConversation) => {
        if (oConversation.conversationId === conversationId) {
          oConversation.messages = oConversation.messages.filter(oMessage =>
            oMessage.messageId != messageId
          );
          return oConversation;
        }
        return oConversation;
      });

      localStorage.setItem('messages', JSON.stringify(messages));
      console.log("messages", messages)
      return messages;
    },
  },
  messageViewed: {
    type: new GraphQLList(conversationType),
    args: {
      messageId: {
        type: new GraphQLNonNull(GraphQLString),
      },
      conversationId: {
        type: new GraphQLNonNull(GraphQLString),
      },
    },
    resolve: (root, { messageId, conversationId }) => {
      const messages = JSON.parse(localStorage.getItem('messages'));

      messages.map((oConversation) => {
        if (oConversation.conversationId === conversationId) {
          oConversation.messages = oConversation.messages.filter(oMessage =>
            oMessage.messageId != messageId
          );
          return oConversation;
        }
        return oConversation;
      });

      localStorage.setItem('messages', JSON.stringify(messages));

      return messages;
    },
  },

};
