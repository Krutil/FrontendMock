import {
  GraphQLObjectType,
  GraphQLInputObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLList,
} from 'graphql';


export const messageType = new GraphQLObjectType({
  name: 'Message',
  fields: () => ({
    messageId: {
      type: GraphQLString,
    },
    messageBody: {
      type: GraphQLString,
    },
    senderId: {
      type: GraphQLString,
    },
    createdAt: {
      type: GraphQLString,
    },
    viewedAt: {
      type: GraphQLString,
    },
  }),
});

export const conversationType = new GraphQLObjectType({
  name: 'Conversation',
  fields: () => ({
    conversationId: {
      type: GraphQLString,
    },
    participants: {
      type: new GraphQLList(GraphQLInt),
    },
    messages: {
      type: new GraphQLList(messageType),
    },
  }),
});

export const messageInputType = new GraphQLInputObjectType({
  name: 'MessageInputType',
  fields: () => ({
    messageBody: {
      type: GraphQLString,
    },
    senderId: {
      type: GraphQLInt,
    },
  }),
});
