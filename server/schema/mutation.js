import {
  GraphQLObjectType,
} from 'graphql';

import { MessagesMutations } from './messages/messages';

export default new GraphQLObjectType({
  name: 'Mutation',
  fields: MessagesMutations,
});
