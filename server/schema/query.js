import {
  GraphQLObjectType,
} from 'graphql';

import { MessagesQueries } from './messages/messages';

export default new GraphQLObjectType({
  name: 'Query',
  fields: MessagesQueries,
});
