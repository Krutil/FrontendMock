import express from 'express';
import graphqlHTTP from 'express-graphql';
import { join } from 'path';
import compression from 'compression';
import schema from './schema';
// import initRabbitMQChannel from './lib/rabbitMQ';
const server = express();
const dev = process.env.NODE_ENV === 'development';

const serverPort = 3000;

// const ch = initRabbitMQChannel();

server.use(compression());

server.use('/graphql', graphqlHTTP({
  schema,
  graphiql: dev,
}));

if (!dev) {
  server.use(express.static(join(__dirname, '/../dist/'), { maxAge: '200d' }));
}

server.listen(serverPort, () => {
  console.info(`\n\n${!dev && `Express started at ${serverPort}`}\n`);
});
