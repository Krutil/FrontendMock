import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { connect } from 'dva';
import { browserHistory, Link } from 'dva/router';
import { Menu, Icon, Popover, Dropdown, Badge } from 'antd';
import styles from './header.less';
import SidebarMenu from '../Sidebar/components/SidebarMenu/sidebarMenu';
import Avatar from '../../common/users/Avatar/avatar';
import { uniqBy as helper_uniqBy } from '../../../utils/helper';

class Header extends React.Component {

  constructor(props) {
    super(props);
    this.approvalCounter = 0;
  }

  componentDidUpdate() {
    this.approvalCounter = 0;
  }

  handleMenuRightClick = (e) => {
    if (e.key === 'logout') { this.props.logout(); }
  };

  handleMenuLeftClick = (e) => {
    if (e.key === 'calendar') { browserHistory.push('/calendar'); }
    if (e.key === 'approval') { browserHistory.push('/approval'); }
    if (e.key === 'trash') { browserHistory.push('/trash'); }
    if (e.key === 'inbox') { browserHistory.push('/messages'); }
  };

  handleVisibleChange = () => {
    this.props.dispatch({
      type: 'app/notificationChecked',
    });
  };

  // handleMessageChecked = () => {
  //   this.props.dispatch({
  //     type: 'app/messagesChecked',
  //   });
  // };

  approvalCheck = (item) => {
    if (item.status === 'inProgress' && item.user.id === this.props.app.user.id) {
      this.approvalCounter += 1;
    } else if (item.status === 'approval' && Number(item.assigned_user) === this.props.app.user.id) {
      this.approvalCounter += 1;
    } else if (item.status === 'approval' && !Number(item.assigned_user) && this.props.app.user.role === 'administrator') {
      this.approvalCounter += 1;
    }
  };

  render() {
    const {
      app,
      assets,
      changeOpenKeys,
      isHamburgerMenu,
      location,
      menuPopoverVisible,
      navOpenKeys,
      notifications,
      requirements,
      risks,
      AppSidebarLeftFold,
      switchMenuPopover,
      user,
    } = this.props;

    const menusProps = {
      changeOpenKeys,
      handleClickNavMenu: switchMenuPopover,
      isHamburgerMenu,
      location,
      navOpenKeys,
      AppSidebarLeftFold,
    };

    let notificationsCounter = 0;
    let deletedCounter = 0;
    let messagesCounter = 0;
    const selectedKeys = [];
    const dropdownItems = [];

    if (location.pathname === '/notifications') { selectedKeys.push('notifications'); }
    if (location.pathname === '/calendar') { selectedKeys.push('calendar'); }
    if (location.pathname === '/approval') { selectedKeys.push('approval'); }
    if (location.pathname === '/trash') { selectedKeys.push('trash'); }

    requirements && requirements.list.forEach(function (item) {
      this.approvalCheck(item);
    }, this);

    assets && assets.list.forEach(function (item) {
      this.approvalCheck(item);
    }, this);

    risks && risks.list.forEach(function (item) {
      this.approvalCheck(item);
    }, this);

    helper_uniqBy(notifications.filter(oNotification => oNotification.activity_type !== 'mention'), item => item.item_type_key)
      .concat(notifications.filter(oNotification => oNotification.activity_type === 'mention'))
      .sort((itemA, itemB) => itemB.created_at > itemA.created_at)
      .filter((oNotification, i) => i < 6)
      .forEach((oNotification) => {
        const {
          id,
          item_type_key,
          item_title,
          item_type,
          category_notification,
          activity_type,
          assigned_to,
        } = oNotification;

        if (category_notification === 'assign' && Number(assigned_to) === app.user.id) {
          dropdownItems.push(
            <Menu.Item className="is-multiline" key={item_type_key}>
              <Link to={`/risk/${item_type}/item/${item_type_key}`}>
                Byl Vám přidělen záznam <strong>{item_title}</strong>.
              </Link>
            </Menu.Item>,
          );

          if (oNotification.created_at > app.user.date_check_notification) {
            notificationsCounter += 1;
          }
        } else if (category_notification === 'approval' && app.user.role === 'administrator') {

          if (oNotification.created_at > app.user.date_check_notification) {
            notificationsCounter += 1;
          }

          dropdownItems.push(
            <Menu.Item className="is-multiline" key={item_type_key}>
              <Link to={`/risk/${item_type}/create/${item_type_key}?approval`}>
                <strong>{item_title}</strong> čeká na schválení.
              </Link>
            </Menu.Item>,
          );
        }
        // else if (category_notification === 'messages') {
        //
        //   if (oNotification.created_at > app.user.date_check_messages
        //         &&
        //       `@${app.user.username}` == oNotification.assigned_to) {
        //     messagesCounter += 1;
        //   }
        // }
        else if (category_notification === 'activity') {
          let text;

          if (activity_type === 'create' && item_type === 'risks') {
            text = `Bylo vytvořeno riziko <strong>${item_title}</strong>.`;
          } else if (activity_type === 'create' && item_type === 'assets') {
            text = `Bylo vytvořeno aktivum <strong>${item_title}</strong>.`;
          } else if (activity_type === 'deleted' && item_type === 'risks') {
            if (oNotification.created_at > app.user.date_check_trash) {
              deletedCounter++;
            }
            text = `Bylo odstraněno riziko <strong>${item_title}</strong>.`;
          } else if (activity_type === 'deleted' && item_type === 'assets') {
            if (oNotification.created_at > app.user.date_check_trash) {
              deletedCounter++;
            }
            text = `Bylo odstraněno aktivum <strong>${item_title}</strong>.`;
          } else if (activity_type === 'deleted' && item_type === 'requirements') {
            if (oNotification.updated_at > app.user.date_check_trash) {
              deletedCounter++;
            }
            text = `Byl odstraněn požadavek <strong>${item_title}</strong>.`;
          } else if (activity_type === 'create' && item_type === 'assets') {
            text = `Bylo vytvořeno aktivum <strong>${item_title}</strong>.`;
          } else if (activity_type === 'mention'
            && item_type === 'requirements'
            && assigned_to === `@${app.user.username}`) {
            text = `Byl jste zmíněn v komentáři u požadavku <strong>${item_title}</strong>.`;
          } else if (activity_type === 'mention'
            && item_type === 'assets'
            && assigned_to === `@${app.user.username}`) {
            text = `Byl jste zmíněn v komentáři u aktiva <strong>${item_title}</strong>.`;
          } else if (activity_type === 'mention'
            && item_type === 'risks'
            && assigned_to === `@${app.user.username}`) {
            text = `Byl jste zmíněn v komentáři u rizika <strong>${item_title}</strong>.`;
          }

          if (text) {
            dropdownItems.push(
              <Menu.Item className="is-multiline" key={id + item_type_key}>
                <Link to={`/risk/${item_type}/item/${item_type_key}`}>
                  <span dangerouslySetInnerHTML={{__html: text}}/>
                </Link>
              </Menu.Item>,
            );

            if (oNotification.created_at > app.user.date_check_notification) {
              notificationsCounter += 1;
            }
          }
        }
      });

    dropdownItems.push(
      <Menu.Item className={`${styles.notificationDropdownLastRow}`} key="more">
        <Link to={'/notifications'}>
          Zobrazit vše
        </Link>
      </Menu.Item>,
    );

    const notificationDropdown = (
      <Menu style={{ width: '320px' }}>
        { dropdownItems }
      </Menu>
    );

    return (
      <div className={styles.header}>
        {
          isHamburgerMenu &&
          <Popover
            placement="bottomLeft" onVisibleChange={switchMenuPopover}
            visible={menuPopoverVisible} overlayClassName={styles.popovermenu}
            trigger="click" content={<SidebarMenu {...menusProps} />}
          >
            <div className={styles.header_menuButton}>
              <Icon type="bars" /><span>&nbsp;Menu</span>
            </div>
          </Popover>
        }

        <Menu
          id="headerMenuLeft"
          className="is-colored"
          mode="horizontal"
          selectedKeys={selectedKeys}
          onClick={this.handleMenuLeftClick}
        >
          <Menu.Item key="notifications">
            <Dropdown
              getPopupContainer={() => document.getElementById('headerMenuLeft')}
              overlay={notificationDropdown}
              trigger={['click']}
              onVisibleChange={this.handleVisibleChange}
            >
              <div><Badge count={notificationsCounter}><Icon type="bell" /></Badge><FormattedMessage
                id="common.notification" defaultMessage="Notification"
              /></div>
            </Dropdown>
          </Menu.Item>
          <Menu.Item key="calendar">
            <a><Icon type="calendar" />Kalendář</a>
          </Menu.Item>
          <Menu.Item key="approval">
            <a><Badge count={this.approvalCounter}><Icon type="check-circle-o" /></Badge><FormattedMessage
              id="common.approval" defaultMessage="approval"
            /></a>
          </Menu.Item>
          <Menu.Item key="inbox">
            <a><Badge count={messagesCounter}><Icon type="mail" /></Badge><FormattedMessage
              id="common.messages"
              defaultMessage="Messages"
            /></a>
          </Menu.Item>
          <Menu.Item key="trash">
            <a><Badge count={deletedCounter}><Icon type="delete" /></Badge><FormattedMessage
              id="common.trash" defaultMessage="trash"
            /></a>
          </Menu.Item>
        </Menu>

        <Menu
          className={styles.header_MenuRight} mode="horizontal"
          onClick={this.handleMenuRightClick}
        >
          <Menu.SubMenu title={<span><Avatar className="ui_avatar" size="s" src={user.avatar} />{user.firstname} {user.lastname}</span>}>
            <Menu.Item key="logout">
              <FormattedMessage id="common.logout" defaultMessage="Logout" />
            </Menu.Item>
          </Menu.SubMenu>
        </Menu>
      </div>
    );
  }
}

Header.propTypes = {
  changeOpenKeys: PropTypes.func,
  dispatch: PropTypes.func,
  isHamburgerMenu: PropTypes.bool,
  location: PropTypes.object,
  logout: PropTypes.func,
  menuPopoverVisible: PropTypes.bool,
  navOpenKeys: PropTypes.array,
  AppSidebarLeftFold: PropTypes.bool,
  switchMenuPopover: PropTypes.func,
  user: PropTypes.object,
  app: PropTypes.object,
  comments: PropTypes.object,
  files: PropTypes.object,
  notifications: PropTypes.array,
  relationships: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    app: state.app,
    comments: state.comments,
    files: state.files,
    notifications: state.notification,
    loadingModelActivity: state.loading.models.activity,
    loadingModelAssets: state.loading.models.assets,
    loadingModelComments: state.loading.models.comments,
    loadingModelFiles: state.loading.models.files,
    loadingModelRelationships: state.loading.models.relationships,
    loadingModelRequirements: state.loading.models.requirements,
    loadingModelRisks: state.loading.models.risks,
    loadingModelSpectators: state.loading.models.spectators,
    relationships: state.relationships,
    requirements: state.requirements,
    assets: state.assets,
    risks: state.risks,
  };
}

export default connect(mapStateToProps)(Header);
