import React from 'react';
import PropTypes from 'prop-types';
import { intlShape, injectIntl, defineMessages } from 'react-intl';
import { Button, Row, Form, Input, Spin } from 'antd';
import { config } from '../../../utils/index';
import styles from './login.less';

const FormItem = Form.Item;

const login = (
  {
    intl,
    loading,
    loginButtonLoading,
    onOk,
    form: {
      getFieldDecorator,
      validateFieldsAndScroll,
    },
  }) => {
  function handleOk() {
    validateFieldsAndScroll((errors, values) => {
      if (errors) {
        return;
      }
      onOk(values);
    });
  }

  /* i18n */
  const messages = defineMessages({
    username: {
      id: 'common.username',
      defaultMessage: 'Username',
    },
    password: {
      id: 'common.password',
      defaultMessage: 'Password',
    },
    login: {
      id: 'common.login',
      defaultMessage: 'Login',
    },
  });

  const { formatMessage } = intl;

  return (
    <div className={styles.form}>
      <Spin spinning={loading} size="large">
        <div className={styles.logo}>
          <img src={config.logoSrc} role="presentation" />
        </div>
        <form>
          <FormItem hasFeedback>
            {getFieldDecorator('username', {
              rules: [
                {
                  required: true,
                  message: 'Please enter your username',
                },
              ],
            })(<Input onPressEnter={handleOk} placeholder={formatMessage(messages.username)} />)}
          </FormItem>
          <FormItem hasFeedback>
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: 'Please enter your password',
                },
              ],
            })(<Input
              type="password" onPressEnter={handleOk}
              placeholder={formatMessage(messages.password)}
            />)}
          </FormItem>
          <Row>
            <Button
              className={styles.form_button} type="primary" size="large" onClick={handleOk}
              loading={loginButtonLoading} style={{ textTransform: 'uppercase' }}
            >
              {formatMessage(messages.login)}
            </Button>
          </Row>
        </form>
      </Spin>
    </div>
  );
};

login.propTypes = {
  form: PropTypes.object,
  intl: intlShape.isRequired,
  loading: PropTypes.bool,
  loginButtonLoading: PropTypes.bool,
  onOk: PropTypes.func,
};

/* https://github.com/yahoo/react-intl/blob/master/examples/translations/src/client/components/locales-menu.js*/
export default injectIntl(Form.create()(login));
