import React from 'react';
import PropTypes from 'prop-types';
import { Menu, Icon } from 'antd';
import { Link } from 'dva/router';
import { menu } from '../../../../../utils/index';

const topMenu = menu.map(item => item.key);

const getMenu = function (
  menuArray,
  AppSidebarLeftFold,
  parentPath,
) {
  parentPath = parentPath || '/';

  return menuArray.map((item) => {
    if (item.child) {
      return (
        <Menu.SubMenu
          key={item.key} title={<span>{item.icon ?
            <Icon
              type={item.icon}
            /> : ''}{AppSidebarLeftFold && topMenu.indexOf(item.key) >= 0 ? '' : item.name}</span>}
        >
          {getMenu(item.child, AppSidebarLeftFold, `${parentPath + item.key}/`)}
        </Menu.SubMenu>
      );
    }

    return (
      <Menu.Item key={item.key}>
        <Link to={parentPath + item.path}>
          {item.icon ? <Icon type={item.icon} /> : ''}
          {AppSidebarLeftFold && topMenu.indexOf(item.key) >= 0 ? '' : item.name}
        </Link>
      </Menu.Item>
    );
  });
};

function SidebarMenu(
  {
    AppSidebarLeftFold,
    location,
    isHamburgerMenu,
    handleClickNavMenu,
    navOpenKeys,
    changeOpenKeys,
  },
) {
  const menuItems = getMenu(menu, AppSidebarLeftFold);

  const onOpenChange = (openKeys) => {
    const latestOpenKey = openKeys.find(key => !(navOpenKeys.indexOf(key) > -1));
    const latestCloseKey = navOpenKeys.find(key => !(openKeys.indexOf(key) > -1));
    let nextOpenKeys = [];

    if (latestOpenKey) {
      nextOpenKeys = getAncestorKeys(latestOpenKey).concat(latestOpenKey);
    }

    if (latestCloseKey) {
      nextOpenKeys = getAncestorKeys(latestCloseKey);
    }

    changeOpenKeys(nextOpenKeys);
  };

  const getAncestorKeys = (key) => {
    const map = {
      navigation2: ['navigation'],
    };
    return map[key] || [];
  };

  // 菜单栏收起时，不能操作openKeys
  const menuProps = !AppSidebarLeftFold ? {
    onOpenChange,
    openKeys: navOpenKeys,
  } : {};

  return (
    <Menu
      {...menuProps}
      mode={AppSidebarLeftFold ? 'vertical' : 'inline'}
      theme="light"
      onClick={handleClickNavMenu}
      defaultSelectedKeys={[location.pathname.split('/')[location.pathname.split('/').length - 1] || 'dashboard']}
    >
      {menuItems}
    </Menu>
  );
}

SidebarMenu.propTypes = {
  AppSidebarLeftFold: PropTypes.bool,
  location: PropTypes.object,
  isHamburgerMenu: PropTypes.bool,
  handleClickNavMenu: PropTypes.func,
  navOpenKeys: PropTypes.array,
  changeOpenKeys: PropTypes.func,
};

export default SidebarMenu;
