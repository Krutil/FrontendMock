import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'antd';
import styles from './sidebar.less';
import SidebarMenu from './components/SidebarMenu/sidebarMenu';

function Sidebar({
    changeOpenKeys,
    isHamburgerMenu,
    location,
    navOpenKeys,
    AppSidebarLeftFold,
    switchSidebar,
  }) {
  const menusProps = {
    changeOpenKeys,
    location,
    navOpenKeys,
    AppSidebarLeftFold,
  };

  return (
    <div>
      {
        !isHamburgerMenu &&
        <div className={styles.menuButton} onClick={switchSidebar} role="button">
          <Icon type={AppSidebarLeftFold ? 'menu-unfold' : 'menu-fold'} />
        </div>
      }
      <SidebarMenu {...menusProps} />
    </div>
  );
}

Sidebar.propTypes = {
  changeOpenKeys: PropTypes.func,
  isHamburgerMenu: PropTypes.bool,
  location: PropTypes.object,
  navOpenKeys: PropTypes.array,
  AppSidebarLeftFold: PropTypes.bool,
  switchSidebar: PropTypes.func,
};

export default Sidebar;
