import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Form, Button, Menu, Dropdown, Collapse, Upload, Table, Icon } from 'antd';
const FormItem = Form.Item;
import styles from './fileUpload.less';

class FileUpload extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      submitDisabled: true,
    };

    this.customPanelStyle = {
      border: 0,
    };
  }

  componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }

  handleChange = (info) => {
    const { listKey, listRowKey, currentItem } = this.props;
    const status = info.file.status;
    if (status !== 'uploading') {
      // console.log(info.file, info.fileList);
    }

    if (status === 'done') {
      // console.log('done', info);
      this.props.cbCreate(info, listKey, listRowKey, currentItem);
    } else if (status === 'error') {
      // console.log('error');
      // message.error(`${info.file.name} file upload failed.`);
    }
  };

  handleMenuClick = commentItem => (e) => {
    const { currentItem } = this.props;
    console.log("this.props", currentItem);
    if (e.key === 'destroy') {
      this.props.cbDestroy(commentItem.id, currentItem);
    }
  };

  handleVisibleChange = (flag) => {
    // console.log('handleVisibleChange flag', flag);
  };

  render() {
    const { dataTable, create, appUser, listKey, listRowKey } = this.props;

    const ButtonProps = {
      type: 'transparent',
      size: 'small',
      shape: 'circle',
      icon: 'ellipsis',
    };

    const tableColumns = [
      {
        className: 'u_noWrap u_widthFull',
        title: 'Název',
        dataIndex: 'title',
        key: 'title',
        render: (text, record) => <a href={`#fileId-${record.key}`}>{text}</a>,
      },
      {
        className: 'u_noWrap',
        title: 'Přiřadil',
        dataIndex: 'user',
        key: 'user',
        render: user => <span>{user.lastname} {user.firstname}</span>,
      },
      {
        className: 'u_noWrap',
        title: 'Vloženo',
        dataIndex: 'created_at',
        key: 'created_at',
      },
      {
        className: 'u_noWrap',
        title: '',
        key: 'action',
        render: (text, record) => {
          const menu = (
            <Menu onClick={this.handleMenuClick(record)}>
              <Menu.Item key="destroy">Odstranit</Menu.Item>
            </Menu>
          );

          return (
            <Dropdown
              overlay={menu} placement="bottomRight"
              onVisibleChange={this.handleVisibleChange} trigger={['click']}
            >
              <Button {...ButtonProps} />
            </Dropdown>
          );
        },
      },
    ];

    const props = {
      name: 'files',
      multiple: false,
      showUploadList: false,
      action: '/api/files/store',
      data: {
        type: 'requirements',
        type_key: 'yS8uV4',
        type_key_node: 'djygggwv',
        title: 'Nazev souboru 2',
        user: 'John Doe',
      },
      headers: { 'Access-Control-Allow-Origin': '*' },
      listType: 'text',
      onChange: this.handleChange,
    };

    const fileContent = (
      <Row className={styles.comment} type="flex" justify="start">
        <Form style={{ margin: 0, width: '100%' }}>
          <FormItem style={{ margin: 0, width: '100%' }}>
            <div className={styles.dropbox}>
              <Upload.Dragger {...props}>
                <p className="ant-upload-drag-icon">
                  <Icon type="inbox" />
                </p>
                <p className="ant-upload-text">Nahrát soubor</p>
                <p className="ant-upload-hint" style={{ lineHeight: '1.25' }}>Klikněte pro výběr
                  souboru, nebo přetáhněte soubor do této oblasti.</p>
              </Upload.Dragger>
            </div>
          </FormItem>
        </Form>
      </Row>
    );

    return (
      <div className={`${styles.upload} ${this.props.className}`}>
        {
          dataTable &&
          <Table
            columns={tableColumns}
            dataSource={dataTable}
            size="middle"
            pagination={false}
            scroll={{ x: 500 }}
          />
        }
        {
          create && appUser &&
          <Collapse bordered={false}>
            <Collapse.Panel
              header={<span style={{ fontSize: '90%', fontWeight: '700', paddingLeft: '.5em' }}>Přidat soubor</span>}
              key="1" style={this.customPanelStyle}
            >
              <div className="contentDark">
                { fileContent }
              </div>
            </Collapse.Panel>
          </Collapse>
        }
      </div>
    );
  }
}

FileUpload.propTypes = {
  dataTable: PropTypes.array,
  cbCreate: PropTypes.func,
  cbDestroy: PropTypes.func,
};

export default Form.create()(FileUpload);
