import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'dva/router';
import {
  Icon,
  Table,
} from 'antd';
import moment from 'moment';

import { childrenToArray } from '../../../utils/helper';

function FolderTable({ currentItem, dataType }) {
  const dataSource = childrenToArray(currentItem.children);

  if (dataSource) {
    dataSource.forEach((item) => {
      if (item.children) {
        item.children = childrenToArray(item.children);
      }
    });
  }

  const locale = {
    emptyText: 'Seznam je prázdný',
  };

  const columns = [
    {
      title: 'Typ',
      dataIndex: 'type',
      key: 'type',
      className: 'u_noWrap',
      render: text => (
        <span>
          {
            text === 'folder' &&
            <Icon type="folder" />
          }
          {
            text === 'item' &&
            <Icon type="file" />
          }
        </span>
      ),
    }, {
      title: 'Název',
      dataIndex: 'title',
      key: 'title',
      render: (text, record) => (
        <Link to={`/risk/${dataType}/item/${record.key}`}>
          <strong>{record.code ? `${record.code} ` : ''}{text}</strong>
        </Link>
      ),
    }, {
      title: 'Účinnost od',
      dataIndex: 'date_from',
      key: 'date_from',
      render: text => <span>{text ? moment(text).format('L') : '—'}</span>,
    }, {
      title: 'Vytvořeno',
      dataIndex: 'created_at',
      key: 'created_at',
      render: text => <span>{text ? moment(text).format('L') : '—'}</span>,
    }, {
      title: 'Upraveno dne',
      dataIndex: 'updated_at',
      key: 'updated_at',
      render: text => <span>{text ? moment(text).format('L') : '—'}</span>,
    },
  ];

  const getBodyWrapper = body => body;

  return (
    <Table
      locale={locale}
      scroll={{ x: '100%' }}
      columns={columns}
      dataSource={dataSource}
      pagination={false}
      simple
      rowKey={record => record.key}
      getBodyWrapper={getBodyWrapper}
    />
  );
}

FolderTable.propTypes = {
  currentItem: PropTypes.object,
  dataType: PropTypes.string,
};

export default FolderTable;
