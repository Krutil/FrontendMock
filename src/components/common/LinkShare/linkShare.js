import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Checkbox,
  DatePicker,
  Form,
  Input,
  message,
  Select,
} from 'antd';

const validToProps = {
  rules: [{
    required: true,
    message: 'Vyberte datum.',
  }],
};

class LinkShare extends React.Component {

  state = {
    submitActive: false,
    validUnlimited: true,
  };

  handleFormSubmit = (e) => {
    e.preventDefault();

    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      const {
        dataUsers,
      } = this.props;
      const validTo = fieldsValue.validTo;
      const assigned_user = dataUsers.filter(item => (
        item.id === Number(fieldsValue.assigned_user)),
      );
      const dataObject = {
        ...fieldsValue,
        validTo: validTo ? validTo.format('YYYY-MM-DD') : null,
        assigned_user: assigned_user[0],
      };

      this.props.cbCreate(dataObject);
      this.props.form.resetFields();

      this.setState({
        submitActive: false,
      });
    });
  };

  toggleValidUnlimited = () => {
    this.setState({
      validUnlimited: !this.state.validUnlimited,
    });
  };

  checkUser = (value) => {
    if (value) {
      this.setState({
        submitActive: true,
      });
    }
  };

  handleLinkInputClick = () => {
    const { location } = this.props;

    setTimeout(function () {
      const textField = document.createElement('textarea');
      textField.style.position = 'absolute';
      textField.style.bottom = 0;
      textField.style.left = 0;
      textField.style.opacity = 0;
      textField.innerText = `http://localhost:8000${location.pathname}`;
      document.body.appendChild(textField);
      textField.select();
      document.execCommand('copy');
      textField.remove();
      message.success('Odkaz byl zkopírován do schránky.', 3);
    }, 500);
  };

  render() {
    const {
      currentItem,
      dataUsers,
      form,
      location,
    } = this.props;
    const {
      submitActive,
      validUnlimited,
    } = this.state;

    const optionsItems = [];

    dataUsers.forEach(function (item) {
      if (dataUsers.length > 0) {
        optionsItems.push(
          <Select.Option
            key={item.id.toString()}
            value={item.id.toString()}
          >
            { `${item.firstname} ${item.lastname}` }
          </Select.Option>,
        );
      }
    });

    return (
      <div className="content">
        <Form
          layout="vertical"
          onSubmit={this.handleFormSubmit}
        >
          <Form.Item label="URL:">
            {
              form.getFieldDecorator('url', {
                initialValue: `http://localhost:8000${location.pathname}`,
              })(
                <Input
                  readOnly
                  size="default"
                  onClick={this.handleLinkInputClick}
                />,
              )
            }
            {
              form.getFieldDecorator('unlimited', {
                initialValue: true,
              })(
                <Checkbox
                  size="default"
                  checked={validUnlimited}
                  onChange={this.toggleValidUnlimited}
                >
                  Odkaz má neomezenou platnost
                </Checkbox>,
              )
            }
          </Form.Item>
          {
            !validUnlimited &&
            <Form.Item label="Platnost do:">
              {
                form.getFieldDecorator('validTo', validToProps)(
                  <DatePicker
                    format="YYYY-MM-DD"
                    style={{ width: '100%' }}
                  />,
                )
              }
            </Form.Item>
          }
          <Form.Item label="Odeslat uživateli:">
            {
              form.getFieldDecorator('assigned_user')(
                <Select
                  placeholder="Vyberte uživatele"
                  className="spaceS"
                  onChange={this.checkUser}
                >
                  {optionsItems}
                </Select>,
              )
            }
            {
              currentItem.private > 0 &&
              <p className="note">
                Vybranému uživateli bude pro tento záznam přiděleno "právo číst" a obdrží notifikaci za pomoci emailu.
              </p>
            }
          </Form.Item>
          {
            submitActive &&
            <Button
              type="primary"
              htmlType="submit"
              size="large"
            >
              Potvrdit
            </Button>
          }
        </Form>
      </div>
    );
  }
}

LinkShare.propTypes = {
  cbCreate: PropTypes.func,
  currentItem: PropTypes.object,
  dataUsers: PropTypes.array,
  form: PropTypes.object,
};

export default Form.create()(LinkShare);
