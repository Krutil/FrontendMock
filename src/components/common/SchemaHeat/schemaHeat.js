import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'antd';
import classnames from 'classnames';
import styles from './schemaHeat.less';

const colors = {
  Null: '#ddd',
  Low: '#dbe58c',
  Minor: '#aecc6c',
  Moderate: '#f2ae6b',
  Significant: '#e67951',
  High: '#a92500',
};

class SchemaHeat extends React.Component {

  state = {
    data: this.props.data,
    buttonDisabled: true,
  };

  handleClick = (indexRow, index) => () => {
    if (this.props.editable) {
      const { data } = this.state;
      data[indexRow].selected = index;

      this.setState({
        data,
        buttonDisabled: false,
      });
    }
  };

  handleSubmit = (e) => {
    e.preventDefault();

    if (this.props.type === 'add') {
      // console.log('cbAdd');
      this.props.cbAdd(this.state.data);
    }
  };

  render() {
    const { data, buttonDisabled } = this.state;
    let scoreSteps = 0;
    let scoreAll = 0;

    data.forEach(function (item) {
      if (item.selected !== null) {
        const position = item.selected + 1;
        const positionValue = item.value / item.steps.length;
        scoreAll += item.value;
        scoreSteps += position * positionValue;
      }
    });

    return (
      <div style={this.props.style}>
        <div className={styles.schema}>
          {
            this.props.type === 'risk' &&
            <h3 style={{ paddingBottom: '1.5em', textDecoration: 'underline' }}>Aktuální stav</h3>
          }
          <table className={styles.schema_content}>
            {
              data.map((item, indexRow) =>
                (<tr key={`row_${indexRow}`}>
                  <td className={styles.schema_labels}><strong>{ item.title }</strong></td>
                  <td
                    className={classnames(styles.schema_steps, { [styles.schema_stepsDisabled]: !this.props.editable })}
                  >
                    {
                      item.steps.map(function (step, index) {
                        return (
                          <div
                            key={`cell_${indexRow}${index}`}
                            className={classnames(styles.schema_step, { [styles.schema_stepSelected]: index === item.selected })}
                            style={{ backgroundColor: colors[step] }}
                            onClick={this.handleClick(indexRow, index)}
                          >
                            { step }
                          </div>
                        );
                      }, this)
                    }
                  </td>
                </tr>),
              )
            }
            {
              scoreSteps > 0 &&
              <tr>
                <td>
                  <strong>Celkové skóre:</strong>
                </td>
                <td style={{ padding: '2em 0' }}>
                  <strong>{ parseInt(scoreSteps / scoreAll * 100) }% důležitost</strong>
                </td>
              </tr>
            }
            {
              !buttonDisabled && this.props.editable &&
              <tr>
                <td>
                  <Button
                    type="primary"
                    htmlType="submit"
                    size="large"
                    style={{ margin: '1em 0' }}
                    onClick={this.handleSubmit}
                  >{this.props.type === 'add' ? 'Přiřadit' : 'Uložit změny'}</Button>
                </td>
              </tr>
            }
          </table>
        </div>
        {
          this.props.type === 'risk' &&
          <div className={styles.schema}>
            {
              this.props.type === 'risk' &&
              <h3 style={{ padding: '1.5em 0', textDecoration: 'underline' }}>Cíl</h3>
            }
            <table className={styles.schema_content}>
              {
                data.map((item, indexRow) =>
                  (<tr key={`row_${indexRow}`}>
                    <td className={styles.schema_labels}><strong>{ item.title }</strong></td>
                    <td
                      className={classnames(styles.schema_steps, { [styles.schema_stepsDisabled]: !this.props.editable })}
                    >
                      {
                        item.steps.map(function (step, index) {
                          return (
                            <div
                              key={`cell_${indexRow}${index}`}
                              className={classnames(styles.schema_step, { [styles.schema_stepSelected]: index === item.selected })}
                              style={{ backgroundColor: colors[step] }}
                              onClick={this.handleClick(indexRow, index)}
                            >
                              { step }
                            </div>
                          );
                        }, this)
                      }
                    </td>
                  </tr>),
                )
              }
              {
                scoreSteps > 0 &&
                <tr>
                  <td>
                    <strong>Celkové skóre:</strong>
                  </td>
                  <td style={{ padding: '2em 0' }}>
                    <strong>{ parseInt(scoreSteps / scoreAll * 100) }% důležitost</strong>
                  </td>
                </tr>
              }
              {
                !buttonDisabled && this.props.editable &&
                <tr>
                  <td>
                    <Button
                      type="primary"
                      htmlType="submit"
                      size="large"
                      style={{ margin: '1em 0' }}
                      onClick={this.handleSubmit}
                    >{this.props.type === 'add' ? 'Přiřadit' : 'Uložit změny'}</Button>
                  </td>
                </tr>
              }
            </table>
          </div>
        }
      </div>
    );
  }
}

SchemaHeat.propTypes = {
  data: PropTypes.array,
};

export default SchemaHeat;
