import React from 'react';
import PropTypes from 'prop-types';
import { Table, Input, Icon, Button, Popconfirm, Dropdown, Menu } from 'antd';
import styles from './tableEditable.less';

class EditableCell extends React.Component {

  state = {
    value: this.props.value,
    editable: false,
  };

  handleChange = (e) => {
    const value = e.target.value;
    this.setState({ value });
  };

  check = () => {
    this.setState({ editable: false });
    if (this.props.onChange) {
      this.props.onChange(this.state.value);
    }
  };

  edit = () => {
    this.setState({ editable: true });
  };

  render() {
    const { value, editable } = this.state;
    return (
      <div className={styles.editable_cell}>
        {
          editable ?
            <div className={styles.editable_cell_input_wrapper}>
              <Input
                size="small"
                value={value}
                onChange={this.handleChange}
                onPressEnter={this.check}
              />
              <Icon
                type="check"
                className={styles.editable_cell_icon_check}
                onClick={this.check}
              />
            </div>
            :
            <div className={styles.editable_cell_text_wrapper}>
              {value || ' '}
              {
                this.props.editable &&
                <Icon
                  type="edit"
                  className={styles.editable_cell_icon}
                  onClick={this.edit}
                />
              }
            </div>
        }
      </div>
    );
  }
}

class TableEditable extends React.Component {

  constructor(props) {
    super(props);

    this.columns = [];
    this.rowsObject = {};

    this.props.dataColumns.forEach(function (item) {
      item.render = (text, record, index) => (
        <EditableCell
          editable={this.props.editable}
          value={text}
          onChange={this.onCellChange(index, item.dataIndex)}
        />
      );
      this.columns.push(item);
      this.rowsObject[item.dataIndex] = '-';
    }, this);

    if (this.props.editable) {
      this.columns.push({
        fixed: 'right',
        title: '',
        width: 85,
        render: (text, record, index) => (
          <Popconfirm title="Odstranit řádek?" onConfirm={() => this.onDelete(index)}>
            <a href="#"><Icon type="close" /></a>
          </Popconfirm>
        ),
      });
    }

    this.state = {
      data: this.props.data,
      dataSource: this.props.data || [],
      count: this.props.data.length,
    };
  }

  render() {
    const { data } = this.state;
    // let scoreSteps = 0;
    // let scoreAll = 0;
    //
    // data && data.forEach(function (item) {
    //   if (item.selected !== null) {
    //     const position = item.selected + 1;
    //     const positionValue = item.value / item.steps.length;
    //     scoreAll += item.value;
    //     scoreSteps += position * positionValue;
    //   }
    // });

    return (
      <div style={this.props.style}>
        <Table
          scroll={{ x: 1000 }}
          columns={this.columns}
          dataSource={this.state.dataSource}
          size="middle"
          style={{ maxWidth: '1000px', margin: '0 auto 1.5em' }}
          rowKey={record => record.id}
          pagination={false}
        />
        <div style={{ maxWidth: '1000px', margin: '0 auto 1.5em' }}>Finanční hodnota celkem: <strong>1
          651 500 Kč</strong></div>
        {
          this.props.editable &&
          <div style={{ maxWidth: '1000px', margin: '0 auto 1.5em' }}>
            <Button type="ghost" className="editable-add-btn" onClick={this.handleAdd}>Nový
              řádek</Button>
            <Button
              style={{ marginLeft: '1em' }} type="primary" className="editable-add-btn"
              onClick={this.handleSubmit}
            >Přiřadit</Button>
          </div>
        }
      </div>
    );
  }

  handleClick = (indexRow, index) => () => {
    if (this.props.editable) {
      const { data } = this.state;
      data[indexRow].selected = index;

      this.setState({
        data,
        buttonDisabled: false,
      });
    }
  };

  handleAdd = () => {
    const { count, dataSource } = this.state;
    const newRow = this.rowsObject;
    newRow.key = count;

    this.setState({
      dataSource: [...dataSource, newRow],
      count: count + 1,
    });
  };

  onCellChange = (index, key) => (value) => {
    const dataSource = [...this.state.dataSource];
    dataSource[index][key] = value;
    this.setState({ dataSource });
  };

  onDelete = (index) => {
    const dataSource = [...this.state.dataSource];
    dataSource.splice(index, 1);
    this.setState({ dataSource });
  };

  handleSubmit = (e) => {
    e.preventDefault();

    if (this.props.type === 'add') {
      // console.log('cbAdd');
      this.props.cbAdd({
        dataColumns: this.props.dataColumns,
        dataSource: this.state.dataSource,
      });
    }
  };
}

TableEditable.propTypes = {
  data: PropTypes.array,
  dataColumns: PropTypes.array,
};

export default TableEditable;
