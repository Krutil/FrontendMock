import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'dva/router';
import {
  Tree,
  Row,
  Col,
  Input,
  Icon,
} from 'antd';
import styles from './treeList.less';

import { formatDataToTree as helperFormatDataToTree } from '../../../utils/helper';

class TreeList extends React.Component {

  state = {
    dataTree: [],
    selectedKeys: [],
    expandedKeys: [],
    searchValue: '',
  };

  componentWillReceiveProps(props) {
    this.formatDataToTree(props.dataList);
  }

  onSearchChange = (e) => {
    const value = e.target.value;

    if (value.length > 2) {
      const expandedKeys = [];
      const data = [...this.props.dataList];

      data.forEach((item) => {
        if (item.title.search(value) !== -1) {
          if (item.key_parents) {
            const keysParents = item.key_parents.split('.');

            keysParents.forEach(function (key) {
              expandedKeys.push(key);
            });
          }

          if (item.children) {
            expandedKeys.push(item.key);
          }
        }
      });

      this.setState({
        expandedKeys,
        searchValue: value,
      });
    } else {
      this.setState({
        expandedKeys: [],
        searchValue: '',
      });
    }
  };

  onExpand = (expandedKeys) => {
    this.setState({
      expandedKeys,
    });
  };

  onDrop = (info) => {
    const dropKey = info.node.props.eventKey;
    const dragKey = info.dragNode.props.eventKey;
    const dataList = [...this.props.dataList];
    const cache = {};

    dataList.forEach((item, index) => {
      if (item.key === dragKey || item.key === dropKey) {
        item.index = index;
        cache[item.key] = item;
      }
    });

    const dragObj = cache[dragKey];
    const dropObj = cache[dropKey];

    if (info.dropToGap) {
      dragObj.key_parents = dropObj.key_parents;
    } else if (dropObj.key_parents) {
      dragObj.key_parents = `${dropObj.key_parents}.${dropObj.key}`;
    } else {
      dragObj.key_parents = dropObj.key;
    }

    this.props.dispatch({
      type: `${this.props.dataType}/update`,
      payload: dragObj,
    });
  };

  goTo = path => () => {
    browserHistory.push(path);
  };

  formatDataToTree(data) {
    const map = helperFormatDataToTree(data);

    this.setState({
      dataTree: map,
    });
  }

  render() {
    const {
      app,
      currentItem,
      dataType,
      dispatch,
    } = this.props;
    const {
      dataTree,
      expandedKeys,
      searchValue,
    } = this.state;
    const selectedKeys = [];

    if (
      currentItem &&
      currentItem.key
    ) {
      if (currentItem.key_parents) {
        const keysParents = currentItem.key_parents.split('.');

        keysParents.forEach(function (key) {
          expandedKeys.push(key);
        });
      }

      if (currentItem.children) {
        expandedKeys.push(currentItem.key);
      }

      selectedKeys.push(currentItem.key);
    }

    const loop = data => Object.keys(data).map((item) => {
      const dataItem = data[item];
      const index = dataItem.title.search(searchValue);
      const beforeStr = dataItem.title.substr(0, index);
      const afterStr = dataItem.title.substr(index + searchValue.length);
      const title = index > -1 ? (
        <span>
          {beforeStr}
          <strong
            style={{
              color: '#66a800',
              textDecoration: 'underline',
              backgroundColor: '#fff799',
            }}
          >
            {searchValue}
          </strong>
          {afterStr}
        </span>
      ) : <span>{dataItem.title}</span>;

      if (dataItem.children) {
        return (
          <Tree.TreeNode
            className={dataItem.type === 'folder' ? styles.ui_tree_folder : styles.ui_tree_file}
            key={dataItem.key}
            title={
              <span
                className="ui_tree_title"
                onClick={this.goTo(`/risk/${dataType}/item/${dataItem.key}`)}
              >
                {
                  dataItem.type === 'folder' ?
                    <Icon type="folder" /> :
                    <Icon type="file-text" />
                }
                {title}
              </span>
            }
          >
            {loop(dataItem.children)}
          </Tree.TreeNode>
        );
      }
      return (
        <Tree.TreeNode
          className={dataItem.type === 'folder' ? styles.ui_tree_folder : styles.ui_tree_file}
          key={dataItem.key}
          title={
            <span
              className="ui_tree_title"
              onClick={this.goTo(`/risk/${dataType}/item/${dataItem.key}`)}
            >
              {
                dataItem.type === 'folder' ?
                  <Icon type="folder" /> : <Icon type="file-text" />
              }
              {title}
            </span>
          }
        />
      );
    });

    return (
      <div className={styles.tree}>
        <Row className={styles.tree_search} type="flex">
          <Col className="flex1">
            <Input.Search
              placeholder="Search"
              onChange={this.onSearchChange}
            />
          </Col>
          <Col
            className={styles.btnFoldToggle}
            onClick={() => dispatch({ type: 'app/switchTreeUnfold' })}
          >
            <Icon type={app.AppPanelTreeUnfold ? 'menu-fold' : 'menu-unfold'} />
          </Col>
        </Row>
        {
          this.state.dataTree !== null &&
          <Row className="flex1 u_overflowAuto" type="flex">
            <Tree
              className="draggable-tree"
              onExpand={this.onExpand}
              selectedKeys={selectedKeys}
              expandedKeys={expandedKeys}
              draggable
              onDrop={this.onDrop}
            >
              {loop(dataTree)}
            </Tree>
          </Row>
        }
      </div>
    );
  }
}

TreeList.propTypes = {
  app: PropTypes.object,
  currentItem: PropTypes.object,
  dataList: PropTypes.array,
  dataType: PropTypes.string,
  dispatch: PropTypes.func,
};

export default TreeList;
