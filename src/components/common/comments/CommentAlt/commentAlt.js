// @flow
import React from 'react';
import { connect } from 'dva';
import moment from 'moment';
import 'moment/locale/cs';
import {
  Row,
  Col,
  Button,
  Menu,
  Dropdown,
} from 'antd';

import CommentCreate from '../CommentCreate/commentCreate';
import CommentAltChildren from './commentAlt';
import Avatar from '../../users/Avatar/avatar';

import styles from './commentAlt.less';
import type { CommentProps } from '../commentsTypes';

function CommentAlt(
  {
    appUser,
    cbReply,
    child,
    comment,
    handleMenuClick,
    mentionItems,
    replyTo,
    typeCreate,
    users,
  }: CommentProps) {
  const words = comment.text.split(' ');

  const wordsFormatted = words.map((word) => {
    if (word.indexOf('@') !== -1) {
      let user = users.list.filter(item => `@${item.username}` === word)[0];
      return `<a href="/profile/${user.username}" title="${user.firstname} ${user.lastname}">${word}</a>`;
    } else if (word.indexOf('#') !== -1) {
      return `<a>${word}</a>`;
    }
    return word;
  });
  const textFormatted = wordsFormatted.join(' ');

  const menu = (
    <Menu onClick={handleMenuClick(comment)}>
      {!child && typeCreate && <Menu.Item key="reply">Odpovědět</Menu.Item>}
      {comment.user.id === appUser.id && <Menu.Item key="destroy">Odstranit</Menu.Item>}
    </Menu>
  );

  const ButtonProps = {
    type: 'transparent',
    size: 'small',
    shape: 'circle',
    icon: 'ellipsis',
  };

  const childrenCommentProps = {
    appUser,
    cbReply,
    child: true,
    handleMenuClick,
    mentionItems,
    replyTo,
    users,
  };

  const commentCreateProps = {
    users,
    appUser,
    cbAction: cbReply,
    mentionItems,
    reply: true,
    currentComment: comment,
  };

  return (
    <div>
      <Row
        className={styles.comment}
        type="flex" justify="start"
      >
        <Col className={styles.comment_avatar}>
          <Avatar size={child ? 's' : null} src={comment.user.avatar} />
        </Col>
        <Col className={styles.comment_content}>
          <h5 className={styles.comment_title}>{comment.user.firstname} {comment.user.lastname}</h5>
          <p dangerouslySetInnerHTML={{ __html: textFormatted }} />
          <div className={styles.comment_date}>
            {moment(comment.created_at).calendar()}
          </div>
          {
            (comment.user.id === appUser.id || !child) &&
            <div className={styles.comment_settings}>
              <Dropdown
                overlay={menu}
                placement="bottomRight"
                trigger={['click']}
              >
                <Button {...ButtonProps} />
              </Dropdown>
            </div>
          }
        </Col>
      </Row>
      {
        comment.childrenComments
        && comment.childrenComments.length > 0
        && comment.childrenComments.map(childrenComment =>
          <CommentAltChildren
            key={`childrenComment_${childrenComment.id}`}
            comment={childrenComment}
            {...childrenCommentProps}
          />,
        )
      }
      {
        replyTo === comment.id
        &&
        typeCreate
        &&
        <CommentCreate {...commentCreateProps} />
      }
    </div>
  );
};

function mapStateToProps(state) {
  return {
    users: state.users,
  };
}

export default connect(mapStateToProps)(CommentAlt);
