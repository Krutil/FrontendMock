// @flow
import React, { Component } from 'react';
import {
  connect,
} from 'dva';
import {
  Row,
  Col,
  Mention,
  Form,
  Button,
} from 'antd';

import Mock from 'mockjs';
import styles from './commentCreate.less';

import type {
  CommentCreateProps,
  CommentCreateState,
} from '../commentsTypes';

class CommentCreate extends Component {

  constructor(props) {
    super(props);

    this.state = {
      createText: '',
      fieldName: `mention-${Mock.mock(/[a-z][A-Z][0-9][a-z][A-Z][0-9]/)}`,
      mentionUserNames: props.users.list.map(user => (
        {
          name: `${user.lastname} ${user.firstname}`,
          value: user.username,
        }),
      ),
      submitDisabled: true,
      suggestions: [],
    };
  }

  state: CommentCreateState;
  props: CommentCreateProps;

  onSearchChange = (value, trigger) => {
    const searchValue = value.toLowerCase();
    const dataSource = trigger === '@' ? this.state.mentionUserNames : this.props.mentionItems;
    const filtered = dataSource.filter(item =>
      item.name.toLowerCase().indexOf(searchValue) !== -1,
    );
    const suggestions = filtered.map(suggestion => (
      <Mention.Nav value={suggestion.value} data={suggestion}>
        <span>{suggestion.name} {suggestion.type &&
        <span style={{ color: '#bbb' }}>({suggestion.type})</span>}</span>
      </Mention.Nav>
    ));

    this.setState({ suggestions });
  };

  handleSubmit = () => {
    const { currentComment, reply, listKey, listRowKey, form } = this.props;
    const { createText } = this.state;
    form.validateFields((errors) => {
      if (errors) {
        return;
      }

      if (reply) {
        form.setFieldsValue({ [this.state.fieldName]: Mention.toContentState('')});
        this.setState({
          submitDisabled: true,
        });
        this.props.cbAction(createText, currentComment);
      } else {
        form.setFieldsValue({ [this.state.fieldName]: Mention.toContentState('')});
        this.setState({
          submitDisabled: true,
        });
        this.props.cbAction(createText, listKey, listRowKey);
      }
    });
  };

  handleTextChange = (editorState) => {
    this.setState({
      createText: Mention.toString(editorState),
      submitDisabled: !Mention.toString(editorState).length,
    });
  };

  render() {
    const {
      appUser,
      reply,
    } = this.props;

    const {
      suggestions,
      submitDisabled,
    } = this.state;

    const MentionProps = {
      multiLines: true,
      prefix: ['@', '#'],
      suggestions,
      notFoundContent: 'Nenalezeno',
      onChange: this.handleTextChange,
      onSearchChange: this.onSearchChange,
    };

    const ButtonProps = {
      type: 'primary',
      size: 'small',
      htmlType: 'submit',
      disabled: submitDisabled,
      onClick: this.handleSubmit,
    };

    return (
      <Row
        className={styles.comment}
        style={{ backgroundColor: '#fff' }}
        type="flex"
        justify="start"
      >
        <Col
          className={styles.comment_avatar}
          style={{ backgroundImage: `url(${appUser.avatar})` }}
        />
        <Col className={styles.comment_content}>
          <Form layout="horizontal">
            <Mention
              {...MentionProps}
              style={{ height: 90 }}
            />
            <Button
              {...ButtonProps}
              style={{ float: 'right', marginTop: '.75em' }}
            >
              {reply ? 'Odpovědět' : 'Odeslat'}
            </Button>
          </Form>
        </Col>
      </Row>
    );
  }
}

const formDecorator = Form.create()(CommentCreate);

export default connect()(formDecorator);
