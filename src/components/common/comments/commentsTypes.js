// @flow

// TODO: unify with AppUserType

export type UserType = {
  avatar: string,
  firstname: string,
  id: number,
  lastname: string
}

// TODO: unify UserType, AppUserType and user types to global flow type

export type CommentType = {
  category: string,
  category_notification: string,
  childrenComments: Array<CommentType>,
  created_at: string,
  id: string,
  text: string,
  type_key: string,
  type_key_node: string,
  user: UserType,
  parentComment: CommentType
}

type SuggestionType = {
  name: string,
  value: string,
}

export type AppUserType = {
  id: number,
  username: string,
  password: string,
  firstname: string,
  lastname: string,
  department: string,
  phone: string,
  email: string,
  date_check_notification: string,
  date_check_approval: string,
  date_check_messages: string,
  date_check_trash: string,
  avatar: string,
  role: string,
  date_last_login: string,
}

export type ItemCommentsAltState = {
  replyTo: string,
}

export type ItemCommentsAltProps = {
  appUser: AppUserType,
  cbCreate: Function,
  cbDestroy: Function,
  cbReply: Function,
  className: string,
  data: Array<CommentType>,
  listKey: string,
  listRowKey: string,
  mentionItems: Array<SuggestionType>,
  typeCreate: boolean,
  users: Array<AppUserType>,
};

// TODO take away list in queries

export type CommentCreateProps = {
  users: {
    list: Array<AppUserType>,
  },
  appUser: AppUserType,
  cbAction: Function,
  currentComment: CommentType,
  react: boolean,
  form: {
    setFieldsValue: Function,
    getFieldDecorator: Function,
    validateFields: Function,
  },
  listKey: string,
  listRowKey: string,
}


export type CommentCreateState = {
  suggestions: Array<SuggestionType>,
  initValue: Object,
  createText: string,
  submitDisabled: boolean,
  isReply: string,
  fieldName: string,
  mentionUserNames: Array<SuggestionType>,
}

export type CommentProps = {
  appUser: AppUserType,
  cbReply: Function,
  child: boolean,
  comment: CommentType,
  handleMenuClick: Function,
  mentionItems: Array<SuggestionType>,
  replyTo: string,
  typeCreate: boolean,
  users: Array<AppUserType>,
}
