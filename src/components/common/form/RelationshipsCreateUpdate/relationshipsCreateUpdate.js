import React from 'react';
import PropTypes from 'prop-types';
import {
  Form,
  Select,
  Button,
} from 'antd';

import styles from './relationshipsCreateUpdate.less';

class RelationshipsCreateUpdate extends React.Component {

  constructor(props) {
    super(props);
    const selectedType = Object.keys(this.props.dataSelect)[0];

    this.state = {
      relationshipsListType: selectedType,
      relationshipsListItems: this.props.dataSelect ? this.props.dataSelect[selectedType] : [],
      relationshipsSelectedValues: [],
      submitDisabled: true,
    };
  }

  onTypeChange = (value) => {
    this.setState({
      relationshipsListType: value,
      relationshipsListItems: this.props.dataSelect[value],
    });
  };

  onTypeItemSelect = (value) => {
    this.setState({
      relationshipsSelectedValues: value,
    });
  };

  handleMenuClick = item => (e) => {
    if (e.key === 'destroy') {
      this.props.cbDestroy(item.id);
    }
  };

  handleCreateSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((errors, values) => {
      if (errors) {
        return;
      }

      const { currentItem, dataSelect } = this.props;
      const dataRelationshipsSelectedValues = this.state.relationshipsSelectedValues;

      dataRelationshipsSelectedValues.forEach((itemSelected) => {
        const oData = dataSelect[this.state.relationshipsListType].find(item => item.key === itemSelected.key)
        const keys = {
          from_key: currentItem.key,
          to_key: oData.key,
        };

        this.props.cbCreate(oData, keys);
      });

      this.props.form.resetFields();

      this.setState({
        relationshipsSelectedValues: [],
        submitDisabled: true,
      });
    });
  };

  render() {
    const { dataSelect } = this.props;
    const dataKeys = {};
    const translation = {
      requirements: 'Požadavky',
      assets: 'Aktiva',
      risks: 'Rizika',
      actions: 'Opatření',
    };

    const options = Object.keys(dataSelect).map((dataSetName, index) => (
      <Select.Option key={index} value={dataSetName}>
        {translation[dataSetName]}
      </Select.Option>
    ));

    const optionsItems = [];
    this.state.relationshipsListItems.forEach(function (item, index) {
      if (!dataKeys[item.key]) {
        optionsItems.push(
          <Select.Option key={index} value={item.key}>
            {item.title}
          </Select.Option>,
        );
      }
    });

    const selectTypeProps = {
      defaultValue: Object.keys(dataSelect)[0],
      onChange: this.onTypeChange,
    };

    const selectItemsProps = {
      mode: 'multiple',
      labelInValue: true,
      value: this.state.relationshipsSelectedValues,
      placeholder: 'Vyberte jednu nebo více vazeb',
      onChange: this.onTypeItemSelect,
    };

    const buttonSubmitProps = {
      type: 'primary',
      onClick: this.handleCreateSubmit,
    };

    return (
      <div className={styles.relationships}>
        <Form layout="vertical" style={{ maxWidth: '600px', margin: '0 auto 1em' }}>
          <Form.Item label="Typ vazby" hasFeedback>
            <Select {...selectTypeProps}>
              {options}
            </Select>
          </Form.Item>
          <Form.Item label="Vybrané vazby" hasFeedback>
            <Select {...selectItemsProps}>
              {optionsItems}
            </Select>
          </Form.Item>
          <Button {...buttonSubmitProps}>
            Přiřadit
          </Button>
        </Form>
      </div>
    );
  }
}

RelationshipsCreateUpdate.propTypes = {
  dataSelect: PropTypes.object,
  currentItem: PropTypes.object,
  cbCreate: PropTypes.func,
  cbDestroy: PropTypes.func,
};

export default Form.create()(RelationshipsCreateUpdate);
