import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Row, Col, Timeline } from 'antd';
import moment from 'moment';
import 'moment/locale/cs';

import styles from './itemActivity.less';

moment.locale('cs');

class itemActivity extends React.Component {

  render() {
    const {
      activity,
      assets,
      comments,
      currentItem,
      files,
      relationships,
      requirements,
      risks,
    } = this.props;

    const notificationsRows = [];

    activity && activity.list.forEach((item) => {
      if (item.assigned_to === `#${currentItem.key}` || item.item_type_key === currentItem.key) {
        item.category_notification = 'activity';
        notificationsRows.push(item);
      }
    });

    relationships && relationships.list.forEach((item) => {
      if (item.from_key === currentItem.key || item.to_key === currentItem.key) {
        item.category_notification = 'relationships';
        notificationsRows.push(item);
      }
    });

    comments && comments.list.forEach((item) => {
      if (item.type_key === currentItem.key /* || item.type_key_node === currentItem.key*/) {
        item.category_notification = 'comments';
        notificationsRows.push(item);
      }
    });

    files && files.list.forEach((item) => {
      if (item.type_key === currentItem.key /* || item.type_key_node === currentItem.key*/) {
        item.category_notification = 'files';
        notificationsRows.push(item);
      }
    });

    notificationsRows.sort((a, b) => {
      let keyA = new Date(a.created_at),
        keyB = new Date(b.created_at);
      if (keyA < keyB) return 1;
      if (keyA > keyB) return -1;
      return 0;
    });

    const dataGroup = {};

    if (notificationsRows.length > 0) {
      notificationsRows.forEach((item) => {
        const date = item.created_at.substring(0, 10);
        dataGroup[date] = dataGroup[date] || [];
        dataGroup[date].push(item);
      });
    }

    const groups = Object.keys(dataGroup).map(function (key, key_index) {
      const notificationsRows = dataGroup[key].map((item, index) => {
        console.log('item', item);

        let text;

        if (item.activity_type === 'create') {
          text = `<strong>${item.user.lastname} ${item.user.firstname}</strong> vytvořil záznam.`;
        }

        if (item.activity_type === 'update') {
          text = `<strong>${item.user.lastname} ${item.user.firstname}</strong> provedl úpravu záznamu.`;
        }

        if (item.activity_type === 'approval') {
          text = `<strong>${item.user.lastname} ${item.user.firstname}</strong> předal záznam ke schválení.`;
        }

        if (item.activity_type === 'accepted') {
          text = `<strong>${item.user.lastname} ${item.user.firstname}</strong> schválil záznam.`;
        }

        if (item.category_notification === 'files') {
          text = `<strong>${item.user.lastname} ${item.user.firstname}</strong> nahrál soubor.`;
        }

        if (item.category_notification === 'relationships' && (item.type_key === currentItem.key /* || item.key === currentItem.key*/)) {
          text = `<strong>${item.user.lastname} ${item.user.firstname}</strong> vytvořil vazbu s <strong>${item.title}</strong>`;
        }

        if (item.category_notification === 'relationships' && item.key === currentItem.key) {
          text = `<strong>${item.user.lastname} ${item.user.firstname}</strong> propojil <strong>${item.title}</strong> s `;
        }

        if (item.category_notification === 'comments' && item.type_key === currentItem.key) {
          text = `<strong>${item.user.lastname} ${item.user.firstname}</strong> napsal komentář.`;
        }

        if (item.category_notification === 'activity' && item.activity_type === 'mention' && item.item_type === 'requirements') {
          text = `Byl zmíněn v komentáři u požadavku <strong>${item.item_title}</strong>.`;
        }

        if (item.category_notification === 'activity' && item.activity_type === 'mention' && item.item_type === 'assets') {
          text = `Byl zmíněn v komentáři u aktiva <strong>${item.item_title}</strong>.`;
        }

        if (item.category_notification === 'activity' && item.activity_type === 'mention' && item.item_type === 'risks') {
          text = `Byl zmíněn v komentáři u rizika <strong>${item.item_title}</strong>.`;
        }

        return (
          <Timeline.Item key={`row_${key_index}_${index}`}>
            <Row type="flex" justify="space-between" align="top">
              <Col className="flex1">
                <span dangerouslySetInnerHTML={{ __html: text }} />
              </Col>
              <Col className={styles.timeline_date}>
                <span>{moment(item.created_at).format('LT')}</span>
              </Col>
            </Row>
          </Timeline.Item>
        );
      }, this);

      return (
        <div className={styles.containerColumns}>
          <div className="contentSeparator_title"><h5>{moment(key).format('LL')}</h5></div>
          <Timeline>
            <div className="content">
              {notificationsRows}
            </div>
          </Timeline>
        </div>
      );
    }, this);

    return (
      <div>
        { groups }
      </div>
    );
  }

}

itemActivity.propTypes = {
  users: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    app: state.app,
    activity: state.activity,
    loadingModelActivity: state.loading.models.activity,
    requirements: state.requirements,
    loadingModelRequirements: state.loading.models.requirements,
    relationships: state.relationships,
    loadingModelRelationships: state.loading.models.relationships,
    comments: state.comments,
    loadingModelComments: state.loading.models.comments,
    files: state.files,
    loadingModelFiles: state.loading.models.files,
    spectators: state.spectators,
    loadingModelSpectators: state.loading.models.spectators,
    assets: state.assets,
    loadingModelAssets: state.loading.models.assets,
    risks: state.risks,
    loadingModelRisks: state.loading.models.risks,
  };
}

export default connect(mapStateToProps)(itemActivity);