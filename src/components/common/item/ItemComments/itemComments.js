import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'dva/router';
import { Row, Col } from 'antd';
import styles from './itemComments.less';

import moment from 'moment';
import 'moment/locale/cs';
moment.locale('cs');

function itemComments({ data, location }) {
  const dataGroup = {};
  console.log('data', data);

  if (data.length > 0) {
    data.forEach((item) => {
      const date = (item.created_at).substring(0, 10);
      dataGroup[date] = dataGroup[date] || [];
      dataGroup[date].push(item);
    });
  }

  console.log('dataGroup', dataGroup);

  const renderUser = item => (
    <Link className={styles.comment_title} to={`${location.pathname}#${item.type_key_node}`}>
      {item.user.firstname} {item.user.lastname}
    </Link>);

  const renderText = item => (<Link to={`${location.pathname}#${item.type_key_node}`}><p>{item.text}</p></Link>);

  const groups = Object.keys(dataGroup).map(function (key, key_index) {
    const rows = dataGroup[key].map((item, index) => (
      <div
        key={item.key}
        className={styles.comment}
      >
        <Row type="flex" justify="start">
          <Col
            className={styles.comment_avatar}
            style={{ backgroundImage: `url(${item.user.avatar})` }}
          />
          <Col className={styles.comment_content}>
            <Row type="flex" justify="space-between" align="middle">
              <Col className="flex1">
                <h5>
                  {renderUser(item)}
                </h5>
              </Col>
              <Col className={styles.comment_date}>
                {moment(item.created_at).format('LT')}
              </Col>
            </Row>
            {renderText(item)}
          </Col>
        </Row>
      </div>
    ), this);

    return (
      <div key={key} className="spaceM">
        <div className="contentSeparator_title"><h5>{moment(key).format('LL')}</h5></div>
        { rows }
      </div>
    );
  }, this);

  return (
    <div>
      { groups }
    </div>
  );
}

itemComments.propTypes = {
  data: PropTypes.array,
};

export default itemComments;
