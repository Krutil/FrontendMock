// @flow
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';

import CommentCreate from '../../comments/CommentCreate/commentCreate';
import CommentAlt from '../../comments/CommentAlt/commentAlt';
import type {
  ItemCommentsAltState,
  ItemCommentsAltProps,
} from '../../comments/commentsTypes';

import styles from './itemCommentsAlt.less';

class ItemCommentsAlt extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      replyTo: '',
    };

    this.handleMenuClick = this.handleMenuClick.bind(this);
  }

  state: ItemCommentsAltState;
  props: ItemCommentsAltProps;

  handleMenuClick = item => (e) => {
    switch (e.key) {
      case 'destroy': {
        this.props.cbDestroy(item.id);
        break;
      }
      case 'reply': {
        this.setState(
          {
            replyTo: item.id,
          },
        );
        break;
      }
      default: {
        break;
      }
    }
  };
  
  render() {
    const {
      appUser,
      cbCreate,
      cbDestroy,
      cbReply,
      data,
      listKey,
      listRowKey,
      mentionItems,
      typeCreate,
      users,
    } = this.props;

    const { replyTo } = this.state;

    const rows = data
      .filter(item => !item.parentComment)
      .map((item) => {
        item.childrenComments
          =
          data.filter(oComment =>
            oComment.parentComment
            &&
            oComment.parentComment.id === item.id,
          );
        return item;
      })
      .map((item) => {
        const CommentAltProps = {
          appUser,
          cbDestroy,
          cbReply,
          child: false,
          childrenComments: item.childrenComments ? item.childrenComments : [],
          comment: item,
          handleMenuClick: this.handleMenuClick,
          mentionItems,
          replyTo,
          typeCreate,
          users,
        };

        return (
          <CommentAlt
            key={`comment_${item.id}`}
            {...CommentAltProps}
          />
        );
      }, this);

    const CommentCreateProps = {
      users,
      appUser,
      cbAction: cbCreate,
      listRowKey,
      listKey,
      mentionItems,
    };

    return (
      <div className={`${styles.comments} ${this.props.className}`}>
        <div>
          <div className="section_title">
            <h3>Komentáře</h3>
          </div>
        </div>
        {data && rows}
        {
          typeCreate
          &&
          appUser
          &&
          <CommentCreate
            {...CommentCreateProps}
          />
        }
      </div>
    );
  }
}

ItemCommentsAlt.propTypes = {
  appUser: PropTypes.object,
  cbCreate: PropTypes.func,
  cbDestroy: PropTypes.func,
  typeCreate: PropTypes.bool,
  data: PropTypes.array,
  listKey: PropTypes.string,
  listRowKey: PropTypes.string,
};

export default connect()(ItemCommentsAlt);
