import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import moment from 'moment';
import 'moment/locale/cs';

import RelationshipsList from '../../relationships/RelationshipsList/relationshipsList';
import ShrinkStretch from '../../../../components/content/ShrinkStretch/shrinkStretch';
import FileUpload from '../../FileUpload/fileUpload';

moment.locale('cs');

class ItemConnections extends React.Component {

  render() {
    const {
      app,
      currentItem,
      currentNode,
      dataFiles,
      dataRelationships,
      type,
      users,
    } = this.props;
    const dataGroupKeys = {};
    let content;

    if (type === 'requirements') {
      currentItem.data.forEach(function (node) {
        if (dataFiles[node.key] || dataRelationships[node.key]) {
          dataGroupKeys[node.key] = {};
          dataGroupKeys[node.key].data = node.data;
          dataGroupKeys[node.key].files = dataFiles[node.key];
          dataGroupKeys[node.key].relationships = dataRelationships[node.key];
        }
      });

      content = Object.keys(dataGroupKeys).map(function (key) {
        return (
          <div key={key}>
            {
              dataGroupKeys[key].data &&
              <ShrinkStretch>
                <div
                  className="content textFormatted spaceM"
                  dangerouslySetInnerHTML={{ __html: dataGroupKeys[key].data }}
                />
                {
                  dataGroupKeys[key].relationships && dataGroupKeys[key].relationships.length > 0 &&
                  <div className="space">
                    <RelationshipsList
                      className="is-arrow"
                      data={dataGroupKeys[key].relationships || []}
                      listKey={currentItem.key}
                      listRowKey={key}
                      listUsers={users.list}
                    />
                  </div>
                }
                {
                  dataGroupKeys[key].files && dataGroupKeys[key].files.length > 0 &&
                  <div className="space">
                    <FileUpload
                      dataTable={dataGroupKeys[key].files}
                      create
                      appUser={app.user}
                      listKey={currentItem.key}
                      listRowKey={key}
                    />
                  </div>
                }
              </ShrinkStretch>
            }
          </div>
        );
      }, this);
    } else {
      content = (
        <div>
          {
            currentItem &&
            <div>
              {
                dataRelationships && dataRelationships.length > 0 &&
                <div>
                  <div className="contentSeparator_title">
                    <h5>Vazby</h5>
                  </div>
                  <div className="content">
                    <div className="space">
                      <RelationshipsList
                        className="is-arrow"
                        data={dataRelationships || []}
                        listKey={currentItem.key}
                        listRowKey={currentNode}
                        listUsers={users.list}
                      />
                    </div>
                  </div>
                </div>
              }
              {
                dataFiles && dataFiles.length > 0 &&
                <div>
                  <div className="contentSeparator_title">
                    <h5>Soubory</h5>
                  </div>
                  <div className="content">
                    <div className="space">
                      <FileUpload
                        dataTable={dataFiles}
                        appUser={app.user}
                        listKey={currentItem.key}
                        listRowKey={currentNode}
                      />
                    </div>
                  </div>
                </div>
              }
            </div>
          }
        </div>
      );
    }

    return (
      <div>
        {
          dataRelationships.root &&
          <div>
            <div className="contentSeparator_title">
              <h5>Vazby</h5>
            </div>
            <div className="content">
              <div className="space">
                <RelationshipsList
                  className="is-arrow"
                  data={dataRelationships.root || []}
                  listKey={currentItem.key}
                  listRowKey={currentNode}
                  listUsers={users.list}
                />
              </div>
            </div>
          </div>
        }
        {
          content &&
          <div>
            {
              type === 'requirements' &&
              <div className="contentSeparator_title">
                <h5>Vazby v dokumentu</h5>
              </div>
            }
            { content }
          </div>
        }
      </div>
    );
  }

}

ItemConnections.propTypes = {
  users: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    app: state.app,
    activity: state.activity,
    loadingModelActivity: state.loading.models.activity,
    requirements: state.requirements,
    loadingModelRequirements: state.loading.models.requirements,
    relationships: state.relationships,
    loadingModelRelationships: state.loading.models.relationships,
    comments: state.comments,
    loadingModelComments: state.loading.models.comments,
    files: state.files,
    loadingModelFiles: state.loading.models.files,
    spectators: state.spectators,
    loadingModelSpectators: state.loading.models.spectators,
    assets: state.assets,
    loadingModelAssets: state.loading.models.assets,
    risks: state.risks,
    loadingModelRisks: state.loading.models.risks,
    users: state.users,
  };
}

export default connect(mapStateToProps)(ItemConnections);
