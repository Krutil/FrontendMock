import React from 'react';
import PropTypes from 'prop-types';
import {
  Col,
  Row,
  Timeline,
} from 'antd';
import moment from 'moment';
import 'moment/locale/cs';

import styles from './itemHistory.less';

moment.locale('cs');


const translation = {
  title: 'Název',
  number: 'Číslo',
  assigned_user: 'Přiřazený uživatel',
  date_plan: 'Plán pořízení/používání/platnosti aktiva',
  date_real: 'Skutečnost pořízení/používání/platnosti aktiva',
  date_valid: 'Platnost rizika',
  description: 'Popis',
  source: 'Kategorie původu',
  cause: 'Příčina',
  impact: 'Dopad',
};

function historyUpdate(
  {
    data,
  },
) {
  const dataGroup = {};

  if (data.length > 0) {
    data.forEach((itemHistory) => {
      const date = itemHistory.created_at.substring(0, 10);
      dataGroup[date] = dataGroup[date] || [];
      dataGroup[date].push(itemHistory);
    });
  }

  const groups = Object.keys(dataGroup).map(function (key, keyIndex) {
    const historyRows = dataGroup[key].map((item, index) => (
      <Timeline.Item key={`row_${keyIndex}_${index}`}>
        <Row type="flex" justify="space-between" align="top">
          <Col className="flex1">
            <span>{ item.user.lastname } { item.user.firstname }&nbsp;</span>
            {
              item.list.map((row, rowIndex) => {
                if (rowIndex === 0) {
                  return (
                    <span key={`row_${keyIndex}_${index}_${rowIndex}`}>
                      změnil(a) atribut <strong>{translation[row.attribute]}</strong> s hodnotou <strong>{row.value.replace(/<\/?[^>]+(>|$)/g, '')}</strong>
                    </span>
                  );
                }
                return (
                  <span key={`row_${keyIndex}_${index}_${rowIndex}`}>
                    , atribut <strong>{translation[row.attribute]}</strong> s hodnotou <strong>{row.value.replace(/<\/?[^>]+(>|$)/g, '')}</strong>
                  </span>);
              })
            }
            .
          </Col>
          <Col className={styles.timeline_date}>
            <span>{moment(item.created_at).format('LT')}</span>
          </Col>
        </Row>
      </Timeline.Item>
    ), this);

    return (
      <div key={key} className="spaceM">
        <div className="contentSeparator_title">
          <h5>{moment(key).format('LL')}</h5>
        </div>
        <div className="content">
          <Timeline>
            { historyRows }
          </Timeline>
        </div>
      </div>
    );
  }, this);

  return (
    <div>
      { groups }
    </div>
  );
}

historyUpdate.propTypes = {
  data: PropTypes.array,
};

export default historyUpdate;
