import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import 'moment/locale/cs';

import styles from './itemHistoryCompare.less';

moment.locale('cs');

const translation = {
  title: 'Název',
  number: 'Číslo',
  assigned_user: 'Přiřazený uživatel',
  date_plan: 'Plán pořízení/používání/platnosti aktiva',
  date_real: 'Skutečnost pořízení/používání/platnosti aktiva',
  date_valid: 'Platnost rizika',
  description: 'Popis',
  source: 'Kategorie původu',
  cause: 'Příčina',
  impact: 'Dopad',
};

function historyCompare(
  {
    data,
    currentItem,
    indexLeft,
    indexRight,
  }) {
  let itemLeft;
  let itemRight;

  if (data.length > 0) {
    itemLeft = { item: currentItem };
    itemRight = { item: currentItem };

    if (data[indexLeft]) {
      itemLeft = data[indexLeft];
    }

    if (data[indexRight]) {
      itemRight = data[indexRight];
    }
  }

  const allowed = {
    status: true,
    title: true,
    number: true,
    assigned_user: true,
    cause: true,
    impact: true,
    date_plan_from: true,
    date_plan_to: true,
    date_real_from: true,
    date_real_to: true,
  };

  return (
    <div>
      {
        Object.keys(currentItem).map((key) => {
          if (allowed[key] && itemLeft.item[key] !== itemRight.item[key]) {
            return (
              <div>
                <div className="section_title">
                  <h5>{translation[key]}</h5>
                </div>
                <div className={styles.rowNegative}>
                  <span className={styles.operationNegative}>-</span>{ itemLeft.item[key] }
                  {
                    itemLeft.user &&
                    <span className={styles.userNegative}>{itemLeft.user.lastname} {itemLeft.user.firstname}</span>
                  }
                </div>
                <div className={styles.rowPositive}>
                  <span className={styles.operationPositive}>+</span>{ itemRight.item[key] }
                  {
                    itemRight.user &&
                    <span className={styles.userPositive}>{itemRight.user.lastname} {itemRight.user.firstname}</span>
                  }
                </div>
              </div>
            );
          }
          return null;
        })
      }
    </div>
  );
}

historyCompare.propTypes = {
  currentItem: PropTypes.object,
  data: PropTypes.array,
  indexLeft: PropTypes.string,
  indexRight: PropTypes.string,
};

export default historyCompare;
