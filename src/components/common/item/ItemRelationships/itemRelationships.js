import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'dva/router';
import {
  Menu,
  Dropdown,
  Collapse,
  Table,
  Form,
  Select,
  Button,
} from 'antd';
import moment from 'moment';
import 'moment/locale/cs';

import styles from './itemRelationships.less';

const customPanelStyle = {
  border: 0,
};

const category = {
  requirements: {
    text: 'Požadavek',
  },
  assets: {
    text: 'Aktivum',
  },
  risks: {
    text: 'Riziko',
  },
};

class ItemRelationships extends React.Component {

  constructor(props) {
    super(props);

    const selectedType = Object.keys(this.props.dataSelect)[0];

    this.state = {
      relationshipsListType: selectedType,
      relationshipsListItems: this.props.dataSelect ? this.props.dataSelect[selectedType] : [],
      relationshipsSelectedValue: [],
      submitDisabled: true,
    };
  }

  onTypeChange = (value) => {
    this.setState({
      relationshipsListType: value,
      relationshipsListItems: this.props.dataSelect[value],
    });
  };

  onTypeItemSelect = (value) => {
    this.setState({
      relationshipsSelectedValue: value,
    });
  };

  handleMenuClick = item => (e) => {
    if (e.key === 'destroy') {
      this.props.cbDestroy(item.id);
    }
  };

  handleAddSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((errors, values) => {
      if (errors) {
        // console.log('Errors in form!');
        return;
      }

      const { currentItem, currentNode, dataSelect } = this.props;
      const oData = dataSelect[this.state.relationshipsListType]
        .find(item => item.key === this.state.relationshipsSelectedValue.key);

      const keys = {
        from_key: currentItem.key,
        from_key_node: currentNode ? currentNode.key : null,
        to_key: oData.key,
      };

      this.props.cbCreate(oData, keys);
      this.props.form.resetFields();

      this.setState({
        relationshipsSelectedValue: [],
        submitDisabled: true,
      });
    });
  };

  render() {
    const { dataSelect, data, editable } = this.props;

    const tableColumns = [
      {
        className: 'u_minWidth15',
        title: 'Název',
        dataIndex: 'item.title',
        key: 'title',
        render: (text, record) => (
          <Link to={`/risk/${record.item.item_type}/item/${record.item.key}`}>
            <strong>{text}</strong>
          </Link>
        ),
      },
      {
        className: 'u_noWrap',
        title: 'Kategorie',
        dataIndex: 'item.item_type',
        render: text => <span>{category[text] && category[text].text}</span>,
      },
      {
        className: 'u_noWrap',
        title: 'Přiřadil',
        dataIndex: 'user',
        render: user => <span>{user.lastname} {user.firstname}</span>,
      },
      {
        className: 'u_noWrap',
        title: 'Přiřazeno',
        dataIndex: 'created_at',
        key: 'created_at',
        render: text => <span>{text ? moment(text).format('L') : '—'}</span>,
      },
    ];

    if (editable) {
      tableColumns.push({
        fixed: 'right',
        width: 40,
        className: 'u_noWrap',
        title: '',
        key: 'action',
        render: (text, record) => {
          const menu = (
            <Menu onClick={this.handleMenuClick(record)}>
              <Menu.Item key="destroy">Zrušit vazbu</Menu.Item>
            </Menu>
          );

          return (
            <Dropdown
              overlay={menu}
              placement="bottomRight"
              onVisibleChange={this.handleVisibleChange}
              trigger={['click']}
            >
              <Button
                size="small"
                shape="circle"
                icon="ellipsis"
                style={{
                  border: 'none',
                }}
              />
            </Dropdown>
          );
        },
      });
    }

    const dataKeys = {};

    data.forEach(function (item) {
      if (item.category === 'requirements') {
        dataKeys[item.key] = dataKeys[item.key] || true;
      }
    });

    const translation = {
      requirements: 'Požadavky',
      assets: 'Aktiva',
      risks: 'Rizika',
      actions: 'Opatření',
    };

    const options = Object.keys(dataSelect).map((dataSetName, index) => (
      <Select.Option key={index} value={dataSetName}>{translation[dataSetName]}</Select.Option>
    ));

    const optionsItems = [];

    this.state.relationshipsListItems.forEach(function (item) {
      if (!dataKeys[item.key]) {
        optionsItems.push(<Select.Option key={item.key} value={item.key}>{item.title}</Select.Option>);
      }
    });

    return (
      <div className={`${styles.relationships} ${this.props.className}`}>
        nic
      </div>
    );
  }
}

ItemRelationships.propTypes = {
  cbCreate: PropTypes.func,
  cbCreateAlt: PropTypes.func,
  cbDestroy: PropTypes.func,
  currentItem: PropTypes.object,
  currentNode: PropTypes.object,
  data: PropTypes.array,
  dataSelect: PropTypes.object,
  type: PropTypes.string,
};

export default Form.create()(ItemRelationships);
