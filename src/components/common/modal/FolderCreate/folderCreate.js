import React from 'react';
import PropTypes from 'prop-types';
import {
  Form,
  Input,
  Select,
  Modal,
} from 'antd';

const folderCreate = (
  {
    modal,
    data = modal.data || {},
    onOk,
    onCancel,
    form: {
      getFieldDecorator,
      validateFields,
      getFieldsValue,
    },
  },
) => {
  function handleOk() {
    validateFields((errors) => {
      if (errors) {
        return;
      }

      const dataObject = {
        ...getFieldsValue(),
        private: 0,
      };

      onOk(dataObject);
    });
  }

  const modalOpts = {
    title: `${modal.action === 'create' ? 'Vytvořit adresář' : 'Upravit adresář'}`,
    visible: modal.visible,
    onOk: handleOk,
    onCancel,
    wrapClassName: 'vertical-center-modal',
    okText: `${modal.action === 'create' ? 'Vytvořit' : 'Upravit'}`,
    cancelText: 'Zpět',
  };

  const dataTree = data.tree || [];

  return (
    <Modal {...modalOpts}>
      <Form layout="vertical">
        <Form.Item label="Název:">
          {
            getFieldDecorator('title', {
              initialValue: data.title,
              rules: [
                {
                  required: true,
                  message: 'Nutné vyplnit',
                },
              ],
            },
          )(<Input size="default" />)}
        </Form.Item>
        <Form.Item label="Kategorie:">
          {
            getFieldDecorator('key_parents', {
              initialValue: data.key_parents ? data.key_parents : null,
            })(
              <Select
                size="default"
                showSearch
                optionFilterProp="children"
                filterOption={(input, option) => option.props.children
                  .toLowerCase()
                  .indexOf(input.toLowerCase()) >= 0}
              >
                <Select.Option key="null" value={null}>Bez kategorie</Select.Option>
                {
                  dataTree && dataTree.map(item => (
                    <Select.Option
                      key={item.key}
                      value={item.key_parents ? `${item.key_parents}.${item.key}` : item.key}
                    >
                      { item.title }
                    </Select.Option>
                  ))
                }
              </Select>,
            )
          }
        </Form.Item>
      </Form>
    </Modal>
  );
};

folderCreate.propTypes = {
  data: PropTypes.object,
  form: PropTypes.object.isRequired,
  modal: PropTypes.object,
  onCancel: PropTypes.func,
  onOk: PropTypes.func,
};

export default Form.create()(folderCreate);
