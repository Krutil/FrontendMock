// @flow
import React from 'react';
import PropTypes from 'prop-types';
import {
  Modal,
} from 'antd';
import TableSwitch from '../../../common/table/TableSwitch/tableSwitch';
import TableDate from '../../../common/table/TableDate/tableDate';
import type {
  DataExpiration,
  DataSwitch,
  UserItemPermissionProps,
  UserItemPermissionState,
} from './userItemPermissionTypes';

const translation = {
  read: 'Čtení',
  edit: 'Úprava',
  write: 'Zápis',
  expiration: 'Expirace',
};

class UserItemPermission extends React.Component {
  constructor(props: UserItemPermissionProps) {
    super(props);

    const { user } = props.modal.data;
    let dataPermissions = [];

    if (user.permissions) {
      dataPermissions = Object.keys(user.permissions).map((key) => {
        const value = user.permissions[key];

        return {
          label: translation[key],
          active: value,
          type: key,
        };
      });
    }

    this.state = {
      dataExpiration: {
        label: translation.expiration,
        date: user.expiration,
        dateFormat: 'YYYY-MM-DD',
        type: 'expiration',
      },
      dataPermissions,
    };
  }

  state: UserItemPermissionState;
  props: UserItemPermissionProps;

  handleSwitchChange = (record: DataSwitch) => {
    let { dataPermissions } = this.state;

    dataPermissions = dataPermissions.map(item => (
      item.type === record.type ? { ...item, active: !item.active } : item
    ));

    this.setState({
      dataPermissions,
    });
  };

  handleDateChange = (record: DataExpiration) => {
    this.setState({
      dataExpiration: record,
    });
  };

  render() {
    const {
      modal,
      onCancel,
      onOk,
    } = this.props;
    const {
      dataExpiration,
      dataPermissions,
    } = this.state;

    const modalProps = {
      title: `${modal.data.user.firstname} ${modal.data.user.lastname}`,
      visible: modal.visible,
      onOk: () => {
        const modalData = modal.data;
        const permissionsObject = {};
        dataPermissions.forEach((item) => {
          permissionsObject[item.type] = item.active || false;
        });
        modalData.user.permissions = permissionsObject;
        modalData.user.expiration = dataExpiration.date;
        onOk(modalData);
      },
      onCancel,
      wrapClassName: 'vertical-center-modal',
      okText: 'Potvrdit',
      cancelText: 'Zpět',
    };

    const tableSwitchProps = {
      dataSource: dataPermissions,
      handleChange: this.handleSwitchChange,
    };

    const tableDateProps = {
      dataSource: [dataExpiration],
      handleChange: this.handleDateChange,
    };

    return (
      <Modal {...modalProps}>
        <div className="space">
          <TableSwitch {...tableSwitchProps} />
        </div>
        <TableDate {...tableDateProps} />
      </Modal>
    );
  }
}

UserItemPermission.propTypes = {
  modal: PropTypes.object,
  onCancel: PropTypes.func,
  onOk: PropTypes.func,
};

export default UserItemPermission;
