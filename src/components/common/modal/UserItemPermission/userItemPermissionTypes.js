/* @flow */

export type DataExpiration = {
  label: string,
  date: ?string,
  dateFormat: string,
  type: string,
}

export type DataSwitch = {
  label: string,
  active: bool,
  type: string,
}

export type UserItemPermissionState = {
  dataExpiration: DataExpiration,
  dataPermissions: Array<DataSwitch>,
}

export type UserItemPermissionProps = {
  modal: Object,
  onCancel: Function,
  onOk: Function,
}
