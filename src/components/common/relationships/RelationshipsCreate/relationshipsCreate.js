import React from 'react';
import PropTypes from 'prop-types';
import {
  Form,
  Select,
  Button,
} from 'antd';

class RelationshipsCreate extends React.Component {

  constructor(props) {
    super(props);

    const selectedType = Object.keys(this.props.dataSelect)[0];

    this.state = {
      relationshipsListType: selectedType,
      relationshipsListItems: this.props.dataSelect ? this.props.dataSelect[selectedType] : [],
      relationshipsSelectedValue: [],
      submitDisabled: true,
    };
  }

  onTypeChange = (value) => {
    this.setState({
      relationshipsListType: value,
      relationshipsListItems: this.props.dataSelect[value],
    });
  };

  onTypeItemSelect = (value) => {
    this.setState({
      relationshipsSelectedValue: value,
    });
  };

  handleMenuClick = item => (e) => {
    if (e.key === 'destroy') {
      this.props.cbDestroy(item.id);
    }
  };

  handleAddSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((errors, values) => {
      if (errors) {
        // console.log('Errors in form!');
        return;
      }

      const { currentItem, currentNode, dataSelect } = this.props;
      const oData = dataSelect[this.state.relationshipsListType]
        .find(item => item.key === this.state.relationshipsSelectedValue.key);

      const keys = {
        from_key: currentItem.key,
        from_key_node: currentNode ? currentNode.key : null,
        to_key: oData.key,
      };

      this.props.cbCreate(oData, keys);
      this.props.form.resetFields();

      this.setState({
        relationshipsSelectedValue: [],
        submitDisabled: true,
      });
    });
  };

  render() {
    const { dataSelect, data } = this.props;
    const dataKeys = {};

    data.forEach(function (item) {
      if (item.category === 'requirements') {
        dataKeys[item.key] = dataKeys[item.key] || true;
      }
    });

    const translation = {
      requirements: 'Požadavky',
      assets: 'Aktiva',
      risks: 'Rizika',
      actions: 'Opatření',
    };

    const options = Object.keys(dataSelect).map((dataSetName, index) => (
      <Select.Option key={index} value={dataSetName}>{translation[dataSetName]}</Select.Option>
    ));

    const optionsItems = [];

    this.state.relationshipsListItems.forEach(function (item) {
      if (!dataKeys[item.key]) {
        optionsItems.push(
          <Select.Option key={item.key} value={item.key}>
            {item.title}
          </Select.Option>,
        );
      }
    });

    return (
      <Form layout="vertical" style={{ maxWidth: '600px', margin: '0 auto 1em' }}>
        <Form.Item label="Typ vazby" hasFeedback>
          <Select
            defaultValue={Object.keys(dataSelect)[0]}
            onChange={this.onTypeChange}
          >
            {options}
          </Select>
        </Form.Item>
        <Form.Item label="Vybrané vazby" hasFeedback>
          <Select
            type="multiple"
            labelInValue
            value={this.state.relationshipsSelectedValue}
            placeholder="Vyberte jednu nebo více vazeb"
            onChange={this.onTypeItemSelect}
          >
            {optionsItems}
          </Select>
        </Form.Item>
        <Button type="primary" onClick={this.handleAddSubmit}>Přiřadit</Button>
      </Form>
    );
  }
}

RelationshipsCreate.propTypes = {
  cbCreate: PropTypes.func,
  cbCreateAlt: PropTypes.func,
  cbDestroy: PropTypes.func,
  currentItem: PropTypes.object,
  currentNode: PropTypes.object,
  data: PropTypes.array,
  dataSelect: PropTypes.object,
  type: PropTypes.string,
};

export default Form.create()(RelationshipsCreate);
