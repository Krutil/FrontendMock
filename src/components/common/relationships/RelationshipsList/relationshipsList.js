import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Link } from 'dva/router';
import {
  Menu,
  Dropdown,
  Table,
  Form,
  Button,
} from 'antd';
import moment from 'moment';
import 'moment/locale/cs';

import styles from './relationshipsList.less';

moment.locale('cs');

const category = {
  requirements: {
    text: 'Požadavek',
  },
  assets: {
    text: 'Aktivum',
  },
  risks: {
    text: 'Riziko',
  },
};

class RelationshipsList extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      submitDisabled: true,
    };
  }

  handleMenuClick = item => (e) => {
    if (e.key === 'destroy') {
      this.props.cbDestroy(item.id);
    }
  };

  render() {
    const { data, editable } = this.props;

    const tableColumns = [
      {
        className: 'u_minWidth15',
        title: 'Název',
        dataIndex: 'item.title',
        key: 'title',
        render: (text, record) => (
          <Link to={`/risk/${record.item.item_type}/item/${record.item.key}`}>
            <strong>{text}</strong>
          </Link>
        ),
      },
      {
        className: 'u_noWrap',
        title: 'Kategorie',
        dataIndex: 'item.item_type',
        render: text => <span>{category[text] && category[text].text}</span>,
      },
      {
        className: 'u_noWrap',
        title: 'Přiřadil',
        dataIndex: 'user',
        render: user => <span>{user.lastname} {user.firstname}</span>,
      },
      {
        className: 'u_noWrap',
        title: 'Přiřazeno',
        dataIndex: 'created_at',
        key: 'created_at',
        render: text => <span>{text ? moment(text).format('L') : '—'}</span>,
      },
    ];

    if (editable) {
      tableColumns.push({
        fixed: 'right',
        className: 'u_noWrap',
        title: '',
        key: 'action',
        render: (text, record) => {
          const menu = (
            <Menu onClick={this.handleMenuClick(record)}>
              <Menu.Item key="destroy">Zrušit vazbu</Menu.Item>
            </Menu>
          );

          return (
            <Dropdown
              overlay={menu}
              placement="bottomRight"
              onVisibleChange={this.handleVisibleChange}
              trigger={['click']}
            >
              <Button
                size="small"
                shape="circle"
                icon="ellipsis"
                style={{
                  border: 'none',
                }}
              />
            </Dropdown>
          );
        },
      });
    }

    return (
      <div className={`${styles.relationships} ${this.props.className}`}>
        <Table
          columns={tableColumns}
          dataSource={data}
          pagination={false}
          scroll={{ x: '100%' }}
        />
      </div>
    );
  }
}

RelationshipsList.propTypes = {
  cbDestroy: PropTypes.func,
  className: PropTypes.any,
  data: PropTypes.array,
  editable: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    relationships: state.relationships,
  };
}

const decoratorForm = Form.create()(RelationshipsList);

export default connect(mapStateToProps)(decoratorForm);
