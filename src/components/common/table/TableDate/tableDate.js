import React from 'react';
import PropTypes from 'prop-types';
import {
  DatePicker,
  Table,
} from 'antd';
import moment from 'moment';

function tableDate(
  {
    columns,
    dataSource,
    handleChange,
  }) {
  const onDateChange = (value, dateFormat, record) => {
    record.date = dateFormat;
    handleChange(record);
  };

  const columnsDefault = [
    {
      dataIndex: 'label',
      key: 'label',
      render: text => (
        <div>
          {text}
        </div>
      ),
    }, {
      dataIndex: 'date',
      key: 'date',
      render: (date, record) => (
        <DatePicker
          defaultValue={date ? moment(date, record.dateFormat) : null}
          format={record.dateFormat}
          style={{ width: '100%' }}
          onChange={(value, dateFormat) => onDateChange(value, dateFormat, record)}
        />
      ),
    },
  ];

  return (
    <Table
      columns={columns || columnsDefault}
      dataSource={dataSource}
      pagination={false}
      simple
      showHeader={false}
      rowKey={record => record.type}
    />
  );
}

tableDate.propTypes = {
  columns: PropTypes.object,
  dataSource: PropTypes.array.isRequired,
  handleChange: PropTypes.func.isRequired,
};

export default tableDate;
