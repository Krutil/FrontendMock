import React from 'react';
import PropTypes from 'prop-types';
import {
  Switch,
  Table,
} from 'antd';
import styles from './tableSwitch.less';

function tableSwitch(
  {
    columns,
    dataSource,
    handleChange,
  }) {
  const columnsDefault = [
    {
      dataIndex: 'label',
      key: 'label',
      className: `${styles.widthFull}`,
      render: (text, record) => (
        <div
          className="is-pointer"
          onClick={() => handleChange(record)}
        >
          {text}
        </div>
      ),
    }, {
      dataIndex: 'active',
      key: 'active',
      render: (text, record) => (
        <Switch
          checked={record.active}
          onChange={() => handleChange(record)}
        />
      ),
    },
  ];

  return (
    <Table
      columns={columns || columnsDefault}
      dataSource={dataSource}
      pagination={false}
      simple
      showHeader={false}
      rowKey={record => record.type}
    />
  );
}

tableSwitch.propTypes = {
  columns: PropTypes.object,
  dataSource: PropTypes.array,
  handleChange: PropTypes.func,
};

export default tableSwitch;
