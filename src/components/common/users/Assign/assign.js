import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Col,
  Form,
  Icon,
  Popover,
  Row,
  Select,
} from 'antd';

import Avatar from '../Avatar/avatar';
import styles from './assign.less';

class UserAssign extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      currentUser: props.user ? props.user.id : null,
      submitActive: false,
    };
  }

  handleFormChange = (changedFields) => {
    console.log('changedFields', changedFields);
    console.log('this.state.currentUser', this.state.currentUser);
    let submitActive = false;

    if (this.state.currentUser !== Number(changedFields)) {
      submitActive = true;
    }

    this.setState({
      submitActive,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();

    const { form, cbUserAssign } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }

      cbUserAssign(Number(fieldsValue.assigned_user));

      this.setState({
        submitActive: false,
      });

      // Should format date value before submit.
      // const rangeValue = fieldsValue.date_plan;
      // const rangeValue2 = fieldsValue.date_real;
      // const values = {
      //   ...fieldsValue,
      //   date_plan: [rangeValue[0].format('YYYY-MM-DD'), rangeValue[1].format('YYYY-MM-DD')],
      //   date_real: [rangeValue2[0].format('YYYY-MM-DD'), rangeValue2[1].format('YYYY-MM-DD')],
      // };
      //
      // values.category = values.category === 'null' ? null : values.category;
      //
      // // console.log('values', values);
      //
      // let dataObject = {};
      //
      // if (this.props.type === 'update') {
      //   dataObject = this.props.data;
      // } else {
      //   dataObject.status = 'inProgress';
      // }
      //
      // dataObject.type = 'item';
      // dataObject.key_parents = values.category;
      // dataObject.title = values.title;
      // dataObject.number = values.number;
      // dataObject.assigned_user = values.assigned_user;
      // dataObject.date_plan_from = values.date_plan[0];
      // dataObject.date_plan_to = values.date_plan[1];
      // dataObject.date_real_from = values.date_real[0];
      // dataObject.date_real_to = values.date_real[1];
      // dataObject.description = values.description;
      //
      // // console.log('dataObject', dataObject);
      //
      // if (this.props.type === 'create') {
      //   // console.log('create');
      //   this.props.cbCreate(dataObject);
      // }
      //
      // if (this.props.type === 'update') {
      //   // console.log('update');
      //   this.props.cbUpdate(dataObject);
      // }
      //
      // this.setState({
      //   changed: false,
      // });
    });
  };

  render() {

    const {
      authUser,
      form,
      title,
      user,
      userList,
    } = this.props;

    const assignChangeContent = (
      <div>
        <Form
          layout="vertical"
          className="ant-form-sm"
          onSubmit={this.handleSubmit}
        >
          <div id="assignSelect">
            <Form.Item label="Přiřazený uživatel:">
              {
                form.getFieldDecorator('assigned_user', {
                  initialValue: user ? `${user.firstname} ${user.lastname}` : null,
                })(
                  <Select
                    size="small"
                    getPopupContainer={() => document.getElementById('assignSelect')}
                    showSearch
                    optionFilterProp="children"
                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    onChange={this.handleFormChange}
                  >
                    <Select.Option key="null" value={null}>Nepřiřazeno</Select.Option>
                    {
                      userList.map(item => (
                        <Select.Option key={item.id.toString()}>
                          { `${item.firstname} ${item.lastname}` }
                        </Select.Option>
                      ))
                    }
                  </Select>,
                )
              }
            </Form.Item>
          </div>
          {
            this.state.submitActive &&
            <Form.Item>
              <Button type="primary" htmlType="submit" size="small">Uložit</Button>
            </Form.Item>
          }
        </Form>
      </div>
    );

    if (authUser.role === 'administrator') {
      return (
        <Popover
          placement="bottom"
          content={assignChangeContent}
          trigger={['click']}
        >
          <Row type="flex" align="middle" className={`${styles.popover_content} is-pointer`}>
            <Col>
              <Avatar size="m" src={user ? user.avatar : ''} className={styles.avatar}/>
            </Col>
            <Col>
              {
                title &&
                <span className={styles.title}>{title}<br /></span>
              }
              {
                user ?
                  <span>{user.firstname} {user.lastname}</span>
                  :
                  <span>Nepřiřazeno</span>
              }
              <Icon type="down" className={styles.iconDown}/>
            </Col>
          </Row>
        </Popover>
      );
    }

    return (
      <Row type="flex" align="middle" className={styles.popover_content}>
        <Col>
          <Avatar size="m" src={user ? user.avatar : ''} className={styles.avatar}/>
        </Col>
        <Col>
          {
            title &&
            <span className={styles.title}>{title}<br /></span>
          }
          {
            user ?
              <span>{user.firstname} {user.lastname}</span>
              :
              <span>Nepřiřazeno</span>
          }
        </Col>
      </Row>
    );
  }
}

UserAssign.propTypes = {
  authUser: PropTypes.object,
  form: PropTypes.object,
  title: PropTypes.string,
  user: PropTypes.object,
  userList: PropTypes.array,
  cbUserAssign: PropTypes.func,
};

export default Form.create()(UserAssign);

