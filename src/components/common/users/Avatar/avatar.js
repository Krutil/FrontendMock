import React from 'react';
import PropTypes from 'prop-types';
import styles from './avatar.less';

function Avatar({ src, size, className }) {
  /* .filter((item, key) => key < 1);*/

  return (
    <div
      className={`${styles.avatar} ${size === 'm' ? styles.avatarM : ''}${size === 's' ? styles.avatarS : ''} ${className}`}
      style={{ backgroundImage: `url(${src})` }}
    />
  );
}

Avatar.propTypes = {
  src: PropTypes.string,
  size: PropTypes.string,
  className: PropTypes.any,
};

export default Avatar;
