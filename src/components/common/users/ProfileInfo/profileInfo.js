import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
} from 'antd';

import styles from './profileInfo.less';

function profileInfo({ user }) {

  return (
    <div className={`${styles.profileInfo} u_overflowAuto`}>
      <div className="content u_center">
        <div
          className={styles.avatar}
          style={{ backgroundImage: `url(${user.avatar})` }}
        />
        <h3>{user.firstname} {user.lastname}</h3>
        <Button
          className="ant-btn-icon-only"
          style={{ marginTop: '.5em' }}
          type="dark"
          icon="edit"
        >
          Upravit
        </Button>
        <Button
          className="ant-btn-icon-only"
          style={{ marginTop: '.5em' }}
          type="dark"
          icon="mail"
        >
          Zprava
        </Button>
        <Button
          className="ant-btn-icon-only"
          style={{ marginTop: '.5em' }}
          type="dark"
          icon="bars"
        />
      </div>
      <div className="content">
        <table className="tableTransparent tableLabel">
          <tr>
            <td className="cGray">username:</td>
            <td><strong>{user.username}</strong></td>
          </tr>
          <tr>
            <td className="cGray">Timezone:</td>
            <td>
              <strong>
                11:46 local time
                <br /><small>(change)</small>
              </strong>
            </td>
          </tr>
          <tr>
            <td className="cGray">email:</td>
            <td><strong>{user.email}</strong></td>
          </tr>
        </table>

      </div>
    </div>
  );
}

profileInfo.propTypes = {
  user: PropTypes.object,
};

export default profileInfo;
