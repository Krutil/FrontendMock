import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import {
  Table,
  Dropdown,
  Button,
  Menu,
  Modal,
} from 'antd';
import moment from 'moment';
import Avatar from '../../../common/users/Avatar/avatar';
import ModalUserItemPermission from '../../../common/modal/UserItemPermission/userItemPermission';

const translation = {
  read: 'Čtení',
  edit: 'Úprava',
  write: 'Zápis',
};

function usersAssignedList(
  {
    cbUpdate,
    currentItem,
    dispatch,
    modal,
  }) {
  const { usersAssigned } = currentItem;
  const handleDeleteItem = id => {

  };
  const handleMenuClick = (record, e) => {
    if (e.key === 'update') {
      dispatch({
        type: 'modal/show',
        payload: {
          visible: true,
          type: 'userItemPermission',
          data: {
            user: record,
          },
        },
      });
      // onEditItem(record);
    } else if (e.key === 'destroy') {
      Modal.confirm({
        width: 500,
        title: 'Odstranit',
        content: 'Odstranit uživateli přístup?',
        okText: 'Odstranit',
        cancelText: 'Zpět',
        onOk() {
          cbUpdate({
            ...currentItem,
            usersAssigned: usersAssigned.filter(item => item.id !== record.id),
          });
        },
        onCancel() {
        },
      });
    }
  };

  let columns = null;
  const locale = {
    emptyText: 'Žádná data k zobrazení',
  };

  if (usersAssigned && usersAssigned.length) {
    columns = [{
      title: '',
      dataIndex: 'avatar',
      key: 'avatar',
      width: 64,
      render: text => <Avatar className="ui_avatar" size="s" src={text} />,
    }, {
      title: 'Příjmení',
      dataIndex: 'lastname',
      key: 'lastname',
    }, {
      title: 'Jméno',
      dataIndex: 'firstname',
      key: 'firstname',
    }, {
      title: 'Systémové jméno',
      dataIndex: 'username',
      key: 'username',
      render: text => <span>@{text}</span>,
    }, {
      title: 'Oprávnění',
      dataIndex: 'permissions',
      key: 'permissions',
      render: object => (
        <span>
          {
            object && Object.keys(object)
              .filter(key => object[key])
              .map((key, index, data) => {
                const value = object[key];

                if (value) {
                  return (
                    <span key={key}>{translation[key]}{data.length - 1 !== index ? ', ' : ''}</span>
                  );
                }
                return null;
              })
          }
        </span>
      ),
    }, {
      title: 'Expirace',
      dataIndex: 'expiration',
      key: 'expiration',
      className: 'u_noWrap',
      render: date => (
        <span>
          {
            date &&
            <span>{moment(date).format('LLL')}</span>
          }
        </span>
      ),
    }, {
      fixed: 'right',
      title: '',
      key: 'operation',
      width: 50,
      render: (text, record) => (
        <Dropdown
          id="area"
          placement="bottomRight"
          trigger={['click']}
          overlay={
            <Menu onClick={e => handleMenuClick(record, e)}>
              <Menu.Item key="update">Upravit</Menu.Item>
              <Menu.Item key="destroy">Odstranit</Menu.Item>
            </Menu>
          }
        >
          <Button
            type="transparent"
            size="small"
            shape="circle"
            icon="ellipsis"
          />
        </Dropdown>
      ),
    },
    ];
  }

  const modalUserItemPermissionProps = {
    modal,
    onOk(data) {
      const usersAssignedChanged = usersAssigned.map((item) => {
        if (item.id === data.user.id) {
          return data.user;
        }
        return item;
      });

      const payload = {
        ...currentItem,
        usersAssigned: usersAssignedChanged,
      };

      dispatch({
        type: 'modal/hide',
      });
      cbUpdate(payload);
    },
    onCancel() {
      dispatch({
        type: 'modal/hide',
      });
    },
  };

  return (
    <div>
      <Table
        locale={locale}
        scroll={{ x: '100%' }}
        columns={columns}
        dataSource={usersAssigned}
        pagination={false}
        simple
        rowKey={record => record.username}
      />
      {
        modal.type === 'userItemPermission'
        && <ModalUserItemPermission {...modalUserItemPermissionProps} />
      }
    </div>
  );
}

usersAssignedList.propTypes = {
  cbUpdate: PropTypes.func,
  currentItem: PropTypes.object,
  dispatch: PropTypes.func,
  modal: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    modal: state.modal,
  };
}

export default connect(mapStateToProps)(usersAssignedList);

