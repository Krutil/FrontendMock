import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'antd';

import styles from './shrinkStretch.less';

class ShrinkStretch extends React.Component {

  state = {
    shrinked: this.props.shrinked || true,
  };

  toggleState = () => {
    this.setState({
      shrinked: !this.state.shrinked,
    });
  };

  render() {
    return (
      <div className={`${styles.shrinked} ${this.props.className}`}>
        <div className={`${styles.shrinked_content} ${this.state.shrinked && 'is-active'}`}>
          {this.props.children}
        </div>
        <div
          className={styles.showMore}
          onClick={this.toggleState}
        >
          {
            this.state.shrinked ?
              <span><Icon type="down" /><Icon type="down" /><Icon type="down" /></span>
              :
              <span><Icon type="up" /><Icon type="up" /><Icon type="up" /></span>
          }
        </div>
      </div>
    );
  }
}

ShrinkStretch.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.any,
  shrinked: PropTypes.bool,
};

export default ShrinkStretch;
