import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Form,
  Icon,
  Input,
  Select,
} from 'antd';

import styles from './search.less';

class Search extends React.Component {

  state = {
    clearVisible: false,
    inputValue: this.props.keyword,
    selectValue: (
      this.props.select
      && this.props.selectProps
    ) ? this.props.selectProps.defaultValue : '',
  };

  handleSearch = (type) => {
    const {
      form,
      onSearch,
    } = this.props;
    form.validateFields((errors) => {
      if (errors) {
        return;
      }

      const values = {
        ...form.getFieldsValue(),
      };

      console.log('values', values);
      const data = {
        keyword: type === 'clear' ? '' : values.searchWhat,
      };

      if (this.props.select) {
        data.field = values.searchWhere;
      }

      onSearch && onSearch(data);
    });
  };

  handleInputChange = (e) => {
    const value = e.target.value;

    if (value === 0) {
      this.setState({
        clearVisible: false,
      });
      this.handleSearch();
    }
    this.setState({
      ...this.state,
      inputValue: value,
      clearVisible: value !== '',
    });
  };

  handleSelectChange = (value) => {
    const { onSelectChange } = this.props;

    if (onSelectChange) {
      onSelectChange(value);
    }

    this.setState({
      ...this.state,
      selectValue: value,
    });
  };

  handleClearInput = () => {
    this.setState({
      inputValue: '',
      clearVisible: false,
    });
    this.props.form.resetFields();
    this.handleSearch();
  };

  render() {
    const {
      form,
      keyword,
      select,
      selectOptions,
      selectProps,
      size,
      style,
    } = this.props;
    const { clearVisible } = this.state;

    return (
      <Form layout="vertical">
        <Input.Group compact size={size} className={styles.search} style={style}>
          {
            select && form.getFieldDecorator('searchWhere', {
              initialValue: selectProps.defaultValue || '',
            })(
              <Select
                dropdownMatchSelectWidth={false}
                onChange={this.handleSelectChange}
                size={size}
              >
                {
                  selectOptions && selectOptions.map((item, key) => (
                    <Select.Option
                      value={item.value}
                      key={key}
                    >
                      {item.name || item.value}
                    </Select.Option>
                  ))}
              </Select>,
            )
          }
          {
            form.getFieldDecorator('searchWhat', {
              initialValue: keyword || '',
            })(
              <div className="ant-input">
                <Input
                  size={size}
                  onChange={this.handleInputChange}
                  onPressEnter={this.handleSearch}
                />
                {
                  clearVisible && <Icon type="cross" onClick={this.handleClearInput} />
                }
              </div>,
            )
          }
          <Button
            style={{ padding: '0 1em' }}
            size={size}
            type="primary"
            icon="search"
            onClick={this.handleSearch}
          />
        </Input.Group>
      </Form>
    );
  }
}

Search.propTypes = {
  form: PropTypes.object.isRequired,
  keyword: PropTypes.string,
  onSearch: PropTypes.func,
  onSelectChange: PropTypes.func,
  select: PropTypes.bool,
  selectOptions: PropTypes.array,
  selectProps: PropTypes.object,
  size: PropTypes.string,
  style: PropTypes.object,
};

export default Form.create()(Search);

