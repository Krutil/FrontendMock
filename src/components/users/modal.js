import React from 'react';
import PropTypes from 'prop-types';
import { Form, Input, Select, Modal } from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;

/* const formItemLayout = {
 labelCol: {
 span: 6,
 },
 wrapperCol: {
 span: 14,
 }
 };*/

const modal = ({
                 visible,
                 type,
                 item = {},
                 onOk,
                 onCancel,
                 form: {
                   getFieldDecorator,
                   validateFields,
                   getFieldsValue,
                 },
               }) => {
  function handleOk() {
    validateFields((errors) => {
      if (errors) {
        return;
      }
      const data = {
        ...getFieldsValue(),
        key: item.key,
      };
      onOk(data);
    });
  }

  const modalOpts = {
    title: `${type === 'create' ? 'Vytvořit uživatele' : 'Upravit uživatele'}`,
    visible,
    onOk: handleOk,
    onCancel,
    wrapClassName: 'vertical-center-modal',
    okText: 'Potvrdit',
    cancelText: 'Zpět',
  };

  return (
    <Modal {...modalOpts}>
      <Form layout="vertical">
        <FormItem label="Jméno:">
          {getFieldDecorator('firstname', {
            initialValue: item.firstname,
            rules: [
              {
                required: true,
                message: 'Nutné vyplnit',
              },
            ],
          })(<Input size="default" />)}
        </FormItem>
        <FormItem label="Příjmení:">
          {getFieldDecorator('lastname', {
            initialValue: item.lastname,
            rules: [
              {
                required: true,
                message: 'Nutné vyplnit',
              },
            ],
          })(<Input size="default" />)}
        </FormItem>
        <FormItem label="Uživatelské jméno:">
          {getFieldDecorator('username', {
            initialValue: item.username,
            rules: [
              {
                required: true,
                message: 'Nutné vyplnit',
              },
            ],
          })(<Input size="default" />)}
        </FormItem>
        <FormItem label="Telefon:">
          {getFieldDecorator('phone', {
            initialValue: item.phone,
            rules: [
              {
                required: true,
                message: 'Nutné vyplnit',
              },
            ],
          })(<Input size="default" />)}
        </FormItem>
        <FormItem label="Email:">
          {getFieldDecorator('email', {
            initialValue: item.email,
            rules: [
              {
                required: true,
                message: 'Nutné vyplnit',
              },
            ],
          })(<Input size="default" />)}
        </FormItem>
        <FormItem label="Oddělení:">
          {getFieldDecorator('department', {
            initialValue: item.department,
            rules: [
              {
                required: true,
                message: 'Nutné vyplnit',
              },
            ],
          })(<Input size="default" />)}
        </FormItem>
      </Form>
    </Modal>
  );
};

modal.propTypes = {
  form: PropTypes.object.isRequired,
  visible: PropTypes.bool,
  type: PropTypes.string,
  item: PropTypes.object,
};

export default Form.create()(modal);
