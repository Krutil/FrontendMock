import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Row, Col } from 'antd';
import SearchGroup from '../ui/search';

const search = (
    {
      field,
      keyword,
      onSearch,
      onAdd,
      form: {
        getFieldDecorator,
        validateFields,
        getFieldsValue,
      },
    },
  ) => {
  const searchGroupProps = {
    field,
    keyword,
    size: 'default',
    select: true,
    selectOptions: [
      { value: 'lastname', name: 'Příjmení' },
      { value: 'firstname', name: 'Jméno' },
      { value: 'username', name: 'Uživatelské jméno' },
      { value: 'role', name: 'Role' },
      { value: 'department', name: 'Oddělení' },
      { value: 'phone', name: 'Telefon' },
      { value: 'email', name: 'Email' },
      { value: 'date_last_login', name: 'Poslední přihlášení' },
    ],
    selectProps: {
      defaultValue: field || 'lastname',
    },
    onSearch: (value) => {
      onSearch(value);
    },
  };

  return (
    <Row gutter={24}>
      <Col lg={10} md={14} sm={14} xs={14}>
        <SearchGroup {...searchGroupProps} />
      </Col>
      <Col
        lg={{ offset: 1, span: 13 }} md={10} sm={10} xs={10}
        style={{ textAlign: 'right' }}
      >
        <Button type="dark" onClick={onAdd}>Vytvořit uživatele</Button>
      </Col>
    </Row>
  );
};

search.propTypes = {
  form: PropTypes.object.isRequired,
  onSearch: PropTypes.func,
  onAdd: PropTypes.func,
  field: PropTypes.string,
  keyword: PropTypes.string,
};

export default Form.create()(search);
