import React from 'react';
import PropTypes from 'prop-types';
import {
  Table,
  Dropdown,
  Button,
  Menu,
  Modal,
} from 'antd';
import classnames from 'classnames';

import Avatar from '../common/users/Avatar/avatar';
import styles from './userList.less';

function userList(
  {
    dataSource,
    loading,
    moment,
    onDeleteItem,
    onEditItem,
    onPageChange,
    pagination,
  }) {
  const handleMenuClick = (record, e) => {

    if (e.key === 'update') {
      // console.log('record', record);
      onEditItem(record);
    } else if (e.key === 'destroy') {
      Modal.confirm({
        width: 500,
        title: 'Odstranit',
        content: 'Skutečně máme tento záznam odstranit?',
        okText: 'Odstranit',
        cancelText: 'Zpět',
        onOk() {
          onDeleteItem(record.id);
        },
        onCancel() {
        },
      });
    }
  };

  const locale = {
    emptyText: 'No data to show',
  };

  const columns = [
    {
      title: '',
      dataIndex: 'avatar',
      key: 'avatar',
      width: 64,
      className: styles.avatar,
      render: text => <Avatar className="ui_avatar" size="s" src={text} />,
    }, {
      title: 'Příjmení',
      dataIndex: 'lastname',
      key: 'lastname',
    }, {
      title: 'Jméno',
      dataIndex: 'firstname',
      key: 'firstname',
    }, {
      title: 'Uživatelské jméno',
      dataIndex: 'username',
      key: 'username',
      render: text => <span>@{text}</span>,
    }, {
      title: 'Role',
      dataIndex: 'role',
      key: 'role',
    }, {
      title: 'Oddělení',
      dataIndex: 'department',
      key: 'department',
    }, {
      title: 'Telefon',
      dataIndex: 'phone',
      key: 'phone',
    }, {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    }, {
      title: 'Poslední přihlášení',
      dataIndex: 'date_last_login',
      key: 'date_last_login',
      render: text => <span>{text ? moment(text).format('LLL') : '—'}</span>,
    }, {
      fixed: 'right',
      title: '',
      key: 'operation',
      width: 50,
      render: (text, record) => (
        <Dropdown
          id="area"
          placement="bottomRight"
          trigger={['click']}
          overlay={
            <Menu onClick={e => handleMenuClick(record, e)}>
              <Menu.Item key="update">Upravit</Menu.Item>
              <Menu.Item key="destroy">Smazat</Menu.Item>
            </Menu>
          }
        >
          <Button
            size="small"
            shape="circle"
            icon="ellipsis"
            style={{
              border: 'none',
            }}
          />
        </Dropdown>
      ),
    },
  ];

  return (
    <div>
      <Table
        locale={locale}
        className={classnames({ [styles.table]: true })}
        scroll={{ x: 1200 }}
        columns={columns}
        dataSource={dataSource}
        loading={loading}
        onChange={onPageChange}
        pagination={pagination}
        simple
        rowKey={record => record.id}
      />
    </div>
  );
}

userList.propTypes = {
  dataSource: PropTypes.array,
  loading: PropTypes.bool,
  moment: PropTypes.func,
  onDeleteItem: PropTypes.func,
  onEditItem: PropTypes.func,
  onPageChange: PropTypes.func,
  pagination: PropTypes.object,
};

export default userList;
