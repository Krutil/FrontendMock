import './index.html';
import 'babel-polyfill';
import dva from 'dva';
import { browserHistory } from 'dva/router';
import createLoading from 'dva-loading';
// import './utils/polyfill';

// 1. Initialize
const app = dva({
  history: browserHistory,
});

// 2. Plugins
app.use(createLoading({
  effects: true,
}));

// 3. Model
app.model(require('./models/appModel'));
app.model(require('./models/settingsModel'));
app.model(require('./models/historyModel'));
app.model(require('./models/activityModel'));
app.model(require('./models/filesModel'));
app.model(require('./models/relationshipsModel'));
app.model(require('./models/commentsModel'));
app.model(require('./models/spectatorsModel'));
app.model(require('./models/usersModel'));
app.model(require('./models/risk/assetsModel'));
app.model(require('./models/risk/requirementsModel'));
app.model(require('./models/risk/risksModel'));

// 4. Router
app.router(require('./router'));

// 5. Start
app.start('#root');
