import 'babel-polyfill';
import dva from 'dva';
import { browserHistory } from 'dva/router';
import createLoading from 'dva-loading';
import { message } from 'antd';
// import './utils/polyfill';
import './index.html';

// 1. Initialize
const app = dva({
  history: browserHistory,
  onError(error) {
    message.error(error.message, 3);
  },
});

// 2. Plugins
app.use(createLoading({
  effects: true,
}));

// 3. Model
app.model(require('./models/activityModel'));
app.model(require('./models/appModel'));
app.model(require('./models/commentsModel'));
app.model(require('./models/filesModel'));
app.model(require('./models/historyModel'));
app.model(require('./models/messagesModel'));
app.model(require('./models/modalModel'));
app.model(require('./models/notificationsModel'));
app.model(require('./models/relationshipsModel'));
app.model(require('./models/risk/assetsModel'));
app.model(require('./models/risk/requirementsModel'));
app.model(require('./models/risk/risksModel'));
app.model(require('./models/settingsModel'));
app.model(require('./models/spectatorsModel'));
app.model(require('./models/usersModel'));

// 4. Router
app.router(require('./router'));

// 5. Start
app.start('#root');
