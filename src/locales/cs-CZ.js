import antdEn from 'antd/lib/locale-provider/cs_CZ';
import appLocaleData from 'react-intl/locale-data/cs';
import enMessages from './cs';

window.appLocale = {
  messages: {
    ...enMessages,
  },
  antd: antdEn,
  locale: 'cs-CZ',
  data: appLocaleData,
};
