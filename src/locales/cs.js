module.exports = {
  'common.username': 'Jméno uživatele',
  'common.password': 'Heslo',
  'common.login': 'Přihlásit se',
  'common.logout': 'Odhlásit se',
  'common.messages': 'Zprávy',
  'common.notification': 'Notifikace',
  'common.trash': 'Koš',
  'common.approval': 'Schválení',
};
