module.exports = {
  'common.logout': 'Logout',
  'common.messages': 'Messages',
  'common.notification': 'Notification',
  'common.username': 'Username',
  'common.password': 'Password',
  'common.login': 'Login',
};
