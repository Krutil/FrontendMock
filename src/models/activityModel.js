import * as activityService from '../services/activityService';
import { parse } from 'qs';

export default {

  namespace: 'activity',

  state: {
    list: [],
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        dispatch({
          type: 'query',
          payload: location.query,
        });
      });
    },
  },

  effects: {
    * query({ payload }, { call, put }) {
      // yield put({type: 'showLoading'});
      // console.log('*query ');
      const response = yield call(activityService.query, parse(payload));
      // console.log('activityService.query response', response);

      if (response) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: response.data,
          },
        });
      }
    },
    * create({ payload }, { call, put }) {
      console.log('1payload', payload);
      const response = yield call(activityService.create, payload);

      if (response && response.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: response.data,
          },
        });
      }
    },
  },

  reducers: {
    querySuccess(state, action) {
      const { list } = action.payload;

      console.log('action.payload', action.payload);
      return {
        ...state,
        list,
      };
    },
  },
};
