import { parse } from 'qs';
import { message } from 'antd';

import * as appService from '../services/appService';

export default {
  namespace: 'app',

  state: {
    login: false,
    loading: false,
    user: {
      name: 'Unknown',
    },
    loginButtonLoading: false,
    menuPopoverVisible: false,
    AppSidebarLeftFold: localStorage.getItem('AppSidebarLeftFold') === 'true',
    AppPanelTreeUnfold: localStorage.getItem('AppPanelTreeUnfold') === 'true',
    isHamburgerMenu: document.body.clientWidth < 769,
    navOpenKeys: JSON.parse(localStorage.getItem('navOpenKeys') || '[]'),
  },

  subscriptions: {
    setup({ dispatch }) {
      // console.log('subscriptions app setup');
      dispatch({ type: 'queryUser' });
      window.onresize = function () {
        dispatch({ type: 'changeNavbar' });
      };
    },
  },

  effects: {
    * login({ payload }, { call, put }) {
      yield put({ type: 'showLoginButtonLoading' });
      const data = yield call(appService.login, parse(payload));

      if (data.success) {
        yield put({
          type: 'loginSuccess',
          payload: {
            user: data.results,
          },
        });
        yield put({ type: 'requirements/query' });
      } else {
        message.error(data.message, 3);
        yield put({
          type: 'loginFail',
        });
      }
    },
    * queryUser({ payload }, { call, put }) {
      yield put({ type: 'showLoading' });
      const data = yield call(appService.userInfo, parse(payload));

      // console.log('queryUser data', data);

      if (data.success) {
        yield put({
          type: 'loginSuccess',
          payload: {
            user: data.user,
          },
        });
      }

      yield put({ type: 'hideLoading' });
    },
    * logout({ payload, dispatch }, { call, put }) {
      const data = yield call(appService.logout, parse(payload));

      if (data.success) {
        message.success(data.message, 3);
        yield put({ type: 'requirements/clear' });
        yield put({
          type: 'logoutSuccess',
        });
      }
    },
    * notificationChecked({ payload }, { select, call, put }) {
      console.log('notificationChecked');
      const user = yield select(state => state.app.user);
      const response = yield call(appService.notificationChecked, parse(user));

      if (response.success) {
        yield put({
          type: 'userUpdate',
          payload: {
            user: response.user,
          },
        });
      }
    },
    * approvalChecked({ payload }, { select, call, put }) {
      console.log('approvalChecked');
      const user = yield select(state => state.app.user);
      const response = yield call(appService.approvalChecked, parse(user));

      if (response.success) {
        yield put({
          type: 'userUpdate',
          payload: {
            user: response.user,
          },
        });
      }
    },
    * trashChecked({ payload }, { select, call, put }) {
      console.log('trashChecked');
      const user = yield select(state => state.app.user);
      const response = yield call(appService.trashChecked, parse(user));

      if (response.success) {
        yield put({
          type: 'userUpdate',
          payload: {
            user: response.user,
          },
        });
      }
    },
    * messagesChecked({ payload }, { select, call, put }) {

      const user = yield select(state => state.app.user);
      const response = yield call(appService.messagesChecked, parse(user));

      if (response.success) {
        yield put({
          type: 'userUpdate',
          payload: {
            user: response.user,
          },
        });
      }
    },
    * switchTreeUnfold({ payload }, { put }) {
      yield put({
        type: 'handleSwitchTreeUnfold',
      });
    },
    * switchSidebar({ payload }, { put }) {
      yield put({
        type: 'handleswitchSidebar',
      });
    },
    * changeNavbar({ payload }, { put }) {
      if (document.body.clientWidth < 769) {
        yield put({ type: 'showNavbar' });
      } else {
        yield put({ type: 'hideNavbar' });
      }
    },
    * switchMenuPopver({ payload }, { put }) {
      yield put({
        type: 'handleSwitchMenuPopver',
      });
    },
  },

  reducers: {
    loginSuccess(state, action) {
      return {
        ...state,
        ...action.payload,
        login: true,
        loginButtonLoading: false,
      };
    },
    logoutSuccess(state) {
      return {
        ...state,
        login: false,
      };
    },
    loginFail(state) {
      return {
        ...state,
        login: false,
        loginButtonLoading: false,
      };
    },
    showLoginButtonLoading(state) {
      return {
        ...state,
        loginButtonLoading: true,
      };
    },
    showLoading(state) {
      return {
        ...state,
        loading: true,
      };
    },
    hideLoading(state) {
      return {
        ...state,
        loading: false,
      };
    },
    userUpdate(state, action) {
      console.log('action', action);
      return {
        ...state,
        user: action.payload.user,
      };
    },
    handleSwitchTreeUnfold(state) {
      localStorage.setItem('AppPanelTreeUnfold', !state.AppPanelTreeUnfold);
      return {
        ...state,
        AppPanelTreeUnfold: !state.AppPanelTreeUnfold,
      };
    },
    handleswitchSidebar(state) {
      localStorage.setItem('AppSidebarLeftFold', !state.AppSidebarLeftFold);
      return {
        ...state,
        AppSidebarLeftFold: !state.AppSidebarLeftFold,
      };
    },
    showNavbar(state) {
      return {
        ...state,
        isHamburgerMenu: true,
      };
    },
    hideNavbar(state) {
      return {
        ...state,
        isHamburgerMenu: false,
      };
    },
    handleSwitchMenuPopver(state) {
      return {
        ...state,
        menuPopoverVisible: !state.menuPopoverVisible,
      };
    },
    handleNavOpenKeys(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};
