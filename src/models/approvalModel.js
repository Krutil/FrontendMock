import { parse } from 'qs';
import pathToRegexp from 'path-to-regexp';
import { select as assetsSelect } from '../services/risk/assetsService';
import { select as risksSelect } from '../services/risk/risksService';

export default {

  namespace: 'approval',

  state: {
    currentItem: {},
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {

        const match = pathToRegexp('/approval/:type/item/:id/').exec(location.pathname);

        if (match) {
          const itemType = match[1];
          const itemKey = match[2];

          dispatch({
            type: 'select',
            payload: {
              itemType,
              itemKey,
            },
          });
        } else {
          dispatch({
            type: 'selectClear',
          });
        }
      });
    },
  },

  effects: {
    * select({ payload }, { call, put }) {
      let response = null;

      if (payload.itemType === 'assets') {
        response = yield call(assetsSelect, { key: payload.itemKey });
      }

      if (payload.itemType === 'risks') {
        response = yield call(risksSelect, { key: payload.itemKey });
      }

      console.log('response',response)

      if (response) {
        yield put({
          type: 'selectSuccess',
          payload: {
            item: response.currentItem,
          },
        });
      }
    },
  },

  reducers: {
    selectSuccess(state, action) {
      const item = action.payload.item;

      return {
        ...state,
        currentItem: item,
      };
    },
    selectClear(state) {
      return {
        ...state,
        currentItem: null,
      };
    },
  },

};
