import * as commentsService from '../services/commentsService';
import { parse } from 'qs';
import { isSpectated } from '../utils/helper';


export default {

  namespace: 'comments',

  state: {
    list: [],
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        dispatch({
          type: 'query',
          payload: location.query,
        });
      });
    },
  },

  effects: {
    * query({ payload }, { call, put }) {
      // yield put({type: 'showLoading'});
      // console.log('*query ');
      const data = yield call(commentsService.query, parse(payload));
      // console.log('commentsService.query data', data);

      if (data) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
          },
        });
      }
    },


    * create({ payload }, { call, put, select }) {
      const data = yield call(commentsService.create, payload);
      const arrMentions = [];

      if (data && data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
          },
        });

        const notificationPayload = {
          category_notification: 'spectators',
          activity_type: 'create',
          subject: 'comment',
          assigned_to: payload.appUser.username,
          item_title: payload.currentItem.title,
          item_type: payload.category,
          item_type_key: payload.keys.listKey,
          user: payload.appUser,
        };

        const spectators = yield select(state => state.spectators);

        if (isSpectated(notificationPayload, spectators.list)) {
          yield put({
            type: 'notification/create',
            payload: notificationPayload,
          });
        }

        if (payload.text.length > 0) {
          const words = payload.text.split(' ');

          words.forEach((item) => {
            const found = false;

            if (!found && (item.indexOf('@') !== -1 || item.indexOf('#') !== -1)) {
              const payloadActivity = {
                activity_type: 'mention',
                assigned_to: item,
                item_title: data.currentItem.title,
                item_type: data.results.category,
                item_type_key: data.results.type_key,
                user: data.results.user,
              };

              arrMentions.push({
                type: 'activity/create',
                payload: payloadActivity,
              });
            }
          });

          let n = 0;

          if (arrMentions.length > 0) {
            while (n < arrMentions.length > 0) {
              yield put(arrMentions[n]);
              n += 1;
            }
          }
          n = 0;
          if (arrMentions.length > 0) {
              while (n < arrMentions.length > 0) {
                yield put({
                  type: 'notification/create',
                  payload: {
                    category_notification: 'activity',
                    activity_type: 'mention',
                    assigned_to: arrMentions[n].payload.assigned_to,
                    item_title: data.currentItem.title,
                    item_type: data.results.category,
                    item_type_key: data.results.type_key,
                    user: data.results.user,
                  },
                });
              n += 1;
            }
            }



          yield put({
            type: 'spectators/updateDate',
            payload: {
              id: data.currentItem.id,
            },
          });
        }
      }
    },
    * destroy({ payload }, { call, put }) {
      const data = yield call(commentsService.destroy, payload);

      if (data && data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
          },
        });

        // console.log("-------", {
        //   category_notification: 'spectators',
        //   activity_type: 'destroy',
        //   // assigned_to: notificationData.user.username,
        //   item_title: notificationData.type_key,
        //   item_type: notificationData.category,
        //   item_type_key: notificationData.type_key,
        //   // user: notificationData.user,
        // });
        // yield put ({
        //   type: 'notification/create',
        //   payload: {
        //     category_notification: 'spectators',
        //     activity_type: 'destroy',
        //     assigned_to: notificationData.user.username,
        //     item_title: notificationData.type_key,
        //     item_type: notificationData.category,
        //     item_type_key: notificationData.type_key,
        //     user: notificationData.user,
        //   },
        // });
      }
    },
  },

  reducers: {
    querySuccess(state, action) {
      const { list } = action.payload;
      return {
        ...state,
        list,
      };
    },
  },
};
