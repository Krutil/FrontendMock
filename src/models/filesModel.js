import * as filesService from '../services/filesService';
import { isSpectated } from '../utils/helper';
import { parse } from 'qs';


export default {

  namespace: 'files',

  state: {
    list: [],
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        // console.log('Model files location', location);

        dispatch({
          type: 'query',
          payload: location.query,
        });
      });
    },
  },

  effects: {
    * query({ payload }, { call, put }) {
      // yield put({type: 'showLoading'});
      // console.log('*query ');
      const data = yield call(filesService.query, parse(payload));

      if (data) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
          },
        });
      }
    },
    * destroy({ payload }, { call, put }) {
      const data = yield call(filesService.destroy, payload);

      if (data && data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
          },
        });
      }
    },
    * create({ payload }, { call, put, select }) {
      const data = yield call(filesService.create, payload);

      if (data && data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
          },
        });

        const spectators = yield select(state => state.spectators);

        const notificationPayload = {
          activity_type: 'create',
          activity_subject: 'file',
          assigned_to: `@${payload.appUser.username}`,
          item_title: payload.currentItem.title,
          item_type: payload.category,
          item_type_key: payload.keys.listKey,
          user: payload.appUser,
          category_notification: 'spectators',
        };
        if (isSpectated(notificationPayload, spectators.list)) {
          yield put({
            type: 'notification/create',
            payload: notificationPayload,
          });
        }
      }
    },
    * update({ payload }, { select, call, put }) {
      const data = yield call(filesService.update, payload);

      if (data && data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
          },
        });
      }
    },
  },

  reducers: {
    querySuccess(state, action) {
      const { list } = action.payload;
      return {
        ...state,
        list,
      };
    },
  },
};
