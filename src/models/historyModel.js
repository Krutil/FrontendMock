import * as historyService from '../services/historyService';
import { parse } from 'qs';
import { message } from 'antd';

export default {

  namespace: 'history',

  state: [],

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        dispatch({
          type: 'query',
          payload: location.query,
        });
      });
    },
  },

  effects: {
    * query({ payload }, { call, put }) {
      const response = yield call(historyService.query, parse(payload));
      // console.log('historyService.query response', response);

      if (response && response.success) {
        yield put({
          type: 'querySuccess',
          payload: response.data,
        });
      }
    },
    * store({ payload }, { call, put }) {
      // console.log('payload', payload);
      const response = yield call(historyService.store, payload);

      if (response && response.success) {
        yield put({
          type: 'querySuccess',
          payload: response.data,
        });
      }
    },
  },

  reducers: {
    querySuccess(state, action) {
      state = action.payload;

      return [
        ...state,
      ];
    },
  },
};
