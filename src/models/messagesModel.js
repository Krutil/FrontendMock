import { parse } from 'qs';
import * as messagesService from '../services/messagesService';

export default {

  namespace: 'messages',

  state: {
    list: [],
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        dispatch({
          type: 'query',
          payload: location.query,
        });
      });
    },
  },

  effects: {
    * query({ payload }, { call, put }) {
      const response = yield call(messagesService.query, parse(payload));
      if (response) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: response,
          },
        });
      }
    },

    * create({ payload }, { call, put }) {
      const response = yield call(messagesService.create, payload);

      if (response && response.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: response.data,
          },
        });
      }
    },
    * destroy({ payload }, { call, put }) {
      const response = yield call(messagesService.destroy, payload);

      if (response && response.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: response.data,
          },
        });
      }
    },
    * messageViewed({ payload }, { call, put }) {
      const response = yield call(messagesService.messageViewed, payload);

      if (response && response.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: response.data,
          },
        });
      }
    },
  },

  reducers: {
    querySuccess(state, action) {
      const { list } = action.payload;
      return {
        list,
      };
    },
  },
};
