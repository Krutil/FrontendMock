/* modalModel.js */
const stateInitial = {
  visible: false,
  type: null,
  action: null,
  data: null,
};

export default {

  namespace: 'modal',

  state: stateInitial,

  reducers: {
    show(state, action) {
      return { ...state, ...action.payload };
    },
    hide() {
      return stateInitial;
    },
  },
};
