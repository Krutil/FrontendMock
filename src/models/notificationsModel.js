import * as notificationsService from '../services/notificationsService';
import { parse } from 'qs';

export default {

  namespace: 'notification',

  state: [],

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        dispatch({
          type: 'query',
          payload: location.query,
        });
      });
    },
  },

  effects: {
    * query({ payload }, { call, put }) {
      const response = yield call(notificationsService.query, parse(payload));

      if (response && response.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: response.data,
          },
        });
      }
    },
    * create({ payload }, { call, put }) {
      const response = yield call(notificationsService.create, payload);
      if (response && response.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: response.data,
          },
        });
      }
    },
  },

  reducers: {
    querySuccess(state, action) {
      const { list } = action.payload;

      return list;
    },
  },
};
