import * as relationshipsService from '../services/relationshipsService';
import { parse } from 'qs';
import pathToRegexp from 'path-to-regexp';
import { isSpectated } from '../utils/helper';

export default {

  namespace: 'relationships',

  state: {
    list: [],
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        dispatch({
          type: 'query',
          payload: location.query,
        });
      });
    },
  },

  effects: {
    * query({ payload }, { call, put }) {
      // yield put({type: 'showLoading'});
      // console.log('*query ');
      const data = yield call(relationshipsService.query, parse(payload));
      // console.log('relationshipsService.query data', data);

      if (data) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
          },
        });
      }
    },
    * create({ payload }, { call, put, select }) {
      console.log('relationshipsService.create', payload);
      const data = yield call(relationshipsService.create, payload);
      if (data && data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
          },
        });

        // const n = data.data.length;
        // const spectators = yield select(state => state.spectators);
        //
        // const notificationPayload = {
        //   activity_type: 'create',
        //   assigned_to: `@${payload.appUser.username}`,
        //   user: payload.appUser,
        //   category_notification: 'spectators',
        //   subject: 'relationship',
        //   item_title: payload.currentItem.title,
        //   item_type_key: data.data[n-1].type_key,
        //   item_type: payload.category,
        // };
        //
        // if (isSpectated(notificationPayload, spectators.list)) {
        //   yield put({
        //     type: 'notification/create',
        //     payload: {
        //       ...notificationPayload,
        //       spectator: isSpectated(notificationPayload, spectators.list, true),
        //     },
        //   });
        // }
      }
    },
    * destroy({ payload }, { call, put }) {
      const data = yield call(relationshipsService.destroy, payload);

      if (data && data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
          },
        });
      }
    },
  },

  reducers: {
    querySuccess(state, action) {
      const { list } = action.payload;
      return {
        ...state,
        list,
      };
    },

  },
};
