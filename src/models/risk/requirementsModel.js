import { parse } from 'qs';
import pathToRegexp from 'path-to-regexp';
import { message } from 'antd';

import * as requirementsService from '../../services/risk/requirementsService';

const stateInitial = {
  list: [],
  currentItem: null,
  modal: {
    visible: false,
    type: null,
    action: null,
  },
};

export default {

  namespace: 'requirements',

  state: stateInitial,

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
         console.log('Model requirements location', location);

        dispatch({
          type: 'query',
          payload: location.query,
        });

        const match = pathToRegexp('/risk/requirements/item/:id/:action?').exec(location.pathname);

        if (match) {
          const itemKey = match[1];
          console.log('itemId', itemKey);
          dispatch({
            type: 'select',
            payload: itemKey,
          });
        } else {
          dispatch({
            type: 'selectClear',
          });
        }
      });
    },
  },

  effects: {
    * query({ payload }, { call, put }) {
      // yield put({type: 'showLoading'});
      const data = yield call(requirementsService.query, parse(payload));

      if (data) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
          },
        });
      }
    },
    * select({ payload }, { call, put }) {
      // yield put({type: 'showLoading'});
      const data = yield call(requirementsService.select, { key: payload });

      if (data) {
        yield put({
          type: 'selectSuccess',
          payload: {
            item: data.data,
          },
        });
      }
    },
    * destroy({ payload }, { call, put }) {
      const data = yield call(requirementsService.destroy, payload);

      if (data && data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.list,
          },
        });
      }
    },
    * restore({ payload }, { call, put }) {
      const data = yield call(requirementsService.restore, { id: payload });

      if (data && data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.list,
          },
        });
      }
    },
    * create({ payload }, { call, put }) {
      const response = yield call(requirementsService.create, payload);

      if (response && response.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: response.list,
          },
        });

        message.success('Záznam byl vytvořen.', 3);

        console.log('createRequirementAlt payload', payload);
        console.log('createRequirementAlt', response);

        if (payload.action === 'createRequirementAlt') {
          const payloadRelationship = {
            appUser: payload.appUser,
            from_key: payload.from_key,
            from_key_node: payload.from_key_node,
            itemFrom: response.results.createdFrom,
            itemTo: response.results,
            to_key: response.results.key,
          };

          yield put({
            type: 'relationships/create',
            payload: payloadRelationship,
          });
        }

        const payloadActivity = {
          activity_type: 'create',
          assigned_user: response.results.assigned_user ? response.results.assigned_user : null,
          item_title: response.results.title,
          item_type: 'requirements',
          item_type_key: response.results.key,
          user: response.results.user,
        };

        yield put({
          type: 'activity/create',
          payload: payloadActivity,
        });
      }
    },
    * update({ payload }, { call, put }) {
      const response = yield call(requirementsService.update, payload);

      if (response && response.success) {
        yield put({
          type: 'updateSuccess',
          payload: {
            list: response.list,
            currentItem: response.item,
          },
        });

        yield put({
          type: 'notification/create',
          payload: {
            activity_type: payload.item.status,
            assigned_to: `#${payload.item.key}`,
            item_title: payload.item.title,
            item_type: 'requirements',
            item_type_key: payload.item.key,
            user: payload.appUser,
            category_notification: 'activity',
          },
        });
      }
    },
    * updateTree({ payload }, { call, put }) {
      const data = yield call(requirementsService.updateTree, payload);

      if (data && data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
          },
        });
      }
    },
    * commentsCreate({ payload }, { call, put }) {
      const response = yield call(requirementsService.commentsCreate, payload);

      if (response && response.success) {
        yield put({
          type: 'commentsUpdateSuccess',
          payload: response.data,
        });
      }
    },
    * commentsDestroy({ payload }, { call, put }) {
      const response = yield call(requirementsService.commentsDestroy, payload);

      if (response && response.success) {
        yield put({
          type: 'commentsUpdateSuccess',
          payload: response.data,
        });
      }
    },
    * folderCreate({ payload }, { call, put }) {
      yield put({ type: 'hideModal' });
      const response = yield call(requirementsService.folderCreate, payload);

      if (response && response.success) {
        yield put({
          type: 'querySuccess',
          payload: response,
        });
      }
    },
  },

  reducers: {
    selectSuccess(state, action) {
      const item = action.payload.item;

      return {
        ...state,
        currentItem: item,
      };
    },
    selectClear(state) {
      return {
        ...state,
        currentItem: null,
      };
    },
    querySuccess(state, action) {
      const { list } = action.payload;
      return {
        ...state,
        list,
      };
    },
    updateSuccess(state, action) {
      const { list, currentItem } = action.payload;

      return {
        ...state,
        list,
        currentItem,
      };
    },
    commentsUpdateSuccess(state, action) {
      return {
        ...state,
        list: action.payload.list,
        currentItem: action.payload.currentItem,
      };
    },
    showModal(state, action) {
      return { ...state, ...action.payload };
    },
    hideModal(state) {
      return {
        ...state,
        modal: {
          visible: false,
          type: null,
          action: null,
          data: null,
        },
      };
    },
    clear() {
      return { ...stateInitial };
    },
  },

};
