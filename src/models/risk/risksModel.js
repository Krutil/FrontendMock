import { parse } from 'qs';
import pathToRegexp from 'path-to-regexp';

import * as risksService from '../../services/risk/risksService';

export default {

  namespace: 'risks',

  state: {
    list: [],
    currentItem: {},
    modalVisible: false,
    modalType: 'create',
    modal: {
      visible: false,
      type: null,
      action: null,
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {

        dispatch({
          type: 'query',
          payload: location.query,
        });

        const match = pathToRegexp('/risk/risks/item/:id/:action?').exec(location.pathname);

        if (match) {
          const itemKey = match[1];
          dispatch({
            type: 'select',
            payload: itemKey,
          });
        } else {
          dispatch({
            type: 'selectClear',
          });
        }
      });
    },
  },

  effects: {
    * query({ payload }, { call, put }) {
      const data = yield call(risksService.query, parse(payload));

      if (data) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.list,
          },
        });
      }
    },
    * select({ payload }, { call, put }) {
      const data = yield call(risksService.select, { key: payload });

      if (data) {
        yield put({
          type: 'selectSuccess',
          payload: {
            item: data.currentItem,
          },
        });
      }
    },
    * destroy({ payload }, { call, put }) {
      const data = yield call(risksService.destroy, payload);

      if (data && data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.list,
          },
        });
      }
    },
    * restore({ payload }, { call, put }) {
      const data = yield call(risksService.restore, { id: payload });

      if (data && data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.list,
          },
        });
      }
    },
    * create({ payload }, { call, put }) {
      const data = yield call(risksService.create, payload);

      if (data && data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.list,
          },
        });


        const payloadActivity = {
          activity_type: 'create',
          assigned_user: data.results.assigned_user ? data.results.assigned_user : null,
          item_title: data.results.title,
          item_type: 'risks',
          item_type_key: data.results.key,
          user: data.results.user,
        };

        yield put({
          type: 'activity/create',
          payload: payloadActivity,
        });

        // yield put({
        //   type: 'notification/create',
        //   payload: {
        //     activity_type: 'create',
        //     assigned_to: `#${data.results.user.username}`,
        //     item_title: data.results.title,
        //     item_type: 'risks',
        //     item_type_key: data.results.key,
        //     user: data.results.user,
        //     category_notification: 'approval',
        //   },
        // });
      }
    },
    * update({ payload }, { select, call, put }) {
      const response = yield call(risksService.update, payload);
      console.log('response',response);

      if (response && response.success) {
        yield put({
          type: 'updateSuccess',
          payload: {
            list: response.list,
            currentItem: response.item,
          },
        });

        const payloadActivity = {
          activity_type: payload.item.status,
          assigned_to: `#${payload.item.key}`,
          item_title: payload.item.title,
          item_type: 'risks',
          item_type_key: payload.item.key,
          user: payload.appUser,
          category_notification: 'activity',
        };

        yield put({
          type: 'notification/create',
          payload: payloadActivity,
        });

        // yield put({
        //   type: 'activity/create',
        //   payload: payloadActivity,
        // });

        console.log('notification/create', payload);

        if (
          !!payload.item.assigned_user
          && payload.item.status === 'accepted'
          && response.itemHistory
          && payload.item.assigned_user !== response.itemHistory.assigned_user
        ) {
          yield put({
            type: 'notification/create',
            payload: {
              activity_type: 'mention',
              assigned_to: payload.item.assigned_user,
              item_title: payload.item.title,
              item_type: 'risks',
              item_type_key: payload.item.key,
              user: payload.appUser,
              category_notification: 'assign',
            },
          });
        }

        const settingsRISK = yield select(state => state.settings.risk);

        if (settingsRISK) {
          if (settingsRISK.risks && settingsRISK.risks.history) {
            const dataObject = {};
            dataObject.item = response.itemHistory;
            dataObject.user = payload.appUser;

            yield put({
              type: 'history/store',
              payload: dataObject,
            });
          }
        }

        const payloadActivity2 = {
          activity_type: 'update',
          assigned_to: `#${response.itemHistory.key}`,
          item_title: response.itemHistory.title,
          item_type: 'risks',
          item_type_key: 'risks',
          user: payload.appUser,
        };

        yield put({
          type: 'activity/create',
          payload: payloadActivity2,
        });
      }
    },
    * schemaAdd({ payload }, { call, put }) {
      const response = yield call(risksService.schemaAdd, payload);

      if (response && response.success) {
        yield put({
          type: 'updateSuccess',
          payload: response,
        });
      }
    },
    * schemaDestroy({ payload }, { call, put }) {
      const response = yield call(risksService.schemaDestroy, payload);

      if (response && response.success) {
        yield put({
          type: 'updateSuccess',
          payload: response,
        });
      }
    },
    * folderCreate({ payload }, { call, put }) {
      yield put({ type: 'hideModal' });
      const response = yield call(risksService.folderCreate, payload);

      if (response && response.success) {
        yield put({
          type: 'querySuccess',
          payload: response,
        });
      }
    },
  },

  reducers: {
    selectSuccess(state, action) {
      const item = action.payload.item;

      return {
        ...state,
        currentItem: item,
      };
    },
    selectClear(state) {
      return {
        ...state,
        currentItem: null,
      };
    },
    querySuccess(state, action) {
      const { list } = action.payload;
      return {
        ...state,
        list,
      };
    },
    updateSuccess(state, action) {
      if (action.payload.currentItem) {
        return {
          ...state,
          list: action.payload.list,
          currentItem: action.payload.currentItem,
        };
      }
      return {
        ...state,
        list: action.payload.list,
      };
    },
    showModal(state, action) {
      return { ...state, ...action.payload };
    },
    hideModal(state) {
      return {
        ...state,
        modal: {
          visible: false,
          type: null,
          action: null,
          data: null,
        },
      };
    },
  },
};
