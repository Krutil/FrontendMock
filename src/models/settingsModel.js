import { parse } from 'qs';
import { message } from 'antd';

import * as settingsService from '../services/settingsService';

const stateInitial = {
  risk: {
    assets: {
      type: 'assets',
      history: false,
      noteUpdate: false,
    },
    risks: {
      type: 'risks',
      history: false,
      noteUpdate: false,
    },
  },
};

export default {
  namespace: 'settings',
  state: stateInitial,

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        dispatch({
          type: 'query',
          payload: location.query,
        });
      });
    },
  },

  effects: {
    * query({ payload }, { call, put }) {
      const response = yield call(settingsService.query, parse(payload));
      console.log('querySuccess', response);

      if (response && response.success) {
        yield put({
          type: 'querySuccess',
          payload: response.data || stateInitial,
        });
      }
    },
    * updateRISK({ payload }, { call, put }) {
      console.log('payload', payload);
      const response = yield call(settingsService.updateRISK, payload);

      if (response && response.success) {
        yield put({
          type: 'querySuccess',
          payload: response.data,
        });
        message.success('Změny byly uloženy', 3);
      }
    },
  },

  reducers: {
    querySuccess(state, action) {
      if (action.payload) {
        return {
          ...action.payload,
        };
      }

      return state;
    },
  },
};
