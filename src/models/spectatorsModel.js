import * as spectatorsService from '../services/spectatorsService';
import { parse } from 'qs';

export default {

  namespace: 'spectators',

  state: {
    list: [],
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        dispatch({
          type: 'query',
          payload: location.query,
        });
      });
    },
  },

  effects: {
    * query({ payload }, { call, put }) {
      // yield put({type: 'showLoading'});
      // console.log('*query ');
      const data = yield call(spectatorsService.query, parse(payload));
      // console.log('spectatorsService.query data', data);
      if (data) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
          },
        });
      }
    },
    * create({ payload }, { call, put }) {
      const data = yield call(spectatorsService.create, payload);
      console.log("spectators", payload);

      if (data && data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
          },
        });


      }
    },
    * destroy({ payload }, { call, put }) {
      const data = yield call(spectatorsService.destroy, payload);

      if (data && data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
          },
        });
      }
    },
    * toggle({ payload }, { call, put }) {
      const data = yield call(spectatorsService.toggle, payload);
      if (data && data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
          },
        });
        console.log("-------------------", data);
        // yield put({
        //     type: 'notification/create',
        //     payload: {
        //       category_notification: 'spectators',
        //       activity_type: 'create',
        //       item_title: payload.title,
        //       item_type: payload.category,
        //       item_type_key: payload.type_key,
        //       user: payload.appUser,
        //     },
        // });
      }
    },
    * updateDate({ payload }, { call, put }) {
      const data = yield call(spectatorsService.updateDate, payload);

      if (data && data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
          },
        });
      }
    },
  },

  reducers: {
    querySuccess(state, action) {
      const { list } = action.payload;
      return {
        ...state,
        list,
      };
    },
  },
};
