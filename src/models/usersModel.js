import * as usersService from '../services/usersService';
import { parse } from 'qs';

export default {

  namespace: 'users',

  state: {
    list: [],
    loading: false,
    currentItem: {},
    modalVisible: false,
    modalType: 'create',
    isMotion: localStorage.getItem('antdAdminUserIsMotion') === 'true',
    pagination: {
      showSizeChanger: true,
      showQuickJumper: true,
      showTotal: total => `Celkem uživatelů: ${total}`,
      current: 1,
      total: null,
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen((location) => {
        /* if (location.pathname === '/users') {
         //console.log('users location.pathname', location.pathname);
         dispatch({
         type: 'query',
         payload: location.query
         })
         }*/

        dispatch({
          type: 'query',
          payload: location.query,
        });
      });
    },
  },

  effects: {
    * query({ payload }, { call, put }) {
      // console.log('users effects *query');
      const data = yield call(usersService.query, parse(payload));

      if (data) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
            pagination: data.page,
          },
        });
      }
    },
    * delete({ payload }, { call, put }) {
      const data = yield call(usersService.remove, { id: payload });

      if (data && data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
            pagination: {
              total: data.page.total,
              current: data.page.current,
            },
          },
        });
      }
    },
    * create({ payload }, { call, put }) {
      yield put({ type: 'hideModal' });
      const data = yield call(usersService.create, payload);

      if (data && data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
            pagination: {
              total: data.page.total,
              current: data.page.current,
            },
          },
        });
      }
    },
    * update({ payload }, { select, call, put }) {
      yield put({ type: 'hideModal' });
      const id = yield select(({ users }) => users.currentItem.id);
      const newUser = { ...payload, id };
      const data = yield call(usersService.update, newUser);

      if (data && data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data,
            pagination: {
              total: data.page.total,
              current: data.page.current,
            },
          },
        });
      }
    },
    * switchIsMotion({ payload }, { put }) {
      yield put({
        type: 'handleSwitchIsMotion',
      });
    },
  },

  reducers: {
    querySuccess(state, action) {
      const { list, pagination } = action.payload;
      return {
        ...state,
        list,
        loading: false,
        pagination: {
          ...state.pagination,
          ...pagination,
        },
      };
    },
    showModal(state, action) {
      return { ...state, ...action.payload, modalVisible: true };
    },
    hideModal(state) {
      return { ...state, modalVisible: false };
    },
    handleSwitchIsMotion(state) {
      localStorage.setItem('antdAdminUserIsMotion', !state.isMotion);
      return { ...state, isMotion: !state.isMotion };
    },
  },

};
