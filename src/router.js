import React from 'react';
import { Router } from 'dva/router';
import App from './scenes/app';

const cached = {};

function registerModel(app, model) {
  if (!cached[model.namespace]) {
    app.model(model);
    cached[model.namespace] = 1;
  }
}

function onRouterUpdate() {
}
/*
 function hashLinkScroll() {
 const { hash, action } = window.location;

 if (hash !== '' && action === 'POP') {
 // Push onto callback queue so it runs after the DOM is updated,
 // this is required when navigating from a different page so that
 // the element is rendered on the page before trying to getElementById.
 setTimeout(() => {
 const id = hash.replace('#', '');
 const element = document.getElementById(id);
 if (element) element.scrollIntoView();
 }, 1000);
 }
 }*/

export default function ({ history, app }) {
  /* https://github.com/ReactTraining/react-router/tree/v2.8.1/docs/guides */
  const routes = [
    {
      path: '/',
      component: App,
      indexRoute: { onEnter: (nextState, replace) => replace('/dashboard') },
      getIndexRoute(nextState, cb) {
        require.ensure([], (require) => {
          registerModel(app, require('./models/dashboardModel'));
          cb(null, { component: require('./scenes/dashboard/dashboard') });
        });
      },
      childRoutes: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {
              registerModel(app, require('./models/dashboardModel'));
              cb(null, require('./scenes/dashboard/dashboard'));
            });
          },
        },
        {
          path: 'risk/requirements',
          name: 'risk/requirements',
          getIndexRoute(nextState, cb) {
            require.ensure([], (require) => {
              cb(null, { component: require('./scenes/risk/requirements/requirements') });
            });
          },
          childRoutes: [
            {
              path: 'create',
              name: 'Requirements Create Item',
              getComponent(nextState, cb) {
                require.ensure([], (require) => {
                  cb(null, require('./scenes/risk/requirements/createUpdate/createUpdate'));
                });
              },
            },
            {
              path: 'create/:id',
              name: 'Requirements Create/Approval Item',
              getComponent(nextState, cb) {
                require.ensure([], (require) => {
                  cb(null, require('./scenes/risk/requirements/createUpdate/createUpdate'));
                });
              },
            },
            {
              path: 'item/:id',
              name: 'Requirements Item',
              getComponent(nextState, cb) {
                require.ensure([], (require) => {
                  cb(null, require('./scenes/risk/requirements/requirements'));
                });
              },
            },
            {
              path: 'item/:id/update',
              name: 'Requirements Item',
              getComponent(nextState, cb) {
                require.ensure([], (require) => {
                  cb(null, require('./scenes/risk/requirements/createUpdate/createUpdate'));
                });
              },
            },
          ],
        },
        {
          path: 'risk/assets',
          name: 'risk/assets',
          getIndexRoute(nextState, cb) {
            require.ensure([], (require) => {
              cb(null, { component: require('./scenes/risk/assets/assets') });
            });
          },
          childRoutes: [
            {
              path: 'create',
              name: 'Assets Create Item',
              getComponent(nextState, cb) {
                require.ensure([], (require) => {
                  cb(null, require('./scenes/risk/assets/createUpdate/createUpdate'));
                });
              },
            },
            {
              path: 'create/:id',
              name: 'Assets Create/Approval Item',
              getComponent(nextState, cb) {
                require.ensure([], (require) => {
                  cb(null, require('./scenes/risk/assets/createUpdate/createUpdate'));
                });
              },
            },
            {
              path: 'item/:id',
              name: 'Detail Assets Item',
              getComponent(nextState, cb) {
                require.ensure([], (require) => {
                  cb(null, require('./scenes/risk/assets/assets'));
                });
              },
            },
            {
              path: 'item/:id/update',
              name: 'Update Assets Item',
              getComponent(nextState, cb) {
                require.ensure([], (require) => {
                  cb(null, require('./scenes/risk/assets/createUpdate/createUpdate'));
                });
              },
            },
          ],
        },
        {
          path: 'risk/risks',
          name: 'risk/risks',
          getIndexRoute(nextState, cb) {
            require.ensure([], (require) => {
              cb(null, { component: require('./scenes/risk/risks/risks') });
            });
          },
          childRoutes: [
            {
              path: 'create',
              name: 'Create Risks Item',
              getComponent(nextState, cb) {
                require.ensure([], (require) => {
                  cb(null, require('./scenes/risk/risks/createUpdate/createUpdate'));
                });
              },
            },
            {
              path: 'create/:id',
              name: 'Risks Item',
              getComponent(nextState, cb) {
                require.ensure([], (require) => {
                  cb(null, require('./scenes/risk/risks/risks'));
                });
              },
            },
            {
              path: 'item/:id',
              name: 'Risks Item',
              getComponent(nextState, cb) {
                require.ensure([], (require) => {
                  cb(null, require('./scenes/risk/risks/risks'));
                });
              },
            },
            {
              path: 'item/:id/update',
              name: 'Update Assets Item',
              getComponent(nextState, cb) {
                require.ensure([], (require) => {
                  cb(null, require('./scenes/risk/risks/createUpdate/createUpdate'));
                });
              },
            },
          ],
        },
        {
          path: 'approval',
          name: 'approval',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {
              registerModel(app, require('./models/approvalModel'));
              cb(null, require('./scenes/approval/approval'));
            });
          },
          childRoutes: [
            {
              path: ':type/item/:id',
              name: 'Risks Item',
              getComponent(nextState, cb) {
                require.ensure([], (require) => {
                  registerModel(app, require('./models/approvalModel'));
                  cb(null, require('./scenes/approval/approval'));
                });
              },
            },
          ],
        },
        {
          path: 'profile',
          name: 'My Profile',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {
              cb(null, require('./scenes/profile/profile'));
            });
          },
          childRoutes: [
            {
              path: ':username',
              name: 'User Profile',
              getComponent(nextState, cb) {
                require.ensure([], (require) => {
                  cb(null, require('./scenes/profile/profile'));
                });
              },
            },
          ],
        },
        {
          path: 'trash',
          name: 'trash',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {
              cb(null, require('./scenes/trash/trash'));
            });
          },
        },
        {
          path: 'search',
          name: 'search',
          getIndexRoute(nextState, cb) {
            require.ensure([], (require) => {
              cb(null, { component: require('./scenes/search/search') });
            });
          },
          childRoutes: [
            {
              path: 'search-dashboard',
              name: 'Search Dashboard',
              getComponent(nextState, cb) {
                require.ensure([], (require) => {
                  registerModel(app, require('./models/dashboardModel'));
                  cb(null, require('./scenes/dashboard/dashboard'));
                });
              },
            },
          ],
        },
        {
          path: 'users',
          name: 'users',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {
              cb(null, require('./scenes/users/users'));
            });
          },
        },
        {
          path: 'settings/risk',
          name: 'RISK',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {
              cb(null, require('./scenes/settings/RISK/risk'));
            });
          },
        },
        {
          path: 'notifications',
          name: 'notifications',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {
              cb(null, require('./scenes/notification/notification'));
            });
          },
        },
        {
          path: 'messages',
          name: 'messages',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {
              cb(null, require('./scenes/messages/messages'));
            });
          },
          childRoutes: [
            {
              path: ':id',
              name: 'messages conversation detail',
              getComponent(nextState, cb) {
                require.ensure([], (require) => {
                  cb(null, require('./scenes/messages/messages'));
                });
              },
            },
          ],
        },
        {
          path: 'calendar',
          name: 'calendar',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {
              cb(null, require('./scenes/calendar/calendar'));
            });
          },
        },
        {
          path: 'logout',
          name: 'Logout',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {

              cb(null, require('./scenes/calendar/calendar'));
            });
          },
        },
        {
          path: '*',
          name: 'error',
          getComponent(nextState, cb) {
            require.ensure([], (require) => {
              cb(null, require('./scenes/error/error'));
            });
          },
        },
      ],
    },
  ];

  return <Router history={history} routes={routes} onUpdate={onRouterUpdate} />;
}
