import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { LocaleProvider } from 'antd';
import { IntlProvider, addLocaleData } from 'react-intl';

import '../css/index.less';
import styles from './app.less';
import { classnames } from '../utils';

import Login from '../components/app/Login/login';
import Header from '../components/app/Header/header';
import Sidebar from '../components/app/Sidebar/sidebar';

const appLocale = window.appLocale;

addLocaleData(appLocale.data);

function App({ children, location, dispatch, app }) {
  const {
    isHamburgerMenu,
    loading,
    login,
    loginButtonLoading,
    menuPopoverVisible,
    navOpenKeys,
    AppSidebarLeftFold,
    user,
  } = app;

  const loginProps = {
    loading,
    loginButtonLoading,
    onOk(data) {
      dispatch({ type: 'app/login', payload: data });
    },
  };

  const headerProps = {
    isHamburgerMenu,
    location,
    menuPopoverVisible,
    navOpenKeys,
    AppSidebarLeftFold,
    user,
    switchMenuPopover() {
      dispatch({ type: 'app/switchMenuPopver' });
    },
    logout() {
      dispatch({ type: 'app/logout' });
    },
    switchSidebar() {
      dispatch({ type: 'app/switchSidebar' });
    },
    changeOpenKeys(openKeys) {
      localStorage.setItem('navOpenKeys', JSON.stringify(openKeys));
      dispatch({
        type: 'app/handleNavOpenKeys',
        payload: { navOpenKeys: openKeys },
      });
    },
  };

  const sidebarProps = {
    location,
    navOpenKeys,
    AppSidebarLeftFold,
    switchSidebar() {
      dispatch({ type: 'app/switchSidebar' });
    },
    changeOpenKeys(openKeys) {
      localStorage.setItem('navOpenKeys', JSON.stringify(openKeys));
      dispatch({
        type: 'app/handleNavOpenKeys',
        payload: { navOpenKeys: openKeys },
      });
    },
  };

  return (
    <LocaleProvider locale={appLocale.antd}>
      <IntlProvider locale={appLocale.locale} messages={appLocale.messages}>
        {
          login ?
            <div
              className={
                classnames(
                  styles.layout,
                  { 'is-folded': isHamburgerMenu ? false : AppSidebarLeftFold },
                  { 'is-hamburgerMenu': isHamburgerMenu },
                )
              }
            >
              {
                !isHamburgerMenu &&
                <aside className={styles.sidebar}>
                  <Sidebar {...sidebarProps} />
                </aside>
              }
              <Header {...headerProps} />
              <div className={styles.main}>
                {children}
              </div>
            </div>
            :
            <div className={styles.loginBg}>
              <Login {...loginProps} />
            </div>
        }
      </IntlProvider>
    </LocaleProvider>
  );
}

App.propTypes = {
  app: PropTypes.object.isRequired,
  children: PropTypes.element.isRequired,
  dispatch: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    app: state.app,
  };
}

export default connect(mapStateToProps)(App);
