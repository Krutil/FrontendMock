import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { message } from 'antd';
import moment from 'moment';
import 'moment/locale/cs';

import ApprovalList from './components/ApprovalList/approvalList';
import AssetApproval from './components/AssetApproval/assetApproval';
import RiskApproval from './components/RiskApproval/riskApproval';

moment.locale('cs');

class Approval extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      dataTree: [],
      selectedKeys: [],
      expandedKeys: [],
      searchValue: '',
      autoExpandParent: true,
      nodeInfo: {
        key: null,
        type: null,
      },
    };
  }

  componentWillMount() {
    this.props.dispatch({
      type: 'app/approvalChecked',
    });
  }

  itemUpdate = (item) => {
    const { app } = this.props;

    const payload = {
      item,
      appUser: app.user,
    };

    this.props.dispatch({
      type: `${item.item_type}/update`,
      payload,
    });
  };

  render() {
    const {
      app,
      approval,
      assets,
      location,
      requirements,
      risks,
      users,
    } = this.props;
    const arrayItems = [];

    requirements && requirements.list.forEach((item) => {
      if (item.status === 'approval' || item.status === 'inProgress') {
        item.item_type_title = 'Požadavky';
        arrayItems.push(item);
      }
    });

    assets && assets.list.forEach((item) => {
      if (item.status === 'approval' || item.status === 'inProgress') {
        item.item_type_title = 'Aktiva';
        arrayItems.push(item);
      }
    });

    risks && risks.list.forEach((item) => {
      if (item.status === 'approval' || item.status === 'inProgress') {
        item.item_type_title = 'Rizika';
        arrayItems.push(item);
      }
    });

    arrayItems.sort((a, b) => {
      let keyA = new Date(a.created_at),
        keyB = new Date(b.created_at);
      if (keyA < keyB) return 1;
      if (keyA > keyB) return -1;
      return 0;
    });

    const approvalListProps = {
      dataSource: arrayItems,
      itemUpdate: this.itemUpdate,
      moment,
      user: app.user,
    };

    const assetApprovalProps = {
      location,
      data: approval.currentItem,
      dataAssets: assets.list,
      dataUsers: users.list,
      cbUpdate: this.itemUpdate,
    };

    return (
      <div className="container is-block">
        <div className="section">
          <div className="section_content">
            <div className="section_title">
              <h2>Schválení{approval.currentItem ? `: ${approval.currentItem.title}` : ''}</h2>
            </div>
            {
              location.pathname === '/approval' &&
              <div className="space">
                <ApprovalList {...approvalListProps} />
              </div>
            }
            {
              approval.currentItem && approval.currentItem.item_type === 'assets' &&
              <div className="space">
                <AssetApproval {...assetApprovalProps} />
              </div>
            }
            {
              approval.currentItem && approval.currentItem.item_type === 'risks' &&
              <div className="space">
                <RiskApproval {...assetApprovalProps} />
              </div>
            }
          </div>
        </div>
      </div>
    );
  }

}

Approval.propTypes = {
  app: PropTypes.object,
  approval: PropTypes.object,
  assets: PropTypes.object,
  dispatch: PropTypes.func,
  requirements: PropTypes.object,
  risks: PropTypes.object,
  users: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    app: state.app,
    approval: state.approval,
    assets: state.assets,
    loadingModelUsers: state.loading.models.users,
    requirements: state.requirements,
    risks: state.risks,
    users: state.users,
  };
}

export default connect(mapStateToProps)(Approval);
