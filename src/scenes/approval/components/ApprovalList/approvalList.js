import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'dva/router';
import { Table, Button, message } from 'antd';

function approvalList(
  {
    dataSource,
    itemUpdate,
    moment,
    user,
  }) {
  const locale = {
    emptyText: 'seznam je prázdný.',
  };


  const itemApproveClick = item => () => {
    itemUpdate({ ...item, status: 'approval' });
    message.success('Záznam byl odeslán ke schválení.', 3);
  };

  /* TODO {
    title: 'Přiřazený uživatel',
    dataIndex: 'assigned_user',
    key: 'assigned_user',
    render: text => <span>{text ? text : '—'}</span>,
  }*/

  const columns = [
    {
      title: 'Název',
      dataIndex: 'title',
      key: 'title',
      render: (text, record) => (
        <Link to={`/risk/${record.item_type}/item/${record.key}/update`}>
          <strong>{text}</strong>
        </Link>
      ),
    }, {
      title: 'Typ',
      dataIndex: 'item_type_title',
      key: 'item_type_title',
    }, {
      title: 'Vytvořeno',
      dataIndex: 'created_at',
      key: 'created_at',
      render: text => <span>{text ? moment(text).format('LLL') : '—'}</span>,
    }, {
      title: 'Vytvořil',
      dataIndex: 'fullname',
      key: 'fullname',
      render: (text, record) => <span>{record.user.lastname} {record.user.firstname}</span>,
    }, {
      title: 'Status',
      key: 'state',
      render: (text, record) => (
        <span>
          {
            record.status === 'inProgress' &&
            <Button onClick={itemApproveClick(record)} type="primary" size="small">Předat ke schválení</Button>
          }
          {
            record.status === 'approval'
            && (
              user.role === 'administrator'
              || Number(record.assigned_user) === user.id
            ) &&
            <Link to={`/approval/${record.item_type}/item/${record.key}`}>
              <Button type="primary" size="small">Zkontrolovat</Button>
            </Link>
          }
          {
            record.status === 'approval'
            && (
              user.role !== 'administrator'
              && Number(record.assigned_user) !== user.id
            ) &&
            <Button disabled type="primary" size="small">Předáno ke schválení</Button>
          }
          {
            record.status === 'accepted' &&
            <span>Schváleno</span>
          }
        </span>
      ),
    },
  ];

  const getBodyWrapper = body => body;

  return (
    <div>
      <Table
        locale={locale}
        scroll={{ x: 600 }}
        columns={columns}
        dataSource={dataSource}
        pagination={false}
        simple
        rowKey={record => record.key}
        getBodyWrapper={getBodyWrapper}
      />
    </div>
  );
}

approvalList.propTypes = {
  dataSource: PropTypes.array,
  itemUpdate: PropTypes.func,
  moment: PropTypes.func,
  user: PropTypes.object,
};

export default approvalList;
