import React from 'react';
import PropTypes from 'prop-types';
import { Form, DatePicker, Input, Select, Button } from 'antd';
import moment from 'moment';

class AssetApprovalForm extends React.Component {

  state = {
    changed: false,
  };

  handleSubmit = (e) => {
    e.preventDefault();

    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }

      // Should format date value before submit.
      const rangeValue = fieldsValue.date_plan;
      const rangeValue2 = fieldsValue.date_real;
      const values = {
        ...fieldsValue,
        date_plan: [rangeValue[0].format('YYYY-MM-DD'), rangeValue[1].format('YYYY-MM-DD')],
        date_real: [rangeValue2 ? rangeValue2[0].format('YYYY-MM-DD') : null, rangeValue2 ? rangeValue2[1].format('YYYY-MM-DD') : null],
      };

      values.category = values.category === 'null' ? null : values.category;

      console.log('values', values);

      let dataObject = {};

      if (this.props.type === 'update') {
        dataObject = this.props.data;
      } else {
        dataObject.status = 'inProgress';
      }

      dataObject.type = 'item';
      dataObject.key_parents = values.category;
      dataObject.private = values.private;
      dataObject.title = values.title;
      dataObject.number = values.number;
      dataObject.assigned_user = values.assigned_user;
      dataObject.date_plan_from = values.date_plan[0];
      dataObject.date_plan_to = values.date_plan[1];
      dataObject.date_real_from = values.date_real[0];
      dataObject.date_real_to = values.date_real[1];
      dataObject.description = values.description;

      console.log('dataObject', dataObject);

      if (this.props.type === 'create') {
        this.props.cbCreate(dataObject);
      }

      if (this.props.type === 'update') {
        this.props.cbUpdate(dataObject);
      }

      this.setState({
        changed: false,
      });
    });
  };

  handleFormChange = (changedFields) => {
    // console.log('changedFields', changedFields);

    this.setState({
      changed: true,
    });
  }

  render() {
    const { dataAssets, dataUsers } = this.props;
    const { getFieldDecorator } = this.props.form;
    let { data } = this.props;

    console.log('this.props.type', this.props.type)


    let rangePlanConfig = {
      rules: [{ type: 'array', required: true, message: 'Vyberte datum.' }],
    };
    let rangeRealConfig = {
      rules: [{ type: 'array', message: 'Vyberte datum¨.' }],
    };

    if (data) {
      if (data.date_plan_from && data.date_plan_to) {
        rangePlanConfig = {
          initialValue: [moment(data.date_plan_from, 'YYYY-MM-DD'), moment(data.date_plan_to, 'YYYY-MM-DD')],
          rules: [{ type: 'array', required: true, message: 'Vyberte datum.' }],
        };
      }

      if (data.date_real_from && data.date_real_to) {
        rangeRealConfig = {
          initialValue: [moment(data.date_real_from, 'YYYY-MM-DD'), moment(data.date_real_to, 'YYYY-MM-DD')],
          rules: [{ type: 'array', message: 'Vyberte datum.' }],
        };
      }
    } else {
      data = {};
    }

    return (
      <Form
        layout="vertical"
        className="contentFormCentered"
        style={{ maxWidth: '600px', margin: '2em auto' }}
        onChange={this.handleFormChange}
        onSubmit={this.handleSubmit}
      >
        <Form.Item label="Název:">
          {getFieldDecorator('title', {
            initialValue: data.title,
            rules: [
              {
                required: true,
                message: 'Nutné vyplnit',
              },
            ],
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Kategorie:">
          {getFieldDecorator('category', {
            initialValue: data.key_parents ? data.key_parents : null,
          })(<Select
            showSearch
            optionFilterProp="children"
            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            onChange={this.handleFormChange}
          >
            <Select.Option key="null" value={null}>Bez kategorie</Select.Option>
            {
              dataAssets && dataAssets.map(item => (
                <Select.Option
                  key={item.key}
                  value={item.key_parents ? `${item.key_parents}.${item.key}` : item.key}
                >{ item.title }</Select.Option>
              ))
            }
          </Select>)}
        </Form.Item>
        <Form.Item label="Číslo záznamu:">
          {getFieldDecorator('number', {
            initialValue: data.number,
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Přiřazený uživatel:">
          {getFieldDecorator('assigned_user', {
            initialValue: data.assigned_user ? data.assigned_user : null,
          })(<Select
            showSearch
            optionFilterProp="children"
            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            onChange={this.handleFormChange}
          >
            <Select.Option key="null" value={null}>Nepřiřazeno</Select.Option>
            {
              dataUsers.map(item => (
                <Select.Option
                  key={item.id.toString()}
                  value={item.id.toString()}
                >{ `${item.firstname} ${item.lastname}` }</Select.Option>
              ))
            }
          </Select>)}
        </Form.Item>
        <Form.Item label="Plán pořízení/používání/platnosti aktiva:">
          {getFieldDecorator('date_plan', rangePlanConfig)(
            <DatePicker.RangePicker
              format="YYYY-MM-DD"
              style={{ width: '100%' }}
              onChange={this.handleFormChange}
            />,
          )}
        </Form.Item>
        <Form.Item label="Skutečnost pořízení/používání/platnosti aktiva:">
          {getFieldDecorator('date_real', rangeRealConfig)(
            <DatePicker.RangePicker
              format="YYYY-MM-DD"
              style={{ width: '100%' }}
              onChange={this.handleFormChange}
            />,
          )}
        </Form.Item>
        <Form.Item label="Popis:">
          {getFieldDecorator('description', {
            initialValue: data.description,
          })(<Input type="textarea" style={{ width: '100%', minHeight: '6em' }} />)}
        </Form.Item>
        {
          this.props.type === 'create' &&
          <Form.Item>
            <Button type="primary" htmlType="submit" size="large">Vytvořit</Button>
          </Form.Item>
        }
        {
          this.props.type === 'update' && this.state.changed &&
          <Form.Item>
            <Button type="primary" htmlType="submit" size="large">Uložit změny</Button>
          </Form.Item>
        }
      </Form>
    );
  }
}

AssetApprovalForm.propTypes = {
  form: PropTypes.object.isRequired,
  type: PropTypes.string,
  data: PropTypes.object,
  dataAssets: PropTypes.array,
  dataUsers: PropTypes.array,
  cbCreate: PropTypes.func,
  cbUpdate: PropTypes.func,
};

export default Form.create()(AssetApprovalForm);
