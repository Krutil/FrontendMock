import React from 'react';
import PropTypes from 'prop-types';
import { Steps, Button, Row, Col, Dropdown, Menu, Icon } from 'antd';
import { connect } from 'dva';
import { browserHistory } from 'dva/router';

import RiskApprovalForm from './component/RiskApprovalForm/riskApprovalForm';
import SchemaForm from '../steps/SchemaCreateUpdate/schemaCreateUpdate';
import SchemaHeat from '../../../../components/common/SchemaHeat/schemaHeat';
import TableEditable from '../../../../components/common/TableEditable/tableEditable';
import RelationshipsCreateUpdate from '../../../../components/common/form/RelationshipsCreateUpdate/relationshipsCreateUpdate';
import RelationshipsList from '../../../../components/common/relationships/RelationshipsList/relationshipsList';
import ApprovalConfirmation from '../steps/ApprovalConfirmation/approvalConfirmation';

class RisksCreateSteps extends React.Component {

  constructor(props) {
    super(props);
    const hashId = parseInt(props.location.hash.replace('#', ''));

    this.state = {
      stepCurrent: hashId ? hashId - 1 : 0,
    };
    console.log('RisksCreateSteps');
  }

  next = () => {
    const stepCurrent = this.state.stepCurrent + 1;
    this.setState({ stepCurrent });
  };

  prev = () => {
    const stepCurrent = this.state.stepCurrent - 1;
    this.setState({ stepCurrent });
  };

  submitConfirmation = (data) => {
    const risk = { ...this.props.data, status: data.status, assigned_user: data.assigned_user };
    // console.log('submitConfirmation', risk);

    const payload = {
      item: risk,
      appUser: this.props.app.user,
    };

    this.props.dispatch({
      type: 'risks/update',
      payload,
    });

    browserHistory.push('/risk/risks');
  };

  handleMenuClick = item => async (e) => {

    if (e.key === 'destroy') {
      this.props.dispatch({
        type: 'risks/schemaDestroy',
        payload: {
          id: this.props.data.id,
          schema_key: item.key,
        },
      });
    }

    await new Promise(resolve => setTimeout(resolve, 500));

    this.props.dispatch({
      type: 'approval/select',
      payload: {
        itemType: 'risks',
        itemKey: this.props.data.key,
      },
    });
  };

  schemaAdd = async (data) => {
    const dataObject = {};
    dataObject.id = this.props.data.id;
    dataObject.schema = data;
    // console.log('schemaAdd', dataObject);

    this.props.dispatch({
      type: 'risks/schemaAdd',
      payload: dataObject,
    });

    await new Promise(resolve => setTimeout(resolve, 500));

    this.props.dispatch({
      type: 'approval/select',
      payload: {
        itemType: 'risks',
        itemKey: this.props.data.key,
      },
    });
  };

  relationshipCreate = (oData, keys) => {
    const payload = {
      appUser: this.props.app.user,
      from_key: keys.from_key,
      from_key_node: keys.from_key_node,
      to_key: keys.to_key,
      itemFrom: this.props.data,
      itemTo: oData,
    };

    this.props.dispatch({
      type: 'relationships/create',
      payload,
    });
  };

  relationshipDestroy = (id) => {
    const payload = {
      id,
    };

    this.props.dispatch({
      type: 'relationships/destroy',
      payload,
    });
  };

  render() {
    const { data, requirements, assets, risks, relationships, users } = this.props;
    const { stepCurrent } = this.state;
    const relationshipData = {};

    if (requirements) {
      relationshipData.requirements = [];

      requirements.list.forEach(function (item) {
        if (item.type === 'item') {
          relationshipData.requirements.push(item);
        }
      });
    }

    if (assets) {
      relationshipData.assets = [];

      assets.list.forEach(function (item) {
        if (item.type === 'item') {
          relationshipData.assets.push(item);
        }
      });
    }

    if (risks) {
      relationshipData.risks = [];

      risks.list.forEach(function (item) {
        if (item.type !== 'folder') {
          relationshipData.risks.push(item);
        }
      });
    }

    const itemRelationships = [];

    relationships.list.forEach((item) => {
      if (item.from_key === data.key || item.to_key === data.key) {
        const oRow = {};
        oRow.id = item.id;
        oRow.created_at = item.created_at;
        oRow.user = item.user;
        oRow.item = item.from_key === data.key ? item.to_item : item.from_item;
        itemRelationships.push(oRow);
      }
    });

    const riskApprovalFormProps = {
      data: this.props.data,
      dataRisks: risks.list,
      dataUsers: users.list,
      type: 'update',
    };

    const steps = [{
      title: 'Informace',
      content: (
        <div className="contentDark">
          <RiskApprovalForm {...riskApprovalFormProps} cbUpdate={this.props.cbUpdate} />
        </div>
      ),
    }, {
      title: 'Hodnocení',
      content: (
        <div className="contentDark">
          <SchemaForm cbAdd={this.schemaAdd} />
          {
            data.schema && data.schema.map((item, index) => (
              <div key={`${item.key}_${index}`}>
                <Row
                  type="flex"
                  justify="space-between"
                  align="middle"
                  className="contentDark_separator"
                  style={{
                    padding: '1.25em 1.5em',
                    borderBottom: '1px #e4e4ef solid',
                  }}
                >
                  <Col className="flex1">
                    <h3>{item.title}</h3>
                  </Col>
                  <Col>
                    <Dropdown
                      placement="bottomRight" trigger={['click']} overlay={
                      <Menu onClick={this.handleMenuClick(item)}>
                        <Menu.Item key="destroy">Odstranit</Menu.Item>
                      </Menu>}
                    >
                      <Button
                        style={{
                          border: 'none',
                          width: '2.5em',
                          padding: '.5em 0',
                          lineHeight: '1',
                          borderRadius: '50%',
                        }}
                      >
                        <Icon type="ellipsis" style={{ fontSize: '1.5em' }} />
                      </Button>
                    </Dropdown>
                  </Col>
                </Row>
                {
                  item.schema_type === 'heat' &&
                  <SchemaHeat
                    data={item.data}
                    style={{ margin: '2em 0' }}
                    editable
                    cbUpdate={this.props.cbUpdate}
                  />
                }
                {
                  item.schema_type === 'table' &&
                  <TableEditable
                    dataColumns={item.data.dataColumns}
                    data={item.data.dataSource}
                    style={{ margin: '2em 0' }}
                    editable
                    cbUpdate={this.props.cbUpdate}
                  />
                }
              </div>
            ))
          }
        </div>
      ),
    }, {
      title: 'Vazby',
      content: (
        <div className="contentDark">
          <RelationshipsCreateUpdate
            dataSelect={relationshipData}
            currentItem={data}
            cbCreate={this.relationshipCreate}
          />
          {
            relationships &&
            <RelationshipsList
              editable
              data={itemRelationships}
              cbDestroy={this.relationshipDestroy}
            />
          }
        </div>
      ),
    }, {
      title: 'Potvrzení',
      content: (
        <div className="contentDark">
          <ApprovalConfirmation
            cbUpdate={this.submitConfirmation}
            data={data}
            dataUsers={users.list}
          />
        </div>
      ),
    }];

    return (
      <div>
        <Steps className="steps-action" style={{ padding: '1rem 1.25rem' }} current={stepCurrent}>
          {steps.map(item => <Steps.Step key={item.title} title={item.title} />)}
        </Steps>
        <div className="steps-content">{steps[stepCurrent].content}</div>
        <div
          className="steps-action" style={{
          borderTop: '1px #e4e4ef solid',
          padding: '1em',
          textAlign: 'right',
          backgroundColor: '#fff',
        }}
        >
          {
            stepCurrent > 0
            &&
            <Button type="ghost" style={{ marginRight: '1em' }} onClick={this.prev}>Předchozí
              krok</Button>
          }
          {
            stepCurrent < steps.length - 1
            &&
            <Button type="primary" onClick={this.next}>Další krok</Button>
          }
          {/* {
           stepCurrent === steps.length - 1
           &&
           <Button type="primary" onClick={this.handleSubmit}>Potvrdit</Button>
           }*/}
        </div>
      </div>
    );
  }
}

RisksCreateSteps.propTypes = {
  data: PropTypes.object,
  requirements: PropTypes.object,
};


function mapStateToProps(state) {
  return {
    app: state.app,
    risks: state.risks,
    loadingModelRisks: state.loading.models.risks,
    loadingModelRelationships: state.loading.models.relationships,
    loadingModelRequirements: state.loading.models.requirements,
    relationships: state.relationships,
    requirements: state.requirements,
    assets: state.assets,
    users: state.users,
  };
}


export default connect(mapStateToProps)(RisksCreateSteps);
