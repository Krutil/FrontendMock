import React from 'react';
import PropTypes from 'prop-types';
import {
  Form,
  Select,
  Button,
} from 'antd';

class ApprovalConfirmation extends React.Component {

  handleSubmit = (e) => {
    e.preventDefault();

    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }

      const values = {
        ...fieldsValue,
      };

      this.props.cbUpdate(values);
    });
  };

  render() {
    const { data, dataUsers } = this.props;
    const { getFieldDecorator } = this.props.form;
    const optionsItems = [];

    console.log('this.props',this.props);

    dataUsers.forEach(function (item) {
      if (dataUsers.length > 0) {
        optionsItems.push(
          <Select.Option
            key={item.id.toString()}
            value={item.id.toString()}
          >
            { `${item.firstname} ${item.lastname}` }
          </Select.Option>,
        );
      }
    });

    return (
      <div>
        <Form
          layout="vertical"
          style={{ maxWidth: '600px', margin: '2em auto' }}
          onSubmit={this.handleSubmit}
        >
          <Form.Item label="Status:">
            {
              getFieldDecorator('status', {
                rules: [
                  {
                    required: true,
                    message: 'Prosím vyberte hodnotu.',
                  },
                ],
              })(
                <Select placeholder="Vyberte status">
                  <Select.Option key="accepted" value="accepted">Schváleno</Select.Option>
                  <Select.Option key="returned" value="returned">Vráceno</Select.Option>
                  <Select.Option key="finished" value="finished">Ukončeno</Select.Option>
                </Select>,
              )
            }
          </Form.Item>
          <Form.Item label="Přiřadit uživateli:">
            {
              getFieldDecorator('assigned_user', {
                initialValue: data.assigned_user,
              })(
                <Select placeholder="Vyberte status">
                  <Select.Option key="null" value={null}>Nepřiřazeno</Select.Option>
                  {optionsItems}
                </Select>,
              )
            }
          </Form.Item>
          <Form.Item>
            {
              data.status === 'approval' &&
              <Button type="primary" htmlType="submit">Schválit</Button>
            }
            {
              data.status !== 'approval' &&
              <Button type="primary" htmlType="submit">Upravit a dokončit</Button>
            }
          </Form.Item>
        </Form>
      </div>
    );
  }
}

ApprovalConfirmation.propTypes = {
  cbUpdate: PropTypes.func.isRequired,
  data: PropTypes.object,
  dataUsers: PropTypes.array,
  form: PropTypes.object,
};

export default Form.create()(ApprovalConfirmation);
