import React from 'react';
import PropTypes from 'prop-types';
import { Form, Input, Select } from 'antd';
import SchemaHeat from '../../../../../components/common/SchemaHeat/schemaHeat';
import TableEditable from '../../../../../components/common/TableEditable/tableEditable';

const dataHeat = [
  {
    title: 'Náklady',
    value: 10,
    selected: null,
    steps: ['Null', 'Low', 'Minor', 'Moderate', 'Significant', 'High'],
  },
  {
    title: 'Čas',
    value: 10,
    selected: null,
    steps: ['Null', 'Low', 'Minor', 'Moderate', 'Significant', 'High'],
  },
  {
    title: 'Výkonnost',
    value: 10,
    selected: null,
    steps: ['Null', 'Low', 'Minor', 'Moderate', 'Significant', 'High'],
  },
  {
    title: 'Pravděpodobnost',
    value: 10,
    selected: null,
    steps: ['Null', 'Low', 'Minor', 'Moderate', 'Significant', 'High'],
  },
];

const dataTableColumns = [
  {
    key: 'c1',
    title: 'Název',
    dataIndex: 'title',
  }, {
    key: 'c2',
    title: 'Hodnota',
    dataIndex: 'value',
  }, {
    key: 'c3',
    title: 'MJ',
    dataIndex: 'unit',
  }, {
    key: 'c4',
    title: 'Vztaženo k',
    dataIndex: 'period',
  }, {
    key: 'c5',
    title: 'Platnost',
    dataIndex: 'date',
  }, {
    key: 'c6',
    title: 'Aktivní',
    dataIndex: 'status',
  },
];

const dataTable = [{
  key: '1',
  title: 'Účetní odpis. hodnota',
  value: '150 000',
  unit: 'Kč',
  period: 'měsíc',
  date: '01/2018 - 01/2020',
  status: 'Aktivní',
}, {
  key: '2',
  title: 'Hodnota výpadku',
  value: '1 500 000',
  unit: 'Kč',
  period: 'měsíc',
  date: '01/2018 - 01/2020',
  status: 'Aktivní',
}, {
  key: '3',
  title: 'Pořizovací hodnota',
  value: '250 000',
  unit: 'Kč',
  period: 'měsíc',
  date: '01/2018 - 01/2020',
  status: '',
}, {
  key: '4',
  title: 'Údržba',
  value: '1 500',
  unit: 'Kč',
  period: 'měsíc',
  date: '01/2018 - 01/2020',
  status: 'Aktivní',
}];

class SchemaCreateUpdate extends React.Component {

  state = {
    changed: false,
  };

  render() {
    const { data } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <div>
        <Form
          layout="vertical"
          style={{ maxWidth: '600px', margin: '2em auto 3.5em' }}
          onChange={this.handleFormChange}
        >
          <Form.Item label="Schéma hodnocení:">
            {getFieldDecorator('schema_type', {
              initialValue: 'null',
              rules: [
                {
                  required: true,
                  message: 'Nutné vyplnit',
                },
              ],
            })(<Select onChange={this.handleChange}>
              <Select.Option key="null" value="null">Vyberte schéma</Select.Option>
              <Select.Option key="heat" value="heat">Heat mapa</Select.Option>
              <Select.Option key="table" value="table">Finanční tabulka</Select.Option>
            </Select>)}
          </Form.Item>
          {
            this.state.schema === 'heat' &&
            <Form.Item label="Název:">
              {getFieldDecorator('title', {
                initialValue: data ? data.title : '',
                rules: [
                  {
                    required: true,
                    message: 'Nutné vyplnit',
                  },
                ],
              })(<Input size="default" />)}
            </Form.Item>
          }
        </Form>
        {
          this.state.schema === 'heat' &&
          <SchemaHeat
            type="add"
            style={{ margin: '2em 0' }}
            data={dataHeat}
            editable
            cbAdd={this.schemaAdd}
          />
        }
        {
          this.state.schema === 'table' &&
          <TableEditable
            type="add"
            data={dataTable}
            dataColumns={dataTableColumns}
            style={{ margin: '2em 0' }}
            editable
            cbAdd={this.schemaAdd}
          />
        }
      </div>
    );
  }

  handleChange = (value) => {
    this.setState({
      schema: value,
    });
  };

  handleFormChange = (changedFields) => {
    this.setState({
      changed: true,
    });
  };

  schemaAdd = (data) => {
    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }

      const values = {
        ...fieldsValue,
      };

      // let dataObject = this.props.form.getFieldsValue();
      const dataObject = values;
      dataObject.data = data;

      if (dataObject.schema_type === 'table') {
        dataObject.title = 'Finanční tabulka';
      }
      this.props.cbAdd(dataObject);
      this.props.form.resetFields();
      this.setState({
        schema: undefined,
      });
    });
  };
}

SchemaCreateUpdate.propTypes = {
  form: PropTypes.object,
  type: PropTypes.string,
  data: PropTypes.object,
};

export default Form.create()(SchemaCreateUpdate);
