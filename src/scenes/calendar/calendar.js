import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Calendar, Tooltip, Icon, Select } from 'antd';
import { browserHistory, Link } from 'dva/router';
import moment from 'moment';
import 'moment/locale/cs';

import stylesApp from '../app.less';
import styles from './calendar.less';

moment.locale('cs');

class CalendarContainer extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      dataTree: [],
      selectedKeys: [],
      expandedKeys: [],
      searchValue: '',
      autoExpandParent: true,
      selectedType: 'all',
      selectedStatus: 'all',
      nodeInfo: {
        key: null,
        type: null,
      },
    };

    this.statusesToTypesMap = {
      accepted: 'normal',
      approval: 'normal',
      inProgress: 'warning',
      deleted: 'error',
    };

    this.itemsByDate = {};
    this.itemsByMonth = {};
  }

  getListData = (value) => {
    const { selectedType, selectedStatus } = this.state;
    const listData = this.itemsByDate[this.tokenizeDate(value)];
    return listData
      ? listData.filter(item => this.isItemTypeStatus(item, selectedType, selectedStatus))
      : [];
  };

  getMonthData = (value) => {
    const { selectedType, selectedStatus } = this.state;
    // TODO real num of tasks
    const listData = this.itemsByMonth[this.tokenizeMonth(value)];
    if (listData) {
      listData.filter(item => this.isItemTypeStatus(item, selectedType, selectedStatus));
      return listData.length;
    }
    return 0;
  };

  isItemTypeStatus = (item, type, status) => {
    return (item.item_type === type || type === 'all')  && (item.status === status || status === 'all');
  };

  statusToType = (status) => {
    const type = this.statusesToTypesMap[status];
    return type || 'normal';
  };

  linkResolver = item => (<Link style={{ color: '#bbbbc4' }} to={`/risk/${item.item_type}/item/${item.key}`} >{item.content}<Icon style={{ margin: '5px' }} type="link" /></Link>);

  dateCellRender = (value: Date):string => {
    const listData = this.getListData(value);
    return (
      <ul className="events">
        {
          listData.map(item => (
            <li className="event" key={`${item.content}-${item.id}-${item.type}`}>
              <Tooltip title={this.linkResolver(item)} trigger="click">
                <span className={`eventBullet event-${item.type}`}>●</span>
                {item.content}
              </Tooltip>
            </li>
          ))
        }
      </ul>
    );
  };

  monthCellRender = (value: Date):string => {
    const num = this.getMonthData(value);
    return num ?
      <div className="notes-month">
        <section>{num}</section>
        <span>úkolů</span>
      </div> : null;
  };


  typeSelectedHandler = (type) => {
    this.setState({ selectedType: type });
  };

  statusSelectedHandler = (status) => {
    this.setState({ selectedStatus: status });
  };

  tokenizeDate = date => moment(date).format('MM/DD/YYYY');
  tokenizeMonth = date => moment(date).format('MM/YYYY');

  parseData = (data, type) => {
    // TODO: perf optimize

    data.forEach((item) => {
      item.content = item.title;
      item.type = this.statusToType(item.status);
      item.item_type = type;
      const dateToken = this.tokenizeDate(item.created_at);
      if (!this.itemsByDate[dateToken]) {
        this.itemsByDate[dateToken] = [];
      }
      this.itemsByDate[dateToken].push(item);

      const monthToken = this.tokenizeMonth(item.created_at);
      if (!this.itemsByMonth[monthToken]) {
        this.itemsByMonth[monthToken] = [];
      }
      this.itemsByMonth[monthToken].push(item);
    });
  }

  render() {
    const { requirements, risks, assets } = this.props;
    console.log("styles");
    const filterTypeOptions = [
      { value: 'all', label: 'Všechny' },
      { value: 'requirements', label: 'Požadavky' },
      { value: 'risks', label: 'Rizika' },
      { value: 'assets', label: 'Aktiva' },
    ].map(item => <Select.Option value={item.value}>{item.label}</Select.Option>);

    const filterStatusOptions = [
      { value: 'all', label: 'Všechny' },
      { value: 'accepted', label: 'Schváleno' },
      { value: 'approval', label: 'Schvaluje se' },
      { value: 'inProgress', label: 'Připravuje se' },
      { value: 'deleted', label: 'Odstraněno' },
    ].map(item => <Select.Option value={item.value}>{item.label}</Select.Option>);

    this.itemsByDate = {};
    this.itemsByMonth = {};
    this.parseData(requirements.list, 'requirements');
    this.parseData(risks.list, 'risks');
    this.parseData(assets.list, 'assets');

    return (
      <div className="container is-block">
        <div className="section">
          <div className="section_content">
            <div className="section_title">
              <h2>Kalendář</h2>
            </div>
            {/*FILTER*/}
            <div className="filter">
              <span className="label">Typ</span>
              <Select onSelect={this.typeSelectedHandler} value={this.state.selectedType}>
                {filterTypeOptions}
              </Select>
              <span className="label right">Stav</span>
              <Select className="status-select" onSelect={this.statusSelectedHandler} value={this.state.selectedStatus}>
                {filterStatusOptions}
              </Select>
            </div>
            <Calendar dateCellRender={this.dateCellRender} monthCellRender={this.monthCellRender} />
          </div>
        </div>
        <div className="spaceM" />
      </div>
    );
  }
}

CalendarContainer.propTypes = {
  users: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
  requirements: PropTypes.object,
  assets: PropTypes.object,
  risks: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    loadingModelUsers: state.loading.models.users,
    users: state.users,
    app: state.app,
    requirements: state.requirements,
    assets: state.assets,
    risks: state.risks,
  };
}

export default connect(mapStateToProps)(CalendarContainer);
