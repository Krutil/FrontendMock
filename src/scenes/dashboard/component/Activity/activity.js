import React from 'react';
import PropTypes from 'prop-types';
import { Table, Tag } from 'antd';
import moment from 'moment';
import 'moment/locale/cs';

import { color } from '../../../../utils';

moment.locale('cs');

const activityType = {
  create: {
    color: color.blue,
    text: 'vytvořeno',
  },
  deleted: {
    color: color.red,
    text: 'smazáno',
  },
  mention: {
    color: color.yellow,
    text: 'zmínka',
  },
  accepted: {
    color: color.green,
    text: 'Schváleno',
  },
  approval: {
    color: color.yellow,
    text: 'Schvaluje se',
  },
  inProgress: {
    color: color.yellow,
    text: 'Připravuje se',
  },
  rejected: {
    color: color.red,
    text: 'Zamítnuto',
  },
  update: {
    color: color.green,
    text: 'Upraveno',
  },
};

const itemType = {
  risks: {
    color: color.blue,
    text: 'Riziko',
  },
  assets: {
    color: color.blue,
    text: 'Aktivum',
  },
  requirements: {
    color: color.blue,
    text: 'Požadavek',
  },
};

function Activity({ data }) {
  const columns = [
    {
      title: 'Název',
      dataIndex: 'item_title',
    }, {
      title: 'aktivita',
      dataIndex: 'activity_type',
      render: text => (
        <span>
          {
            activityType[text] &&
            <Tag color={activityType[text].color}>{activityType[text].text}</Tag>
          }
        </span>
      ),
    }, {
      title: 'typ',
      dataIndex: 'item_type',
      render: text => <span>{itemType[text].text}</span>,
    }, {
      title: 'datum',
      dataIndex: 'created_at',
      className: 'u_noWrap',
      render: text => moment(new Date(text).format('yyyy-MM-dd')).format('LL'),
    },
  ];
  return (
    <div>
      <h3>Aktivita</h3>
      <br />
      <Table
        pagination={false}
        columns={columns}
        rowKey={(record, key) => key}
        dataSource={data.filter((item, key) => key < 5)}
      />
    </div>
  );
}

Activity.propTypes = {
  data: PropTypes.array,
};

export default Activity;
