import React from 'react';
import PropTypes from 'prop-types';
import {
  Row,
  Col,
  Tooltip,
} from 'antd';
import moment from 'moment';
import 'moment/locale/cs';

import styles from './comments.less';

moment.locale('cs');

function Comments({ data }) {

  const renderUser = item => (<span>{item.user.firstname} {item.user.lastname}</span>);
  const renderText = item => (<p>{item.text}</p>);

  const rows = data.map((item, index) => (
    <div
      key={item.key}
      className={styles.comment}
    >
      <Row type="flex" justify="start">
        <Col
          className={styles.comment_avatar}
          style={{ backgroundImage: `url(${item.user.avatar})` }}
        />
        <Col className={styles.comment_content}>
          <Row type="flex" justify="space-between" align="middle">
            <Col className="flex1">
              <h5>
                {renderUser(item)}
              </h5>
            </Col>
            <Col className={styles.comment_date}>
              <Tooltip title={moment(item.created_at).format('LLL')}>
                {moment(item.created_at).format('LT')}
              </Tooltip>
            </Col>
          </Row>
          {renderText(item)}
        </Col>
      </Row>
    </div>
  ), this);
  return (
    <div className={styles.comments}>
      <h3 className={styles.title}>Komentáře</h3>
      <br />
      { rows }
    </div>
  );
}

Comments.propTypes = {
  data: PropTypes.array,
};

export default Comments;
