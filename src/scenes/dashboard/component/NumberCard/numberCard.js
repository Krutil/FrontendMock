import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'dva/router';
import { Icon, Card } from 'antd';
import CountUp from 'react-countup';
import styles from './numberCard.less';

function NumberCard({ icon, color, title, url, number, numberInProgress, countUp }) {
  const redirect = (link, approval) => () => {

    if (!isNaN(approval) && approval > 0) {
      browserHistory.push('/approval');
    } else {
      browserHistory.push(url);
    }
  };

  return (
    <Card
      className={styles.numberCard} bordered={false} bodyStyle={{ padding: 0 }}
      onClick={redirect(url, numberInProgress)}
    >
      <Icon className={styles.iconWarp} style={{ color }} type={icon} />
      <div className={styles.content}>
        <p className={styles.title}>{title || 'No Title'}</p>
        <p className={styles.number}>
          {
            !isNaN(numberInProgress) && numberInProgress > 0 &&
            <span>
              <CountUp
                start={0}
                end={numberInProgress}
                duration={2.75}
                useEasing
                useGrouping
                separator=","
                {...countUp || {}}
              />
              <span className="separatorTitle">/</span>
            </span>
          }
          <CountUp
            start={0}
            end={number}
            duration={2.75}
            useEasing
            useGrouping
            separator=","
            {...countUp || {}}
          />
        </p>
      </div>
    </Card>
  );
}

NumberCard.propTypes = {
  icon: PropTypes.string,
  color: PropTypes.string,
  title: PropTypes.string,
  url: PropTypes.string,
  number: PropTypes.number,
  numberInProgress: PropTypes.number,
  countUp: PropTypes.object,
};

export default NumberCard;
