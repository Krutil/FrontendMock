import React from 'react';
import PropTypes from 'prop-types';
import { Table, Tag } from 'antd';
import { color } from '../../../../utils/index';

const category = {
  requirements: {
    text: 'Požadavek',
  },
  assets: {
    text: 'Aktivum',
  },
  risks: {
    text: 'Riziko',
  },
};

const status = {
  accepted: {
    color: color.green,
    text: 'Schváleno',
  },
  inProgress: {
    color: color.yellow,
    text: 'Připravuje se',
  },
  rejected: {
    color: color.red,
    text: 'Zamítnuto',
  },
};

function RecentAssetsRisks({ data }) {
  const columns = [
    {
      title: 'Název',
      dataIndex: 'title',
    }, {
      title: 'Kategorie',
      dataIndex: 'item_type',
      render: text => <span>{category[text] && category[text].text}</span>,
    }, {
      title: 'ID',
      dataIndex: 'key',
      render: text => <span >{text}</span>,
    }, {
      title: 'Stav',
      dataIndex: 'status',
      render: text => <Tag color={status[text].color}>{status[text].text}</Tag>,
    }, {
      title: 'Datum',
      dataIndex: 'created_at',
      render: text => new Date(text).format('yyyy-MM-dd'),
    },
  ];
  return (
    <div>
      <h3>Nejnovější záznamy</h3>
      <br />
      <Table
        pagination={false}
        columns={columns}
        rowKey={(record, key) => key}
        dataSource={data.filter((item, key) => key < 5)}
      />
    </div>
  );
}

RecentAssetsRisks.propTypes = {
  data: PropTypes.array,
};

export default RecentAssetsRisks;
