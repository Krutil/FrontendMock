import React from 'react';
import PropTypes from 'prop-types';
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from 'recharts';
import moment from 'moment';
import 'moment/locale/cs';

import styles from './tasks.less';
import { color } from '../../../../utils/index';

moment.locale('cs');

function Tasks({ data }) {
  const keyToLabel = {
    'Tasks completed': 'Úkoly dokončené',
    'Tasks created': 'Úkoly vytvořené',
    'Tasks total': 'Úkoly celkem',
  };

  data.map((item, index) => {
    item.date = moment().subtract(index, 'days').format('LL');
    return item;
  });

  return (
    <div className={styles.sales}>
      <h3 className={styles.title}>Úkoly</h3>
      <ResponsiveContainer minHeight={360}>

        <BarChart data={data} maxBarSize={55}>
          <XAxis
            dataKey="date" axisLine={{ stroke: color.borderBase, strokeWidth: 1 }}
            tickLine={false}
          />
          <YAxis axisLine={false} tickLine={false} />
          <CartesianGrid strokeDasharray="4 4" />
          <Legend
            verticalAlign="top"
            content={(props) => {
              const { payload } = props;
              return (<ul className={`${styles.legend} clearfix`}>
                {payload.map((item, key) => (<li key={key}>
                  <span className={styles.radiusdot} style={{ background: item.color }} />{keyToLabel[item.value]}
                </li>)) }
              </ul>);
            }}
          />
          <Tooltip
            wrapperStyle={{ border: 'none', boxShadow: '4px 4px 40px rgba(0, 0, 0, 0.05)' }}
            content={(content) => {
              const list = content.payload.map((item, key) => (<li
                key={key}
                className={styles.tipitem}
              >
                <span
                  className={styles.radiusdot}
                  style={{ background: item.color }}
                />{`${keyToLabel[item.name]}:${item.value}`}
              </li>));
              return (
                <div className={styles.tooltip}><p className={styles.tiptitle}>{content.label}</p>
                  <ul>{list}</ul>
                </div>);
            }}
          />
          <Bar
            type="monotone" dataKey="Tasks total" stackId="a" stroke={color.purple} fill={color.purple}
            strokeWidth={2} dot={{ fill: '#fff' }} activeDot={{
              r: 5,
              fill: '#fff',
              stroke: color.blue,
            }}
          />
          <Bar
            type="monotone" dataKey="Tasks completed" stackId="a" stroke={color.grass} fill={color.grass}
            strokeWidth={2} dot={{ fill: '#fff' }} activeDot={{
              r: 5,
              fill: '#fff',
              stroke: color.green,
            }}
          />
          <Bar
            type="monotone" dataKey="Tasks created" stackId="a" stroke={color.sky} fill={color.sky}
            strokeWidth={2} dot={{ fill: '#fff' }} activeDot={{
              r: 5,
              fill: '#fff',
              stroke: color.blue,
            }}
          />

        </BarChart>
      </ResponsiveContainer>
    </div>
  );
}

Tasks.propTypes = {
  data: PropTypes.array,
};

export default Tasks;
