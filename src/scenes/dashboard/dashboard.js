import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Row, Col, Card } from 'antd';
import moment from 'moment';
import NumberCard from './component/NumberCard/numberCard';
import RecentAssetsRisks from './component/RecentAssetsRisks/recentAssetsRisks';
import Tasks from './component/Tasks/tasks';
import Comments from './component/Comments/comments';
import Activity from './component/Activity/activity';

const bodyStyle = {
  bodyStyle: {
    height: 500,
  },
};

function dashboard({ app, dashboard, comments, requirements, assets, risks, activity }) {
  const { tasks } = dashboard;

  //TODO: make sure comments are sorted by created_at, DESC (BE, FE ?)
  comments.list = comments.list.slice(comments.list.length - 4);

  let status = 'inProgress';

  if (app.user.role === 'administrator' || app.user.role === 'riskManager') {
    status = 'approval';
  }

  const assetsInProgress = assets.list.filter(asset => asset.status === status).length;
  const risksInProgress = risks.list.filter(risk => risk.status === status).length;
  const risksAssets = assets.list.concat(risks.list);

  risksAssets.sort((a, b) => {
    // Turn your strings into dates, and then subtract them
    // to get a value that is either negative, positive, or zero.
    return moment(b.created_at).toDate() - moment(a.created_at).toDate();
  });

  const recentAssetsRisks = risksAssets.slice(risksAssets.length - 5);

  const numbers = [
    {
      icon: 'file-text',
      color: '#9adc53',
      title: 'Požadavky',
      url: 'risk/requirements',
      number: requirements.list.length,
    }, {
      icon: 'book',
      color: '#9adc53',
      title: 'Aktiva',
      url: 'risk/assets',
      number: assets.list.length,
      numberInProgress: assetsInProgress,
    }, {
      icon: 'exception',
      color: '#9adc53',
      title: 'Rizika',
      url: 'risk/risks',
      number: risks.list.length,
      numberInProgress: risksInProgress,
    }, {
      icon: 'share-alt',
      color: '#9adc53',
      title: 'Opatření',
      url: 'risk/precaution',
      number: 0,
    },
  ];

  const numberCards = numbers.map((item, key) => (
    <Col key={key} lg={6} md={12}>
      <NumberCard {...item} />
    </Col>
  ));

  return (
    <div className="container is-block">
      <div className="section">
        <Row gutter={24}>
          {numberCards}
          <Col lg={24} md={24}>
            <Card bordered={false} className="spaceM">
              <Tasks data={tasks} />
            </Card>
          </Col>
          <Col lg={24} md={24} className="spaceM">
            <Card bordered={false} {...bodyStyle}>
              <RecentAssetsRisks data={recentAssetsRisks} />
            </Card>
          </Col>
          <Col lg={12} md={24} className="spaceM">
            <Card bordered={false} {...bodyStyle}>
              <Comments data={comments.list} />
            </Card>
          </Col>
          <Col lg={12} md={24} className="spaceM">
            <Card bordered={false} {...bodyStyle}>
              <Activity data={activity.list} />
            </Card>
          </Col>
        </Row>
      </div>
    </div>
  );
}

dashboard.propTypes = {
  dashboard: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    app: state.app,
    dashboard: state.dashboard,
    requirements: state.requirements,
    risks: state.risks,
    loadingModelRisks: state.loading.models.risks,
    assets: state.assets,
    loadingModelAssets: state.loading.models.assets,
    relationships: state.relationships,
    loadingModelRelationships: state.loading.models.relationships,
    comments: state.comments,
    loadingModelComments: state.loading.models.comments,
    files: state.files,
    loadingModelFiles: state.loading.models.files,
    spectators: state.spectators,
    loadingModelSpectators: state.loading.models.spectators,
    users: state.users,
    loadingModelUsers: state.loading.models.users,
    history: state.history,
    activity: state.activity,
  };
}

export default connect(mapStateToProps)(dashboard);
