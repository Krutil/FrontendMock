import React from 'react';
import { Icon } from 'antd';
import styles from './error.less';

const Error = () => (
  <div className="section">
    <div className="section_content">
      <div className={styles.error}>
        <Icon type="frown-o" />
        <h1>404</h1>
        <p>Stránka nebyla nalezena</p>
      </div>
    </div>
  </div>
);

export default Error;
