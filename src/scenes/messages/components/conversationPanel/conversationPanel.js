// @flow
import React from 'react';
import { Row, Col, Input } from 'antd';
import ConversationShortCut from '../conversationShortCut/conversationShortCut';
import { removeDiacritics } from '../../../../utils/helper';
import styles from './conversationPanel.less';
import Mock from 'mockjs';
import type { Conversation, App } from '../../messagesTypes';

type conversationPanelPropTypes = {
  conversations: Array<Conversation>,
  app: App,
  dispatch: Function,
  handleSearch: Function,
  searchValue: string,
  params: Object,
  users: Object,
}

const conversationPanel = (
  {
    conversations,
    app,
    dispatch,
    users,
    handleSearch,
    searchValue,
    params,
  } : conversationPanelPropTypes) => (
    <div className={styles.conversations}>
      <div className={`content ${styles.tree_search}`}>
        <Input.Search
          placeholder="Search"
          onChange={handleSearch}
          value={searchValue}
        />
      </div>
      <Row type="flex" className={`${styles.conversationList} u_overflowAuto`}>
        {
          Object.keys(users)
          &&
          Object.keys(users)
            .filter(oUserKey => users[oUserKey].id !== app.user.id)
            .filter((oUserKey) => {
              const oUser = users[oUserKey];

              if (
                (!removeDiacritics(`${oUser.firstname.toLowerCase()} ${oUser.lastname.toLowerCase()}`)
                    .includes(searchValue)
                )
              ) {
                return false;
              }
              return true;
            })
            .map((oUserKey) => {
              const oUser = users[oUserKey];
              const oConversation = conversations
                &&
                conversations.filter(oConversation =>
                  oConversation.participants.includes(oUser.id)
                  && oConversation.participants.includes(app.user.id)
                )[0];

              if (oConversation) {
                oUser.lastMessage = oConversation.messages[oConversation.messages.length - 1];

                if (oUser.lastMessage) {
                  oUser.priority = 1;
                } else {
                  oUser.priority = 0;
                }
                return oUser;
              }
              oUser.priority = 0;
              return oUser;
            })
            .sort((oUserA, oUserB) => {
              if (oUserA.priority === 1 || oUserB.priority === 1) {

                if (oUserA.priority === 1 && oUserB.priority === 1) {
                  return new Date(oUserB.lastMessage.createdAt) - new Date(oUserA.lastMessage.createdAt);
                }
                return oUserB.priority - oUserA.priority;
              }

              if (oUserA.priority === 0 && oUserB.priority === 0) {
                if (oUserA.lastname < oUserB.lastname) return -1;

                if (oUserA.lastname > oUserB.lastname) return 1;
                return 0;
              }
              return oUserA.priority - oUserB.priority;
            })
            .map((oUser, i) => {
              const oConversation = conversations
                &&
                conversations.filter((oConversation) =>
                  oConversation.participants.includes(oUser.id)
                  &&
                  oConversation.participants.includes(app.user.id),
                )[0];
              const active = params.id === `@${oUser.username.split('.').join('')}`;
              return (
                <Row key={`conversations-${i}`} className={`${styles.conversationList_item} ${params.id === '@'+oUser.username.split('.').join('') ? 'is-active' : ''}`}>
                  <Col>
                    <ConversationShortCut
                      active={active}
                      oRecipient={oUser}
                      oLastMessage={
                        oConversation !== undefined ? oConversation.messages[oConversation.messages.length - 1] : { messageBody: '' }
                      }
                      conversationId={oConversation && oConversation.conversationId}
                    />
                  </Col>
                </Row>
              );
            })
        }
      </Row>
    </div>
);

export default conversationPanel;
