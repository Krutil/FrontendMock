// @flow
import moment from 'moment';
import 'moment/locale/cs';
import React from 'react';
import {
  Link,
} from 'dva/router';
import {
  Row,
  Col,
} from 'antd';
import styles from './conversationShortCut.less';
import type {
  User,
  Message,
} from '../../messagesTypes';

import Avatar from '../../../../components/common/users/Avatar/avatar';

type conversationShortCutPropTypes = {
  oLastMessage: Message,
  oRecipient: User,
}

const conversationShortCut = (
  {
    oLastMessage,
    oRecipient,
  } : conversationShortCutPropTypes) => (
    <Link
      style={{ textDecoration: 'none' }}
      to={`/messages/@${oRecipient.username.split('.').join('')}`}
    >
      <Row
        type="flex"
        justify="start"
        className={`${styles['conversation-short-cut']}`}
      >
        <Col>
          <Avatar size="m" src={oRecipient.avatar} />
        </Col>
        <Row type="flex" justify="center" className={styles.contentRight}>
          <div className={styles['conversation-user-fullname']}>
            {oRecipient && `${oRecipient.firstname} ${oRecipient.lastname}`}
          </div>
          <div className={styles['date-last-message']}>
            {oLastMessage && oLastMessage.createdAt && moment(oLastMessage.createdAt).calendar()}
          </div>
          <div className={styles['conversation-message-body']}>
            {oLastMessage && oLastMessage.messageBody && `${oLastMessage.messageBody.substring(0, 20)} ...`}
          </div>
        </Row>
      </Row>
    </Link>
);

export default conversationShortCut;
