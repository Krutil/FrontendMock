/* @flow */
import React from 'react';
import type { Conversation, App } from '../../messagesTypes';

type MessageViewedProps = {
  app: App,
  dispatch: Function,
  oConversation: Conversation,
  params: Object,
  children: any,
}

class MessageViewed extends React.Component {
  componentDidUpdate() {
    const {
        app,
        dispatch,
        oConversation,
        params,
      } = this.props;

    if (params.id && oConversation !== undefined) {
      oConversation.messages.forEach((oMessage) => {
        if (oMessage.viewedAt === undefined && oMessage.senderId !== app.user.id) {
          dispatch({
            type: 'messages/messageViewed',
            payload: {
              conversationId: oConversation.conversationId,
            },
          });
        }
      });
    }
  }
  props: MessageViewedProps;
  render() {
    const { children } = this.props;
    return (
      <div className="u_heightFull">
        {children}
      </div>
    );
  }
}

export default MessageViewed;
