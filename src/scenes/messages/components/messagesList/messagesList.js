// @flow
import React from 'react';
import {
  Row,
  Col,
  Mention,
  Form,
  Button,
  Menu,
  Dropdown,
} from 'antd';
import { connect } from 'dva';
import moment from 'moment';

import styles from './messagesList.less';
import type {
  User,
  Message,
} from '../../messagesTypes';

const { toEditorState, toString } = Mention;


type MessageData = {
  text: string,
  id: string,
  user: User,
  viewedAt: string,
  created_at: string,
  conversationUser: bool,
}

type SuggestionType = {
  name: string,
  value: string,
}

type MessagesListProps = {
  data: Array<MessageData>,
  create: bool,
  users: Array<User>,
  appUser: User,
  cbCreate: Function,
  title: string,
  cbDestroy: Function,
  form: Object,
  listKey: string,
  mentionItems: Array<SuggestionType>,
  listRowKey: string,
}


type MessagesState = {
  suggestions: [SuggestionType],
  initValue: Object,
  submitDisabled: boolean,
  createText: string,
  submitDisabled: boolean,
}


class MessagesList extends React.Component {
  constructor(props) {
    super(props);
    const mentionUsernames = [];

    props.users.list.forEach(function (item) {
      mentionUsernames.push({
        name: `${item.lastname} ${item.firstname}`,
        value: item.username,
      });
    });

    this.mentionUsernames = mentionUsernames;

    this.state = {
      suggestions: [],
      initValue: toEditorState(''),
      submitDisabled: true,
      createText: '',
    };
  }
  state: MessagesState;

  componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }

  componentWillUpdate() {
    const node = this.conversationMessages;
    this.shouldScrollBottom = node.scrollTop + node.offsetHeight === node.scrollHeight;
  }

  componentDidUpdate() {
    if (this.shouldScrollBottom) {
      const node = this.conversationMessages;
      node.scrollTop = node.scrollHeight;
    }
  }

  props: MessagesListProps;
  mentionUsernames: Array<SuggestionType>;
  conversationMessages: Array<Message>;
  onSelect: Function;
  shouldScrollBottom: boolean;
  render() {
    const { data, create, appUser, title } = this.props;
    const { getFieldDecorator } = this.props.form;

    const rows = data.map(function (item, index) {
      const words = item.text.split(' ');

      const wordsFormatted = words.map((item) => {
        if (item.indexOf('@') !== -1 || item.indexOf('#') !== -1) {
          return `<a>${item}</a>`;
        }
        return item;
      });

      const textFormatted = wordsFormatted.join(' ');

      const menu = (
        <Menu onClick={this.handleMenuClick(item)}>
          <Menu.Item key="destroy">Odstranit</Menu.Item>
        </Menu>
      );

      return (
        <Row key={`${item.key}_${index}`} className={styles.message} type="flex" justify="start">
          <Col
            className={styles.message_avatar}
            style={{ backgroundImage: `url(${item.user.avatar})` }}
          />
          <Col className={styles.message_content}>
            <h5 className={styles.message_title}>{item.user.firstname} {item.user.lastname}</h5>
            <p dangerouslySetInnerHTML={{ __html: textFormatted }} />
            <div className={styles.message_date}>
              {item.created_at} {item.conversationUser && (item.viewedAt ? `zobrazeno: ${moment(item.viewedAt).calendar()}` : 'uživatel si zatím zprávu nezobrazil')}
            </div>
            {
              item.user.id === appUser.id &&
              <div className={styles.message_settings}>
                <Dropdown
                  overlay={menu} placement="bottomRight"
                  onVisibleChange={this.handleVisibleChange} trigger={['click']}
                >
                  <Button
                    type="dark"
                    size="small"
                    shape="circle"
                    icon="ellipsis"
                    style={{
                      border: 'none',
                    }}
                  />
                </Dropdown>
              </div>
            }
          </Col>
        </Row>
      );
    }, this);

    const messageCreate = (
      <Row className={`${styles.message} contentDark`} type="flex" justify="start">
        <Col className={styles.message_avatar} style={{ backgroundImage: `url(${appUser.avatar})` }} />
        <Col className={styles.message_content}>
          <Form layout="horizontal">
            {getFieldDecorator('mention', {
              initialValue: this.state.initValue,
            })(
              <Mention
                multiLines
                prefix={['@', '#']}
                suggestions={this.state.suggestions}
                onChange={this.handleCreateTextChange}
                onSelect={this.onSelect}
                onSearchChange={this.onSearchChange}
                style={{ height: 70 }}
              />,
            )}
            <Button
              type="primary"
              size="small"
              htmlType="submit"
              disabled={this.state.submitDisabled}
              style={{ float: 'right', marginTop: '.75em' }}
              onClick={this.handleCreateSubmit}
            >Odeslat</Button>
          </Form>
        </Col>
      </Row>
    );

    // console.log('>> render', getFieldValue('mention') === this.state.initValue);

    return (
      <div className={styles.messages}>
        <div>
          <div style={{ padding: '1em 1em', borderBottom: '1px #e4e4ef solid' }}>
            <h3>{title}</h3>
          </div>
        </div>
        <div
          ref={(ref) => { this.conversationMessages = ref; }}
          className={styles.messages_content}
        >
          {data && rows}
        </div>
        {create && appUser && messageCreate}
      </div>
    );
  }

  handleCreateSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((errors) => {
      if (errors) {
        // console.log('Errors in form!');
        return;
      }

      this.setState({
        createText: '',
        submitDisabled: true,
      });

      this.props.form.resetFields();

      const { listKey, listRowKey } = this.props;
      this.props.cbCreate(this.state.createText, listKey, listRowKey);
    });
  };

  handleCreateTextChange = (editorState) => {
    this.setState({
      createText: toString(editorState),
      submitDisabled: !toString(editorState).length,
    });
  };

  onSearchChange = (value, trigger) => {
    const searchValue = value.toLowerCase();
    const dataSource = trigger === '@' ? this.mentionUsernames : this.props.mentionItems;
    const filtered = dataSource.filter(item =>
      item.name.toLowerCase().indexOf(searchValue) !== -1,
    );
    const suggestions = filtered.map(suggestion =>
      (<Mention.Nav value={suggestion.value} data={suggestion}>
        <span>{suggestion.name} {suggestion.type &&
        <span style={{ color: '#bbb' }}>({suggestion.type})</span>}</span>
      </Mention.Nav>));
    this.setState({ suggestions });
  };

  handleMenuClick = item => (e) => {
    if (e.key === 'destroy') {
      this.props.cbDestroy(item.id);
    }
  };

  props: MessagesListProps;
}

const decorator = Form.create()(MessagesList);

export default connect()(decorator);
