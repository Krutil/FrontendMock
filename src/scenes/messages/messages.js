/* @flow */
import 'moment/locale/cs';
import React from 'react';
import { connect } from 'dva';
import {
        Layout,
        Icon,
        Spin,
      } from 'antd';
import moment from 'moment';
import ConversationPanel from './components/conversationPanel/conversationPanel';
import Messages from './components/messagesList/messagesList';
import MessageViewed from './components/messageViewed/messageViewed';
import type { Conversation, User, App } from './messagesTypes';

moment.locale('cs');
type MessageContainerState = {
  searchValue: string,
}

type MessageContainerProps = {
  loadingModelUsers: bool,
  loadingModelMessages: bool,
  params: Object,
  messages: Array<Conversation>,
  users: Array<User>,
  dispatch: Function,
  app: App,
}

class MessagesContainer extends React.Component {
  constructor() {
    super();
    this.state = {
      searchValue: '',
    };

    this.messageCreate.bind(this);
    this.messageDestroy.bind(this);
  }

  state: MessageContainerState;

  componentWillMount = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'app/messagesChecked',
    });
  }

  props: MessageContainerProps;

  handleSearch = (e) => {
    this.setState({
      searchValue: e.target.value,
    });
  }

  messageCreate = (
      oConversation,
      text,
      loggedUser,
      conversationUser) => {
    const { app } = this.props;
    const payload = {
      messageBody: text,
      senderId: app.user.id,
      conversationId: oConversation && oConversation.conversationId,
      participants: [loggedUser.id, conversationUser.id],
      user: app.user,
      assigned_to: conversationUser.username,
    };

    this.props.dispatch({
      type: 'messages/create',
      payload,
    });
  }

  messageDestroy = (conversationId:string, messageId:string) => {
    const payload = {
      messageId,
      conversationId,
    };
    this.props.dispatch({
      type: 'messages/destroy',
      payload,
    });
  }

  render() {
    const {
      app,
      dispatch,
      users,
      messages,
      params,
      loadingModelUsers,
      loadingModelMessages,
    } = this.props;

    const usersHash = users.list.reduce((hash, oUser) => {
      hash[oUser.username.split('.').join('')] = oUser;
      return hash;
    }, {});

    const loggedUser:User = app.user;

    const conversationUser:User = params.id ? usersHash[params.id.split('@')[1]] : {};
    const oConversation:Conversation = messages.list && messages.list.data
      &&
      messages.list.data.filter((oConversation) => {
        if (oConversation.participants.includes(loggedUser && loggedUser.id)
                &&
            oConversation.participants.includes(conversationUser && conversationUser.id)) {
          return true;
        }
        return false;
      })[0];
    return (
      <div className="container">
        <div
          className={'section is-sidebar is-fullHeight'}
        >
          <Spin spinning={loadingModelUsers}>
            <Layout className="section_content">
              <ConversationPanel
                app={app}
                handleSearch={this.handleSearch}
                conversations={messages.list.data}
                searchValue={this.state.searchValue}
                users={usersHash}
                params={params}
                aUsers={users}
                dispatch={dispatch}
              />
            </Layout>
          </Spin>
        </div>
        <div className="section is-fullHeight">
          <Spin spinning={loadingModelMessages}>
            <Layout className="section_content">
              {
                location.pathname === `/messages/${params.id}` ?
                  <MessageViewed
                    dispatch={dispatch}
                    oConversation={oConversation}
                    params={params}
                    conversationUser={conversationUser}
                    app={app}
                  >
                    <Messages
                      data={oConversation ?
                        oConversation.messages
                          .map(oMessage => ({
                            text: oMessage.messageBody,
                            id: oMessage.messageId,
                            user: oMessage.senderId
                            ==
                            loggedUser.id ? loggedUser : conversationUser,
                            viewedAt: oMessage.viewedAt,
                            conversationUser: oMessage.senderId === loggedUser.id,
                          }))
                        :
                        []
                      }
                      message
                      title={oConversation === undefined ? conversationUser && `S uživatelem ${conversationUser.firstname} ${conversationUser.lastname} zatím nemáte žádné zprávy`
                        :
                        conversationUser && `${conversationUser.firstname} ${conversationUser.lastname}`}
                      appUser={app.user}
                      users={users}
                      create
                      cbCreate={(text:string) => {
                        this.messageCreate(
                          oConversation,
                          text,
                          loggedUser,
                          conversationUser,
                        );
                      }}
                      cbDestroy={(messageId:string) => {
                        this.messageDestroy(oConversation.conversationId, messageId);
                      }}
                    />
                  </MessageViewed>
                :
                  <div className="content">
                    <div
                      style={{
                        maxWidth: '400px',
                        margin: '-150px 0 0 -200px',
                        position: 'absolute',
                        top: '50%',
                        left: '50%',
                        textAlign: 'center',
                      }}
                    >
                      <p style={{ fontSize: '6em', color: '#ededf8' }}>
                        <Icon type="mail" />
                      </p>
                      <p style={{ fontSize: '1.5em' }}>Vyberte konverzaci</p>
                      <p style={{ color: '#bbbbc4' }}>Z levého menu vyberte konverzaci s uživatelem.</p>
                    </div>
                  </div>
              }
            </Layout>
          </Spin>
        </div>
      </div>
    );
  }

}


function mapStateToProps(state) {
  return {
    app: state.app,
    messages: state.messages,
    users: state.users,
    loadingModelMessages: state.loading.models.messages,
    loadingModelUsers: state.loading.models.users,
  };
}

export default connect(mapStateToProps)(MessagesContainer);
