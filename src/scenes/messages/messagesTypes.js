/* @flow */

export type Message = {
  messageId: string,
  messageBody: string,
  senderId: string,
  viewedAt: string,
  offsetHeight: string,
  scrollHeight: string,
}

export type Conversation = {
  conversationId: string,
  participants: Array<number>,
  messages: Array<Message>,
}

export type MessagesState = {
  searchValue: string,
}

export type User = {
  id: number,
  username: string,
  password: string,
  firstname: string,
  lastname: string,
  department: string,
  phone: string,
  email: string,
  date_check_notification: string,
  date_check_approval: string,
  date_check_messages: string,
  date_check_trash: string,
  avatar: string,
  role: string,
  date_last_login: string,
}

export type App = {
  isHamburgerMenu: bool,
  loading: bool,
  login: bool,
  loginButtonLoading: bool,
  menuPopoverVisible: bool,
  navOpenKeys: Array<string>,
  AppSidebarLeftFold: bool,
  AppPanelTreeUnfold: bool,
  user: User
}



export type Notification = {
  activity_type: string,
  assigned_to: string,
  item_title: string,
  item_type: string,
  item_type_key: string,
  user: User,
  category_notification: string,
}
