import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Timeline } from 'antd';
import moment from 'moment';
import 'moment/locale/cs';

import styles from './notification.less';

moment.locale('cs');

class NotificationsContainer extends React.Component {


  render() {
    const {
      notifications,
    } = this.props;

    const notificationsRows = notifications.filter((oNotification) => oNotification.category_notification !== 'messages');

    notificationsRows.sort((a, b) => {
      let keyA = new Date(a.created_at),
        keyB = new Date(b.created_at);
      if (keyA < keyB) return 1;
      if (keyA > keyB) return -1;
      return 0;
    });

    const dataGroup = {};

    if (notificationsRows.length > 0) {
      notificationsRows.forEach((item) => {
        const date = item.created_at.substring(0, 10);
        dataGroup[date] = dataGroup[date] || [];
        dataGroup[date].push(item);
      });
    }

    const groups = Object.keys(dataGroup).map((key, key_index) => {
      const notificationsViewRows = dataGroup[key].map((item, index) => {
        let text;
        if (item.category_notification === 'spectators' && item.item_type === 'assets') {
          text = `Došlo ke změně u aktiva <strong>${item.item_title}</strong>, které sledujete.`;
        }
        if (item.category_notification === 'spectators' && item.item_type === 'risks') {
          text = `Došlo ke změně u rizika <strong>${item.item_title}</strong>, které sledujete.`;
        }

        if (item.category_notification === 'spectators' && item.item_type === 'requirements') {
          text = `Došlo ke změně u požadavku <strong>${item.item_title}</strong>, který sledujete.`;
        }

        if (item.category_notification === 'approval' && item.item_type === 'assets') {
          text = `Aktivum <strong>${item.item_title}</strong> čeká na schválení.`;
        }

        if (item.category_notification === 'approval' && item.item_type === 'risks') {
          text = `Riziko <strong>${item.item_title}</strong> čeká na schválení.`;
        }

        if (item.category_notification === 'activity' && item.activity_type === 'create' && item.item_type === 'assets') {
          text = `Bylo vytvořeno aktivum <strong>${item.item_title}</strong>.`;
        }

        if (item.category_notification === 'activity' && item.activity_type === 'create' && item.item_type === 'risks') {
          text = `Bylo vytvořeno riziko <strong>${item.item_title}</strong>.`;
        }

        if (item.category_notification === 'activity' && item.activity_type === 'deleted' && item.item_type === 'risks') {
          text = `Bylo odstraněno riziko <strong>${item.item_title}</strong>.`;
        }

        if (item.category_notification === 'activity' && item.activity_type === 'deleted' && item.item_type === 'assets') {
          text = `Bylo odstraněno aktivum <strong>${item.item_title}</strong>.`;
        }

        if (item.category_notification === 'activity' && item.activity_type === 'deleted' && item.item_type === 'requirements') {
          text = `Byl odstraněn požadavek <strong>${item.item_title}</strong>.`;
        }

        if (item.category_notification === 'activity' && item.activity_type === 'mention' && item.item_type === 'requirements') {
          text = `Byl jste zmíněn v komentáři u požadavku <strong>${item.item_title}</strong>.`;
        }

        if (item.category_notification === 'activity' && item.activity_type === 'mention' && item.item_type === 'assets') {
          text = `Byl jste zmíněn v komentáři u aktiva <strong>${item.item_title}</strong>.`;
        }

        if (item.category_notification === 'activity' && item.activity_type === 'mention' && item.item_type === 'risks') {
          text = `Byl jste zmíněn v komentáři u rizika <strong>${item.item_title}</strong>.`;
        }

        return (
          <Timeline.Item key={`row_${key_index}_${index}`}>
              <div>
                <span style={{ color: '#a2a2ad' }}>{moment(item.created_at).format('LT')}</span>
              </div>
              <div className={styles.timeline_row}>
                <span dangerouslySetInnerHTML={{ __html: text }} />
              </div>
          </Timeline.Item>
        );
      }, this);

      return (
        <div key={key} className="spaceM">
          <div className="contentSeparator_title spaceM"><h5>{moment(key).format('LL')}</h5></div>
          <div className="content">
            <Timeline>
              { notificationsViewRows }
            </Timeline>
          </div>
        </div>
      );
    }, this);

    return (
      <div className="container is-block">
        <div className="section">
          <div className="section_content">
            <div className="section_title">
              <h2>Notifikace</h2>
            </div>
            { groups }
          </div>
        </div>
        <div className="spaceM" />
      </div>
    );
  }

}

NotificationsContainer.propTypes = {
  notifications: PropTypes.array,
};

function mapStateToProps(state) {
  return {
    notifications: state.notification,
    loadingModelSpectators: state.loading.models.spectators,
  };
}

export default connect(mapStateToProps)(NotificationsContainer);
