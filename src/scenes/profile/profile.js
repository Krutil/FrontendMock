import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'dva/router';
import { connect } from 'dva';
import {
  Icon,
  Menu,
  Spin,
} from 'antd';
import moment from 'moment';

import ProfileInfo from '../../components/common/users/ProfileInfo/profileInfo';

class ProfileContainer extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      expandedKeys: [],
      searchValue: '',
      selectedKeys: [],
    };
  }

  handleClickTabs = (e) => {
    const { location } = this.props;
    let path = location.pathname;

    if (e.key !== 'overview') {
      path = `${location.pathname}?${e.key}`;
    }

    browserHistory.push(path);
  };

  render() {
    const {
      app,
      dispatch,
      location,
      risks,
      spectators,
    } = this.props;

    console.log('ProfileContainer location', location);
    let tabSelected = location.search;

    if (!tabSelected.length > 0) {
      tabSelected = 'overview';
    }
    tabSelected = tabSelected.replace('?', '');

    return (
      <div>
        <div className="container">
          <div className="section">
            <div className="tableGrid section_content">
              <div className="tableGrid_item section_sidebar">
                <Spin spinning={this.props.loadingModelUsers}>
                  <ProfileInfo user={app.user} />
                </Spin>
              </div>
              <div className="tableGrid_item tableGrid_item--full">
                <Spin spinning={this.props.loadingModelUsers}>
                  <Menu
                    onClick={this.handleClickTabs}
                    selectedKeys={[tabSelected]}
                    mode="horizontal"
                    className="is-bigger"
                  >
                    <Menu.Item key="overview">
                      <Icon type="file" />
                      Přehled
                    </Menu.Item>
                    <Menu.Item key="activity" disabled={false}>
                      <Icon type="bulb" />
                      Aktivity
                    </Menu.Item>
                    <Menu.Item key="connections">
                      <Icon type="task" />
                      Úkoly
                    </Menu.Item>
                    <Menu.Item key="comments">
                      <Icon type="message" />
                      Komentáře
                    </Menu.Item>
                    <Menu.Item key="users">
                      <Icon type="user" />
                      Uživatelé
                    </Menu.Item>
                  </Menu>
                  <div className="content">
                    bbb
                  </div>
                </Spin>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ProfileContainer.propTypes = {
  app: PropTypes.object,
  dispatch: PropTypes.func,
  loadingModelUsers: PropTypes.bool,
  location: PropTypes.object,
  risks: PropTypes.object,
  users: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    app: state.app,
    comments: state.comments,
    files: state.files,
    loadingModelComments: state.loading.models.comments,
    loadingModelFiles: state.loading.models.files,
    loadingModelRelationships: state.loading.models.relationships,
    loadingModelRisks: state.loading.models.risks,
    loadingModelSpectators: state.loading.models.spectators,
    loadingModelUsers: state.loading.models.users,
    relationships: state.relationships,
    risks: state.risks,
    spectators: state.spectators,
    users: state.users,
  };
}

export default connect(mapStateToProps)(ProfileContainer);
