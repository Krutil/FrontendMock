import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { browserHistory } from 'dva/router';
import {
  Layout,
  Icon,
  Spin,
  Button,
} from 'antd';
import moment from 'moment';
import 'moment/locale/cs';

import HeaderSecond from './components/AssetsHeader/assetsHeader';
import TreeList from '../../../components/common/TreeList/treeList';
import AssetsDetail from './components/AssetsDetail/assetsDetail';
import ModalFolderCreate from '../../../components/common/modal/FolderCreate/folderCreate';
import FolderTable from '../../../components/common/FolderTable/folderTable';

moment.locale('cs');

class AssetsContainer extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      expandedKeys: [],
      searchValue: '',
      selectedKeys: [],
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.assets.currentItem !== this.props.assets.currentItem) {
      this.setState({
        expandedKeys: [],
        searchValue: '',
        selectedKeys: [],
      });
    }
  }

  handleAssetsCreate = () => {
    browserHistory.push('/risk/assets/create');
  };

  handleFolderCreate = () => {
    this.props.dispatch({
      type: 'assets/showModal',
      payload: {
        modal: {
          visible: true,
          type: 'modalFolder',
          action: 'create',
          data: {
            tree: this.props.assets.list,
          },
        },
      },
    });
  };

  render() {
    const {
      expandedKeys,
      searchValue,
      selectedKeys,
    } = this.state;
    const {
      app,
      assets,
      dispatch,
      location,
      spectators,
    } = this.props;
    const { modal, currentItem } = this.props.assets;

    if (
      currentItem
      && currentItem.key
      && selectedKeys[0] !== currentItem.key
      && searchValue === ''
    ) {
      if (currentItem.key_parents) {
        const keysParents = currentItem.key_parents.split('.');

        keysParents.forEach(function (key) {
          expandedKeys.push(key);
        });
      }

      if (currentItem.children) {
        expandedKeys.push(currentItem.key);
      }

      selectedKeys.push(currentItem.key);
    }

    const spectatorsData = {};

    if (spectators && currentItem) {
      spectators.list.forEach(function (item) {
        if (item.type_key === currentItem.key) {
          spectatorsData[item.user.id] = spectatorsData[item.user.id] || {};
          spectatorsData[item.user.id] = item;
        }
      });
    }

    const assetsDetailProps = {
      currentItem,
      spectatorsData,
      location,
    };

    /* Vytvoreni adresare */
    const modalFolderProps = {
      modal,
      onOk(data) {
        dispatch({
          type: 'assets/folderCreate',
          payload: data,
        });
      },
      onCancel() {
        dispatch({
          type: 'assets/hideModal',
        });
      },
    };

    /* TODO ujasnit si s BE jak filtrovat a zda na obou stranach */
    const dataList = this.props.assets.list.filter(item =>
    (item.status === 'accepted' || item.type === 'folder')
    && (!item.private || Number(item.assigned_user) === this.props.app.user.id));

    /* Strom v levem panelu s aktivy */
    const treeListProps = {
      dataList,
      dataType: 'assets',
      currentItem: assets.currentItem,
      app,
      dispatch,
    };

    const headerSecondProps = {
      currentItem: assets.currentItem,
    };

    const actualCurrentItem = currentItem
      && assets.list.find(item => item.key === currentItem.key);

    const folderTableProps = {
      dataType: 'assets',
      currentItem: actualCurrentItem,
    };

    return (
      <div>
        <HeaderSecond {...headerSecondProps} />
        <div className="container is-secondHeader">
          {
            location.pathname !== '/risk/assets/create' &&
            <div className={`section is-sidebar is-fullHeight ${app.AppPanelTreeUnfold ? 'is-unfolded' : ''}`}>
              <Spin spinning={this.props.loadingModelAssets}>
                <Layout className="section_content">
                  <TreeList {...treeListProps} />
                </Layout>
              </Spin>
            </div>
          }
          <div className="section is-fullHeight" style={{ maxWidth: '1000px' }}>
            <Spin spinning={this.props.loadingModelAssets}>
              <Layout className="section_content">
                {
                  location.pathname === '/risk/assets' && !assets.currentItem &&
                  <div className="content">
                    <div style={{ marginBottom: 24, textAlign: 'right' }}>
                      <Button type="dark" onClick={this.handleAssetsCreate}>
                        Vytvořit aktivum
                      </Button>
                      <Button
                        type="dark" onClick={this.handleFolderCreate}
                        style={{ marginLeft: '.75em' }}
                      ><Icon type="folder-add" /></Button>
                    </div>
                    <div
                      style={{
                        maxWidth: '400px',
                        margin: '-150px 0 0 -200px',
                        position: 'absolute',
                        top: '50%',
                        left: '50%',
                        textAlign: 'center',
                      }}
                    >
                      <p style={{ fontSize: '6em', color: '#ededf8' }}>
                        <Icon type="file-text" />
                      </p>
                      <p style={{ fontSize: '1.5em' }}>Vyberte záznam</p>
                      <p style={{ color: '#bbbbc4' }}>Z levého menu vyberte název aktiva pro zobrazení jeho detailu nebo klikněte na adresář pro jejich přehled.</p>
                    </div>
                  </div>
                }
                {
                  assets.currentItem && assets.currentItem.type === 'folder' &&
                  <div className="containerColumns">
                    <div className="section_title">
                      <h3>Obsah</h3>
                    </div>
                    <div className="u_overflowAuto">
                      <FolderTable {...folderTableProps} />
                    </div>
                  </div>
                }
                {
                  assets.currentItem && assets.currentItem.type !== 'folder' &&
                  <AssetsDetail {...assetsDetailProps} />
                }
              </Layout>
            </Spin>
          </div>
        </div>
        { modal.type === 'modalFolder' && <ModalFolderCreate {...modalFolderProps} /> }
      </div>
    );
  }
}

AssetsContainer.propTypes = {
  location: PropTypes.object,
  dispatch: PropTypes.func,
  app: PropTypes.object,
  assets: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    app: state.app,
    requirements: state.requirements,
    assets: state.assets,
    risks: state.risks,
    loading: state.loading,
    loadingModelAssets: state.loading.models.assets,
    relationships: state.relationships,
    loadingModelRelationships: state.loading.models.relationships,
    comments: state.comments,
    loadingModelComments: state.loading.models.comments,
    files: state.files,
    loadingModelFiles: state.loading.models.files,
    spectators: state.spectators,
    loadingModelSpectators: state.loading.models.spectators,
    users: state.users,
    loadingModelUsers: state.loading.models.users,
    history: state.history,
  };
}

export default connect(mapStateToProps)(AssetsContainer);
