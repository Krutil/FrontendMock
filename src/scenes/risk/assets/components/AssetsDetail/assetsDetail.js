import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { browserHistory } from 'dva/router';
import {
  Badge,
  Button,
  Col,
  Collapse,
  Dropdown,
  Icon,
  Menu,
  message,
  Modal,
  Row,
  Select,
  Spin,
  Tooltip,
} from 'antd';
import moment from 'moment';
import styles from './assetsDetail.less';

import FileUpload from '../../../../../components/common/FileUpload/fileUpload';
import HistoryCompare from '../../../../../components/common/item/ItemHistoryCompare/itemHistoryCompare';
import ItemActivity from '../../../../../components/common/item/ItemActivity/itemActivity';
import ItemComments from '../../../../../components/common/item/ItemComments/itemComments';
import ItemCommentsAlt from '../../../../../components/common/item/ItemCommentsAlt/itemCommentsAlt';
import ItemConnections from '../../../../../components/common/item/ItemConnections/itemConnections';
import LinkShare from '../../../../../components/common/LinkShare/linkShare';
import RelationshipsCreate from '../../../../../components/common/relationships/RelationshipsCreate/relationshipsCreate';
import RelationshipsList from '../../../../../components/common/relationships/RelationshipsList/relationshipsList';
import SchemaHeat from '../../../../../components/common/SchemaHeat/schemaHeat';
import TableEditable from '../../../../../components/common/TableEditable/tableEditable';
import UserAssign from '../../../../../components/common/users/Assign/assign';
import UsersAssignedList from '../../../../../components/common/users/UsersAssignedList/usersAssignedList';

class AssetsDetailContainer extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      nodeInfo: {
        key: null,
        type: 'relationships',
      },
      sidebarRightVisible: false,
    };
  }

  assetUpdate = (item) => {
    const { app, dispatch } = this.props;
    const payload = {
      item,
      appUser: app.user,
    };

    dispatch({
      type: 'assets/update',
      payload,
    });
  };

  userUpdate = (id) => {
    const { app, currentItem, dispatch } = this.props;
    const payload = {
      item: {
        ...currentItem,
        assigned_user: id,
      },
      appUser: app.user,
    };

    dispatch({
      type: 'assets/update',
      payload,
    });
  };

  /* TODO: Na jake strane rozhodovat o pravu jiz jednou pridaneho uzivatele?
   Upravit zaznam na BE/FE? A mit na to samostatnou route? */
  userAccessAssign = (data) => {
    const { currentItem } = this.props;
    const currentItemUpdated = { ...currentItem };
    const usersAssignedObject = {
      ...data.assigned_user,
      permissions: {
        read: true,
        write: false,
        edit: false,
      },
      expiration: data.unlimited ? null : data.validTo,
    };

    currentItemUpdated.usersAssigned = currentItemUpdated.usersAssigned || [];
    currentItemUpdated.usersAssigned.push(usersAssignedObject);
    this.assetUpdate(currentItemUpdated);
  };

  handleClickTabs = (e) => {
    const { location } = this.props;
    let path = location.pathname;

    if (e.key !== 'overview') {
      path = `${location.pathname}?${e.key}`;
    }

    browserHistory.push(path);
  };

  handleSpectatorClick = item => async () => {
    await new Promise(resolve => setTimeout(resolve, 300));

    if (item) {
      item.following = !item.following;

      const payload = {
        id: item.id,
      };

      this.props.dispatch({
        type: 'spectators/toggle',
        payload,
      });
    } else {
      const payload = {
        appUser: this.props.app.user,
        title: this.props.assets.currentItem.title,
        category: 'assets',
        type_key: this.props.assets.currentItem.key,
      };

      this.props.dispatch({
        type: 'spectators/create',
        payload,
      });
    }
  };

  handleMenuClick = (e) => {
    const { location, assets } = this.props;

    if (e.key === 'linkCopy') {
      browserHistory.push(location.pathname);

      setTimeout(function () {
        const textField = document.createElement('textarea');
        textField.style.position = 'absolute';
        textField.style.bottom = 0;
        textField.style.left = 0;
        textField.style.opacity = 0;
        textField.innerText = `http://localhost:8000${location.pathname}`;
        document.body.appendChild(textField);
        textField.select();
        document.execCommand('copy');
        textField.remove();
        message.success('Odkaz byl zkopírován do schránky.', 3);
      }, 1000);
    }

    if (e.key === 'update') {
      browserHistory.push(`${location.pathname}/update`);
    }

    if (e.key === 'destroy') {
      const item = { ...assets.currentItem };
      item.status = 'deleted';
      this.assetUpdate(item);
      browserHistory.push('/risk/assets');
    }
  };

  handleClickSidebarTab = (item, type) => (event) => {
    if (this.state.nodeInfo.type === type && this.state.nodeInfo.key === item.key) {
      this.setState({
        nodeInfo: {
          key: null,
          type: null,
        },
      });
    } else {
      this.setState({
        nodeInfo: {
          key: item.key,
          type,
        },
      });
    }
  };

  setSidebarRight = bool => () => {
    this.setState({
      sidebarRightVisible: bool,
    });

    let timeout = 0;

    if (!bool) {
      timeout = 300;
    }

    setTimeout(function () {
      document.body.classList.toggle('is-sidebarRight', bool);
    }, timeout);
  };

  showModalLinkShare = () => {
    const { currentItem } = this.props;

    this.setState({
      nodeInfo: {
        key: currentItem.key,
        type: 'linkShare',
      },
      sidebarRightVisible: true,
    });
  };

  relationshipCreate = (oData, keys, action) => {
    /* vytvarime vazbu mezi itemy */
    const payload = {
      appUser: this.props.app.user,
      from_key: keys.from_key,
      from_key_node: keys.from_key_node,
      to_key: keys.to_key,
      itemFrom: this.props.currentItem,
      itemTo: oData,
    };

    this.props.dispatch({
      type: 'relationships/create',
      payload,
    });
  };

  relationshipDestroy = (id) => {
    const payload = {
      id,
    };

    this.props.dispatch({
      type: 'relationships/destroy',
      payload,
    });
  };

  commentCreate = (text, listKey, listRowKey) => {
    const payload = {
      text,
      appUser: this.props.app.user,
      category: 'assets',
      keys: {
        listKey,
        listRowKey,
      },
      currentItem: this.props.assets.currentItem,
    };

    this.props.dispatch({
      type: 'comments/create',
      payload,
    });
  };

  commentDestroy = (id) => {
    const payload = {
      id,
    };

    this.props.dispatch({
      type: 'comments/destroy',
      payload,
    });
  };

  commentReply = (text, parentComment) => {

    const payload = {
      text,
      category: 'assets',
      appUser: this.props.app.user,
      keys: {
        listKey: this.state.nodeInfo.key,
        listRowKey: this.state.nodeInfo.key,
      },
      currentItem: this.props.assets.currentItem,
      parentComment,
    };

    this.props.dispatch({
      type: 'comments/create',
      payload,
    });
  };

  fileCreate = (file, listKey, listRowKey, currentItem) => {
    const payload = {
      file,
      appUser: this.props.app.user,
      category: 'assets',
      keys: {
        listKey,
        listRowKey,
      },
      currentItem
    };

    this.props.dispatch({
      type: 'files/create',
      payload,
    });
  };

  fileDestroy = (fileId) => {
    const payload = {
      id: fileId,
    };

    this.props.dispatch({
      type: 'files/destroy',
      payload,
    });
  };

  historyDateChange = (index) => {
    this.setState({
      historyDateLeftIndex: index,
    });
  };


  historyDateChange2 = (index) => {
    this.setState({
      historyDateRightIndex: index,
    });
  };

  helperStatusTranslation = (status) => {
    switch (status) {
      case 'accepted':
        return 'Schváleno';
      case 'approval':
        return 'Schvaluje se';
      case 'inProgress':
        return 'V přípravě';
      default:
        return '';
    }
  };

  render() {
    const {
      app,
      assets,
      comments,
      currentItem,
      files,
      location,
      relationships,
      requirements,
      risks,
      spectators,
      users,
      historyState,
    } = this.props;
    const mentionItems = [];
    const userLastLogin = app.user.date_last_login;
    let sidebarRightNotifications = 0;
    let tabSelected = location.search;

    if (!tabSelected.length > 0) {
      tabSelected = 'overview';
    }
    tabSelected = tabSelected.replace('?', '');

    const relationshipData = {
      requirements: [],
      assets: [],
      risks: [],
    };

    if (requirements) {
      requirements.list.forEach(function (item) {
        if (item.type !== 'folder') {
          relationshipData.requirements.push(item);
        }

        mentionItems.push({
          name: item.title,
          value: item.key,
        });
      });
    }

    if (assets) {
      assets.list.forEach(function (item) {
        if (item.type !== 'folder') {
          relationshipData.assets.push(item);
        }

        mentionItems.push({
          name: item.title,
          value: item.key,
        });
      });
    }

    if (risks) {
      risks.list.forEach(function (item) {
        if (item.type !== 'folder') {
          relationshipData.risks.push(item);
        }

        mentionItems.push({
          name: item.title,
          value: item.key,
        });
      });
    }

    const itemRelationshipsArray = [];

    relationships.list.forEach((item) => {
      if (item.from_key === currentItem.key || item.to_key === currentItem.key) {
        const oRow = {};
        oRow.id = item.id;
        oRow.created_at = item.created_at;
        oRow.user = item.user;
        oRow.item = item.from_key === currentItem.key ? item.to_item : item.from_item;
        itemRelationshipsArray.push(oRow);

        if (userLastLogin < item.created_at) {
          sidebarRightNotifications += 1;
        }
      }
    });

    const itemCommentsArray = [];

    if (comments) {
      comments.list.forEach(function (item) {
        if (item.category === 'assets' && item.type_key === currentItem.key) {
          itemCommentsArray.push(item);

          if (userLastLogin < item.created_at) {
            sidebarRightNotifications += 1;
          }
        }
      });
    }

    const itemFilesArray = [];

    if (files) {
      files.list.forEach(function (item) {
        if (item.category === 'assets' && item.type_key === currentItem.key) {
          itemFilesArray.push({ ...item, key: item.id });

          if (userLastLogin < item.created_at) {
            sidebarRightNotifications += 1;
          }
        }
      });
    }

    const spectatorsData = {};

    if (spectators && assets.currentItem) {
      spectators.list.forEach(function (item) {
        if (item.type_key === assets.currentItem.key) {
          spectatorsData[item.user.id] = spectatorsData[item.user.id] || {};
          spectatorsData[item.user.id] = item;
        }
      });
    }

    const tabActivityData = {};

    if (assets.currentItem && assets.currentItem.type !== 'folder' && location.search === '?activity') {
      if (assets.currentItem.data && assets.currentItem.data.length > 0) {
        if (comments) {
          comments.list.forEach(function (item) {
            if (item.type_key === assets.currentItem.key) {
              item.type_category = 'comments';

              const date = item.created_at.substring(0, 10);
              tabActivityData[date] = tabActivityData[date] || [];
              tabActivityData[date].push(item);
            }
          });
        }

        if (relationships) {
          relationships.list.forEach(function (item) {
            if (item.from_key === assets.currentItem.key || item.to_key === assets.currentItem.key) {
              item.type_category = 'relationships';

              const date = item.created_at.substring(0, 10);
              tabActivityData[date] = tabActivityData[date] || [];
              tabActivityData[date].push(item);
            }
          });
        }

        if (files) {
          files.list.forEach(function (item) {
            if (item.type_key === assets.currentItem.key) {
              item.type_category = 'files';

              const date = item.created_at.substring(0, 10);
              tabActivityData[date] = tabActivityData[date] || [];
              tabActivityData[date].push(item);
            }
          });
        }
      }
    }

    const tabCommentsData = [];

    if (assets.currentItem && assets.currentItem.type !== 'folder' && location.search === '?comments') {
      if (comments && comments.list.length > 0) {
        comments.list.forEach(function (item) {
          if (item.category === 'assets' && item.type_key === assets.currentItem.key) {
            tabCommentsData.push(item);
          }
        });
      }
    }

    let currentItemUserAssigned = null;

    if (assets.currentItem && assets.currentItem.assigned_user) {
      currentItemUserAssigned = users.list.filter(item => item.id === Number(assets.currentItem.assigned_user))[0];
    }

    const itemHistoryArray = [];

    if (
      assets
      && assets.currentItem
      && assets.currentItem.type === 'item'
      && location.search === '?history'
    ) {
      if (historyState) {
        historyState.forEach(function (historyItem) {
          if (historyItem.item.key === assets.currentItem.key) {
            itemHistoryArray.push(historyItem);
          }
        });
      }
    }

    const itemActivityProps = {
      currentItem: assets.currentItem,
    };

    const itemConnectionsProps = {
      currentItem,
      dataRelationships: itemRelationshipsArray,
      dataFiles: itemFilesArray,
    };

    const itemCommentsProps = {
      data: tabCommentsData,
      location,
    };

    const usersAssignedListProps = {
      currentItem,
      cbUpdate: this.assetUpdate,
    };

    const itemHistoryProps = {
      data: itemHistoryArray,
    };

    const itemHistoryCompareProps = {
      currentItem: assets.currentItem,
      data: itemHistoryArray,
      indexLeft: this.state.historyDateLeftIndex,
      indexRight: this.state.historyDateRightIndex,
    };

    const linkShareProps = {
      cbCreate: this.userAccessAssign,
      currentItem: assets.currentItem,
      dataUsers: users.list,
      location,
    };

    return (
      <div className={styles.containerColumns}>
        <div>
          <div id={currentItem.key} className={`u_overflowAuto ${styles.contentSeparator}`}>
            <Menu
              onClick={this.handleClickTabs}
              selectedKeys={[tabSelected]}
              mode="horizontal"
              className="is-bigger is-transparent"
            >
              <Menu.Item key="overview">
                <Icon type="file" />
                Přehled
              </Menu.Item>
              <Menu.Item key="activity" disabled={false}>
                <Icon type="bulb" />
                Aktivity
              </Menu.Item>
              <Menu.Item key="connections">
                <Icon type="share-alt" />
                Propojení
              </Menu.Item>
              <Menu.Item key="comments">
                <Icon type="message" />
                Komentáře
              </Menu.Item>
              <Menu.Item key="users">
                <Icon type="user" />
                Uživatelé
              </Menu.Item>
              {
                historyState.length > 0 &&
                <Menu.Item key="history">
                  <Icon type="clock-circle-o" />
                  Historie
                </Menu.Item>
              }
            </Menu>
          </div>
        </div>
        <div className="u_overflowAuto">
          {
            location.search === '' &&
            <div>
              <div className="contentDark">
                <Row
                  type="flex"
                  justify="space-between"
                  align="middle"
                >
                  <Col>
                    <UserAssign
                      authUser={app.user}
                      title="Správce"
                      user={currentItemUserAssigned}
                      userList={users.list}
                      cbUserAssign={this.userUpdate}
                    />
                  </Col>
                  <Col>
                    <Button
                      className={`${spectatorsData[app.user.id] && spectatorsData[app.user.id].following ? 'is-positive' : ''}`}
                      type="dark"
                      icon="eye-o"
                      onClick={this.handleSpectatorClick(spectatorsData[app.user.id])}
                    >
                      { spectatorsData[app.user.id] && spectatorsData[app.user.id].following ? 'Sledováno' : 'Sledovat' }
                    </Button>
                    <Badge
                      count={sidebarRightNotifications}
                    >
                      <Button
                        className="ant-btn-icon-only"
                        style={{ margin: '0 .5em' }}
                        type="dark"
                        onClick={this.setSidebarRight(true)}
                      >
                        <Icon type="bars" />
                      </Button>
                    </Badge>
                    <Dropdown
                      placement="bottomRight"
                      trigger={['click']}
                      overlay={
                        <Menu onClick={e => this.handleMenuClick(e)}>
                          <Menu.Item key="update">Upravit</Menu.Item>
                          <Menu.Item key="destroy">Odstranit</Menu.Item>
                          <Menu.Item key="linkCopy">Kopírovat odkaz</Menu.Item>
                        </Menu>
                      }
                    >
                      <Button type="dark" icon="ellipsis" />
                    </Dropdown>
                  </Col>
                </Row>
              </div>
              <div className="content">
                <table className="tableSimple tableLabel">
                  <tr>
                    <td className="cGray">Status:</td>
                    <td><strong>{this.helperStatusTranslation(assets.currentItem.status)}</strong></td>
                  </tr>
                  {
                    assets.currentItem.number &&
                    <tr>
                      <td className="cGray">Číslo:</td>
                      <td><strong>{assets.currentItem.number}</strong></td>
                    </tr>

                  }
                  <tr>
                    <td className="cGray">Odhadovaná platnost:</td>
                    <td><strong>{moment(assets.currentItem.date_plan_from).format('LL')} {assets.currentItem.date_plan_to ? ` – ${moment(assets.currentItem.date_plan_to).format('LL')}` : ''}</strong></td>
                  </tr>
                  <tr>
                    <td className="cGray">Skutečná platnost:</td>
                    <td><strong>{moment(assets.currentItem.date_real_from).format('LL')} {assets.currentItem.date_real_to ? ` – ${moment(assets.currentItem.date_real_to).format('LL')}` : ''}</strong></td>
                  </tr>
                  {
                    assets.currentItem.user &&
                    <tr>
                      <td className="cGray">Vytvořil:</td>
                      <td><strong>{assets.currentItem.user.lastname} {assets.currentItem.user.firstname}</strong></td>
                    </tr>
                  }
                  {
                    assets.currentItem.created_at &&
                    <tr>
                      <td className="cGray">Vytvořeno:</td>
                      <td>
                        <Tooltip
                          placement="top"
                          title="Upravil: Jan Novák"
                        >
                          <strong>{ moment(assets.currentItem.created_at).format('LLL') }</strong>
                        </Tooltip>
                      </td>
                    </tr>
                  }
                  {
                    assets.currentItem.updated_at &&
                    <tr>
                      <td className="cGray">Upraveno:</td>
                      <td>
                        <strong>
                          <Tooltip
                            placement="top"
                            title="Upravil: Jan Novák"
                          >
                            { moment(assets.currentItem.updated_at).format('LLL') }
                          </Tooltip>
                        </strong>
                      </td>
                    </tr>
                  }
                  {
                    assets.currentItem.description &&
                    <tr>
                      <td className="cGray">Popis:</td>
                      <td>
                        <span dangerouslySetInnerHTML={{ __html: assets.currentItem.description }} />
                      </td>
                    </tr>
                  }
                </table>
              </div>
              {
                assets.currentItem && assets.currentItem.schema && assets.currentItem.schema.length > 0 &&
                <div>
                  <div className="section_title">
                    <h3>
                      <Icon type="bar-chart" style={{ color: '#a5a5a5', marginRight: '.5em' }} />
                      Hodnocení
                    </h3>
                  </div>
                  <div className="contentDark">
                    {
                      assets.currentItem.schema && assets.currentItem.schema.map((item, index) => (
                        <div key={`${item.key}_${index}`} className="spaceXL">
                          <h4>{item.title}</h4>
                          {
                            item.schema_type === 'heat' &&
                            <SchemaHeat data={item.data} style={{ margin: '2em 0' }} />
                          }
                          {
                            item.schema_type === 'table' &&
                            <TableEditable
                              dataColumns={item.data.dataColumns}
                              data={item.data.dataSource}
                              style={{ margin: '2em 0' }}
                            />
                          }
                        </div>
                      ))
                    }
                  </div>
                </div>
              }
            </div>
          }
          {
            location.search === '?activity' &&
            <ItemActivity {...itemActivityProps} />
          }
          {
            location.search === '?connections' &&
            <ItemConnections {...itemConnectionsProps} />
          }
          {
            location.search === '?comments' &&
            <ItemComments {...itemCommentsProps} />
          }
          {
            location.search === '?users' &&
            <div>
              <UsersAssignedList {...usersAssignedListProps} />
              <div className="content" style={{ textAlign: 'right' }}>
                <Button
                  icon="plus"
                  type="dark"
                  onClick={this.showModalLinkShare}
                >
                  Přidat uživatele
                </Button>
              </div>
            </div>
          }
          {
            location.search === '?history' &&
            <div>
              <Row
                type="flex"
                justify="space-between"
                align="middle"
                className="contentSeparator_title"
              >
                <Col>
                  <Row gutter={10}>
                    <Col span={12}>
                      <Select
                        style={{ width: 180 }}
                        placeholder="Porovnat"
                        onChange={this.historyDateChange}
                      >
                        <Select.Option value="current">Aktuální verze</Select.Option>
                        {
                          itemHistoryProps.data.map((item, index) => (
                            <Select.Option
                              key={index}
                            >{moment(item.created_at).format('l LT')}</Select.Option>
                          ))
                        }
                      </Select>
                    </Col>
                    <Col span={12}>
                      <Select
                        style={{ width: 180 }}
                        placeholder="Porovnat"
                        onChange={this.historyDateChange2}
                      >
                        <Select.Option value="current">Aktuální verze</Select.Option>
                        {
                          itemHistoryProps.data.map((item, index) => (
                            <Select.Option
                              key={index}
                            >{moment(item.created_at).format('l LT')}</Select.Option>
                          ))
                        }
                      </Select>
                    </Col>
                  </Row>
                </Col>
              </Row>
              {
                this.state.historyDateLeftIndex !== undefined
                && this.state.historyDateRightIndex !== undefined
                && <HistoryCompare {...itemHistoryCompareProps} />
              }
            </div>
          }
        </div>
        <Modal
          key="sidebarRight"
          wrapClassName="sidebarRight"
          visible={this.state.sidebarRightVisible}
          title={
            <div>
              <Badge
                className={`${this.state.nodeInfo.type === 'relationships' && 'is-active'}`}
                count={itemRelationshipsArray ? itemRelationshipsArray.length : 0}
                onClick={this.handleClickSidebarTab(assets.currentItem, 'relationships')}
              >
                <Icon type="share-alt" />
              </Badge>
              <Badge
                className={`${this.state.nodeInfo.type === 'comments' && 'is-active'}`}
                count={itemCommentsArray.length}
                onClick={this.handleClickSidebarTab(assets.currentItem, 'comments')}
              >
                <Icon type="message" />
              </Badge>
              <Badge
                className={`${this.state.nodeInfo.type === 'files' && 'is-active'}`}
                count={itemFilesArray.length}
                onClick={this.handleClickSidebarTab(assets.currentItem, 'files')}
              >
                <Icon type="file" />
              </Badge>
              <Badge
                className={`${this.state.nodeInfo.type === 'linkShare' && 'is-active'}`}
                onClick={this.handleClickSidebarTab(assets.currentItem, 'linkShare')}
              >
                <Icon type="link" />
              </Badge>
            </div>
          }
          footer={null}
          onCancel={this.setSidebarRight(false)}
        >
          {
            this.state.nodeInfo.type === 'relationships' &&
            <Spin
              className="space"
              spinning={this.props.loadingModelRelationships}
            >
              <RelationshipsList
                editable
                data={itemRelationshipsArray}
              />
              <Collapse accordion bordered={false}>
                <Collapse.Panel
                  header={<span style={{ fontSize: '90%', fontWeight: '700', paddingLeft: '.5em' }}>Přiřadit vazbu</span>}
                  key="1"
                >
                  <div className="contentDark">
                    <RelationshipsCreate
                      type="requirements"
                      data={itemRelationshipsArray}
                      dataSelect={relationshipData}
                      currentItem={currentItem}
                      currentNode={null}
                      listRequirements={requirements.list}
                      listUsers={users.list}
                      cbCreate={this.relationshipCreate}
                      cbDestroy={this.relationshipDestroy}
                    />
                  </div>
                </Collapse.Panel>
              </Collapse>
            </Spin>
          }
          {
            this.state.nodeInfo.type === 'comments' &&
            <Spin className="space" spinning={this.props.loadingModelComments}>
              <ItemCommentsAlt
                data={itemCommentsArray}
                typeCreate={true}
                appUser={app.user}
                users={users}
                currentItem={assets.currentItem}
                mentionItems={mentionItems}
                listKey={assets.currentItem.key}
                listRowKey={assets.currentItem.key}
                cbCreate={this.commentCreate}
                cbDestroy={this.commentDestroy}
                cbReply={this.commentReply}
              />
            </Spin>
          }
          {
            this.state.nodeInfo.type === 'files' &&
            <Spin className="space" spinning={this.props.loadingModelFiles}>
              <FileUpload
                dataTable={itemFilesArray}
                create
                appUser={app.user}
                listKey={assets.currentItem.key}
                currentItem={assets.currentItem}
                listRowKey={assets.currentItem.key}
                cbCreate={this.fileCreate}
                cbDestroy={this.fileDestroy}
              />
            </Spin>
          }
          {
            this.state.nodeInfo.type === 'linkShare' &&
            <LinkShare {...linkShareProps} />
          }
        </Modal>
      </div>
    );
  }
}

AssetsDetailContainer.propTypes = {
  app: PropTypes.object,
  currentItem: PropTypes.object,
  dispatch: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    app: state.app,
    users: state.users,
    assets: state.assets,
    risks: state.risks,
    historyState: state.history,
    relationships: state.relationships,
    loadingModelRelationships: state.loading.models.relationships,
    requirements: state.requirements,
    loadingModelAssets: state.loading.models.requirements,
    comments: state.comments,
    loadingModelComments: state.loading.models.comments,
    files: state.files,
    loadingModelFiles: state.loading.models.files,
    spectators: state.spectators,
    loadingModelSpectators: state.loading.models.spectators,
  };
}

export default connect(mapStateToProps)(AssetsDetailContainer);
