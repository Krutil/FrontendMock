import React from 'react';
import PropTypes from 'prop-types';
import styles from './assetsHeader.less';

class AssetsHeader extends React.Component {

  render() {
    const { currentItem } = this.props;

    return (
      <div className={styles.headerSecond}>
        <div className={styles.headerSecond_container}>
          <h1>RISK<span
            className="separatorTitle"
          >/</span>Aktiva{currentItem && currentItem.title &&
          <span><span className="separatorTitle">/</span>{currentItem.title}</span>}</h1>
        </div>
      </div>
    );
  }
}

AssetsHeader.propTypes = {
  currentItem: PropTypes.object,
};

export default AssetsHeader;
