import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  DatePicker,
  Form,
  Input,
  Modal,
  Select,
} from 'antd';
import moment from 'moment';

class AssetsCreateUpdateForm extends React.Component {

  state = {
    changed: false,
    modalNoteUpdate: false,
  };

  handleSubmit = (e) => {
    e.preventDefault();

    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }

      // Should format date value before submit.
      const rangeValue = fieldsValue.date_plan;
      const rangeValue2 = fieldsValue.date_real;
      const values = {
        ...fieldsValue,
        date_plan: [rangeValue[0].format('YYYY-MM-DD'), rangeValue[1].format('YYYY-MM-DD')],
        date_real: [rangeValue2 ? rangeValue2[0].format('YYYY-MM-DD') : null, rangeValue2 ? rangeValue2[1].format('YYYY-MM-DD') : null],
      };

      values.category = values.category === 'null' ? null : values.category;

       console.log('values', values);

      let dataObject = {};

      if (this.props.type === 'update') {
        dataObject = this.props.data;
      } else {
        dataObject.status = 'inProgress';
      }

      dataObject.type = 'item';
      dataObject.key_parents = values.category;
      dataObject.private = values.private;
      dataObject.title = values.title;
      dataObject.number = values.number;
      dataObject.assigned_user = values.assigned_user;
      dataObject.date_plan_from = values.date_plan[0];
      dataObject.date_plan_to = values.date_plan[1];
      dataObject.date_real_from = values.date_real[0];
      dataObject.date_real_to = values.date_real[1];
      dataObject.description = values.description;

       console.log('dataObject', dataObject);

      if (this.props.type === 'create') {
        this.props.cbCreate(dataObject);
      }

      if (this.props.type === 'update') {
        this.props.cbUpdate(dataObject);
      }

      this.setState({
        changed: false,
        modalNoteUpdate: false,
      });
    });
  };

  handleFormChange = (changedFields) => {
    this.setState({
      changed: true,
    });
  };

  modalNoteUpdateVisible = (e) => {
    e.preventDefault();

    this.setState({
      modalNoteUpdate: true,
    });
  };

  modalNoteUpdateInvisible = () => {
    this.setState({
      modalNoteUpdate: false,
    });
  };

  render() {
    const { modalNoteUpdate } = this.state;
    const {
      dataAssets,
      dataUsers,
      settingsAssets,
    } = this.props;
    const { getFieldDecorator } = this.props.form;
    let { data } = this.props;

    let rangePlanConfig = {
      rules: [{ type: 'array', required: true, message: 'Vyberte datum.' }],
    };
    let rangeRealConfig = {
      rules: [{ type: 'array', message: 'Vyberte datum¨.' }],
    };

    if (data) {
      if (data.date_plan_from && data.date_plan_to) {
        rangePlanConfig = {
          initialValue: [moment(data.date_plan_from, 'YYYY-MM-DD'), moment(data.date_plan_to, 'YYYY-MM-DD')],
          rules: [{ type: 'array', required: true, message: 'Vyberte datum.' }],
        };
      }

      if (data.date_real_from && data.date_real_to) {
        rangeRealConfig = {
          initialValue: [moment(data.date_real_from, 'YYYY-MM-DD'), moment(data.date_real_to, 'YYYY-MM-DD')],
          rules: [{ type: 'array', message: 'Vyberte datum.' }],
        };
      }
    } else {
      data = {};
    }

    const modalProps = {
      title: 'Poznámka k úpravě',
      visible: modalNoteUpdate,
      onOk: this.handleSubmit,
      onCancel: this.modalNoteUpdateInvisible,
      wrapClassName: 'vertical-center-modal',
      okText: 'Potvrdit',
      cancelText: 'Zpět',
    };

    return (
      <div>
        <Form
          layout="vertical"
          className="contentFormCentered"
          style={{ maxWidth: '600px', margin: '2em auto' }}
          onChange={this.handleFormChange}
          onSubmit={
            settingsAssets.noteUpdate
            && this.props.type === 'update'
            && data.status === 'accepted'
              ? this.modalNoteUpdateVisible : this.handleSubmit
          }
        >
          <Form.Item label="Název:">
            {getFieldDecorator('title', {
              initialValue: data.title,
              rules: [
                {
                  required: true,
                  message: 'Nutné vyplnit',
                },
              ],
            })(<Input />)}
          </Form.Item>
          <Form.Item label="Přístup:">
            {getFieldDecorator('private', {
              initialValue: data.private !== undefined ? data.private : 0,
            })(<Select
              showSearch
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              onChange={this.handleFormChange}
            >
              <Select.Option key="0" value={0}>Veřejný</Select.Option>
              <Select.Option key="1" value={1}>Vybraní uživatelé</Select.Option>
            </Select>)}
          </Form.Item>
          <Form.Item label="Kategorie:">
            {getFieldDecorator('category', {
              initialValue: data.key_parents ? data.key_parents : null,
            })(<Select
              showSearch
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              onChange={this.handleFormChange}
            >
              <Select.Option key="null" value={null}>Bez kategorie</Select.Option>
              {
                dataAssets && dataAssets.map(item => (
                  <Select.Option
                    key={item.key}
                    value={item.key_parents ? `${item.key_parents}.${item.key}` : item.key}
                  >{ item.title }</Select.Option>
                ))
              }
            </Select>)}
          </Form.Item>
          <Form.Item label="Číslo záznamu:">
            {getFieldDecorator('number', {
              initialValue: data.number,
            })(<Input />)}
          </Form.Item>
          <Form.Item label="Přiřazený uživatel:">
            {getFieldDecorator('assigned_user', {
              initialValue: data.assigned_user ? String(data.assigned_user) : null,
            })(<Select
              showSearch
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              onChange={this.handleFormChange}
            >
              <Select.Option key="null" value={null}>Nepřiřazeno</Select.Option>
              {
                dataUsers.map(item => (
                  <Select.Option key={item.id.toString()}>
                    { `${item.firstname} ${item.lastname}` }
                  </Select.Option>
                ))
              }
            </Select>)}
          </Form.Item>
          <Form.Item label="Plán pořízení/používání/platnosti aktiva:">
            {getFieldDecorator('date_plan', rangePlanConfig)(
              <DatePicker.RangePicker
                format="YYYY-MM-DD"
                style={{ width: '100%' }}
                onChange={this.handleFormChange}
              />,
            )}
          </Form.Item>
          <Form.Item label="Skutečnost pořízení/používání/platnosti aktiva:">
            {getFieldDecorator('date_real', rangeRealConfig)(
              <DatePicker.RangePicker
                format="YYYY-MM-DD"
                style={{ width: '100%' }}
                onChange={this.handleFormChange}
              />,
            )}
          </Form.Item>
          <Form.Item label="Popis:">
            {getFieldDecorator('description', {
              initialValue: data.description,
            })(<Input type="textarea" style={{ width: '100%', minHeight: '6em' }} />)}
          </Form.Item>
          {
            this.props.type === 'create' &&
            <Form.Item>
              <Button type="primary" htmlType="submit" size="large">Vytvořit</Button>
            </Form.Item>
          }
          {
            this.props.type === 'update' && this.state.changed &&
            <Form.Item>
              <Button type="primary" htmlType="submit" size="large">Uložit změny</Button>
            </Form.Item>
          }
        </Form>
        {
          modalNoteUpdate &&
          <Modal {...modalProps}>
            <Form layout="vertical">
              {
                getFieldDecorator('noteUpdateText', {
                  rules: [{required: true}],
                })(
                  <Input type="textarea" style={{width: '100%', minHeight: '8em'}}/>,
                )
              }
            </Form>
          </Modal>
        }
      </div>
    );
  }
}

AssetsCreateUpdateForm.propTypes = {
  cbCreate: PropTypes.func,
  cbUpdate: PropTypes.func,
  data: PropTypes.object,
  dataAssets: PropTypes.array,
  dataUsers: PropTypes.array,
  form: PropTypes.object.isRequired,
  settingsAssets: PropTypes.object.isRequired,
  type: PropTypes.string,
};

export default Form.create()(AssetsCreateUpdateForm);
