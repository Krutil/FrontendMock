import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'dva/router';
import { connect } from 'dva';
import {
  Spin,
} from 'antd';
import pathToRegexp from 'path-to-regexp';
import moment from 'moment';
import 'moment/locale/cs';

import HeaderSecond from '../components/AssetsHeader/assetsHeader';
import AssetsCreateUpdateForm from './components/AssetsCreateUpdateForm/assetsCreateUpdateForm';

moment.locale('cs');

class AssetsCreateUpdateContainer extends React.Component {

  assetCreate = (item) => {
    const payload = {
      item,
      appUser: this.props.app.user,
    };

    this.props.dispatch({
      type: 'assets/create',
      payload,
    });

    browserHistory.push('/risk/assets');
  };

  assetUpdate = (item) => {
    const { app, location } = this.props;

    const payload = {
      item,
      appUser: app.user,
    };

    this.props.dispatch({
      type: 'assets/update',
      payload,
    });
  };

  render() {
    const {
      app,
      assets,
      comments,
      dispatch,
      files,
      location,
      relationships,
      requirements,
      risks,
      settings,
      spectators,
      users,
      } = this.props;
    const mentionItems = [];

    const url = pathToRegexp('/risk/assets/item/:id/:action?').exec(location.pathname);
    const urlAction = url && url.length > 2 ? url[2] : '';
    console.log('urlAction', urlAction)


    const headerSecondProps = {
      location,
      changeOpenKeys(openKeys) {
        localStorage.setItem('navOpenKeys', JSON.stringify(openKeys));
        dispatch({ type: 'app/handleNavOpenKeys', payload: { navOpenKeys: openKeys } });
      },
    };

    const relationshipData = {
      assets: [],
      requirements: [],
      risks: [],
    };

    assets && assets.list.forEach(function (item) {
      if (item.type !== 'folder') {
        const dataObject = {};
        dataObject.key = item.key;
        dataObject.title = item.title;
        relationshipData.assets.push(dataObject);
      }

      mentionItems.push({
        name: item.title,
        value: item.key,
      });
    });

    requirements && requirements.list.forEach(function (item) {
      if (item.type !== 'folder') {
        const dataObject = {};
        dataObject.key = item.key;
        dataObject.title = item.title;
        relationshipData.requirements.push(dataObject);
      }

      mentionItems.push({
        name: item.title,
        value: item.key,
      });
    });

    risks && risks.list.forEach(function (item) {
      if (item.type !== 'folder') {
        const dataObject = {};
        dataObject.key = item.key;
        dataObject.title = item.title;
        relationshipData.risks.push(dataObject);
      }

      mentionItems.push({
        name: item.title,
        value: item.key,
      });
    });

    const relationshipsTableData = {};

    if (relationships) {
      relationships.list.forEach(function (item) {
        relationshipsTableData[item.type_key_node] = relationshipsTableData[item.type_key_node] || [];
        relationshipsTableData[item.type_key_node].push(item);
      });
    }

    const commentsData = {};

    if (comments) {
      comments.list.forEach(function (item) {
        if (item.category === 'assets') {
          commentsData[item.type_key_node] = commentsData[item.type_key_node] || [];
          commentsData[item.type_key_node].push(item);
        }
      });
    }

    const filesTableData = {};

    if (files) {
      files.list.forEach(function (item) {
        if (item.category === 'assets') {
          filesTableData[item.type_key_node] = filesTableData[item.type_key_node] || [];
          filesTableData[item.type_key_node].push({ ...item, key: item.id });
        }
      });
    }

    const spectatorsData = {};

    if (spectators && assets.currentItem) {
      spectators.list.forEach(function (item) {
        if (item.type_key === assets.currentItem.key) {
          spectatorsData[item.user.id] = spectatorsData[item.user.id] || {};
          spectatorsData[item.user.id] = item;
        }
      });
    }

    const assetsCreateUpdateFormProps = {
      data: assets.currentItem ? assets.currentItem : null,
      dataAssets: assets.list,
      dataUsers: users.list,
      settingsAssets: settings.risk.assets,
      type: 'create',
    };

    return (
      <div>
        <HeaderSecond
          {...headerSecondProps}
          currentItem={assets.currentItem}
          spectatorsData={spectatorsData} tabSelected={location.search}
        />
        <div>
          <div className="container is-secondHeader">
            <div className="section" style={{ maxWidth: '1000px' }}>
              <Spin spinning={this.props.loadingModelAssets}>
                <div className="section_content">
                  <div>
                    <div className="section_title">
                      <h2>
                        { location.pathname === '/risk/assets/create'
                          ? 'Vytvořit aktivum' : 'Upravit aktivum' }
                      </h2>
                    </div>
                    <div className="contentDark">
                      {
                        location.pathname === '/risk/assets/create' &&
                        <AssetsCreateUpdateForm
                          {...assetsCreateUpdateFormProps}
                          type="create"
                          cbCreate={this.assetCreate}
                        />
                      }
                      {
                        urlAction === 'update' &&
                        <AssetsCreateUpdateForm
                          {...assetsCreateUpdateFormProps}
                          type="update"
                          cbUpdate={this.assetUpdate}
                        />
                      }
                    </div>
                  </div>
                </div>
              </Spin>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AssetsCreateUpdateContainer.propTypes = {
  location: PropTypes.object,
  dispatch: PropTypes.func,
  app: PropTypes.object,
  assets: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    app: state.app,
    assets: state.assets,
    comments: state.comments,
    files: state.files,
    loadingModelAssets: state.loading.models.assets,
    loadingModelComments: state.loading.models.comments,
    loadingModelFiles: state.loading.models.files,
    loadingModelRelationships: state.loading.models.relationships,
    loadingModelSpectators: state.loading.models.spectators,
    relationships: state.relationships,
    risks: state.risks,
    settings: state.settings,
    spectators: state.spectators,
    users: state.users,
  };
}

export default connect(mapStateToProps)(AssetsCreateUpdateContainer);
