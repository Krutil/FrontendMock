import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import {
  browserHistory,
  Link,
} from 'dva/router';
import {
  Badge,
  Button,
  Col,
  Collapse,
  Dropdown,
  Icon,
  Menu,
  message,
  Modal,
  Row,
  Spin,
  Tooltip,
} from 'antd';
import moment from 'moment';
import styles from './requirementsDetail.less';

import FileUpload from '../../../../../components/common/FileUpload/fileUpload';
import ItemActivity from '../../../../../components/common/item/ItemActivity/itemActivity';
import ItemComments from '../../../../../components/common/item/ItemComments/itemComments';
import ItemCommentsAlt from '../../../../../components/common/item/ItemCommentsAlt/itemCommentsAlt';
import ItemConnections from '../../../../../components/common/item/ItemConnections/itemConnections';
import LinkShare from '../../../../../components/common/LinkShare/linkShare';
import RelationshipsCreate from '../../../../../components/common/relationships/RelationshipsCreate/relationshipsCreate';
import RelationshipsList from '../../../../../components/common/relationships/RelationshipsList/relationshipsList';
import RequirementsCreateForm from '../../../../../scenes/risk/requirements/createUpdate/components/RequirementsCreateUpdateForm/requirementsCreateUpdateForm';
import ShrinkStretch from '../../../../../components/content/ShrinkStretch/shrinkStretch';
import UserAssign from '../../../../../components/common/users/Assign/assign';
import UsersAssignedList from '../../../../../components/common/users/UsersAssignedList/usersAssignedList';

class RequirementsDetailContainer extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      nodeSelected: {
        item: null,
        parentKey: null,
        type: null,
      },
      sidebarRightVisible: false,
    };
  }

  requirementUpdate = (item) => {
    const payload = {
      item,
      appUser: this.props.app.user,
    };

    this.props.dispatch({
      type: 'requirements/update',
      payload,
    });
  };

  userUpdate = (id) => {
    const { app, currentItem, dispatch } = this.props;
    const payload = {
      item: {
        ...currentItem,
        assigned_user: id,
      },
      appUser: app.user,
    };

    dispatch({
      type: 'requirements/update',
      payload,
    });
  };

  /* TODO: Na jake strane rozhodovat o pravu jiz jednou pridaneho uzivatele?
  Upravit zaznam na BE/FE? A mit na to samostatnou route? */
  userAccessAssign = (data) => {
    const { currentItem } = this.props;
    const currentItemUpdated = { ...currentItem };
    const usersAssignedObject = {
      ...data.assigned_user,
      permissions: {
        read: true,
        write: false,
        edit: false,
      },
      expiration: data.unlimited ? null : data.validTo,
    };

    currentItemUpdated.usersAssigned = currentItemUpdated.usersAssigned || [];
    currentItemUpdated.usersAssigned.push(usersAssignedObject);
    this.requirementUpdate(currentItemUpdated);
  };

  handleClickTabs = (e) => {
    const { location } = this.props;
    let path = location.pathname;

    if (e.key !== 'overview') {
      path = `${location.pathname}?${e.key}`;
    }

    browserHistory.push(path);
  };

  handleSpectatorClick = item => async () => {
    await new Promise(resolve => setTimeout(resolve, 300));

    if (item) {
      item.following = !item.following;

      const payload = {
        id: item.id,
      };

      this.props.dispatch({
        type: 'spectators/toggle',
        payload,
      });
    } else {
      const payload = {
        appUser: this.props.app.user,
        title: this.props.requirements.currentItem.title,
        category: 'requirements',
        type_key: this.props.requirements.currentItem.key,
      };

      this.props.dispatch({
        type: 'spectators/create',
        payload,
      });
    }
  };

  handleMenuClick = (e) => {
    const { location } = this.props;

    if (e.key === 'update') {
      browserHistory.push(`${location.pathname}/update`);
    }

    if (e.key === 'destroy') {
      const item = { ...this.props.requirements.currentItem };
      item.status = 'deleted';
      this.requirementUpdate(item);
      browserHistory.push('/risk/requirements');
    }
  };

  handleNodeClick = (item, type) => (event) => {
    const { sidebarRightVisible } = this.state.sidebarRightVisible;

    if (!sidebarRightVisible) {
      this.setSidebarRight(true);
    }

    if (this.state.nodeSelected.type === type && this.state.nodeSelected.key === item.key) {

    } else {
      this.setState({
        nodeSelected: {
          item,
          parentKey: this.props.requirements.currentItem.key,
          type,
        },
      });
    }
  };

  handleClickLink = key => (event) => {
    const { location } = this.props;
    browserHistory.push(`${location.pathname}#${key}`);

    setTimeout(function () {
      const textField = document.createElement('textarea');
      textField.style.position = 'absolute';
      textField.style.bottom = 0;
      textField.style.left = 0;
      textField.style.opacity = 0;
      textField.innerText = `http://localhost:8000${location.pathname}#${key}`;
      document.body.appendChild(textField);
      textField.select();
      document.execCommand('copy');
      textField.remove();
      message.success('Odkaz byl zkopírován do schránky.', 3);
    }, 1000);
  };

  setSidebarRight = (bool) => {
    this.setState({
      sidebarRightVisible: bool,
    });

    let timeout = 0;

    if (!bool) {
      timeout = 300;
    }

    setTimeout(function () {
      document.body.classList.toggle('is-sidebarRight', bool);
    }, timeout);
  };

  relationshipCreate = (oData, keys, action) => {
    if (action === 'createRequirementAlt') {
      /* Vytvarime odvozeny pozadavek */
      const payload = {
        action: 'createRequirementAlt',
        appUser: this.props.app.user,
        from_key: keys.from_key,
        from_key_node: keys.from_key_node,
        itemNew: oData,
      };

      this.props.dispatch({
        type: 'requirements/create',
        payload,
      });
    } else {
      /* vytvarime vazbu mezi itemy */
      const payload = {
        appUser: this.props.app.user,
        from_key: keys.from_key,
        from_key_node: keys.from_key_node,
        to_key: keys.to_key,
        itemFrom: this.props.currentItem,
        itemTo: oData,
      };

      this.props.dispatch({
        type: 'relationships/create',
        payload,
      });
    }
  };

  relationshipDestroy = (id) => {
    const payload = {
      id,
    };

    this.props.dispatch({
      type: 'relationships/destroy',
      payload,
    });
  };

  /* Vytvarime odvozeny pozadavek */
  requirementCreate = (oData) => {
    const { currentItem } = this.props;

    oData.type = 'item_alt';
    oData.key_parents = 'aaabbb';
    oData.createdFrom = currentItem;

    const keys = {
      from_key: currentItem.key,
      from_key_node: this.state.nodeSelected.item.key,
    };

    this.relationshipCreate(oData, keys, 'createRequirementAlt');
  };

  commentCreate = (text, listKey, listRowKey) => {
    const payload = {
      text,
      appUser: this.props.app.user,
      category: 'requirements',
      keys: {
        listKey,
        listRowKey,
      },
      currentItem: this.props.requirements.currentItem,
    };

    this.props.dispatch({
      type: 'comments/create',
      payload,
    });
  };

  commentDestroy = (id) => {
    const payload = {
      id,
    };

    this.props.dispatch({
      type: 'comments/destroy',
      payload,
    });
  };

  commentReply = (text, parentComment) => {

    const payload = {
      text,
      category: 'requirements',
      appUser: this.props.app.user,
      keys: {
        listKey: this.state.nodeSelected.key,
        listRowKey: this.state.nodeSelected.key,
      },
      currentItem: this.props.requirements.currentItem,
      parentComment,
    };

    this.props.dispatch({
      type: 'comments/create',
      payload,
    });
  };

  fileCreate = (file, listKey, listRowKey) => {
    const payload = {
      file,
      appUser: this.props.app.user,
      category: 'requirements',
      keys: {
        listKey,
        listRowKey,
      },
      currentItem: this.props.currentItem
    };

    this.props.dispatch({
      type: 'files/create',
      payload,
    });
  };

  fileDestroy = (fileId) => {
    const payload = {
      id: fileId,
    };

    this.props.dispatch({
      type: 'files/destroy',
      payload,
    });
  };

  render() {
    const {
      app,
      assets,
      comments,
      currentItem,
      dispatch,
      files,
      location,
      relationships,
      requirements,
      risks,
      spectators,
      users,
    } = this.props;
    const hashId = location.hash.replace('#', '');
    const mentionItems = [];
    let tabSelected = location.search;

    if (!tabSelected.length > 0) {
      tabSelected = 'overview';
    }
    tabSelected = tabSelected.replace('?', '');

    const relationshipData = {
      requirements: [],
      assets: [],
      risks: [],
    };

    if (requirements) {
      requirements.list.forEach(function (item) {
        if (item.type !== 'folder') {
          relationshipData.requirements.push(item);
        }

        mentionItems.push({
          name: item.title,
          value: item.key,
        });
      });
    }

    if (assets) {
      assets.list.forEach(function (item) {
        if (item.type !== 'folder') {
          relationshipData.assets.push(item);
        }

        mentionItems.push({
          name: item.title,
          value: item.key,
        });
      });
    }

    if (risks) {
      risks.list.forEach(function (item) {
        if (item.type !== 'folder') {
          relationshipData.risks.push(item);
        }

        mentionItems.push({
          name: item.title,
          value: item.key,
        });
      });
    }

    const itemNodeRelationships = {};

    if (relationships) {
      relationships.list.forEach(function (item) {
        if (item.from_key === currentItem.key || item.to_key === currentItem.key) {
          const oRow = {};
          oRow.id = item.id;
          oRow.created_at = item.created_at;
          oRow.user = item.user;
          oRow.item = item.from_key === currentItem.key ? item.to_item : item.from_item;

          let nodeKey = item.from_key === currentItem.key ? item.from_key_node : item.to_key_node;

          if (!nodeKey) {
            nodeKey = 'root';
          }

          itemNodeRelationships[nodeKey] = itemNodeRelationships[nodeKey] || [];
          itemNodeRelationships[nodeKey].push(oRow);
        }
      });
    }

    const commentsData = {};

    if (comments) {
      comments.list.forEach(function (item) {
        if (item.category === 'requirements') {
          commentsData[item.type_key_node] = commentsData[item.type_key_node] || [];
          commentsData[item.type_key_node].push(item);
        }
      });
    }

    const itemNodeFiles = {};

    if (files) {
      files.list.forEach(function (item) {
        if (item.category === 'requirements' && item.type_key === currentItem.key) {
          itemNodeFiles[item.type_key_node] = itemNodeFiles[item.type_key_node] || [];
          itemNodeFiles[item.type_key_node].push({ ...item, key: item.id });
        }
      });
    }

    const spectatorsData = {};

    if (spectators && requirements.currentItem) {
      spectators.list.forEach(function (item) {
        if (item.type_key === requirements.currentItem.key) {
          spectatorsData[item.user.id] = spectatorsData[item.user.id] || {};
          spectatorsData[item.user.id] = item;
        }
      });
    }

    const tabActivityData = {};

    if (requirements.currentItem && requirements.currentItem.type !== 'folder' && location.search === '?activity') {
      if (requirements.currentItem.data && requirements.currentItem.data.length > 0) {
        if (comments) {
          comments.list.forEach(function (item) {
            if (item.type_key === requirements.currentItem.key) {
              item.type_category = 'comments';

              const date = item.created_at.substring(0, 10);
              tabActivityData[date] = tabActivityData[date] || [];
              tabActivityData[date].push(item);
            }
          });
        }

        if (relationships) {
          relationships.list.forEach(function (item) {
            if (item.from_key === requirements.currentItem.key || item.to_key === requirements.currentItem.key) {
              item.type_category = 'relationships';

              const date = item.created_at.substring(0, 10);
              tabActivityData[date] = tabActivityData[date] || [];
              tabActivityData[date].push(item);
            }
          });
        }

        if (files) {
          files.list.forEach(function (item) {
            if (item.type_key === requirements.currentItem.key) {
              item.type_category = 'files';

              const date = item.created_at.substring(0, 10);
              tabActivityData[date] = tabActivityData[date] || [];
              tabActivityData[date].push(item);
            }
          });
        }
      }
    }

    const tabCommentsData = [];

    if (requirements.currentItem && requirements.currentItem.type !== 'folder' && location.search === '?comments') {
      if (comments && comments.list.length > 0) {
        comments.list.forEach(function (item) {
          if (item.category === 'requirements' && item.type_key === requirements.currentItem.key) {
            tabCommentsData.push(item);
          }
        });
      }
    }

    let currentItemUserAssigned = null;

    if (currentItem && currentItem.assigned_user) {
      currentItemUserAssigned = users.list.filter(item => item.id === Number(currentItem.assigned_user))[0];
    }

    const itemActivityProps = {
      currentItem: requirements.currentItem,
    };

    const itemConnectionsProps = {
      currentItem,
      dataRelationships: itemNodeRelationships,
      dataFiles: itemNodeFiles,
      type: 'requirements',
    };

    const itemCommentsProps = {
      data: tabCommentsData,
      location,
    };

    const usersAssignedListProps = {
      currentItem,
      cbUpdate: this.requirementUpdate,
    };

    const linkShareProps = {
      cbCreate: this.userAccessAssign,
      currentItem: requirements.currentItem,
      dataUsers: users.list,
      location,
    };

    const requirementsCreateFormProps = {
      type: 'createAlt',
      data: {},
      listRequirements: requirements.list,
      listUsers: users.list,
    };

    return (
      <div className={styles.containerColumns}>
        <div>
          <div id={currentItem.key} className={`u_overflowAuto ${styles.contentSeparator}`}>
            <Menu
              onClick={this.handleClickTabs}
              selectedKeys={[tabSelected]}
              mode="horizontal"
              className="is-bigger is-transparent"
            >
              <Menu.Item key="overview">
                <Icon type="file" />
                Přehled
              </Menu.Item>
              <Menu.Item key="activity" disabled={false}>
                <Icon type="bulb" />
                Aktivity
              </Menu.Item>
              <Menu.Item key="connections">
                <Icon type="share-alt" />
                Propojení
              </Menu.Item>
              <Menu.Item key="comments">
                <Icon type="message" />
                Komentáře
              </Menu.Item>
              <Menu.Item key="users">
                <Icon type="user" />
                Uživatelé
              </Menu.Item>
            </Menu>
          </div>
        </div>
        <div className="u_overflowAuto">
          {
            location.search === '' &&
            <div>
              <div className="contentDark">
                <Row
                  type="flex"
                  justify="space-between"
                  align="middle"
                >
                  <Col>
                    <UserAssign
                      authUser={app.user}
                      title="Správce"
                      user={currentItemUserAssigned}
                      userList={users.list}
                      cbUserAssign={this.userUpdate}
                    />
                  </Col>
                  <Col>
                    <Button
                      className={`${spectatorsData[app.user.id] && spectatorsData[app.user.id].following ? 'is-positive' : ''}`}
                      type="dark"
                      icon="eye-o"
                      onClick={this.handleSpectatorClick(spectatorsData[app.user.id])}
                    >
                      { spectatorsData[app.user.id] && spectatorsData[app.user.id].following ? 'Sledováno' : 'Sledovat' }
                    </Button>
                    <Dropdown
                      placement="bottomRight"
                      trigger={['click']}
                      overlay={
                        <Menu onClick={e => this.handleMenuClick(e)}>
                          <Menu.Item key="update">Upravit</Menu.Item>
                          <Menu.Item key="destroy">Odstranit</Menu.Item>
                        </Menu>
                      }
                    >
                      <Button type="dark" icon="ellipsis" />
                    </Dropdown>
                    {
                      currentItem.type === 'item_alt' &&
                      <Badge
                        count={0}
                      >
                        <Button
                          className="ant-btn-icon-only"
                          style={{ marginLeft: '.5em' }}
                          type="dark"
                          onClick={this.handleNodeClick(currentItem, 'relationships')}
                        >
                          <Icon type="bars" />
                        </Button>
                      </Badge>
                    }
                  </Col>
                </Row>
              </div>
              <div className="content">
                <table className="tableSimple tableLabel">
                  {
                    currentItem.status &&
                    <tr>
                      <td className="cGray">Status:</td>
                      <td><strong>{currentItem.status}</strong></td>
                    </tr>
                  }
                  {
                    currentItem.code &&
                    <tr>
                      <td className="cGray">Číslo:</td>
                      <td><strong>{currentItem.code}</strong></td>
                    </tr>
                  }
                  <tr>
                    <td className="cGray">Platnost:</td>
                    <td><strong>{moment(currentItem.date_valid_from).format('LL')} {currentItem.date_valid_to ? ` – ${moment(currentItem.date_valid_to).format('LL')}` : ''}</strong></td>
                  </tr>
                  {
                    currentItem.updated_at &&
                    <tr>
                      <td className="cGray">Upraveno:</td>
                      <td>
                        <Tooltip
                          placement="top"
                          title="Upravil: Jan Novák"
                        >
                          <strong>{moment(currentItem.updated_at).format('LLL')}</strong>
                        </Tooltip>
                      </td>
                    </tr>
                  }
                  {
                    currentItem.createdFrom &&
                    <tr>
                      <td className="cGray">Odvozeno:</td>
                      <td>
                        <Link to={`/risk/${currentItem.createdFrom.item_type}/item/${currentItem.createdFrom.key}`}>
                          <strong>{currentItem.createdFrom.title}</strong>
                        </Link>
                      </td>
                    </tr>
                  }
                  {
                    currentItem.data &&
                    <tr>
                      <td colSpan={2}>
                        <Row className="nodeContainer textFormatted">
                          {
                            currentItem.data.map(item => (
                              <div
                                key={item.key}
                                id={item.key}
                                className={`node ${item.key === hashId ? 'nodeHighlighted' : ''} ${(itemNodeRelationships[item.key] && itemNodeRelationships[item.key].length > 0) || (commentsData[item.key] && commentsData[item.key].length > 0) || (itemNodeFiles[item.key] && itemNodeFiles[item.key].length > 0) ? 'is-active' : ''}`}
                                data-key={item.key}
                                onClick={this.handleNodeClick(item, 'relationships')}
                              >
                                <div dangerouslySetInnerHTML={{ __html: item.data }} />
                                <div className="node_settings">
                                  <Badge
                                    count={0}
                                  >
                                    <Button
                                      className="node_settings_item ant-btn-icon-only"
                                      style={{ marginLeft: '.5em' }}
                                      type="dark"
                                    >
                                      <Icon type="bars" />
                                    </Button>
                                  </Badge>
                                </div>
                              </div>
                            ))
                          }
                        </Row>
                      </td>
                    </tr>
                  }
                </table>
              </div>
            </div>
          }
          {
            location.search === '?activity' &&
            <ItemActivity {...itemActivityProps} />
          }
          {
            location.search === '?connections' &&
            <ItemConnections {...itemConnectionsProps} />
          }
          {
            location.search === '?comments' &&
            <ItemComments {...itemCommentsProps} />
          }
          {
            location.search === '?users' &&
            <div className="content">
              <UsersAssignedList {...usersAssignedListProps} />
            </div>
          }
        </div>
        {
          this.state.nodeSelected.item &&
          <Modal
            key="sidebarRight"
            wrapClassName="sidebarRight"
            visible={this.state.sidebarRightVisible}
            title={
              <div>
                <Badge
                  className={`${this.state.nodeSelected.type === 'relationships' && 'is-active'}`}
                  count={itemNodeRelationships[this.state.nodeSelected.item.key] ? itemNodeRelationships[this.state.nodeSelected.item.key].length : 0}
                  onClick={this.handleNodeClick(this.state.nodeSelected.item, 'relationships')}
                >
                  <Icon type="share-alt" />
                </Badge>
                <Badge
                  className={`${this.state.nodeSelected.type === 'comments' && 'is-active'}`}
                  count={commentsData[this.state.nodeSelected.item.key] ? commentsData[this.state.nodeSelected.item.key].length : 0}
                  onClick={this.handleNodeClick(this.state.nodeSelected.item, 'comments')}
                >
                  <Icon type="message" />
                </Badge>
                <Badge
                  className={`${this.state.nodeSelected.type === 'files' && 'is-active'}`}
                  count={itemNodeFiles[this.state.nodeSelected.item.key] ? itemNodeFiles[this.state.nodeSelected.item.key].length : 0}
                  onClick={this.handleNodeClick(this.state.nodeSelected.item, 'files')}
                >
                  <Icon type="file" />
                </Badge>
                <span className={`ant-badge ${this.state.nodeSelected.type === 'linkShare' && 'is-active'}`}>
                  <Icon type="link" onClick={this.handleNodeClick(this.state.nodeSelected.item, 'linkShare')}/>
                </span>
              </div>
            }
            footer={null}
            onCancel={() => this.setSidebarRight(false)}
          >
            {
              (
                (currentItem.data && currentItem.data.length > 0)
                || this.state.nodeSelected.item.data
              )
              && this.state.nodeSelected.type !== 'linkShare' &&
              <ShrinkStretch>
                <div className="textFormatted" dangerouslySetInnerHTML={{ __html: currentItem.type === 'item_alt' ? currentItem.data[0].data : this.state.nodeSelected.item.data }} />
              </ShrinkStretch>
            }
            {
              this.state.nodeSelected.type === 'relationships' &&
              <Spin
                className="space"
                spinning={this.props.loadingModelRelationships}
              >
                <div className="section_title">
                  <h3>Vazby</h3>
                </div>
                <RelationshipsList
                  editable
                  data={itemNodeRelationships[this.state.nodeSelected.item.key]
                    ? itemNodeRelationships[this.state.nodeSelected.item.key] : []}
                />
                <Collapse accordion bordered={false}>
                  <Collapse.Panel
                    header={<span style={{ fontSize: '90%', fontWeight: '700', paddingLeft: '.5em' }}>Přiřadit vazbu</span>}
                    key="1"
                  >
                    <div className="contentDark">
                      <RelationshipsCreate
                        type="requirements"
                        data={itemNodeRelationships[this.state.nodeSelected.item.key] ? itemNodeRelationships[this.state.nodeSelected.item.key] : []}
                        dataSelect={relationshipData}
                        currentItem={currentItem}
                        currentNode={this.state.nodeSelected.item}
                        listRequirements={requirements.list}
                        listUsers={users.list}
                        cbCreate={this.relationshipCreate}
                        cbDestroy={this.relationshipDestroy}
                      />
                    </div>
                  </Collapse.Panel>
                  <Collapse.Panel
                    header={<span style={{ fontSize: '90%', fontWeight: '700', paddingLeft: '.5em' }}>Vytvořit odvozený požadavek</span>}
                    key="2"
                  >
                    <div className="contentDark">
                      <RequirementsCreateForm
                        {...requirementsCreateFormProps}
                        cbCreate={this.requirementCreate}
                      />
                    </div>
                  </Collapse.Panel>
                </Collapse>
              </Spin>
            }
            {
              this.state.nodeSelected.type === 'comments' &&
              <Spin className="space" spinning={this.props.loadingModelComments}>
                <ItemCommentsAlt
                  data={commentsData[this.state.nodeSelected.item.key] ? commentsData[this.state.nodeSelected.item.key] : []}
                  typeCreate={true}
                  appUser={app.user}
                  users={users}
                  currentItem={this.state.nodeSelected.item}
                  mentionItems={mentionItems}
                  listKey={this.state.nodeSelected.item.key}
                  listRowKey={this.state.nodeSelected.item.key}
                  cbCreate={this.commentCreate}
                  cbDestroy={this.commentDestroy}
                  cbReply={this.commentReply}
                />
              </Spin>
            }
            {
              this.state.nodeSelected.type === 'files' &&
              <Spin className="space" spinning={this.props.loadingModelFiles}>
                <FileUpload
                  dataTable={itemNodeFiles[this.state.nodeSelected.item.key]}
                  create
                  currentItem={this.state.nodeSelected.item}
                  appUser={app.user}
                  listKey={this.state.nodeSelected.item.key}
                  listRowKey={this.state.nodeSelected.item.key}
                  cbCreate={this.fileCreate}
                  cbDestroy={this.fileDestroy}
                />
              </Spin>
            }
            {
              this.state.nodeSelected.type === 'linkShare' &&
              <LinkShare {...linkShareProps} />
            }
          </Modal>
        }
      </div>
    );
  }
}

RequirementsDetailContainer.propTypes = {
  app: PropTypes.object,
  currentItem: PropTypes.object,
  dispatch: PropTypes.func,
  location: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    app: state.app,
    users: state.users,
    assets: state.assets,
    risks: state.risks,
    relationships: state.relationships,
    loadingModelRelationships: state.loading.models.relationships,
    requirements: state.requirements,
    loadingModelRequirements: state.loading.models.requirements,
    comments: state.comments,
    loadingModelComments: state.loading.models.comments,
    files: state.files,
    loadingModelFiles: state.loading.models.files,
    spectators: state.spectators,
    loadingModelSpectators: state.loading.models.spectators,
  };
}

export default connect(mapStateToProps)(RequirementsDetailContainer);
