import React from 'react';
import PropTypes from 'prop-types';
import styles from './requirementsHeader.less';

class RequirementsHeader extends React.Component {

  render() {
    const { currentItem } = this.props;

    return (
      <div className={styles.headerSecond}>
        <div className={styles.headerSecond_container}>
          <h1>RISK<span
            className="separatorTitle"
          >/</span>Požadavky{currentItem && currentItem.title &&
            <span><span className="separatorTitle">/</span>{currentItem.title}</span>}</h1>
        </div>
      </div>
    );
  }
}

RequirementsHeader.propTypes = {
  user: PropTypes.object,
  currentItem: PropTypes.object,
  tabSelected: PropTypes.string,
  navOpenKeys: PropTypes.array,
  changeOpenKeys: PropTypes.func,
};

export default RequirementsHeader;
