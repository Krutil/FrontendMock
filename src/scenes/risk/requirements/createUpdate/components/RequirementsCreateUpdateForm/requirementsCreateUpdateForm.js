import React from 'react';
import PropTypes from 'prop-types';
import { Form, DatePicker, Input, Select, Button } from 'antd';
import moment from 'moment';

class RequirementsCreateForm extends React.Component {

  state = {
    changed: false,
  };

  handleSubmit = (e) => {
    e.preventDefault();

    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }

      console.log('fieldsValue', fieldsValue);
      // Format date value before submit.
      const date_from = fieldsValue.date_from;
      const date_to = fieldsValue.date_to;

      const values = {
        ...fieldsValue,
        date_from: date_from ? date_from.format('YYYY-MM-DD') : null,
        date_to: date_to ? date_to.format('YYYY-MM-DD') : null,
      };

      values.category = values.category === 'null' ? null : values.category;

      // console.log('values', values);

      let dataObject = {};

      if (this.props.type === 'update') {
        dataObject = this.props.data;
        dataObject.data = this.props.data.data;
      } else {
        dataObject.data = null;
      }

      dataObject.type = values.type;
      dataObject.key_parents = values.category;
      dataObject.title = values.title;
      dataObject.code = values.code;
      dataObject.assigned_user = values.assigned_user;
      dataObject.date_from = values.date_from;
      dataObject.date_to = values.date_to;

      if (values.description) {
        dataObject.data = [];
        dataObject.data.push(values.description);
      }

      console.log('dataObject', dataObject);

      if (this.props.type !== 'update') {
        this.props.cbCreate(dataObject);
      }

      if (this.props.type === 'update') {
        this.props.cbUpdate(dataObject);
      }

      this.props.form.resetFields();

      this.setState({
        changed: false,
      });
    });
  };

  handleFormChange = (changedFields) => {
    // console.log('changedFields', changedFields);

    this.setState({
      changed: true,
    });
  }

  render() {
    const { data, listRequirements, listUsers } = this.props;
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 9 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 15 },
      },
    };

    // console.log('listRequirements', listRequirements);
    // console.log('listUsers', listUsers);

    const date_now = moment();
    let dateConfigFrom = {
      initialValue: moment(date_now, 'YYYY-MM-DD'),
      rules: [{ required: true, message: 'Vyberte datum.' }],
    };
    let dateConfigTo = {
      rules: [{ required: false, message: 'Vyberte datum.' }],
    };

    if (data.date_from) {
      dateConfigFrom = {
        initialValue: moment(data.date_from, 'YYYY-MM-DD'),
        rules: [{ required: true, message: 'Vyberte datum.' }],
      };
    }

    if (data.date_to) {
      dateConfigTo = {
        initialValue: moment(data.date_to, 'YYYY-MM-DD'),
        rules: [{ required: false, message: 'Vyberte datum.' }],
      };
    }

    if (!data) {
      return null;
    }

    return (
      <Form
        className="contentFormCentered"
        onChange={this.handleFormChange}
        onSubmit={this.handleSubmit}
      >
        <Form.Item {...formItemLayout} label="Název:">
          {getFieldDecorator('title', {
            initialValue: data.title,
            rules: [
              {
                required: true,
                message: 'Nutné vyplnit',
              },
            ],
          })(<Input />)}
        </Form.Item>
        <Form.Item {...formItemLayout} label="Číslo záznamu:">
          {getFieldDecorator('code', {
            initialValue: data.code,
          })(<Input />)}
        </Form.Item>
        {
          this.props.type !== 'createAlt' &&
          <Form.Item {...formItemLayout} label="Typ:">
            {getFieldDecorator('type', {
              initialValue: data.type,
              rules: [
                {
                  required: true,
                  message: 'Prosím vyberte hodnotu.',
                },
              ],
            })(<Select placeholder="Vyberte typ požadavku">
              <Select.Option key="item" value="item">Právní a jiný požadavek</Select.Option>
              <Select.Option key="item_alt" value="item_alt">Odvozený požadavek</Select.Option>
            </Select>)}
          </Form.Item>
        }
        {
          this.props.type !== 'createAlt' &&
          <Form.Item {...formItemLayout} label="Kategorie:">
            {getFieldDecorator('category', {
              initialValue: data.key_parents ? data.key_parents : null,
            })(<Select
              showSearch
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              onChange={this.handleFormChange}
            >
              <Select.Option key="null" value={null}>Bez kategorie</Select.Option>
              {
                listRequirements && listRequirements.map(item => (
                  <Select.Option
                    key={item.key}
                    value={item.key_parents ? `${item.key_parents}.${item.key}` : item.key}
                  >{ item.title }</Select.Option>
                ))
              }
            </Select>)}
          </Form.Item>
        }
        <Form.Item {...formItemLayout} label="Přiřazený uživatel:">
          {getFieldDecorator('assigned_user', {
            initialValue: data.assigned_user ? data.assigned_user : null,
          })(<Select
            showSearch
            optionFilterProp="children"
            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            onChange={this.handleFormChange}
          >
            <Select.Option key="null" value={null}>Nepřiřazeno</Select.Option>
            {
              listUsers.map(item => (
                <Select.Option
                  key={item.id.toString()}
                  value={item.id.toString()}
                >{ `${item.firstname} ${item.lastname}` }</Select.Option>
              ))
            }
          </Select>)}
        </Form.Item>
        <Form.Item {...formItemLayout} label="Platnost od:">
          {getFieldDecorator('date_from', dateConfigFrom)(
            <DatePicker
              format="YYYY-MM-DD"
              style={{ width: '100%' }}
              onChange={this.handleFormChange}
            />,
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} label="Platnost do:">
          {getFieldDecorator('date_to', dateConfigTo)(
            <DatePicker
              format="YYYY-MM-DD"
              style={{ width: '100%' }}
              onChange={this.handleFormChange}
            />,
          )}
        </Form.Item>
        {
          !data.data &&
          <Form.Item {...formItemLayout} label="Popis:">
            {getFieldDecorator('description', {
              initialValue: data.description,
            })(<Input type="textarea" style={{ width: '100%', minHeight: '6em' }} />)}
          </Form.Item>
        }
        {
          this.props.type === 'create' &&
          <div className="u_textRight">
            <Button type="primary" htmlType="submit" size="large">Vytvořit</Button>
          </div>
        }
        {
          this.props.type === 'createAlt' &&
          <div className="u_textRight">
            <Button type="primary" htmlType="submit" size="default">Vytvořit odvozený
              požadavek</Button>
          </div>
        }
        {
          this.props.type === 'update' && this.state.changed &&
          <div className="u_textRight">
            <Button type="primary" htmlType="submit" size="large">Uložit změny</Button>
          </div>
        }
      </Form>
    );
  }
}

RequirementsCreateForm.propTypes = {
  form: PropTypes.object.isRequired,
  type: PropTypes.string,
  data: PropTypes.object,
  listRequirements: PropTypes.array,
  listUsers: PropTypes.array,
};

export default Form.create()(RequirementsCreateForm);
