import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'dva/router';
import { connect } from 'dva';
import {
  Spin,
} from 'antd';
import pathToRegexp from 'path-to-regexp';
import moment from 'moment';
import 'moment/locale/cs';

import HeaderSecond from '../components/RequirementsHeader/requirementsHeader';
import RequirementsCreateUpdateForm from './components/RequirementsCreateUpdateForm/requirementsCreateUpdateForm';

moment.locale('cs');

class RequirementsCreateUpdateContainer extends React.Component {

  requirementCreate = (item) => {
    const payload = {
      item,
      appUser: this.props.app.user,
    };

    this.props.dispatch({
      type: 'requirements/create',
      payload,
    });

    browserHistory.push('/risk/requirements');
  };

  requirementUpdate = (item) => {
    const { app, location } = this.props;

    const payload = {
      item,
      appUser: app.user,
    };

    this.props.dispatch({
      type: 'requirements/update',
      payload,
    });
  };

  render() {
    const { location, app, requirements, assets, risks, relationships, comments, files, spectators, users, dispatch } = this.props;
    const mentionItems = [];

    const url = pathToRegexp('/risk/requirements/item/:id/:action?').exec(location.pathname);
    const urlAction = url && url.length > 2 ? url[2] : '';


    const headerSecondProps = {
      location,
      changeOpenKeys(openKeys) {
        localStorage.setItem('navOpenKeys', JSON.stringify(openKeys));
        dispatch({ type: 'app/handleNavOpenKeys', payload: { navOpenKeys: openKeys } });
      },
    };

    const relationshipData = {
      requirements: [],
      assets: [],
      risks: [],
    };

    requirements && requirements.list.forEach(function (item) {
      if (item.type !== 'folder') {
        const dataObject = {};
        dataObject.key = item.key;
        dataObject.title = item.title;
        relationshipData.requirements.push(dataObject);
      }

      mentionItems.push({
        name: item.title,
        value: item.key,
      });
    });

    assets && assets.list.forEach(function (item) {
      if (item.type !== 'folder') {
        const dataObject = {};
        dataObject.key = item.key;
        dataObject.title = item.title;
        relationshipData.assets.push(dataObject);
      }

      mentionItems.push({
        name: item.title,
        value: item.key,
      });
    });

    risks && risks.list.forEach(function (item) {
      if (item.type !== 'folder') {
        const dataObject = {};
        dataObject.key = item.key;
        dataObject.title = item.title;
        relationshipData.risks.push(dataObject);
      }

      mentionItems.push({
        name: item.title,
        value: item.key,
      });
    });

    const relationshipsTableData = {};

    if (relationships) {
      relationships.list.forEach(function (item) {
        relationshipsTableData[item.type_key_node] = relationshipsTableData[item.type_key_node] || [];
        relationshipsTableData[item.type_key_node].push(item);
      });
    }

    const commentsData = {};

    if (comments) {
      comments.list.forEach(function (item) {
        if (item.category === 'requirements') {
          commentsData[item.type_key_node] = commentsData[item.type_key_node] || [];
          commentsData[item.type_key_node].push(item);
        }
      });
    }

    const filesTableData = {};

    if (files) {
      files.list.forEach(function (item) {
        if (item.category === 'requirements') {
          filesTableData[item.type_key_node] = filesTableData[item.type_key_node] || [];
          filesTableData[item.type_key_node].push({ ...item, key: item.id });
        }
      });
    }

    const spectatorsData = {};

    if (spectators && requirements.currentItem) {
      spectators.list.forEach(function (item) {
        if (item.type_key === requirements.currentItem.key) {
          spectatorsData[item.user.id] = spectatorsData[item.user.id] || {};
          spectatorsData[item.user.id] = item;
        }
      });
    }

    const requirementsCreateFormProps = {
      data: requirements.currentItem ? requirements.currentItem : {},
      listRequirements: requirements.list,
      listUsers: users.list,
      type: 'create',
      onOk(data) {
        // console.log('onOk', data);
      },
      onCancel() {
        // console.log('onCancel');
      },
    };

     console.log('location', location);

    return (
      <div>
        <HeaderSecond
          {...headerSecondProps} currentItem={requirements.currentItem}
          spectatorsData={spectatorsData} tabSelected={location.search}
        />
        <div>
          <div className="container is-secondHeader">
            <div className="section" style={{ maxWidth: '1000px' }}>
              <Spin spinning={this.props.loadingModelRequirements}>
                <div className="section_content">
                  <div>
                    <div className="section_title">
                      <h2>
                        { location.pathname === '/risk/requirements/create'
                          ? 'Vytvořit požadavek' : 'Upravit požadavek' }
                      </h2>
                    </div>
                    <div className="contentDark">
                      {
                        location.pathname === '/risk/requirements/create' &&
                        <RequirementsCreateUpdateForm
                          {...requirementsCreateFormProps}
                          type="create"
                          cbCreate={this.requirementCreate}
                        />
                      }
                      {
                        urlAction === 'update' &&
                        <RequirementsCreateUpdateForm
                          {...requirementsCreateFormProps}
                          type="update"
                          cbUpdate={this.requirementUpdate}
                        />
                      }
                    </div>
                  </div>
                </div>
              </Spin>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

RequirementsCreateUpdateContainer.propTypes = {
  location: PropTypes.object,
  dispatch: PropTypes.func,
  app: PropTypes.object,
  requirements: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    app: state.app,
    users: state.users,
    assets: state.assets,
    risks: state.risks,
    relationships: state.relationships,
    loadingModelRelationships: state.loading.models.relationships,
    requirements: state.requirements,
    loadingModelRequirements: state.loading.models.requirements,
    comments: state.comments,
    loadingModelComments: state.loading.models.comments,
    files: state.files,
    loadingModelFiles: state.loading.models.files,
    spectators: state.spectators,
    loadingModelSpectators: state.loading.models.spectators,
  };
}

export default connect(mapStateToProps)(RequirementsCreateUpdateContainer);
