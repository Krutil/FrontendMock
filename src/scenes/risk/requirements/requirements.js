import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'dva/router';
import { connect } from 'dva';
import {
  Layout,
  Icon,
  Spin,
  Button,
} from 'antd';
import moment from 'moment';
import 'moment/locale/cs';

import HeaderSecond from './components/RequirementsHeader/requirementsHeader';
import TreeList from '../../../components/common/TreeList/treeList';
import RequirementsDetail from './components/RequirementsDetail/requirementsDetail';
import FolderTable from '../../../components/common/FolderTable/folderTable';
import ModalFolderCreate from '../../../components/common/modal/FolderCreate/folderCreate';

moment.locale('cs');

class RequirementsContainer extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      nodeInfo: {
        key: null,
        type: null,
      },
    };
  }

  handleRequirementsCreate = () => {
    browserHistory.push('/risk/requirements/create');
  };

  handleFolderCreate = () => {
    this.props.dispatch({
      type: 'requirements/showModal',
      payload: {
        modal: {
          visible: true,
          type: 'modalFolder',
          action: 'create',
          data: {
            tree: this.props.requirements.list,
          },
        },
      },
    });
  };

  render() {
    const { location, app, requirements, dispatch } = this.props;
    const { currentItem } = requirements;

    const headerSecondProps = {
      location,
      changeOpenKeys(openKeys) {
        localStorage.setItem('navOpenKeys', JSON.stringify(openKeys));
        dispatch({ type: 'app/handleNavOpenKeys', payload: { navOpenKeys: openKeys } });
      },
    };

    const requirementsDetailProps = {
      currentItem,
      location,
    };

    const modalFolderProps = {
      modal: requirements.modal,
      onOk(data) {
        dispatch({
          type: 'requirements/folderCreate',
          payload: data,
        });
      },
      onCancel() {
        dispatch({
          type: 'requirements/hideModal',
        });
      },
    };

    /* TODO ujasnit si s BE jak filtrovat a zda na obou stranach */
    const dataList = this.props.requirements.list.filter(item =>
    (!item.private || Number(item.assigned_user) === this.props.app.user.id));

    const treeListProps = {
      dataList,
      dataType: 'requirements',
      currentItem: requirements.currentItem,
      app,
      dispatch,
    };

    const actualCurrentItem = currentItem
      && requirements.list.find(item => item.key === currentItem.key);

    const folderTableProps = {
      dataType: 'requirements',
      currentItem: actualCurrentItem,
    };

    return (
      <div>
        <HeaderSecond
          {...headerSecondProps}
          currentItem={requirements.currentItem}
          tabSelected={location.search}
        />
        <div>
          <div className="container is-secondHeader">
            {
              location.pathname !== '/risk/requirements/create' &&
              <div
                className={`section is-sidebar is-fullHeight ${app.AppPanelTreeUnfold ? 'is-unfolded' : ''}`}
              >
                <Spin spinning={this.props.loadingModelRequirements}>
                  <Layout className="section_content">
                    <TreeList {...treeListProps} />
                  </Layout>
                </Spin>
              </div>
            }
            <div className="section is-fullHeight" style={{ maxWidth: '1000px' }}>
              <Spin spinning={this.props.loadingModelRequirements}>
                <Layout className="section_content">
                  {
                    location.pathname === '/risk/requirements' && !requirements.currentItem &&
                    <div className="content">
                      <div style={{ marginBottom: 24, textAlign: 'right' }}>
                        <Button type="dark" onClick={this.handleRequirementsCreate}>
                          Vytvořit požadavek
                        </Button>
                        <Button
                          type="dark" onClick={this.handleFolderCreate}
                          style={{ marginLeft: '.75em' }}
                        ><Icon type="folder-add" /></Button>
                      </div>
                      <div
                        style={{
                          maxWidth: '400px',
                          margin: '-150px 0 0 -200px',
                          position: 'absolute',
                          top: '50%',
                          left: '50%',
                          textAlign: 'center',
                        }}
                      >
                        <p style={{ fontSize: '6em', color: '#ededf8' }}>
                          <Icon type="file-text" />
                        </p>
                        <p style={{ fontSize: '1.5em' }}>Vyberte záznam</p>
                        <p style={{ color: '#bbbbc4' }}>
                          Z levého menu vyberte název požadavku pro zobrazení jeho detailu nebo klikněte na adresář pro jejich přehled.
                        </p>
                      </div>
                    </div>
                  }
                  {
                    requirements.currentItem && requirements.currentItem.type === 'folder' &&
                    <div className="containerColumns">
                      <div className="section_title">
                        <h3>Obsah</h3>
                      </div>
                      <div className="u_overflowAuto">
                        <FolderTable {...folderTableProps} />
                      </div>
                    </div>
                  }
                  {
                    requirements.currentItem && requirements.currentItem.type !== 'folder' &&
                    <RequirementsDetail {...requirementsDetailProps} />
                  }
                </Layout>
              </Spin>
            </div>
          </div>
        </div>
        { requirements.modal.type === 'modalFolder' && <ModalFolderCreate {...modalFolderProps} /> }
      </div>
    );
  }
}

RequirementsContainer.propTypes = {
  location: PropTypes.object,
  dispatch: PropTypes.func,
  app: PropTypes.object,
  requirements: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    app: state.app,
    users: state.users,
    assets: state.assets,
    risks: state.risks,
    relationships: state.relationships,
    loadingModelRelationships: state.loading.models.relationships,
    requirements: state.requirements,
    loadingModelRequirements: state.loading.models.requirements,
    comments: state.comments,
    loadingModelComments: state.loading.models.comments,
    files: state.files,
    loadingModelFiles: state.loading.models.files,
    spectators: state.spectators,
    loadingModelSpectators: state.loading.models.spectators,
  };
}

export default connect(mapStateToProps)(RequirementsContainer);
