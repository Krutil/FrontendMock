import React from 'react';
import PropTypes from 'prop-types';
import styles from './risksHeader.less';

class RisksHeader extends React.Component {

  render() {
    const { currentItem } = this.props;

    return (
      <div className={styles.headerSecond}>
        <div className={styles.headerSecond_container}>
          <h1>RISK<span
            className="separatorTitle"
          >/</span>Rizika{currentItem && currentItem.title &&
          <span><span className="separatorTitle">/</span>{currentItem.title}</span>}</h1>
        </div>
      </div>
    );
  }
}

RisksHeader.propTypes = {
  currentItem: PropTypes.object,
};

export default RisksHeader;
