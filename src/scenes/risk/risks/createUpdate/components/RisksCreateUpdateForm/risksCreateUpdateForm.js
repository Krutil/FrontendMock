import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  DatePicker,
  Form,
  Input,
  Modal,
  Select,
} from 'antd';
import moment from 'moment';

class RisksCreateUpdateForm extends React.Component {

  state = {
    changed: false,
    modalNoteUpdate: false,
  };

  handleSubmit = (e) => {
    e.preventDefault();

    const {
      cbCreate,
      cbUpdate,
      data,
      form,
      type,
    } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }

      // Should format date value before submit.
      const rangeValue = fieldsValue.date_valid;
      const values = {
        ...fieldsValue,
        date_valid: [rangeValue[0].format('YYYY-MM-DD'), rangeValue[1].format('YYYY-MM-DD')],
      };

      values.category = values.category === 'null' ? null : values.category;
      let dataObject = {};

      if (type === 'update') {
        dataObject = data;
      } else {
        dataObject.status = 'inProgress';
      }

      dataObject.type = 'item';
      dataObject.key_parents = values.category;
      dataObject.private = values.private;
      dataObject.title = values.title;
      dataObject.number = values.number;
      dataObject.assigned_user = values.assigned_user;
      dataObject.source = values.source;
      dataObject.date_valid_from = values.date_valid[0];
      dataObject.date_valid_to = values.date_valid[1];
      dataObject.cause = values.cause;
      dataObject.impact = values.impact;

      if (type === 'create') {
        cbCreate(dataObject);
      }

      if (type === 'update') {
        cbUpdate(dataObject);
      }

      this.setState({
        changed: false,
        modalNoteUpdate: false,
      });
    });
  };

  handleFormChange = (changedFields) => {
    this.setState({
      changed: true,
    });
  };

  modalNoteUpdateVisible = (e) => {
    e.preventDefault();

    this.setState({
      modalNoteUpdate: true,
    });
  };

  modalNoteUpdateInvisible = () => {
    this.setState({
      modalNoteUpdate: false,
    });
  };

  render() {
    const { modalNoteUpdate } = this.state;
    const {
      dataRisks,
      dataUsers,
      settingsRisks,
    } = this.props;
    const { getFieldDecorator } = this.props.form;
    const dataUsersFiltered = dataUsers.filter(item => item.role === 'Vlastník rizik');
    let { data } = this.props;

    let rangePlanConfig = {
      rules: [{ type: 'array', required: true, message: 'Vyberte datum.' }],
    };

    if (data) {
      if (data.date_valid_from && data.date_valid_to) {
        rangePlanConfig = {
          initialValue: [moment(data.date_valid_from, 'YYYY-MM-DD'), moment(data.date_valid_to, 'YYYY-MM-DD')],
          rules: [{ type: 'array', required: true, message: 'Vyberte datum.' }],
        };
      }
    } else {
      data = {};
    }

    const modalProps = {
      title: 'Poznámka k úpravě',
      visible: modalNoteUpdate,
      onOk: this.handleSubmit,
      onCancel: this.modalNoteUpdateInvisible,
      wrapClassName: 'vertical-center-modal',
      okText: 'Potvrdit',
      cancelText: 'Zpět',
    };

    return (
      <div>
        <Form
          layout="vertical"
          className="contentFormCentered"
          style={{ maxWidth: '600px', margin: '2em auto' }}
          onChange={this.handleFormChange}
          onSubmit={
            settingsRisks.noteUpdate
            && this.props.type === 'update'
            && data.status === 'accepted'
              ? this.modalNoteUpdateVisible : this.handleSubmit
          }
        >
          <Form.Item label="Název:">
            {getFieldDecorator('title', {
              initialValue: data.title,
              rules: [
                {
                  required: true,
                  message: 'Nutné vyplnit',
                },
              ],
            })(<Input />)}
          </Form.Item>
          <Form.Item label="Přístup:">
            {getFieldDecorator('private', {
              initialValue: data.private !== undefined ? data.private : 0,
            })(<Select
              showSearch
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              onChange={this.handleFormChange}
            >
              <Select.Option key="0" value={0}>Veřejný</Select.Option>
              <Select.Option key="1" value={1}>Vybraní uživatelé</Select.Option>
            </Select>)}
          </Form.Item>
          <Form.Item label="Kategorie:">
            {getFieldDecorator('category', {
              initialValue: data.key_parents ? data.key_parents : null,
            })(<Select
              showSearch
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              onChange={this.handleFormChange}
            >
              <Select.Option key="null" value={null}>Bez kategorie</Select.Option>
              {
                dataRisks && dataRisks.map(item => (
                  <Select.Option
                    key={item.key}
                    value={item.key_parents ? `${item.key_parents}.${item.key}` : item.key}
                  >{ item.title }</Select.Option>
                ))
              }
            </Select>)}
          </Form.Item>
          <Form.Item label="Číslo záznamu:">
            {getFieldDecorator('number', {
              initialValue: data.number,
            })(<Input />)}
          </Form.Item>
          <Form.Item label="Přiřazený uživatel:">
            {getFieldDecorator('assigned_user', {
              initialValue: data.assigned_user ? String(data.assigned_user) : null,
            })(<Select
              showSearch
              optionFilterProp="children"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              onChange={this.handleFormChange}
            >
              <Select.Option key="null" value={null}>Nepřiřazeno</Select.Option>
              {
                dataUsers.map(item => (
                  <Select.Option key={item.id.toString()}>
                    { `${item.firstname} ${item.lastname}` }
                  </Select.Option>
                ))
              }
            </Select>)}
          </Form.Item>
          <Form.Item label="Platnost rizika:">
            {getFieldDecorator('date_valid', rangePlanConfig)(
              <DatePicker.RangePicker
                format="YYYY-MM-DD"
                style={{ width: '100%' }}
                onChange={this.handleFormChange}
              />,
            )}
          </Form.Item>
          <Form.Item label="Kategorie původu:">
            {getFieldDecorator('source', {
              initialValue: data.source ? data.source : undefined,
              rules: [
                {
                  required: true,
                  message: 'Nutné vybrat',
                },
              ],
            })(<Select
              mode="multiple"
              labelInValue
              placeholder="Vyberte jednu nebo více kategorií"
              onChange={this.handleFormChange}
            >
              <Select.Option key="c1" value="Přírodní původ">Přírodní původ</Select.Option>
              <Select.Option key="c2" value="Fyzikální původ">Fyzikální původ</Select.Option>
              <Select.Option key="c3" value="Lidský faktor">Lidský faktor</Select.Option>
              <Select.Option key="c4" value="Legislativně právní faktor">Legislativně právní
                faktor</Select.Option>
            </Select>)}
          </Form.Item>
          <Form.Item label="Příčina:">
            {getFieldDecorator('cause', {
              initialValue: data.cause,
            })(<Input type="textarea" style={{ width: '100%', minHeight: '8em' }} />)}
          </Form.Item>
          <Form.Item label="Dopad:">
            {getFieldDecorator('impact', {
              initialValue: data.impact,
            })(<Input type="textarea" style={{ width: '100%', minHeight: '8em' }} />)}
          </Form.Item>
          {
            this.props.type === 'create' &&
            <Form.Item>
              <Button type="primary" htmlType="submit" size="large">Vytvořit</Button>
            </Form.Item>
          }
          {
            this.props.type === 'update' && this.state.changed &&
            <Form.Item>
              <Button type="primary" htmlType="submit" size="large">Uložit změny</Button>
            </Form.Item>
          }
        </Form>
        <Modal {...modalProps}>
          <Form layout="vertical">
            {
              getFieldDecorator('noteUpdateText')(
                <Input type="textarea" style={{ width: '100%', minHeight: '8em' }} />,
              )
            }
          </Form>
        </Modal>
      </div>
    );
  }
}

RisksCreateUpdateForm.propTypes = {
  data: PropTypes.object,
  dataRisks: PropTypes.array,
  dataUsers: PropTypes.array,
  form: PropTypes.object.isRequired,
  settingsRisks: PropTypes.object.isRequired,
  type: PropTypes.string,
};

export default Form.create()(RisksCreateUpdateForm);
