import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'dva/router';
import { connect } from 'dva';
import {
  Spin,
} from 'antd';
import pathToRegexp from 'path-to-regexp';
import moment from 'moment';
import 'moment/locale/cs';

import HeaderSecond from '../components/RisksHeader/risksHeader';
import RisksCreateUpdateForm from './components/RisksCreateUpdateForm/risksCreateUpdateForm';

moment.locale('cs');

class RisksCreateUpdateContainer extends React.Component {

  riskCreate = (item) => {
    const payload = {
      item,
      appUser: this.props.app.user,
    };

    this.props.dispatch({
      type: 'risks/create',
      payload,
    });

    browserHistory.push('/risk/risks');
  };

  riskUpdate = (item) => {
    const { app, location } = this.props;

    const payload = {
      item,
      appUser: app.user,
    };

    this.props.dispatch({
      type: 'risks/update',
      payload,
    });
  };

  render() {
    const {
      app,
      assets,
      comments,
      dispatch,
      files,
      location,
      relationships,
      requirements,
      risks,
      settings,
      spectators,
      users,
    } = this.props;
    const mentionItems = [];
    const url = pathToRegexp('/risk/risks/item/:id/:action?').exec(location.pathname);
    const urlAction = url && url.length > 2 ? url[2] : '';

    const headerSecondProps = {
      location,
      changeOpenKeys(openKeys) {
        localStorage.setItem('navOpenKeys', JSON.stringify(openKeys));
        dispatch({ type: 'app/handleNavOpenKeys', payload: { navOpenKeys: openKeys } });
      },
    };

    const relationshipData = {
      assets: [],
      requirements: [],
      risks: [],
    };

    assets && assets.list.forEach(function (item) {
      if (item.type !== 'folder') {
        const dataObject = {};
        dataObject.key = item.key;
        dataObject.title = item.title;
        relationshipData.assets.push(dataObject);
      }

      mentionItems.push({
        name: item.title,
        value: item.key,
      });
    });

    requirements && requirements.list.forEach(function (item) {
      if (item.type !== 'folder') {
        const dataObject = {};
        dataObject.key = item.key;
        dataObject.title = item.title;
        relationshipData.requirements.push(dataObject);
      }

      mentionItems.push({
        name: item.title,
        value: item.key,
      });
    });

    risks && risks.list.forEach(function (item) {
      if (item.type !== 'folder') {
        const dataObject = {};
        dataObject.key = item.key;
        dataObject.title = item.title;
        relationshipData.risks.push(dataObject);
      }

      mentionItems.push({
        name: item.title,
        value: item.key,
      });
    });

    const relationshipsTableData = {};

    if (relationships) {
      relationships.list.forEach(function (item) {
        relationshipsTableData[item.type_key_node] = relationshipsTableData[item.type_key_node] || [];
        relationshipsTableData[item.type_key_node].push(item);
      });
    }

    const commentsData = {};

    if (comments) {
      comments.list.forEach(function (item) {
        if (item.category === 'risks') {
          commentsData[item.type_key_node] = commentsData[item.type_key_node] || [];
          commentsData[item.type_key_node].push(item);
        }
      });
    }

    const filesTableData = {};

    if (files) {
      files.list.forEach(function (item) {
        if (item.category === 'risks') {
          filesTableData[item.type_key_node] = filesTableData[item.type_key_node] || [];
          filesTableData[item.type_key_node].push({ ...item, key: item.id });
        }
      });
    }

    const spectatorsData = {};

    if (spectators && risks.currentItem) {
      spectators.list.forEach(function (item) {
        if (item.type_key === risks.currentItem.key) {
          spectatorsData[item.user.id] = spectatorsData[item.user.id] || {};
          spectatorsData[item.user.id] = item;
        }
      });
    }

    const risksCreateFormProps = {
      data: risks.currentItem ? risks.currentItem : null,
      dataRisks: risks.list,
      dataUsers: users.list,
      settingsRisks: settings.risk.risks,
      type: 'create',
    };

    return (
      <div>
        <HeaderSecond
          {...headerSecondProps}
          currentItem={risks.currentItem}
          spectatorsData={spectatorsData} tabSelected={location.search}
        />
        <div>
          <div className="container is-secondHeader">
            <div className="section" style={{ maxWidth: '1000px' }}>
              <Spin spinning={this.props.loadingModelRisks}>
                <div className="section_content">
                  <div>
                    <div className="section_title">
                      <h2>
                        { location.pathname === '/risk/risks/create'
                          ? 'Vytvořit riziko' : 'Upravit riziko' }
                      </h2>
                    </div>
                    <div className="contentDark">
                      {
                        location.pathname === '/risk/risks/create' &&
                        <RisksCreateUpdateForm
                          {...risksCreateFormProps}
                          type="create"
                          cbCreate={this.riskCreate}
                        />
                      }
                      {
                        urlAction === 'update' &&
                        <RisksCreateUpdateForm
                          {...risksCreateFormProps}
                          type="update"
                          cbUpdate={this.riskUpdate}
                        />
                      }
                    </div>
                  </div>
                </div>
              </Spin>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

RisksCreateUpdateContainer.propTypes = {
  location: PropTypes.object,
  dispatch: PropTypes.func,
  app: PropTypes.object,
  risks: PropTypes.object,
  loadingModelRisks: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    app: state.app,
    assets: state.assets,
    comments: state.comments,
    files: state.files,
    loadingModelAssets: state.loading.models.assets,
    loadingModelComments: state.loading.models.comments,
    loadingModelFiles: state.loading.models.files,
    loadingModelRelationships: state.loading.models.relationships,
    loadingModelRisks: state.loading.models.risks,
    loadingModelSpectators: state.loading.models.spectators,
    relationships: state.relationships,
    risks: state.risks,
    settings: state.settings,
    spectators: state.spectators,
    users: state.users,
  };
}

export default connect(mapStateToProps)(RisksCreateUpdateContainer);
