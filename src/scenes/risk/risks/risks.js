import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'dva/router';
import { connect } from 'dva';
import {
  Layout,
  Icon,
  Spin,
  Button,
} from 'antd';
import moment from 'moment';
import 'moment/locale/cs';

import HeaderSecond from './components/RisksHeader/risksHeader';
import TreeList from '../../../components/common/TreeList/treeList';
import RisksDetail from './components/RisksDetail/risksDetail';
import ModalFolderCreate from '../../../components/common/modal/FolderCreate/folderCreate';
import FolderTable from '../../../components/common/FolderTable/folderTable';

moment.locale('cs');

class RisksContainer extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      expandedKeys: [],
      searchValue: '',
      selectedKeys: [],
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.risks.currentItem !== this.props.risks.currentItem) {
      this.setState({
        expandedKeys: [],
        searchValue: '',
        selectedKeys: [],
      });
    }
  }

  handleRisksCreateClick = () => {
    browserHistory.push('/risk/risks/create');
  };

  handleFolderCreate = () => {
    this.props.dispatch({
      type: 'risks/showModal',
      payload: {
        modal: {
          visible: true,
          type: 'modalFolder',
          action: 'create',
          data: {
            tree: this.props.risks.list,
          },
        },
      },
    });
  };

  render() {
    const {
      searchValue,
      selectedKeys,
      expandedKeys,
    } = this.state;
    const {
      app,
      dispatch,
      location,
      risks,
      spectators,
    } = this.props;
    const { modal, currentItem } = risks;

    if (
      currentItem
      && currentItem.key
      && selectedKeys[0] !== currentItem.key
      && searchValue === ''
    ) {
      if (currentItem.key_parents) {
        const keysParents = currentItem.key_parents.split('.');

        keysParents.forEach(function (key) {
          expandedKeys.push(key);
        });
      }

      if (currentItem.children) {
        expandedKeys.push(currentItem.key);
      }

      selectedKeys.push(currentItem.key);
    }

    const spectatorsData = {};

    if (spectators && currentItem) {
      spectators.list.forEach(function (item) {
        if (item.type_key === currentItem.key) {
          spectatorsData[item.user.id] = spectatorsData[item.user.id] || {};
          spectatorsData[item.user.id] = item;
        }
      });
    }

    const risksDetailProps = {
      currentItem,
      spectatorsData,
      location,
    };

    /* Vytvoreni adresare */
    const modalFolderProps = {
      modal,
      onOk(data) {
        dispatch({
          type: 'risks/folderCreate',
          payload: data,
        });
      },
      onCancel() {
        dispatch({
          type: 'risks/hideModal',
        });
      },
    };

    /* TODO ujasnit si s BE jak filtrovat a zda na obou stranach */
    const dataList = this.props.risks.list.filter(item =>
    (item.status === 'accepted' || item.type === 'folder')
    && (!item.private || Number(item.assigned_user) === this.props.app.user.id));

    /* Strom v levem panelu s riziky */
    const treeListProps = {
      dataList,
      dataType: 'risks',
      currentItem: risks.currentItem,
      app,
      dispatch,
    };

    const headerSecondProps = {
      currentItem: risks.currentItem,
    };

    const actualCurrentItem = currentItem
      && risks.list.find(item => item.key === currentItem.key);

    const folderTableProps = {
      dataType: 'risks',
      currentItem: actualCurrentItem,
    };

    return (
      <div>
        <HeaderSecond {...headerSecondProps} />
        <div className="container is-secondHeader">
          {
            location.pathname !== '/risk/risks/create' &&
            <div
              className={`section is-sidebar is-fullHeight ${app.AppPanelTreeUnfold ? 'is-unfolded' : ''}`}
            >
              <Spin spinning={this.props.loadingModelRisks}>
                <Layout className="section_content">
                  <TreeList {...treeListProps} />
                </Layout>
              </Spin>
            </div>
          }
          <div className="section is-fullHeight" style={{ maxWidth: '1000px' }}>
            <Spin spinning={this.props.loadingModelRisks}>
              <Layout className="section_content">
                {
                  location.pathname === '/risk/risks' && !risks.currentItem &&
                  <div className="content">
                    <div style={{ marginBottom: 24, textAlign: 'right' }}>
                      <Button type="dark" onClick={this.handleRisksCreateClick}>
                        Vytvořit riziko
                      </Button>
                      <Button
                        type="dark" onClick={this.handleFolderCreate}
                        style={{ marginLeft: '.75em' }}
                      ><Icon type="folder-add" /></Button>
                    </div>
                    <div
                      style={{
                        maxWidth: '400px',
                        margin: '-150px 0 0 -200px',
                        position: 'absolute',
                        top: '50%',
                        left: '50%',
                        textAlign: 'center',
                      }}
                    >
                      <p style={{ fontSize: '6em', color: '#ededf8' }}>
                        <Icon type="file-text" />
                      </p>
                      <p style={{ fontSize: '1.5em' }}>Vyberte záznam</p>
                      <p style={{ color: '#bbbbc4' }}>Z levého menu vyberte název rizika pro
                        zobrazení jeho detailu nebo klikněte na adresář pro jejich přehled.</p>
                    </div>
                  </div>
                }
                {
                  risks.currentItem && risks.currentItem.type === 'folder' &&
                  <div className="containerColumns">
                    <div className="section_title">
                      <h3>Obsah</h3>
                    </div>
                    <div className="u_overflowAuto">
                      <FolderTable {...folderTableProps} />
                    </div>
                  </div>
                }
                {
                  risks.currentItem && risks.currentItem.type !== 'folder' &&
                  <RisksDetail {...risksDetailProps} />
                }
              </Layout>
            </Spin>
          </div>
        </div>
        { modal.type === 'modalFolder' && <ModalFolderCreate {...modalFolderProps} /> }
      </div>
    );
  }
}

RisksContainer.propTypes = {
  location: PropTypes.object,
  dispatch: PropTypes.func,
  app: PropTypes.object,
  risks: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    app: state.app,
    risks: state.risks,
    loadingModelRisks: state.loading.models.risks,
    relationships: state.relationships,
    loadingModelRelationships: state.loading.models.relationships,
    comments: state.comments,
    loadingModelComments: state.loading.models.comments,
    files: state.files,
    loadingModelFiles: state.loading.models.files,
    spectators: state.spectators,
    loadingModelSpectators: state.loading.models.spectators,
    users: state.users,
    loadingModelUsers: state.loading.models.users,
  };
}

export default connect(mapStateToProps)(RisksContainer);
