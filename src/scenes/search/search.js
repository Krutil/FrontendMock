import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import {
  Checkbox,
  Col,
  Row,
} from 'antd';

import SearchGroup from '../../components/ui/search';

class Search extends React.Component {

  handleSearch = (data) => {
    console.log('handleSearch data', data);
  };

  handleSelect = (type) => {
    console.log('handleSelect type', type);
  };

  toggleFilter = type => () => {
    console.log(type);
  };

  render() {
    const {
      assets,
      dispatch,
      location,
      requirements,
      risks,
    } = this.props;
    const {
      field,
      keyword,
    } = location.query;
    const deletedItems = [];

    requirements.list.forEach((item) => {
      if (item.status === 'deleted') {
        item.item_type = 'requirements';
        item.item_type_title = 'Požadavky';
        deletedItems.push(item);
      }
    });

    assets.list.forEach((item) => {
      if (item.status === 'deleted') {
        item.item_type = 'assets';
        item.item_type_title = 'Aktiva';
        deletedItems.push(item);
      }
    });

    risks.list.forEach((item) => {
      if (item.status === 'deleted') {
        item.item_type = 'risks';
        item.item_type_title = 'Rizika';
        deletedItems.push(item);
      }
    });

    const searchGroupProps = {
      keyword,
      size: 'large',
      select: true,
      selectOptions: [
        { value: 'requirements', name: 'Požadavky' },
        { value: 'assets', name: 'Aktiva' },
        { value: 'risks', name: 'Rizika' },
        { value: 'all', name: 'Vše' },
      ],
      selectProps: {
        defaultValue: field || 'requirements',
      },
      onSearch: (value) => {
        this.handleSearch(value);
      },
      onSelectChange: (value) => {
        this.handleSelect(value);
      },
    };

    return (
      <div className="container is-block">
        <div className="section u_maxWidthL">
          <div className="section_content">
            <div className="section_title">
              <h2>Vyhledávání</h2>
            </div>
            <div className="contentDark">
              <SearchGroup {...searchGroupProps} />
            </div>
            <div className="content">
              <Row gutter={24}>
                <Col md={8} sm={12}>
                  <Checkbox
                    size="default"
                    checked
                    className="spaceS"
                    onChange={this.toggleFilter('title')}
                  >
                    Název
                  </Checkbox>
                </Col>
                <Col md={8} sm={12}>
                  <Checkbox
                    size="default"
                    className="spaceS"
                    onChange={this.toggleFilter('title')}
                  >
                    Obsah
                  </Checkbox>
                </Col>
                <Col md={8} sm={12}>
                  <Checkbox
                    size="default"
                    className="spaceS"
                    onChange={this.toggleFilter('title')}
                  >
                    Uživatelé
                  </Checkbox>
                </Col>
                <Col md={8} sm={12}>
                  <Checkbox
                    size="default"
                    className="spaceS"
                    onChange={this.toggleFilter('title')}
                  >
                    Komentáře
                  </Checkbox>
                </Col>
                <Col md={8} sm={12}>
                  <Checkbox
                    size="default"
                    className="spaceS"
                    onChange={this.toggleFilter('title')}
                  >
                    Propojení
                  </Checkbox>
                </Col>
                <Col md={8} sm={12}>
                  <Checkbox
                    size="default"
                    className="spaceS"
                    onChange={this.toggleFilter('title')}
                  >
                    Soubory
                  </Checkbox>
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Search.propTypes = {
  assets: PropTypes.object,
  dispatch: PropTypes.func,
  requirements: PropTypes.object,
  risks: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    app: state.app,
    assets: state.assets,
    loadingModelUsers: state.loading.models.users,
    requirements: state.requirements,
    risks: state.risks,
    users: state.users,
  };
}

export default connect(mapStateToProps)(Search);
