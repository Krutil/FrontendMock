import React from 'react';
import PropTypes from 'prop-types';
import { Form, Checkbox, Switch, Button, Table } from 'antd';

class RiskForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      submitActive: false,
      dataSource: this.formatData(this.props.data.list),
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.data !== nextProps.data) {
      this.setState({
        submitActive: false,
        dataSource: this.formatData(nextProps.data.list),
      });
    }
  }

  formatData(data) {
    const dataSource = [];

    data && data.forEach(function (item, index) {
      dataSource.push({ index, label: item.label, key: item.key, active: item.active });
    });

    return dataSource;
  }

  handleCheckboxChange = record => () => {
    const { dataSource } = this.state;
    dataSource[record.index].active = !dataSource[record.index].active;

    this.setState({
      submitActive: true,
      dataSource,
    });
  };

  handleFormSubmit = (e) => {
    e.preventDefault();

    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }

      const attr = {};
      this.state.dataSource.forEach((item) => {
        attr[item.key] = item.active;
      });

      const payload = {
        type: this.props.data.type,
        ...attr,
      };

      this.props.cbUpdate(payload);
    });
  };

  render() {
    const { dataSource, submitActive } = this.state;
    const { getFieldDecorator } = this.props.form;
    const columns = [
      {
        title: 'Akce',
        dataIndex: 'label',
        key: 'label',
        render: (text, record) => (
          <div className="is-pointer" onClick={this.handleCheckboxChange(record)}>
            {text}
          </div>
        ),
      }, {
        title: 'Pamatovat',
        dataIndex: 'active',
        key: 'active',
        render: (text, record) => (
          <Switch
            checked={record.active}
            onChange={this.handleCheckboxChange(record)}
          />
        ),
      },
    ];

    return (
      <div>
        <Form
          layout="vertical"
          style={{ maxWidth: '600px', margin: '2em auto' }}
          onSubmit={this.handleFormSubmit}
        >
          <Form.Item>
            <Table
              columns={columns}
              dataSource={dataSource}
              pagination={false}
              simple
              showHeader={false}
              rowKey={record => record.key}
            />
          </Form.Item>
          {
            dataSource && dataSource.length > 0 && submitActive &&
            <Form.Item>
              <Button type="primary" htmlType="submit">Uložit</Button>
            </Form.Item>
          }
        </Form>
      </div>
    );
  }
}

RiskForm.propTypes = {
  form: PropTypes.object,
  type: PropTypes.string,
  data: PropTypes.object,
};

export default Form.create()(RiskForm);
