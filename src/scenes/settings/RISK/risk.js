import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Row, Col, Tabs } from 'antd';
import moment from 'moment';
import 'moment/locale/cs';

import RISKForm from './components/RISKForm/riskForm';

moment.locale('cs');

class SettingsRISK extends React.Component {

  render() {
    const { dispatch, settings } = this.props;
    const oDataDefault = {
      list: [
        {
          label: 'Pamatovat historii změn',
          key: 'history',
          active: false,
        },
        {
          label: 'Nutná poznámka při úpravě',
          key: 'noteUpdate',
          active: false,
        },
      ],
    };

    let assetsData = {
      list: [],
    };
    let risksData = {
      list: [],
    };

    if (settings && settings.risk && settings.risk.assets) {
      assetsData.list.push(
        {
          label: 'Pamatovat historii změn',
          key: 'history',
          active: settings.risk.assets.history,
        },
        {
          label: 'Nutná poznámka při úpravě',
          key: 'noteUpdate',
          active: settings.risk.assets.noteUpdate,
        },
      );
      assetsData.type = 'assets';
    } else {
      assetsData = {
        ...oDataDefault,
        type: 'assets',
      };
    }

    if (settings && settings.risk && settings.risk.risks) {
      risksData.list.push(
        {
          label: 'Pamatovat historii změn',
          key: 'history',
          active: settings.risk.risks.history,
        },
        {
          label: 'Nutná poznámka při úpravě',
          key: 'noteUpdate',
          active: settings.risk.risks.noteUpdate,
        },
      );
      risksData.type = 'risks';
    } else {
      risksData = {
        ...oDataDefault,
        type: 'risks',
      };
    }

    const assetsProps = {
      data: assetsData,
      moment,
      location,
      cbUpdate(dataObject) {
        dispatch({
          type: 'settings/updateRISK',
          payload: dataObject,
        });
      },
    };

    const risksProps = {
      data: risksData,
      moment,
      location,
      cbUpdate(dataObject) {
        dispatch({
          type: 'settings/updateRISK',
          payload: dataObject,
        });
      },
    };

    return (
      <div className="container is-block">
        <div className="section u_maxWidthM">
          <div className="section_content">
            <div className="section_title">
              <h2>RISK</h2>
            </div>
            <Tabs defaultActiveKey="assets" className="ant-tabs-gray">
              <Tabs.TabPane tab="Aktiva" key="assets">
                <RISKForm {...assetsProps} />
              </Tabs.TabPane>
              <Tabs.TabPane tab="Rizika" key="risks">
                <RISKForm {...risksProps} />
              </Tabs.TabPane>
            </Tabs>
          </div>
        </div>
      </div>
    );
  }

}

SettingsRISK.propTypes = {
  settings: PropTypes.object,
  dispatch: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    app: state.app,
    settings: state.settings,
  };
}

export default connect(mapStateToProps)(SettingsRISK);
