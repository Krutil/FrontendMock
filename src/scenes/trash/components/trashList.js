import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'dva/router';
import { Table, Menu, Dropdown, Button, Icon, Modal } from 'antd';

function trashList({ dataSource, moment, onRestore, onDelete }) {
  const handleMenuClick = (record, e) => {
    if (e.key === 'restore') {
      Modal.confirm({
        width: 500,
        title: 'Obnovit',
        content: 'Skutečně záznam obnovit?',
        okText: 'Obnovit',
        cancelText: 'Zpět',
        onOk() {
          onRestore(record);
        },
        onCancel() {
        },
      });
    } else if (e.key === 'destroy') {
      Modal.confirm({
        width: 500,
        title: 'Odstranit',
        content: 'Skutečně záznam odstranit?',
        okText: 'Odstranit',
        cancelText: 'Zpět',
        onOk() {
          onDelete(record);
        },
        onCancel() {
        },
      });
    }
  };

  const locale = {
    emptyText: 'Koš je prázdný',
  };

  const columns = [
    {
      title: 'Název',
      dataIndex: 'title',
      key: 'title',
      render: (text, record) => (<Link
        to={`/risk/${record.item_type}/item/${record.key}`}
      ><strong>{text}</strong></Link>),
    }, {
      title: 'Typ záznamu',
      dataIndex: 'item_type_title',
      key: 'item_type_title',
    }, {
      title: 'Odstraněno dne',
      dataIndex: 'updated_at',
      key: 'updated_at',
      render: text => <span>{text ? moment(text).format('LLL') : '—'}</span>,
    }, {
      fixed: 'right',
      title: '',
      key: 'operation',
      width: 65,
      render: (text, record) => (
        <Dropdown
          placement="bottomRight"
          trigger={['click']}
          overlay={
            <Menu onClick={e => handleMenuClick(record, e)}>
              <Menu.Item key="restore">Obnovit</Menu.Item>
              <Menu.Item key="destroy">Odstranit</Menu.Item>
            </Menu>
          }
        >
          <Button
            type="dark"
            size="small"
            shape="circle"
            icon="ellipsis"
            style={{
              border: 'none',
              width: 'initial',
              padding: '0',
              lineHeight: '1',
              borderRadius: '50%',
            }}
          />
        </Dropdown>),
    },
  ];

  const getBodyWrapper = body => body;

  return (
    <div>
      <Table
        locale={locale}
        scroll={{ x: 1000 }}
        columns={columns}
        dataSource={dataSource}
        pagination={false}
        simple
        rowKey={record => record.key}
        getBodyWrapper={getBodyWrapper}
      />
    </div>
  );
}

trashList.propTypes = {
  dataSource: PropTypes.array,
  moment: PropTypes.object,
  onDelete: PropTypes.func,
  onRestore: PropTypes.func,
};

export default trashList;
