import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import moment from 'moment';
import 'moment/locale/cs';
import TrashList from './components/trashList';

moment.locale('cs');

class TrashContainer extends React.Component {
  componentWillMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'app/trashChecked',
    });
  }

  render() {
    const { requirements, assets, risks, dispatch } = this.props;
    const deletedItems = [];

    requirements.list.forEach((item) => {
      if (item.status === 'deleted') {
        item.item_type = 'requirements';
        item.item_type_title = 'Požadavky';
        deletedItems.push(item);
      }
    });

    assets.list.forEach((item) => {
      if (item.status === 'deleted') {
        item.item_type = 'assets';
        item.item_type_title = 'Aktiva';
        deletedItems.push(item);
      }
    });

    risks.list.forEach((item) => {
      if (item.status === 'deleted') {
        item.item_type = 'risks';
        item.item_type_title = 'Rizika';
        deletedItems.push(item);
      }
    });

    deletedItems.sort((a, b) => {
      const keyA = new Date(a.updated_at);
      const keyB = new Date(b.updated_at);
      if (keyA < keyB) return 1;
      if (keyA > keyB) return -1;
      return 0;
    });

    const trashListProps = {
      dataSource: deletedItems,
      moment,
      onDelete(item) {
        dispatch({
          type: `${item.item_type}/destroy`,
          payload: item,
        });
      },
      onRestore(item) {
        dispatch({
          type: `${item.item_type}/restore`,
          payload: item.id,
        });
      },
    };

    return (
      <div className="container is-block">
        <div className="section">
          <div className="section_content">
            <div className="section_title">
              <h2>Koš</h2>
            </div>
            <div className="space">
              <TrashList {...trashListProps} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

TrashContainer.propTypes = {
  assets: PropTypes.object,
  dispatch: PropTypes.func,
  requirements: PropTypes.object,
  risks: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    app: state.app,
    assets: state.assets,
    loadingModelUsers: state.loading.models.users,
    requirements: state.requirements,
    risks: state.risks,
    users: state.users,
  };
}

export default connect(mapStateToProps)(TrashContainer);
