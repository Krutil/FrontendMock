import React from 'react';
import PropTypes from 'prop-types';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';
import moment from 'moment';
import 'moment/locale/cs';

import UserList from '../../components/users/userList';
import UserSearch from '../../components/users/search';
import UserModal from '../../components/users/modal';

moment.locale('cs');

function users({ location, dispatch, users, loadingModelUsers }) {
  const {
    list,
    pagination,
    currentItem,
    modalVisible,
    modalType,
    isMotion,
  } = users;
  const {
    field,
    keyword,
  } = location.query;

  const userModalProps = {
    item: modalType === 'create' ? {} : currentItem,
    type: modalType,
    visible: modalVisible,
    onOk(data) {
      dispatch({
        type: `users/${modalType}`,
        payload: data,
      });
    },
    onCancel() {
      dispatch({
        type: 'users/hideModal',
      });
    },
  };

  const userListProps = {
    dataSource: list,
    loading: loadingModelUsers,
    pagination,
    moment,
    location,
    isMotion,
    onPageChange(page) {
      const { query, pathname } = location;
      dispatch(routerRedux.push({
        pathname,
        query: {
          ...query,
          page: page.current,
          pageSize: page.pageSize,
        },
      }));
    },
    onDeleteItem(id) {
      dispatch({
        type: 'users/delete',
        payload: id,
      });
    },
    onEditItem(item) {
      dispatch({
        type: 'users/showModal',
        payload: {
          modalType: 'update',
          currentItem: item,
        },
      });
    },
  };

  const userSearchProps = {
    field,
    keyword,
    isMotion,
    onSearch(fieldsValue) {
      console.log('fieldsValue',fieldsValue)
      fieldsValue.keyword.length ?
        dispatch(routerRedux.push({
          pathname: '/users',
          query: {
            field: fieldsValue.field ? fieldsValue.field : 'name',
            keyword: fieldsValue.keyword,
          },
        }))
        :
        dispatch(routerRedux.push({
        pathname: '/users',
      }));
    },
    onAdd() {
      dispatch({
        type: 'users/showModal',
        payload: {
          modalType: 'create',
        },
      });
    },
    switchIsMotion() {
      dispatch({ type: 'users/switchIsMotion' });
    },
  };

  const UserModalGen = () => <UserModal {...userModalProps} />;

  return (
    <div className="container is-block">
      <div className="section spaceL">
        <div className="section_content">
          <div className="content">
            <UserSearch {...userSearchProps} />
          </div>
          <UserList {...userListProps} />
          <UserModalGen />
        </div>
      </div>
    </div>
  );
}

users.propTypes = {
  dispatch: PropTypes.func,
  loadingModelUsers: PropTypes.bool,
  location: PropTypes.object,
  users: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    loadingModelUsers: state.loading.models.users,
    users: state.users,
  };
}

export default connect(mapStateToProps)(users);
