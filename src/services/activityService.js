import { request } from '../utils';

export async function query(params) {
  return request('/api/activity', {
    method: 'get',
    data: params,
  });
}

export async function create(params) {
  return request('/api/activity', {
    method: 'post',
    data: params,
  });
}
