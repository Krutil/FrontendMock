import { request } from '../utils';

export async function login(params) {
  return request('/api/login', {
    method: 'post',
    data: params,
  });
}

export async function logout(params) {
  return request('/api/logout', {
    method: 'post',
    data: params,
  });
}

export async function userInfo(params) {
  return request('/api/userInfo', {
    method: 'get',
    data: params,
  });
}

export async function notificationChecked(params) {
  return request('/api/notificationChecked', {
    method: 'put',
    data: params,
  });
}

export async function messagesChecked(params) {
  return request('/api/messagesChecked', {
    method: 'put',
    data: params,
  });
}

export async function approvalChecked(params) {
  return request('/api/approvalChecked', {
    method: 'put',
    data: params,
  });
}

export async function trashChecked(params) {
  return request('/api/trashChecked', {
    method: 'put',
    data: params,
  });
}
