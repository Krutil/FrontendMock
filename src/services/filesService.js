import { request } from '../utils';

export async function query(params) {
  return request('/api/files', {
    method: 'get',
    data: params,
  });
}

export async function create(params) {
  return request('/api/files', {
    method: 'post',
    data: params,
  });
}

export async function update(params) {
  return request('/api/files', {
    method: 'put',
    data: params,
  });
}

export async function destroy(params) {
  // console.log('--params', params);
  return request('/api/files', {
    method: 'delete',
    data: params,
  });
}
