import { request } from '../utils';

export async function query(params) {
  return request('/api/history', {
    method: 'get',
    data: params,
  });
}

export async function store(params) {
  return request('/api/history/', {
    method: 'post',
    data: params,
  });
}
