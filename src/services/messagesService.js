import axios from 'axios';
import { request } from '../utils';

export const query = async () => {
  const result = await axios.post('/graphql', {
    query: `query {
              messages {
                conversationId
                participants
                messages {
                  messageId,
                  messageBody,
                  senderId,
                  createdAt,
                  viewedAt
                }
              }
            }`,
  });

  const { data: { data: { messages } } } = result;

  return { success: true, data: messages };
};

export const create = async (params) => {
  const result = await axios.post('/graphql', {
    query: `mutation (
      $messageBody: String!,
      $senderId: Int!,
      $conversationId: String,
      $participants: [Int]!)
               {
                 postMessage(newMessage: {
                    messageBody: $messageBody,
                    senderId: $senderId,
                  },
                   conversationId: $conversationId,
                   participants: $participants,
                 )
                 {
                  conversationId,
                  participants,
                  messages {
                    messageId,
                    messageBody,
                    senderId,
                    createdAt,
                    viewedAt,
                	}
                }
              }`,
    variables: JSON.parse(JSON.stringify(params)),
  });

  const { data: { data: { postMessage } } } = result;
  return { success: true, data: { data: postMessage } };
};

export const destroy = async (params) => {
  const result = await axios.post('/graphql', {
    query: `mutation ($messageId: String!, $conversationId:String!) {
              deleteMessage(messageId: $messageId, conversationId: $conversationId) {
                conversationId,
                participants,
                messages {
                  messageId,
                  messageBody,
                  senderId,
                  createdAt,
                  viewedAt
                }
              }
            }`,
    variables: params,
  });
  const { data: { data: { deleteMessage } } } = result;

  return { success: true, data: { data: deleteMessage } };
};

export const messageViewed = async params => (
request('/api/messageViewed', {
  method: 'put',
  data: params,
}));
