import { request } from '../utils';


export const query = async params => (
  request('/api/notifications', {
    method: 'get',
    data: params,
  }));

export const create = async params => (
  request('/api/notifications', {
    method: 'post',
    data: params,
  }));
