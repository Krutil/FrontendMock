import { request } from '../utils';

export async function query(params) {
  return request('/api/relationships', {
    method: 'get',
    data: params,
  });
}

export async function create(params) {
  return request('/api/relationships', {
    method: 'post',
    data: params,
  });
}

export async function destroy(params) {
  return request('/api/relationships', {
    method: 'delete',
    data: params,
  });
}
