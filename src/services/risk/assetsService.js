import { request } from '../../utils';

export async function query(params) {
  return request('/api/risk/assets', {
    method: 'get',
    data: params,
  });
}

export async function select(params) {
  params.request = true;
  return request('/api/risk/assets/item/id', {
    method: 'put',
    data: params,
  });
}

export async function create(params) {
  return request('/api/risk/assets', {
    method: 'post',
    data: params,
  });
}

export async function destroy(params) {
  return request('/api/risk/assets', {
    method: 'delete',
    data: params,
  });
}

export async function restore(params) {
  return request('/api/risk/assets/restore', {
    method: 'put',
    data: params,
  });
}

export async function update(params) {
  return request('/api/risk/assets', {
    method: 'put',
    data: params,
  });
}

export async function schemaAdd(params) {
  params.request = true;
  return request('/api/risk/assets/schema', {
    method: 'put',
    data: params,
  });
}

export async function schemaDestroy(params) {
  params.request = true;
  return request('/api/risk/assets/schema', {
    method: 'delete',
    data: params,
  });
}

export async function updateTree(params) {
  return request('/api/risk/assets/tree', {
    method: 'put',
    data: params,
  });
}

export async function folderCreate(params) {
  params.request = true;
  return request('/api/risk/assets/folder/create', {
    method: 'post',
    data: params,
  });
}
