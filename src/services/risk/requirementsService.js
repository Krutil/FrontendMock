import { request } from '../../utils';

export async function query(params) {
  return request('/api/risk/requirements', {
    method: 'get',
    data: params,
  });
}

export async function select(params) {
  params.request = true;
  return request('/api/risk/requirements/item/id', {
    method: 'put',
    data: params,
  });
}

export async function create(params) {
  return request('/api/risk/requirements', {
    method: 'post',
    data: params,
  });
}

export async function destroy(params) {
  return request('/api/risk/requirements', {
    method: 'delete',
    data: params,
  });
}

export async function restore(params) {
  return request('/api/risk/requirements/restore', {
    method: 'put',
    data: params,
  });
}

export async function update(params) {
  return request('/api/risk/requirements', {
    method: 'put',
    data: params,
  });
}

export async function updateTree(params) {
  return request('/api/risk/requirements/tree', {
    method: 'put',
    data: params,
  });
}

export async function commentsCreate(params) {
  params.request = true;
  return request('/api/risk/requirements/comments/create', {
    method: 'post',
    data: params,
  });
}

export async function commentsDestroy(params) {
  params.request = true;
  return request('/api/risk/requirements/comments/destroy', {
    method: 'put',
    data: params,
  });
}

export async function folderCreate(params) {
  params.request = true;
  return request('/api/risk/requirements/folder/create', {
    method: 'post',
    data: params,
  });
}
