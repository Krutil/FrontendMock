import { request } from '../../utils';

export async function query(params) {
  return request('/api/risk/risks', {
    method: 'get',
    data: params,
  });
}

export async function select(params) {
  params.request = true;
  return request('/api/risk/risks/item/id', {
    method: 'put',
    data: params,
  });
}

export async function create(params) {
  return request('/api/risk/risks', {
    method: 'post',
    data: params,
  });
}

export async function destroy(params) {
  return request('/api/risk/risks', {
    method: 'delete',
    data: params,
  });
}

export async function restore(params) {
  return request('/api/risk/risks/restore', {
    method: 'put',
    data: params,
  });
}


export async function update(params) {
  return request('/api/risk/risks', {
    method: 'put',
    data: params,
  });
}

export async function schemaAdd(params) {
  params.request = true;
  return request('/api/risk/risks/schema', {
    method: 'put',
    data: params,
  });
}

export async function schemaDestroy(params) {
  params.request = true;
  return request('/api/risk/risks/schema', {
    method: 'delete',
    data: params,
  });
}

export async function updateTree(params) {
  return request('/api/risk/risks/tree', {
    method: 'put',
    data: params,
  });
}

export async function commentsCreate(params) {
  params.request = true;
  return request('/api/risk/risks/comments/create', {
    method: 'post',
    data: params,
  });
}

export async function commentsDestroy(params) {
  params.request = true;
  return request('/api/risk/risks/comments/destroy', {
    method: 'put',
    data: params,
  });
}

export async function folderCreate(params) {
  params.request = true;
  return request('/api/risk/risks/folder/create', {
    method: 'post',
    data: params,
  });
}
