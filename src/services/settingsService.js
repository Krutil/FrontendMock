import { request } from '../utils';

export async function query(params) {
  return request('/api/settings', {
    method: 'get',
    data: params,
  });
}

export async function updateRISK(params) {
  return request('/api/settings/risk', {
    method: 'put',
    data: params,
  });
}
