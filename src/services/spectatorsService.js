import { request } from '../utils';

export async function query(params) {
  return request('/api/spectators', {
    method: 'get',
    data: params,
  });
}

export async function create(params) {
  return request('/api/spectators', {
    method: 'post',
    data: params,
  });
}

export async function destroy(params) {
  return request('/api/spectators', {
    method: 'delete',
    data: params,
  });
}

export async function toggle(params) {
  return request('/api/spectators', {
    method: 'put',
    data: params,
  });
}

export async function updateDate(params) {
  console.log('updateDate');
  return request('/api/spectators/updateDate', {
    method: 'put',
    data: params,
  });
}
