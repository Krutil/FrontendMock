module.exports = () => ({
  '@border-radius-base': '.4em',
  '@border-radius-sm': '.3em',
  '@shadow-color': 'rgba(0,0,0,0.05)',
  '@shadow-1-down': '4px 4px 40px @shadow-color',
  '@border-color-split': '#f4f4f4',
  '@border-color-base': '#e5e5e5',
  '@menu-dark-bg': '#3e3e3e',
  '@primary-color': '#657ff7',
  '@text-color': '#444',
  '@font-family': '"Roboto", Arial, Helvetica, sans-serif',
  '@font-size-base': '16px',
  '@line-height-base': '1.5',
});
