/* Sidebar menu is generated from this file */

module.exports = [
  {
    key: 'dashboard',
    path: 'dashboard',
    name: 'Přehled',
    icon: 'home',
    clickable: false,
  },
  {
    key: 'audit',
    path: 'audit',
    name: 'Audit',
    icon: 'solution',
    clickable: true,
  },
  {
    key: 'risk',
    path: 'risk',
    name: 'RISK',
    icon: 'exception',
    clickable: false,
    child: [
      {
        key: 'risk.requirements',
        path: 'requirements',
        name: 'Požadavky',
      },
      {
        key: 'risk.assets',
        path: 'assets',
        name: 'Aktiva',
      },
      {
        key: 'risk.risks',
        path: 'risks',
        name: 'Rizika',
      },
      {
        key: 'risk.precaution',
        path: 'precaution',
        name: 'Opatření',
      },
    ],
  },
  {
    key: 'search',
    path: 'search',
    name: 'Vyhledávání',
    icon: 'search',
    clickable: false,
  },
  {
    key: 'users',
    path: 'users',
    name: 'Uživatelé',
    icon: 'user',
    clickable: false,
  },
  {
    key: 'settings',
    path: 'settings',
    name: 'Nastavení',
    icon: 'setting',
    clickable: false,
    child: [
      {
        key: 'settings.risk',
        path: 'risk',
        name: 'RISK',
      },
    ],
  },
];
