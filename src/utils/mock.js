const Mock = require('mockjs');
const mockData = [
  require('../../mock/usersMock'),
  require('../../mock/appMock'),
  require('../../mock/settingsMock'),
  require('../../mock/dashboardMock'),
  require('../../mock/filesMock'),
  require('../../mock/notificationsMock'),
  require('../../mock/relationshipsMock'),
  require('../../mock/commentsMock'),
  require('../../mock/spectatorsMock'),
  require('../../mock/activityMock'),
  require('../../mock/messagesMock'),  
  require('../../mock/historyMock'),
  require('../../mock/requirementsMock'),
  require('../../mock/assetsMock'),
  require('../../mock/risksMock'),
];

function serialize(str) {
  const paramArray = str.split('&');
  const query = {};

  for (const i in paramArray) {
    query[paramArray[i].split('=')[0]] = paramArray[i].split('=')[1];
  }
  return query;
}

for (const i in mockData) {
  for (const key in mockData[i]) {
    Mock.mock(eval(`/${key.split(' ')[1].replace(/\//g, '\\\/')}/`), key.split(' ')[0].toLowerCase(), (options) => {
      /* Better Mock url match */
      let keyEdited = key;

      if (options.body && options.body.request) {
        keyEdited = `${options.type} ${options.url}`;
      }

      // console.log('options', options);
      // console.log('keyEdited', keyEdited);
      // console.log('key', key);

      if (keyEdited.split(' ')[0].toLowerCase() === 'get') {
        options.query = options.url.split('?')[1]
          ? serialize(options.url.split('?')[1])
          : (options.body
            ? serialize(options.body)
            : {});
      }
      const res = {};
      let result = {};
      res.json = function (data) {
        result = data;
      };
      mockData[i][keyEdited](options, res);
      return result;
    });
  }
}

Mock.setup({ timeout: '10-600' });
