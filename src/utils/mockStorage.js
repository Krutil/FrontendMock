const Watch = require('watchjs');
import config from './config';

  export default function mockStorage(name, defaultValue) {
  const key = config.prefix + name;
  global[key] = localStorage.getItem(key) ? JSON.parse(localStorage.getItem(key)) : defaultValue;
  !localStorage.getItem(key) && localStorage.setItem(key, JSON.stringify(global[key]));

  Watch.watch(global[key], () => {
    console.log('+++ Watch.watch(global[key] -> localStorage.setItem');
    localStorage.setItem(key, JSON.stringify(global[key]));
  });
  return key;
}
