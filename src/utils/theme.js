module.exports = {
  color: {
    green: '#9adc53',
    blue: '#76bbf5',
    purple: '#d897eb',
    red: '#f16060',
    redLight: '#f69899',
    yellow: '#f8c82e',
    peach: '#f797d6',
    borderBase: '#e5e5e5',
    borderSplit: '#f4f4f4',
    grass: '#d6fbb5',
    sky: '#c1e0fc',
  },
};
