const getWebpackConfig = require('./webpack/getWebpackConfig');

module.exports = getWebpackConfig('production');
