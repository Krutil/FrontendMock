const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const path = require('path');

const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const notifier = require('node-notifier');
const chalk = require('chalk');
const WriteFilePlugin = require('write-file-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const queryString = require('querystring');
const os = require('os');
const getTheme = require('../src/theme');

const fs = require('fs');

const join = path.join;
const tmpdir = os.tmpdir;
const ProgressPlugin = webpack.ProgressPlugin;
const stringify = queryString.stringify;

const getBabelCommonConfig = () => ({
  cacheDirectory: tmpdir(),
  presets: [
    require.resolve('babel-preset-es2015-ie'),
    require.resolve('babel-preset-react'),
    require.resolve('babel-preset-stage-0'),
  ],
  plugins: [
    require.resolve('babel-plugin-add-module-exports'),
    require.resolve('babel-plugin-transform-decorators-legacy'),

  ],
});

const getTSCommonConfig = () => ({
  target: 'es6',
  jsx: 'preserve',
  moduleResolution: 'node',
  declaration: false,
  sourceMap: true,
});

const getWebpackCommonConfig = (env) => {
  const babelQuery = getBabelCommonConfig();
  const tsQuery = getTSCommonConfig();
  tsQuery.declaration = false;
  const theme = getTheme();

  return {
    babel: babelQuery,
    cache: true,
    ts: {
      transpileOnly: true,
      compilerOptions: { target: 'es6',
        jsx: 'preserve',
        moduleResolution: 'node',
        declaration: false,
        sourceMap: true },
    },
    stats: {
      children: false,
    },

    output: {
      path: path.join(__dirname, '/../dist'),
      filename: '[name].js',
      chunkFilename: '[name].js',
      publicPath: '/',
    },

    devtool: env === 'development' ? '#eval' : '#cheap-source-map',

    resolve: {
      modulesDirectories: ['node_modules', join(__dirname, '../node_modules')],
      extensions: ['', '.web.tsx', '.web.ts', '.web.jsx', '.web.js', '.ts', '.tsx', '.js', '.jsx', '.json'],
    },

    resolveLoader: {
      modulesDirectories: ['node_modules', join(__dirname, '../node_modules')],
    },

    entry: {
      'cs-CZ': './src/locales/cs-CZ.js',
      index: [
        'babel-polyfill',
        './src/index.js',
        `webpack-hot-middleware/client?${stringify({
          path: '/__webpack_hmr',
          reload: true,
        })}`,
      ],

    },

    node: {
      child_process: 'empty',
      cluster: 'empty',
      dgram: 'empty',
      dns: 'empty',
      fs: 'empty',
      module: 'empty',
      net: 'empty',
      readline: 'empty',
      repl: 'empty',
      tls: 'empty',
    },

    module: {
      noParse: [/moment.js/],
      loaders: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: require.resolve('babel-loader'),
          query: babelQuery,
        },
        {
          test: /\.jsx$/,
          loader: require.resolve('babel-loader'),
          query: babelQuery,
        },
        {
          test: /\.tsx?$/,
          loaders: [require.resolve('babel-loader'), require.resolve('ts-loader')],
        },
        {
          test(filePath) {
            return /\.css$/.test(filePath) && !/\.module\.css$/.test(filePath);
          },
          loader: ExtractTextPlugin.extract(
            `${require.resolve('css-loader')}` +
            `?sourceMap&-restructuring&-autoprefixer!${require.resolve('postcss-loader')}`,
          )
        },
        {
          test: /\.module\.css$/,
          loader: ExtractTextPlugin.extract(
            `${require.resolve('css-loader')}` +
            '?sourceMap&-restructuring&modules&localIdentName=[local]___[hash:base64:5]&-autoprefixer!' +
            `${require.resolve('postcss-loader')}?{"sourceMap":true}`,
          ),
        },
        {
          test(filePath) {
            return /\.less$/.test(filePath) && !/\.module\.less$/.test(filePath);
          },
          loader: ExtractTextPlugin.extract(
            `${require.resolve('css-loader')}?sourceMap&-autoprefixer!` +
            `${require.resolve('postcss-loader')}?{"sourceMap":true}!` +
            `${require.resolve('less-loader')}?{"sourceMap":true,"modifyVars":${JSON.stringify(theme)}}`,
          ),
        },
        {
          test: /\.module\.less$/,
          loader: ExtractTextPlugin.extract(
            `${require.resolve('css-loader')}?` +
            'sourceMap&modules&localIdentName=[local]___[hash:base64:5]&-autoprefixer!' +
            `${require.resolve('postcss-loader')}?{"sourceMap":true}!` +
            `${require.resolve('less-loader')}?` +
            `{"sourceMap":true,"modifyVars":${JSON.stringify(theme)}}`,
          ),
        },
        {
          test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
          loader: `${require.resolve('url-loader')}?` +
          'limit=10000&minetype=application/font-woff',
        },
        {
          test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
          loader: `${require.resolve('url-loader')}?` +
          'limit=10000&minetype=application/font-woff',
        },
        {
          test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
          loader: `${require.resolve('url-loader')}?` +
          'limit=10000&minetype=application/octet-stream',
        },
        { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
          loader: `${require.resolve('url-loader')}?` +
        'limit=10000&minetype=application/vnd.ms-fontobject',
        },
        {
          test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
          loader: `${require.resolve('url-loader')}?` +
          'limit=10000&minetype=image/svg+xml',
        },
        {
          test: /\.(png|jpg|jpeg|gif)(\?v=\d+\.\d+\.\d+)?$/i,
          loader: `${require.resolve('url-loader')}?limit=10000`,
        },
        {
          test: /\.json$/,
          loader: `${require.resolve('json-loader')}`,
        },
        {
          test: /\.html?$/,
          loader: `${require.resolve('file-loader')}?name=[name].[ext]`,
        },
      ],
    },
    plugins: [
      new ExtractTextPlugin('[name].css', {
        disable: false,
        allChunks: true,
      }),
      new CaseSensitivePathsPlugin(),
      new FriendlyErrorsWebpackPlugin({
        onErrors: (severity, errors) => {
          if (severity !== 'error') {
            notifier.notify({
              title: 'ant tool',
              message: 'warn',
              contentImage: join(__dirname, '../assets/warn.png'),
              sound: 'Glass',
            });
            return;
          }
          const error = errors[0];
          notifier.notify({
            title: 'ant tool',
            message: `${severity} : ${error.name}`,
            subtitle: error.file || '',
            contentImage: join(__dirname, '../assets/fail.png'),
            sound: 'Glass',
          });
        },
      }),
      new ProgressPlugin((percentage, msg) => {
        const stream = process.stderr;
        if (stream.isTTY && percentage < 0.71) {
          stream.cursorTo(0);
          stream.write(`📦  ${chalk.magenta(msg)}`);
          stream.clearLine(1);
        } else if (percentage === 1) {
          console.log(chalk.green('\nwebpack: bundle build is now finished.'));
        }
      }),
      new WriteFilePlugin(),
      new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoErrorsPlugin(),
      new CopyWebpackPlugin(
        [
          {
            from: './assets',
            to: 'assets',
          },
        ],
      ),
    ],
  };
};

module.exports = function (env) {
  const webpackConfig = getWebpackCommonConfig(env);

  webpackConfig.babel.plugins.push('transform-runtime');
  webpackConfig.babel.plugins.push('transform-flow-strip-types');
  webpackConfig.babel.plugins.push(['import', {
    libraryName: 'antd',
    style: true,
  }]);

  // Support hmr
  if (env === 'development') {
    webpackConfig.devtool = '#eval';
    webpackConfig.babel.plugins.push(['dva-hmr', {
      "container": "#root",
      "quiet": false,
      entries: [
        './src/index.js',
      ],
    },
]);
  } else {
    webpackConfig.babel.plugins.push('dev-expression');
  }

  // Don't extract common.js and common.css
  webpackConfig.plugins = webpackConfig.plugins.filter(plugin => (
    !(plugin instanceof webpack.optimize.CommonsChunkPlugin)
  ));

  // Support CSS Modules
  // Parse all less files as css module.
  webpackConfig.module.loaders.forEach((loader) => {
    if (typeof loader.test === 'function' && loader.test.toString().indexOf('\\.less$') > -1) {
      loader.include = /node_modules/;
      loader.test = /\.less$/;
    }

    if (loader.test.toString() === '/\\.module\\.less$/') {
      loader.exclude = /node_modules/;
      loader.test = /\.less$/;
    }

    if (typeof loader.test === 'function' && loader.test.toString().indexOf('\\.css$') > -1) {
      loader.include = /node_modules/;
      loader.test = /\.css$/;
    }

    if (loader.test.toString() === '/\\.module\\.css$/') {
      loader.exclude = /node_modules/;
      loader.test = /\.css$/;
    }
  });

  if (env='production') {
  //   webpackConfig.plugins.push(new webpack.NoErrorsPlugin());
  //   webpackConfig.plugins.push(new webpack.optimize.DedupePlugin());
  //   webpackConfig.plugins.push(new webpack.optimize.UglifyJsPlugin({
  //   compress: {
  //       warnings: false
  //       }
  //   }))
  //   webpackConfig.plugins.push(new webpack.optimize.AggressiveMergingPlugin());
  //   webpackConfig.plugins.push(new webpack.DefinePlugin({
  //     'process.env': {
  //       'NODE_ENV': JSON.stringify('production')
  //     }
  //   })
  //  );
  }

  return webpackConfig;
};
