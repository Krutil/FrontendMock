import express from 'express';
import webpack from 'webpack';
import history from 'connect-history-api-fallback';
import WebpackDevMiddleware from 'webpack-dev-middleware';
import WebpackHotMiddleware from 'webpack-hot-middleware';
import httpProxy from 'http-proxy';

import getConfig from '../webpack/getWebpackConfig';

const apiProxy = httpProxy.createProxyServer();

const hotServerPort = 8000;

const hotServer = express();


const dev = process.env.NODE_ENV === 'development';
if (dev) {
  hotServer.use('/graphql', (req, res) => {
    apiProxy.web(req, res, { target: 'http://localhost:3000/graphql' });
  });

  hotServer.use(history({
    index: '/index.html',
  }));

  const config = getConfig(process.env.NODE_ENV);

  const compiler = webpack(config);
  const options = {
    publicPath: '/',
    noInfo: true,
    stats: {
      colors: true,
    },
  };

  hotServer.use(WebpackDevMiddleware(compiler, options));

  hotServer.use(WebpackHotMiddleware(compiler));

  hotServer.listen(hotServerPort, () => {
    console.info(`\n\nWebpack dev server listens on http://localhost:${hotServerPort} \n`);
  });
} else {
  const config = getConfig(process.env.NODE_ENV);
  const compiler = webpack(config);
  compiler.run((err, stats) => {

  });
}
